#include <CrashMail.h>

/*************************************************************************/
/*                                                                       */
/*                                Read Pkt                               */
/*                                                                       */
/*************************************************************************/

BOOL messageend;
BOOL shortread,longread;

stripspace(UBYTE *str)
{
   /* Strips all chars <33 from a string. For tagnames */

   ULONG c=0,d=0;

   for(c=0;str[c];c++)
      if(str[c]>32) str[d++]=str[c];

   str[d]=0;
}

ULONG ReadNull(UBYTE *buf, ULONG maxlen, struct jbFile *jb)
{
   /* Reads from jb until buffer full or NULL */

   WORD ch,c=0;

   if(shortread) return(0);

   ch=jbGetChar(jb);

   while(ch!=-1 && ch!=0 && c!=maxlen-1)
   {
      buf[c++]=ch;
      ch=jbGetChar(jb);
   }
   buf[c]=0;

   if(ch==-1)
      shortread=TRUE;

   if(ch!=0 && c==maxlen-1)
      longread=TRUE;

   return(c);
}

ULONG ReadCR(UBYTE *buf, ULONG maxlen, struct jbFile *jb)
{
   /* Reads from jb until buffer full or CR */

   WORD ch,c=0;

   ch=jbGetChar(jb);

   while(ch!=-1 && ch!=0 && ch!=10 && ch !=13 && c!=maxlen-1)
   {
      buf[c++]=ch;
      if(c!=maxlen-1) ch=jbGetChar(jb);
   }

   if(ch==13 || ch==10)
      buf[c++]=ch;

   if(ch==0)  messageend=TRUE;
   if(ch==-1) shortread=TRUE;

   return(c);
}


AddSeenby(UBYTE *str,struct jbList *list)
{
   /* Add a node string to SEEN-BY list */

   ULONG c=0,d;
   UBYTE buf[60];
   UWORD lastnet,num;

   lastnet=0;

   while(str[c]!=13 && str[c]!=0)
   {
      d=0;

      while(str[c]!=13 && str[c]!=0 && str[c]!=32 && d<59)
         buf[d++]=str[c++];

      buf[d]=0;
      while(str[c]==32) c++;

      if(buf[0])
      {
         num=0;
         d=0;

         while(buf[d]>='0' && buf[d]<='9')
         {
            num*=10;
            num+=buf[d]-'0';
            d++;
         }

         if(buf[d]=='/')
         {
            lastnet=num;
            num=atoi(&buf[d+1]);
            AddNodes2DList(list,lastnet,num);
         }
         else if(buf[d]==0)
         {
            AddNodes2DList(list,lastnet,num);
         }

         if(nomem) return;
      }
   }
}

AddNodes2DList(struct jbList *list,UWORD net,UWORD node)
{
   /* Add a node to SEEN-BY list */

   struct Nodes2D *tmplist;
   UWORD num;

   for(tmplist=list->First;tmplist;tmplist=tmplist->Next)
   {
      for(num=0;num<tmplist->Nodes;num++)
         if(tmplist->Net[num]==net && tmplist->Node[num]==node) return;
   }

   if(num==PKT_NUM2D)
   {
      if(!(tmplist=(struct Nodes2D *)AllocMem(sizeof(struct Nodes2D),MEMF_ANY)))
      {
         nomem=TRUE;
         return;
      }

      jbAddNode(list,(struct jbNode *)tmplist);
      tmplist->Nodes=0;
      tmplist->Next=NULL;
      num=0;
   }

   tmplist=(struct Nodes2D *)list->Last;
   tmplist->Net[num]=net;
   tmplist->Node[num]=node;
   tmplist->Nodes++;
}

RemNodes2DList(struct jbList *list,UWORD net,UWORD node)
{
   /* Rem a node from SEEN-BY list */

   struct Nodes2D *tmplist;
   UWORD num;

   for(tmplist=list->First;tmplist;tmplist=tmplist->Next)
      for(num=0;num<tmplist->Nodes;num++)
         if(tmplist->Net[num]==net && tmplist->Node[num]==num)
         {
            tmplist->Net[num]=0;
            tmplist->Node[num]=0;
         }
}

RemNodes2DListPat(struct jbList *list,struct Node2DPat *pat)
{
   struct Nodes2D *tmplist;
   UWORD num;

   for(tmplist=list->First;tmplist;tmplist=tmplist->Next)
      for(num=0;num<tmplist->Nodes;num++)
      {
         if(Compare2DPat(pat,tmplist->Net[num],tmplist->Node[num])==0)
         {
            tmplist->Net[num]=0;
            tmplist->Node[num]=0;
         }
      }
}

AddPath(UBYTE *str,struct jbList *list)
{
   /* Add a node string to PATH list */

   struct Path *path;
   ULONG d;

   if(str[0]==0)
      return;

   path=list->Last;

   if(path->Paths == PKT_NUMPATH)
   {
      if(!(path=(struct Path *)AllocMem(sizeof(struct Path),MEMF_ANY)))
      {
         nomem=TRUE;
         return;
      }

      jbAddNode(list,(struct jbNode *)path);
      path->Paths=0;
      path->Next=NULL;
   }

   d=0;

   while(d<99 && str[d]>=32)
   {
      path->Path[path->Paths][d]=str[d];
      d++;
   }

   path->Path[path->Paths][d]=0;
   strip(path->Path[path->Paths]);
   path->Paths++;
}

RemLastPath(struct jbList *list)
{
   struct Path *path;
   ULONG c;
   UBYTE *str=NULL;

   for(path=list->First;path;path=path->Next)
      for(c=0;c<path->Paths;c++)
         if(path->Path[c][0]!=0) str=path->Path[c];

   if(!str)
      return;

   for(c=strlen(str);c!=0;c--)
      if(str[c]==32) break;

   str[c]=0;
}

CheckPktDest(struct Node4D *node)
{
   struct Aka *aka;
   struct Node4D tmpnode;

   for(aka=AkaList.First;aka;aka=aka->Next)
   {
      if(Compare4D(node,&aka->Node)==0) return(TRUE);

      /* Fakenet */

      Copy4D(&tmpnode,&aka->Node);
      tmpnode.Net=aka->FakeNet;
      tmpnode.Node=tmpnode.Point;
      tmpnode.Point=0;

      if(Compare4D(node,&tmpnode)==0) return(TRUE);
   }

   return(FALSE);
}

void BadPacket(UBYTE *filename,UBYTE *comment)
{
   UBYTE destname[100],numbuf[10];
   UBYTE *buf;
   BPTR  ifh,ofh,l;
   ULONG rlen,num,errcode;

   LogWrite(3,TOSSINGERR,"Renaming %s to .bad",filename);

   if(!(buf=(UBYTE *)AllocMem(PKT_READLEN,MEMF_ANY)))
   {
      nomem=TRUE;
      return;
   }

   if(!(ifh=Open(filename,MODE_OLDFILE)))
   {
      LogWrite(1,SYSTEMERR,"Can't move %s",filename);
      FreeMem(buf,PKT_READLEN);
      return;
   }

   num=0;

   do
   {
      do
      {
         mystrncpy(destname,cfg_Inbound,90);
         AddPart(destname,FilePart(filename),90);
         strcat(destname,".bad");

         if(num != 0)
         {
            sprintf(numbuf,",%ld",num);
            strcat(destname,numbuf);
         }

         num++;
         errcode=0;

         l=Lock(destname,SHARED_LOCK);
         if(!l) errcode=IoErr();

         if(l)
            UnLock(l);

      } while(l || errcode==ERROR_OBJECT_IN_USE);

      if(!(ofh=Open(destname,MODE_NEWFILE)))
      {
         if(IoErr()!=ERROR_OBJECT_IN_USE)
         {
            LogWrite(1,SYSTEMERR,"Can't move %s",filename);
            FreeMem(buf,PKT_READLEN);
            Close(ifh);
            return;
         }
      }
   } while(!ofh);

   while(rlen=Read(ifh,buf,PKT_READLEN))
   {
      if(Write(ofh,buf,rlen)!=rlen)
      {
         diskfull=TRUE;
         Close(ifh);
         Close(ofh);
         FreeMem(buf,PKT_READLEN);
         return;
      }
   }
   Close(ifh);
   Close(ofh);
   FreeMem(buf,PKT_READLEN);

   SetComment(destname,comment);
}

ProcessKludge(UBYTE *kludge, ULONG klen)
{
   struct Node4D node;
   UBYTE buf[60];
   ULONG c,d;

   if(strncmp(kludge,"\x01RESCANNED",10)==0)
      MemMessage.Rescanned=TRUE;

   if(strncmp(kludge,"\x01MSGID:",7)==0)
   {
      strncpy(MemMessage.MSGID,&kludge[8],79);
      if(klen-9<79) MemMessage.MSGID[klen-9]=0;
      else          MemMessage.MSGID[79]=0;
   }

   if(strncmp(kludge,"\x01REPLY:",7)==0)
   {
      strncpy(MemMessage.REPLY,&kludge[8],79);
      if(klen-9<79) MemMessage.REPLY[klen-9]=0;
      else          MemMessage.REPLY[79]=0;
   }

   if(MemMessage.Area[0]==0)
   {
      if(strncmp(kludge,"\x01FMPT",5)==0)
         MemMessage.OrigNode.Point=atoi(&kludge[6]);

      if(strncmp(kludge,"\x01TOPT",5)==0)
          MemMessage.DestNode.Point=atoi(&kludge[6]);

      if(strncmp(kludge,"\x01INTL",5)==0)
      {
         if(kludge[5]==':')
            c=7;

         else
            c=6;

         for(d=0;d<59 && kludge[c]!=32 && kludge[c]!=13;c++,d++)
            buf[d]=kludge[c];

         buf[d]=0;

         if(Parse4D(buf,&node))
         {
            MemMessage.DestNode.Zone = node.Zone;
            MemMessage.DestNode.Net  = node.Net;
            MemMessage.DestNode.Node = node.Node;
         }

         if(kludge[c]==32) c++;

         for(d=0;d<59 && kludge[c]!=32 && kludge[c]!=13;c++,d++)
            buf[d]=kludge[c];

         buf[d]=0;

         if(Parse4D(buf,&node))
         {
            MemMessage.OrigNode.Zone = node.Zone;
            MemMessage.OrigNode.Net  = node.Net;
            MemMessage.OrigNode.Node = node.Node;

LogWrite(6,DEBUG,"INTL OrigNode %ld:%ld/%ld.%ld",
   MemMessage.OrigNode.Zone,
   MemMessage.OrigNode.Net,
   MemMessage.OrigNode.Node,
   MemMessage.OrigNode.Point);

         }
      }
   }
}

ReadPkt(UBYTE *pkt,ULONG size)
{
   struct jbFile *jb;
   struct TextChunk *ThisChunk;
   struct Aka *tmpaka;
   struct ConfigNode *tmpcnode;
   ULONG rlen,msgnum=0,msgoffset;
   UBYTE buf[20],errorbuf[200];
   BOOL pw;
   struct PktHeader45 *pktheader45;

   if(!(jb=jbOpen(pkt,MODE_OLDFILE,PKT_READLEN)))
   {
      LogWrite(1,SYSTEMERR,"Unable to open %s",pkt);
      return;
   }

   shortread=FALSE;
   longread=FALSE;
   pkt_pw=FALSE;
   pkt_fake=FALSE;
   pkt_4d=FALSE;
   pkt_5d=FALSE;

   PktOrig.Zone=0;
   PktOrig.Net=0;
   PktOrig.Node=0;
   PktOrig.Point=0;

   PktDest.Zone=0;
   PktDest.Net=0;
   PktDest.Node=0;
   PktDest.Point=0;

   if(jbRead(jb,&PktHeader,sizeof(struct PktHeader))!=sizeof(struct PktHeader))
   {
      LogWrite(1,TOSSINGERR,"Packet header in %s is too short",pkt);
      jbClose(jb);
      BadPacket(pkt,"Packet header is too short");
      return;
   }

   if(PktHeader.PktType!=0x0200)
   {
      LogWrite(1,TOSSINGERR,"%s is not a Type-2 packet",pkt);
      jbClose(jb);
      BadPacket(pkt,"Not a type-2 packet");
      return;
   }

   if(SWAP(PktHeader.Baud) == 2)
   {
      pktheader45=(struct PktHeader45 *)&PktHeader;

      /* PktOrig och PktDest */

      PktOrig.Zone=SWAP(pktheader45->OrigZone);
      PktOrig.Net=SWAP(pktheader45->OrigNet);
      PktOrig.Node=SWAP(pktheader45->OrigNode);
      PktOrig.Point=SWAP(pktheader45->OrigPoint);

      PktDest.Zone=SWAP(pktheader45->DestZone);
      PktDest.Net=SWAP(pktheader45->DestNet);
      PktDest.Node=SWAP(pktheader45->DestNode);
      PktDest.Point=SWAP(pktheader45->DestPoint);

      pkt_5d=TRUE;
   }
   else
   {
      /* PktOrig och PktDest */

      if(PktHeader.OrigZone)
      {
         PktOrig.Zone=SWAP(PktHeader.OrigZone);
         PktDest.Zone=SWAP(PktHeader.DestZone);
      }
      else if(PktHeader.QOrigZone)
      {
         PktOrig.Zone=SWAP(PktHeader.QOrigZone);
         PktDest.Zone=SWAP(PktHeader.QDestZone);
      }
      else
      {
         PktOrig.Zone=0;
         PktDest.Zone=0;
      }

      PktOrig.Net=SWAP(PktHeader.OrigNet);
      PktOrig.Node=SWAP(PktHeader.OrigNode);
      PktDest.Net=SWAP(PktHeader.DestNet);
      PktDest.Node=SWAP(PktHeader.DestNode);

      if(PktHeader.CWValidCopy==SWAP(PktHeader.CapabilWord))
      {
         pkt_4d=TRUE;

         if(SWAP(PktHeader.OrigPoint)!=0 && SWAP(PktHeader.OrigNet)==0xffff)
            PktOrig.Net=SWAP(PktHeader.AuxNet);

         PktOrig.Point=SWAP(PktHeader.OrigPoint);
         PktDest.Point=SWAP(PktHeader.DestPoint);
      }
   }

   /* Check packet destination */

   if((cfg_Flags & CFG_CHECKPKTDEST) && !no_security)
      if(!CheckPktDest(&PktDest))
      {
         LogWrite(1,TOSSINGERR,"Addressed to %lu:%lu/%lu.%lu, not to me",PktDest.Zone,PktDest.Net,PktDest.Node,PktDest.Point);
         sprintf(errorbuf,"Addressed to %lu:%lu/%lu.%lu, not to me",PktDest.Zone,PktDest.Net,PktDest.Node,PktDest.Point);
         jbClose(jb);
         BadPacket(pkt,errorbuf);
         return;
      }

   /* FakeNet -> 4D */

   for(tmpaka=AkaList.First;tmpaka;tmpaka=tmpaka->Next)
   {
      if(PktOrig.Net == tmpaka->FakeNet)
      {
         pkt_fake=TRUE;
         PktOrig.Point=PktOrig.Node;
         PktOrig.Node=tmpaka->Node.Node;
         PktOrig.Net=tmpaka->Node.Net;
      }
   }

   /* Fixa zone */

   if(PktOrig.Zone == 0)
      for(tmpcnode=CNodeList.First;tmpcnode;tmpcnode=tmpcnode->Next)
      {
         if(Compare4D(&PktOrig,&tmpcnode->Node)==0)
         {
            PktOrig.Zone=tmpcnode->Node.Zone;
            break;
         }
      }

   if(PktDest.Zone == 0)
      for(tmpaka=AkaList.First;tmpaka;tmpaka=tmpaka->Next)
      {
         if(Compare4D(&PktDest,&tmpaka->Node)==0)
         {
            PktDest.Zone=tmpaka->Node.Zone;
            break;
         }
      }

   if(PktOrig.Zone == 0) PktOrig.Zone=cfg_DefaultZone;
   if(PktDest.Zone == 0) PktDest.Zone=cfg_DefaultZone;

   strncpy(buf,PktHeader.Password,8);
   buf[8]=0;

   pw=CheckPacketPW(&PktOrig,buf);

   for(tmpcnode=CNodeList.First;tmpcnode;tmpcnode=tmpcnode->Next)
      if(Compare4D(&PktOrig,&tmpcnode->Node)==0)
         LastNode=tmpcnode;

  buf[0]=0;

   if(pkt_pw)
   {
      if(buf[0]==0) strcpy(buf," / ");
      else          strcat(buf,",");
      strcat(buf,"pw");
   }

   if(pkt_fake)
   {
      if(buf[0]==0) strcpy(buf," / ");
      else          strcat(buf,",");
      strcat(buf,"fake");
   }

   if(pkt_4d)
   {
      if(buf[0]==0) strcpy(buf," / ");
      else          strcat(buf,",");
      strcat(buf,"4d");
   }

   if(pkt_5d)
   {
      if(buf[0]==0) strcpy(buf," / ");
      else          strcat(buf,",");
      strcat(buf,"5d");
   }

   if(SWAP(PktHeader.Baud) == 2)
   {
      UBYTE domain[10];

      strncpy(domain,pktheader45->OrigDomain,8);
      domain[8]=0;

      LogWrite(1,ACTIONINFO,"Tossing %s (%luK) from %ld:%ld/%ld.%ld@%s%s",FilePart(pkt),
                                                              (size+512)/1024,
                                                              PktOrig.Zone,
                                                              PktOrig.Net,
                                                              PktOrig.Node,
                                                              PktOrig.Point,
                                                              domain,
                                                              buf);
   }
   else
   {
      PktHeader.Year       =  SWAP(PktHeader.Year);
      PktHeader.Month      =  SWAP(PktHeader.Month);
      PktHeader.Day        =  SWAP(PktHeader.Day);
      PktHeader.Hour       =  SWAP(PktHeader.Hour);
      PktHeader.Minute     =  SWAP(PktHeader.Minute);
      PktHeader.Second     =  SWAP(PktHeader.Second);

      if(PktHeader.Month > 11)
         PktHeader.Month=12;

      LogWrite(1,ACTIONINFO,"Tossing %s (%luK) from %ld:%ld/%ld.%ld (%02ld-%s-%02ld %02ld:%02ld:%02ld)%s",FilePart(pkt),
                                                                                              (size+512)/1024,
                                                                                              PktOrig.Zone,
                                                                                              PktOrig.Net,
                                                                                              PktOrig.Node,
                                                                                              PktOrig.Point,
                                                                                              PktHeader.Day,
                                                                                              MonthNames[PktHeader.Month],
                                                                                              PktHeader.Year % 100,
                                                                                              PktHeader.Hour,
                                                                                              PktHeader.Minute,
                                                                                              PktHeader.Second,
                                                                                              buf);
   }

   if(no_security)
      LogWrite(1,TOSSINGINFO,"No security packet, overriding all security checks...");

   if(!pw && !no_security)
   {
      LogWrite(1,TOSSINGERR,"Wrong password");
      jbClose(jb);
      BadPacket(pkt,"Wrong password");
      return;
   }

   msgoffset=jbFTell(jb);

   if(jbRead(jb,&PktMsgHeader,sizeof(struct PktMsgHeader)) < 2)
   {
      LogWrite(1,TOSSINGERR,"Message header for msg #1 (offset %ld) is too short",msgoffset);
      jbClose(jb);
      sprintf(errorbuf,"Message header for msg #1 (offset %ld) is too short",msgoffset);
      BadPacket(pkt,errorbuf);
      return;
   }

   while(PktMsgHeader.PktType==0x0200)
   {
      msgnum++;
      Printf("Message %lu              \x0d",msgnum);

      /* Init variables */

      ((struct TextChunk *)MemMessage.TextChunks.First)->Length=NULL;
      ((struct Nodes2D *)MemMessage.SeenBy.First)->Nodes=0;
      ((struct Path *)MemMessage.Path.First)->Paths=0;

      ThisChunk=MemMessage.TextChunks.First;

      MemMessage.Area[0]=0;
      MemMessage.MSGID[0]=0;
      MemMessage.REPLY[0]=0;
      MemMessage.OrigNode.Zone=0;
      MemMessage.OrigNode.Point=0;
      MemMessage.DestNode.Zone=0;
      MemMessage.DestNode.Point=0;
      MemMessage.BadReason[0]=0;
      MemMessage.Rescanned=FALSE;

LogWrite(6,DEBUG,"-3 OrigNode %ld:%ld/%ld.%ld",
   MemMessage.OrigNode.Zone,
   MemMessage.OrigNode.Net,
   MemMessage.OrigNode.Node,
   MemMessage.OrigNode.Point);

      /* Get header strings */

      ReadNull(MemMessage.DateTime,20,jb);
      ReadNull(MemMessage.To,36,jb);
      ReadNull(MemMessage.From,36,jb);
      ReadNull(MemMessage.Subject,72,jb);

      /* Corrupt packet? */

      if(shortread)
      {
         LogWrite(1,TOSSINGERR,"Message header for msg #%lu (offset %ld) is short",msgnum,msgoffset);
         sprintf(errorbuf,"Message header for msg #%lu (offset %ld) is short",msgnum,msgoffset);
         jbClose(jb);
         BadPacket(pkt,errorbuf);
         return;
      }

      if(longread)
      {
         LogWrite(1,TOSSINGERR,"Header strings too long in msg #%lu (offset %ld)",msgnum,msgoffset);
         sprintf(errorbuf,"Header strings too long in msg #%lu (offset %ld)",msgnum,msgoffset);
         jbClose(jb);
         BadPacket(pkt,errorbuf);
         return;
      }

LogWrite(6,DEBUG,"-2 OrigNode %ld:%ld/%ld.%ld",
   MemMessage.OrigNode.Zone,
   MemMessage.OrigNode.Net,
   MemMessage.OrigNode.Node,
   MemMessage.OrigNode.Point);

      /* Start reading message text */

      messageend=FALSE;

      rlen=ReadCR(ThisChunk->Data,PKT_CHUNKLEN-ThisChunk->Length,jb);

      /* Echomail or netmail? */

      if(strncmp(ThisChunk->Data,"AREA:",5)==0)
      {
         ThisChunk->Data[rlen]=0;
         mystrncpy(MemMessage.Area,&ThisChunk->Data[5],40);
         rlen=0;
      }

      /* Strip spaces from area name */

      stripspace(MemMessage.Area);

      /* Process kludges */

      if(ThisChunk->Data[0]==1) ProcessKludge(ThisChunk->Data,rlen);
      ThisChunk->Length=rlen;

      /* Get rest of text */

// LogWrite(6,DEBUG,"PktCheckpoint 1");

      while(messageend==FALSE)
      {
         if(PKT_CHUNKLEN-ThisChunk->Length<PKT_MINREADLEN)
         {
            if(!(ThisChunk=(struct TextChunk *)AllocMem(sizeof(struct TextChunk),MEMF_ANY)))
            {
               LogWrite(1,SYSTEMERR,"Out of memory!");
               jbClose(jb);
               CleanMemMessage();
               return;
            }

            jbAddNode(&MemMessage.TextChunks,(struct jbNode *)ThisChunk);
            ThisChunk->Length=0;
         }

         rlen=ReadCR(&ThisChunk->Data[ThisChunk->Length],PKT_CHUNKLEN-ThisChunk->Length,jb);

         if(shortread)
         {
            jbClose(jb);
            CleanMemMessage();
            sprintf(errorbuf,"Got unexpected EOF when reading msg #%lu (offset %ld)",msgnum,msgoffset);
            LogWrite(1,TOSSINGERR,"Got unexpected EOF when reading msg #%lu (offset %ld)",msgnum,msgoffset);
            BadPacket(pkt,errorbuf);
            return;
         }

         if(rlen > 8 && MemMessage.Area[0] && strncmp(&ThisChunk->Data[ThisChunk->Length],"SEEN-BY:",8)==0)
            AddSeenby(&ThisChunk->Data[ThisChunk->Length+8],&MemMessage.SeenBy);


         else if(rlen > 6 && MemMessage.Area[0] && strncmp(&ThisChunk->Data[ThisChunk->Length],"\x01PATH:",6)==0)
            AddPath(&ThisChunk->Data[ThisChunk->Length+7],&MemMessage.Path);

         else
         {
            if(ThisChunk->Data[ThisChunk->Length]==1)
               ProcessKludge(&ThisChunk->Data[ThisChunk->Length],rlen);

            ThisChunk->Length+=rlen;
         }

         if(nomem)
         {
            CleanMemMessage();
            /* Copy back to inbound */
            jbClose(jb);
            return;
         }
      }

// LogWrite(6,DEBUG,"PktCheckpoint 2");

LogWrite(6,DEBUG,"-1 OrigNode %ld:%ld/%ld.%ld",
   MemMessage.OrigNode.Zone,
   MemMessage.OrigNode.Net,
   MemMessage.OrigNode.Node,
   MemMessage.OrigNode.Point);

      MemMessage.OrigNode.Net  = SWAP(PktMsgHeader.OrigNet);
      MemMessage.OrigNode.Node = SWAP(PktMsgHeader.OrigNode);

      MemMessage.DestNode.Net  = SWAP(PktMsgHeader.DestNet);
      MemMessage.DestNode.Node = SWAP(PktMsgHeader.DestNode);

      if(MemMessage.DestNode.Zone == 0) MemMessage.DestNode.Zone=PktDest.Zone;
      if(MemMessage.OrigNode.Zone == 0) MemMessage.DestNode.Zone=PktOrig.Zone;

      MemMessage.Attr=SWAP(PktMsgHeader.Attr);
      MemMessage.Cost=SWAP(PktMsgHeader.Cost);

      if(SetSignal(0L,0L) & SIGBREAKF_CTRL_C)
      {
         CleanMemMessage();
         jbClose(jb);
         exitprg=TRUE;
         return;
      }

// LogWrite(6,DEBUG,"PktCheckpoint 3");

LogWrite(6,DEBUG,"0 OrigNode %ld:%ld/%ld.%ld",
   MemMessage.OrigNode.Zone,
   MemMessage.OrigNode.Net,
   MemMessage.OrigNode.Node,
   MemMessage.OrigNode.Point);

      HandleMessage();
      CleanMemMessage();

// LogWrite(6,DEBUG,"PktCheckpoint 4");

      if(nomem || diskfull || exitprg)
      {
         jbClose(jb);
         return;
      }

      msgoffset=jbFTell(jb);

      if(jbRead(jb,&PktMsgHeader,sizeof(struct PktMsgHeader)) < 2)
      {
         sprintf(errorbuf,"Packet header too short for msg #%lu (offset %ld)",msgnum+1,msgoffset);
         LogWrite(1,TOSSINGERR,"Packet header too short for msg #%lu (offset %ld)",msgnum+1,msgoffset);
         jbClose(jb);
         BadPacket(pkt,errorbuf);
         return;
      }
   }

   if(PktMsgHeader.PktType!=0)
   {
      sprintf(errorbuf,"Unknown message type %lu for message #%lu (offset %ld)",SWAP(PktMsgHeader.PktType),msgnum+1,msgoffset);
      LogWrite(1,TOSSINGERR,"Unknown message type %lu for message #%lu (offset %ld)",SWAP(PktMsgHeader.PktType),msgnum+1,msgoffset);
      jbClose(jb);
      BadPacket(pkt,errorbuf);
      return;
   }

   jbClose(jb);
}

CleanMemMessage()
{
   if(MemMessage.TextChunks.First->Next)
      jbFreeList(&MemMessage.TextChunks,MemMessage.TextChunks.First->Next,sizeof(struct TextChunk));

   if(MemMessage.SeenBy.First->Next)
      jbFreeList(&MemMessage.SeenBy,MemMessage.SeenBy.First->Next,sizeof(struct Nodes2D));

   if(MemMessage.Path.First->Next)
      jbFreeList(&MemMessage.Path,MemMessage.Path.First->Next,sizeof(struct Path));
}

/*************************************************************************/
/*                                                                       */
/*                               Write Pkt                               */
/*                                                                       */
/*************************************************************************/

struct Pkt *FirstPkt;
struct Pkt *LastPkt;

WriteEchoMail(struct ConfigNode *node, struct Aka *aka)
{
   UBYTE buf[100];
   struct Pkt *pkt;
   struct TextChunk  *chunk;
   struct Nodes2D    *oldseenby,*oldlast;
   struct Node4D n4d;

   MemMessage.Type=PKTS_ECHOMAIL;

   if(!(node->Pkt && Compare4D(&aka->Node,&node->Pkt->Orig)==0 && node->Pkt->Type==PKTS_ECHOMAIL))
   {
      for(pkt=PktList.First;pkt;pkt=pkt->Next)
         if(Compare4D(&pkt->Dest,&node->Node)==0 && Compare4D(&pkt->Orig,&aka->Node)==0 && pkt->Type==PKTS_ECHOMAIL) break;

      node->Pkt=pkt;
   }

   if(!node->Pkt || (cfg_MaxPktSize!=0 && node->Pkt->jb->len > cfg_MaxPktSize))
   {
      if(node->Pkt) FinishPacket(node->Pkt);

      if(!(node->Pkt=(struct Pkt *)AllocMem(sizeof(struct Pkt),MEMF_CLEAR)))
      {
         nomem=TRUE;
         return;
      }

      node->Pkt->Type=PKTS_ECHOMAIL;
      CopyMem(&node->Node,&node->Pkt->Dest,sizeof(struct Node4D));
      CopyMem(&aka->Node,&node->Pkt->Orig,sizeof(struct Node4D));

      jbAddNode(&PktList,(struct jbNode *)node->Pkt);

      node->Pkt->jb=(struct jbFile *)CreatePkt(&node->Node,node,aka);

      if(!node->Pkt->jb || nomem || diskfull) return;
   }

   Copy4D(&MemMessage.DestNode,&node->Node);
   Copy4D(&MemMessage.OrigNode,&aka->Node);

   WriteMsgHeader(node->Pkt->jb,&node->Node,node,aka,FALSE);
   if(diskfull) return;

   sprintf(buf,"AREA:%s\x0d",MemMessage.Area);
   jbWrite(node->Pkt->jb,buf,strlen(buf));
   if(diskfull) return;

   for(chunk=MemMessage.TextChunks.First;chunk;chunk=chunk->Next)
   {
      jbWrite(node->Pkt->jb,chunk->Data,chunk->Length);
      if(diskfull) return;
   }

   if(node->Node.Zone != aka->Node.Zone || (node->Flags & NODE_TINYSEENBY))
   {
      oldseenby=MemMessage.SeenBy.First;
      oldlast=MemMessage.SeenBy.Last;

      if(!(MemMessage.SeenBy.First=(struct Nodes2D *)AllocMem(sizeof(struct Nodes2D),MEMF_ANY)))
      {
         MemMessage.SeenBy.First=oldseenby;
         nomem=TRUE;
         return;
      }

      MemMessage.SeenBy.Last=MemMessage.SeenBy.First;
      ((struct Nodes2D *)MemMessage.SeenBy.First)->Nodes=0;
      ((struct Nodes2D *)MemMessage.SeenBy.First)->Next=NULL;

      if(node->Node.Point == 0) AddNodes2DList(&MemMessage.SeenBy,node->Node.Net,node->Node.Node);
      if(aka->Node.Point == 0) AddNodes2DList(&MemMessage.SeenBy,aka->Node.Net,aka->Node.Node);
   }

   if(node->Flags & NODE_STONEAGE && node->Node.Point!=0)
      AddNodes2DList(&MemMessage.SeenBy,aka->FakeNet,node->Node.Point);

   if(node->Flags & NODE_STONEAGE && aka->Node.Point!=0)
   {
      AddNodes2DList(&MemMessage.SeenBy,aka->FakeNet,aka->Node.Point);

      n4d.Zone=aka->Node.Zone;
      n4d.Net=aka->FakeNet;
      n4d.Node=aka->Node.Point;
      n4d.Point=0;

      AddNodePath(&MemMessage.Path,&n4d);
      if(nomem) return;
   }
   else if(aka->Node.Point == 0 || (cfg_Flags & CFG_PATH3D))
   {
      AddNodePath(&MemMessage.Path,&aka->Node);
      if(nomem) return;
   }

   if(!(node->Flags & NODE_NOSEENBY))
   {
      SortNodes2D(&MemMessage.SeenBy);
      WriteNodes2D(node->Pkt->jb,&MemMessage.SeenBy);
   }

   WritePath(node->Pkt->jb,&MemMessage.Path);

   if(node->Flags & NODE_STONEAGE && node->Node.Point!=0)
      RemNodes2DList(&MemMessage.SeenBy,aka->FakeNet,node->Node.Point);

   if(node->Flags & NODE_STONEAGE && aka->Node.Point!=0)
   {
      RemNodes2DList(&MemMessage.SeenBy,aka->FakeNet,aka->Node.Point);
      RemLastPath(&MemMessage.Path);
   }
   else if(aka->Node.Point == 0 || (cfg_Flags & CFG_PATH3D))
   {
      RemLastPath(&MemMessage.Path);
   }

   if(node->Node.Zone != aka->Node.Zone || (node->Flags & NODE_TINYSEENBY))
   {
      jbFreeList(&MemMessage.SeenBy,MemMessage.SeenBy.First,sizeof(struct Nodes2D));
      MemMessage.SeenBy.First=oldseenby;
      MemMessage.SeenBy.Last=oldlast;
   }

   jbPutChar(node->Pkt->jb,0);
}

WriteNodes2D(struct jbFile *jb,struct jbList *list)
{
   UWORD c;
   struct Nodes2D *nodes;
   UWORD lastnet;
   UBYTE buf[100],buf2[50],buf3[20];

   strcpy(buf,"SEEN-BY:");
   lastnet=0;

   for(nodes=list->First;nodes;nodes=nodes->Next)
   {
      for(c=0;c<nodes->Nodes;c++)
      {
         if(nodes->Net[c]!=0 || nodes->Node[c]!=0)
         {
            strcpy(buf2," ");

            if(nodes->Net[c]!=lastnet)
            {
               sprintf(buf3,"%lu/",nodes->Net[c]);
               strcat(buf2,buf3);
            }

            sprintf(buf3,"%lu",nodes->Node[c]);
            strcat(buf2,buf3);

            lastnet=nodes->Net[c];

            if(strlen(buf)+strlen(buf2) > 77)
            {
               jbWrite(jb,buf,strlen(buf));
               jbPutChar(jb,13);

               strcpy(buf,"SEEN-BY:");
               sprintf(buf2," %lu/%lu",nodes->Net[c],nodes->Node[c]);
               lastnet=nodes->Net[c];

               strcat(buf,buf2);
            }
            else
            {
               strcat(buf,buf2);
            }
         }
      }
   }

   if(strlen(buf)>8)
   {
      jbWrite(jb,buf,strlen(buf));
      jbPutChar(jb,13);
   }
}

WritePath(struct jbFile *jb,struct jbList *list)
{
   UWORD c;
   struct Path *path;

   for(path=list->First;path;path=path->Next)
      for(c=0;c<path->Paths;c++)
         if(path->Path[c][0]!=0)
         {
            jbWrite(jb,"\x01PATH: ",7);
            jbWrite(jb,path->Path[c],strlen(path->Path[c]));
            jbPutChar(jb,13);
         }
}

WriteNetMail(struct Node4D *dest,struct Aka *aka)
{
   struct Node4D n4d;
   struct Pkt *pkt;
   struct ConfigNode *cnode;
   struct TextChunk *chunk;

   for(cnode=CNodeList.First;cnode;cnode=cnode->Next)
      if(Compare4D(&cnode->Node,dest)==0) break;

   pkt=NULL;

   if(cnode)
   {
      if((cnode->Flags & NODE_PACKNETMAIL) && MemMessage.Type==PKTS_NORMAL)
      {
         MemMessage.Type=PKTS_ECHOMAIL;
         pkt=cnode->Pkt;
      }
   }

   if(!(pkt && Compare4D(&aka->Node,&pkt->Orig)==0 && pkt->Type == MemMessage.Type))
   {
      for(pkt=PktList.First;pkt;pkt=pkt->Next)
        if(Compare4D(dest,&pkt->Dest)==0 && Compare4D(&aka->Node,&pkt->Orig)==0 && pkt->Type == MemMessage.Type) break;
   }

   if(!pkt || (cfg_MaxPktSize!=0 && pkt->jb->len > cfg_MaxPktSize))
   {
      if(pkt) FinishPacket(pkt);

      if(!(pkt=(struct Pkt *)AllocMem(sizeof(struct Pkt),MEMF_CLEAR)))
      {
         nomem=TRUE;
         return;
      }

      pkt->Type=MemMessage.Type;
      CopyMem(dest,&pkt->Dest,sizeof(struct Node4D));
      CopyMem(&aka->Node,&pkt->Orig,sizeof(struct Node4D));

      jbAddNode(&PktList,(struct jbNode *)pkt);

      pkt->jb=(struct jbFile *)CreatePkt(dest,cnode,aka);
      if(cnode && (cnode->Flags & NODE_PACKNETMAIL)) cnode->Pkt=pkt;

      if(!pkt->jb || nomem || diskfull) return;
   }

   if(cnode)
   {
      if(cnode->Flags & NODE_STONEAGE)
      {
         if(dest->Point != 0)
         {
            n4d.Zone=dest->Zone;
            n4d.Net=aka->FakeNet;
            n4d.Node=dest->Point;
            n4d.Point=0;
            Remap(&MemMessage.OrigNode,&n4d,MemMessage.To);
         }
         else if(aka->Node.Point != 0)
         {
            n4d.Zone=aka->Node.Zone;
            n4d.Net=aka->FakeNet;
            n4d.Node=aka->Node.Point;
            n4d.Point=0;
            Remap(&n4d,&MemMessage.DestNode,MemMessage.To);
         }
      }
   }

   WriteMsgHeader(pkt->jb,dest,cnode,aka,TRUE);
   if(diskfull) return;

   for(chunk=MemMessage.TextChunks.First;chunk;chunk=chunk->Next)
   {
      jbWrite(pkt->jb,chunk->Data,chunk->Length);
      if(diskfull) return;
   }
   jbPutChar(pkt->jb,0);
}


CreatePkt(struct Node4D *dest,struct ConfigNode *node,struct Aka *aka)
{
   UBYTE buf[100],buf2[100];
   struct jbFile *jb;
   struct DateTime dt;
   struct DateStamp ds;
   UBYTE date[LEN_DATSTRING];
   ULONG num,c;
   BPTR l;

   DateStamp(&ds);
   num=ds.ds_Days*24*60*60+ds.ds_Minute*60+ds.ds_Tick/50;

   jb=FALSE;

   while(!jb && !exitprg)
   {
      sprintf(buf2,"%08lx.newpkt",num);
      strcpy(buf,cfg_PacketCreate);
      AddPart(buf,buf2,100);

      l=Lock(buf,SHARED_LOCK);

      while(l || IoErr()==ERROR_OBJECT_IN_USE)
      {
         if(l) UnLock(l);
         num++;
         sprintf(buf2,"%08lx.newpkt",num);
         strcpy(buf,cfg_PacketCreate);
         AddPart(buf,buf2,100);
         l=Lock(buf,SHARED_LOCK);
      }

      if(!(jb=jbOpen(buf,MODE_NEWFILE,PKT_WRITELEN)))
      {
         if(IoErr()==ERROR_OBJECT_IN_USE)
            num++;

         else
            exitprg=TRUE;
      }
   }

   if(!jb)
   {
      LogWrite(1,SYSTEMERR,"Unable to create packet %08lx.newpkt",num);
      exitprg=TRUE;
      return(NULL);
   }

   PktHeader.OrigNode=SWAP(aka->Node.Node);
   PktHeader.DestNode=SWAP(dest->Node);

   DateStamp(&dt.dat_Stamp);
   dt.dat_Format=FORMAT_USA;
   dt.dat_Flags=0;
   dt.dat_StrDay=NULL;
   dt.dat_StrDate=date;
   dt.dat_StrTime=NULL;
   DateToStr(&dt);

   PktHeader.Month=SWAP(atoi(date)-1);
   PktHeader.Day=SWAP(atoi(&date[3]));
   PktHeader.Hour=SWAP(dt.dat_Stamp.ds_Minute/60);
   PktHeader.Minute=SWAP(dt.dat_Stamp.ds_Minute%60);
   PktHeader.Second=SWAP(dt.dat_Stamp.ds_Tick/50);

   if(atoi(&date[6])>90)
      PktHeader.Year=SWAP(atoi(&date[6])+1900);

   else
      PktHeader.Year=SWAP(atoi(&date[6])+2000);

   PktHeader.Baud=0;
   PktHeader.PktType=0x0200;
   PktHeader.OrigNet=SWAP(aka->Node.Net);
   PktHeader.DestNet=SWAP(dest->Net);
   PktHeader.ProdCodeLow=0xfe;
   PktHeader.RevisionMajor=VERSION_MAJOR;
   PktHeader.QOrigZone=SWAP(aka->Node.Zone);
   PktHeader.QDestZone=SWAP(dest->Zone);
   PktHeader.AuxNet=0;
   PktHeader.CWValidCopy=1;
   PktHeader.ProdCodeHigh=0;
   PktHeader.RevisionMinor=VERSION_MINOR;
   PktHeader.CapabilWord=SWAP(1);
   PktHeader.OrigZone=SWAP(aka->Node.Zone);
   PktHeader.DestZone=SWAP(dest->Zone);
   PktHeader.OrigPoint=SWAP(aka->Node.Point);
   PktHeader.DestPoint=SWAP(dest->Point);
   PktHeader.ProdData=0;

   for(c=0;c<8;c++)
      PktHeader.Password[c]=0;

   if(node)
      strncpy(PktHeader.Password,node->PacketPW,8);

   if(node)
   {
      if(node->Flags & NODE_STONEAGE)
      {
         PktHeader.OrigPoint=0;
         PktHeader.DestPoint=0;

         if(node->Node.Point!=0)
         {
            PktHeader.DestPoint=0;
            PktHeader.DestNet=SWAP(aka->FakeNet);
            PktHeader.DestNode=SWAP(node->Node.Point);
         }

         if(aka->Node.Point!=0)
         {
            PktHeader.OrigPoint=0;
            PktHeader.OrigNet=SWAP(aka->FakeNet);
            PktHeader.OrigNode=SWAP(aka->Node.Point);
         }

         PktHeader.OrigZone=0;
         PktHeader.DestZone=0;
         PktHeader.CapabilWord=0;
         PktHeader.CWValidCopy=0;
      }
   }

   jbWrite(jb,&PktHeader,sizeof(struct PktHeader));
   return(jb);
}

BOOL WriteNull(struct jbFile *jb,UBYTE *str)
{
   jbWrite(jb,str,strlen(str)+1);
   return(TRUE);
}

WriteMsgHeader(struct jbFile *jb,struct Node4D *dest,struct ConfigNode *node,struct Aka *aka,BOOL netmail)
{
   PktMsgHeader.PktType=0x0200;
   PktMsgHeader.OrigNode=SWAP(MemMessage.OrigNode.Node);
   PktMsgHeader.DestNode=SWAP(MemMessage.DestNode.Node);
   PktMsgHeader.OrigNet=SWAP(MemMessage.OrigNode.Net);
   PktMsgHeader.DestNet=SWAP(MemMessage.DestNode.Net);
   PktMsgHeader.Attr=SWAP(MemMessage.Attr);
   PktMsgHeader.Cost=SWAP(MemMessage.Cost);

   if(node && !netmail)
   {
      if(node->Flags & NODE_STONEAGE && node->Node.Point!=0)
      {
         PktMsgHeader.DestNet=SWAP(aka->FakeNet);
         PktMsgHeader.DestNode=SWAP(MemMessage.DestNode.Point);
      }

      if(node->Flags & NODE_STONEAGE && aka->Node.Point!=0)
      {
         PktMsgHeader.OrigNet=SWAP(aka->FakeNet);
         PktMsgHeader.OrigNode=SWAP(MemMessage.OrigNode.Point);
      }
   }

   if(node && netmail)
   {
      if(node->Flags & NODE_STONEAGE && aka->Node.Point!=0)
      {
         PktMsgHeader.OrigNet=SWAP(aka->FakeNet);
         PktMsgHeader.OrigNode=SWAP(aka->Node.Point);
      }
   }

   jbWrite(jb,&PktMsgHeader,sizeof(struct PktMsgHeader));

   WriteNull(jb,MemMessage.DateTime);
   WriteNull(jb,MemMessage.To);
   WriteNull(jb,MemMessage.From);
   WriteNull(jb,MemMessage.Subject);
}

FindPkt(struct Node4D *node,UBYTE type)
{
   struct Pkt *pkt;

   for(pkt=PktList.First;pkt;pkt=pkt->Next)
      if(Compare4D(node,&pkt->Dest)==0 && type==pkt->Type) return(pkt);

   return(NULL);
}

FinishPacket(struct Pkt *pkt)
{
   UBYTE *type;
   UBYTE name[100],comment[100];

   NameFromFH(pkt->jb->fh,name,100);

   if(pkt->Type == PKTS_ECHOMAIL)
   {
      sprintf(comment,"Echomail %lu:%lu/%lu.%lu",pkt->Dest.Zone,pkt->Dest.Net,pkt->Dest.Node,pkt->Dest.Point);
   }
   else
   {
      switch(pkt->Type)
      {
         case PKTS_HOLD:   type="Hold";
                           break;
         case PKTS_NORMAL: type="Normal";
                           break;
         case PKTS_DIRECT: type="Direct";
                           break;
         case PKTS_CRASH:  type="Crash";
                           break;
      }

      sprintf(comment,"Netmail %s %lu:%lu/%lu.%lu",type,
                                               pkt->Dest.Zone,pkt->Dest.Net,pkt->Dest.Node,pkt->Dest.Point);
   }
   jbPutChar(pkt->jb,0);
   jbPutChar(pkt->jb,0);
   jbClose(pkt->jb);
   jbFreeNode(&PktList,(struct jbNode *)pkt,sizeof(struct Pkt));

   if(diskfull) return;

   SetComment(name,comment);
}

ClosePackets()
{
   struct Pkt *pkt,*pkt2;

   pkt=PktList.First;

   while(pkt)
   {
      pkt2=pkt->Next;
      FinishPacket(pkt);
      pkt=pkt2;
   }
}

