#include <CrashMail.h>

void LogScanResults(void)
{
   LogWrite(2,MISCINFO,"");

   if(scan_num==0)
     LogWrite(2,TOSSINGINFO,"No messages exported");

   else if(scan_num==1)
      LogWrite(2,TOSSINGINFO,"1 message exported");

   else
   {
      LogWrite(2,TOSSINGINFO,"%lu messages exported",scan_num);
   }
}

BOOL DoScanArea(UBYTE *tagname)
{
   struct Area *area;

   for(area=AreaList.First;area;area=area->Next)
      if(stricmp(area->Tagname,tagname)==0) break;

   if(area)
   {
      if((area->Path[0]!=0 && !(area->Flags & AREA_DEFAULT) && !(area->Flags & AREA_BAD)) || area->Flags & AREA_NETMAIL)
      {
         switch(area->AreaType)
         {
            case AREATYPE_UMS: ExportAreaUMS(area);
                               break;
            case AREATYPE_MSG: ExportAreaMSG(area);
                               break;
         }

         return(TRUE);
      }
      else
      {
         LogWrite(1,USERERR,"You can't scan area %s",tagname);
      }
   }
   else
   {
      LogWrite(1,USERERR,"Unknown area %s",tagname);
   }

   return(FALSE);
}

BOOL Scan(void)
{
   struct Area *Area;
   struct DateStamp ds;
   BOOL scannedumsmail;

   PktOrig.Zone=0;
   PktOrig.Net=0;
   PktOrig.Node=0;
   PktOrig.Point=0;

   scannedumsmail=FALSE;

   isscanning=TRUE;
   isrescanning=FALSE;

   if(!InitSession())
      return(FALSE);

   LogWrite(2,ACTIONINFO,"Scanning areas for messages to export");

   if(UMS)
   {
      UMSSelectTags(UMSHandle,UMSTAG_SelWriteLocal,TRUE,
                              UMSTAG_SelSet,       0,
                              UMSTAG_SelUnset,     UMSFLAG_TOEXPORT | UMSFLAG_EXPORTAREA | UMSFLAG_EXPORTED,
                              TAG_DONE);

      UMSSelectTags(UMSHandle,UMSTAG_SelWriteLocal, TRUE,
                              UMSTAG_SelMask,       UMSUSTATF_ViewAccess | UMSUSTATF_ReadAccess | UMSUSTATF_Old,
                              UMSTAG_SelMatch,      UMSUSTATF_ViewAccess | UMSUSTATF_ReadAccess,
                              UMSTAG_SelSet,        UMSFLAG_TOEXPORT,
                              UMSTAG_SelUnset,      0,
                              TAG_DONE);

      UMSSelectTags(UMSHandle,UMSTAG_SelWriteLocal, TRUE,
                              UMSTAG_SelReadGlobal, TRUE,
                              UMSTAG_SelMask,       UMSGSTATF_Parked,
                              UMSTAG_SelMatch,      UMSGSTATF_Parked,
                              UMSTAG_SelUnset,      UMSFLAG_TOEXPORT,
                              UMSTAG_SelSet,        0,
                              TAG_DONE);
    }

   for(Area=AreaList.First;Area;Area=Area->Next)
   {
      if((Area->Path[0]!=0 && !(Area->Flags & AREA_DEFAULT) && !(Area->Flags & AREA_BAD)) || Area->Flags & AREA_NETMAIL)
      {
         switch(Area->AreaType)
         {
            case AREATYPE_UMS: ExportAreaUMS(Area);
                               break;
            case AREATYPE_MSG: ExportAreaMSG(Area);
                               break;
         }
      }

      if(nomem || diskfull || exitprg)
      {
         FinishSession();
         return(FALSE);
      }

      if(SetSignal(0L,0L) & SIGBREAKF_CTRL_C)
      {
         FinishSession();
         return(FALSE);
      }
   }

   DateStamp(&ds);

   LogScanResults();
   PutStr("\n");

   FinishSession();

   return(TRUE);
}

BOOL ScanList(UBYTE *file)
{
   BPTR fh;
   UBYTE buf[100];

   if(!(fh=Open(file,MODE_OLDFILE)))
   {
      LogWrite(1,USERERR,"Unable to open %s",file);
      return(FALSE);
   }

   if(UMS)
   {
      UMSSelectTags(UMSHandle,UMSTAG_SelWriteLocal,TRUE,
                              UMSTAG_SelSet,       0,
                              UMSTAG_SelUnset,     UMSFLAG_TOEXPORT | UMSFLAG_EXPORTAREA | UMSFLAG_EXPORTED,
                              TAG_DONE);

      UMSSelectTags(UMSHandle,UMSTAG_SelWriteLocal, TRUE,
                              UMSTAG_SelMask,       UMSUSTATF_ViewAccess | UMSUSTATF_ReadAccess | UMSUSTATF_Old,
                              UMSTAG_SelMatch,      UMSUSTATF_ViewAccess | UMSUSTATF_ReadAccess,
                              UMSTAG_SelSet,        UMSFLAG_TOEXPORT,
                              UMSTAG_SelUnset,      0,
                              TAG_DONE);
   }

   if(!InitSession())
      return(FALSE);

   isscanning=TRUE;
   isrescanning=FALSE;

   PktOrig.Zone=0;
   PktOrig.Net=0;
   PktOrig.Node=0;
   PktOrig.Point=0;

   while(FGets(fh,buf,100) && !exitprg && !nomem)
   {
      if(strlen(buf)>2)
      {
         if(buf[strlen(buf)-1]<32)
            buf[strlen(buf)-1]=0;

         DoScanArea(buf);
      }
   }

   Close(fh);

   if(!exitprg && !nomem)
      LogScanResults();

   FinishSession();

   return(TRUE);
}

BOOL ScanArea(UBYTE *tagname)
{
   if(UMS)
   {
      UMSSelectTags(UMSHandle,UMSTAG_SelWriteLocal,TRUE,
                              UMSTAG_SelSet,       0,
                              UMSTAG_SelUnset,     UMSFLAG_TOEXPORT | UMSFLAG_EXPORTAREA | UMSFLAG_EXPORTED,
                              TAG_DONE);

      UMSSelectTags(UMSHandle,UMSTAG_SelWriteLocal, TRUE,
                              UMSTAG_SelMask,       UMSUSTATF_ViewAccess | UMSUSTATF_ReadAccess | UMSUSTATF_Old,
                              UMSTAG_SelMatch,      UMSUSTATF_ViewAccess | UMSUSTATF_ReadAccess,
                              UMSTAG_SelSet,        UMSFLAG_TOEXPORT,
                              UMSTAG_SelUnset,      0,
                              TAG_DONE);
   }

   if(!InitSession())
      return(FALSE);
      
   isscanning=TRUE;
   isrescanning=FALSE;

   PktOrig.Zone=0;
   PktOrig.Net=0;
   PktOrig.Node=0;
   PktOrig.Point=0;

   if(!DoScanArea(tagname))
   {
      return(FALSE);
   }

   if(!exitprg && !nomem)
   {
      LogScanResults();
      PutStr("\n");
   }

   FinishSession();

   return(TRUE);
}

