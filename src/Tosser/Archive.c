#include <CrashMail.h>

UBYTE *prinames[]={"Normal","Hold","Normal","Direct","Crash"};

ArchiveOutbound()
{
   struct jbList ArcList;
   struct jbList PktList;
   struct jbList PktList2;
   struct FileToProcess *ftp;
   UBYTE arcname[100],buf[200],buf2[200],newname[200],*dowhat;

   ScanForNewPkt(cfg_PacketCreate,&PktList);
   ScanForPktTmp(cfg_PacketDir,&PktList2);

   if(exitprg || nomem)
   {
      jbFreeList(&PktList,PktList.First,sizeof(struct FileToProcess));
      jbFreeList(&PktList2,PktList2.First,sizeof(struct FileToProcess));
      return;          
   }

   ScanForNewOldArc(cfg_PacketDir,&ArcList);

   if(exitprg || nomem)
   {
      jbFreeList(&PktList,PktList.First,sizeof(struct FileToProcess));
      jbFreeList(&PktList2,PktList2.First,sizeof(struct FileToProcess));
      jbFreeList(&ArcList,ArcList.First,sizeof(struct FileToProcess));
      return;
   }

   SortFTPList(&PktList);
   SortFTPList(&PktList2);
   SortFTPList(&ArcList);

   LogWrite(3,ACTIONINFO,"Scanning for files to pack");

   PackOutbound(&PktList,&ArcList,cfg_PacketCreate,FALSE);

   if(exitprg || diskfull || nomem)
   {
      jbFreeList(&PktList,PktList.First,sizeof(struct FileToProcess));
      jbFreeList(&PktList2,PktList2.First,sizeof(struct FileToProcess));
      jbFreeList(&ArcList,ArcList.First,sizeof(struct FileToProcess));
      return;
   }

   PackOutbound(&PktList2,&ArcList,cfg_PacketDir,TRUE);

   if(exitprg || diskfull || nomem)
   {
      jbFreeList(&PktList,PktList.First,sizeof(struct FileToProcess));
      jbFreeList(&PktList2,PktList2.First,sizeof(struct FileToProcess));
      jbFreeList(&ArcList,ArcList.First,sizeof(struct FileToProcess));
      return;
   }

   LogWrite(3,ACTIONINFO,"Scanning outbound directory for lost bundles");

   for(ftp=ArcList.First;ftp && !exitprg;ftp=ftp->Next)
   {
      if(strncmp(ftp->Comment,"Add to",6)==0)
      {
         jbcpos=7;
         jbstrcpy(buf,ftp->Comment,100);

         dowhat="";

         if(jbstrcpy(buf2,ftp->Comment,30))
         {
            if(stricmp(buf2,"Truncate")==0)
               dowhat="#";

            if(stricmp(buf2,"Delete")==0)
               dowhat="-";
         }

         strcpy(newname,cfg_Outbound);
         AddPart(newname,buf,100);

         LogWrite(2,TOSSINGINFO,"Writing %s to %s",ftp->Name,buf);

         strcpy(buf2,cfg_PacketDir);
         AddPart(buf2,ftp->Name,200);

         if(AddFlowFile(newname,dowhat,buf2))
         {
            strcpy(arcname,cfg_PacketDir);
            AddPart(arcname,ftp->Name,100);
            SetComment(arcname,"");
         }
      }

      if(SetSignal(0L,0L)  & SIGBREAKF_CTRL_C)
         exitprg=TRUE;
   }

   jbFreeList(&PktList,PktList.First,sizeof(struct FileToProcess));
   jbFreeList(&PktList2,PktList2.First,sizeof(struct FileToProcess));
   jbFreeList(&ArcList,ArcList.First,sizeof(struct FileToProcess));
}

UpdateFile(UBYTE *file,struct jbList *list)
{
   BPTR l;
   struct FileInfoBlock *fib;
   struct FileToProcess *ftp;

   if(!(fib=(struct FileInfoBlock *)AllocMem(sizeof(struct FileInfoBlock),MEMF_ANY)))
   {
      return;
   }
   if(!(l=Lock(file,SHARED_LOCK)))
   {
      FreeMem(fib,sizeof(struct FileInfoBlock));
      return;
   }

   Examine(l,fib);

   for(ftp=list->First;ftp;ftp=ftp->Next)
      if(stricmp(ftp->Name,FilePart(file))==0) break;

   if(ftp)
   {
      ftp->Size=fib->fib_Size;
   }
   else
   {
      if(ftp=(struct FileToProcess *)AllocMem(sizeof(struct FileToProcess),MEMF_ANY))
      {
         mystrncpy(ftp->Name,fib->fib_FileName,40);
         strcpy(ftp->Comment,fib->fib_Comment);
         ftp->Size=fib->fib_Size;
         CopyMem(&fib->fib_Date,&ftp->Date,sizeof(struct DateStamp));
         jbAddNode(list,(struct jbNode *)ftp);
      }
   }

   FreeMem(fib,sizeof(struct FileInfoBlock));
   UnLock(l);
}

BOOL IsInUse(UBYTE *file)
{
   BPTR fh;

   if(!(fh=Lock(file,SHARED_LOCK)))
      return(FALSE); /* If it doesn't exist, it can't be in use */

   UnLock(fh);

   if(!(fh=Lock(file,EXCLUSIVE_LOCK)))
      return(TRUE); /* If can't be locked exclusively, it is in use */

   UnLock(fh);
   
   return(FALSE);
}


PackOutbound(struct jbList *PktList,struct jbList *ArcList,UBYTE *dir,BOOL isoutbound)
{
   struct Node4D dest;
   BPTR ifh,ofh,l;
   UBYTE buf[200],buf2[200],pktname[100],arcname[100],basename[100],newname[100],ch;
   UBYTE newpri;
   struct FileToProcess *ftp;
   struct ConfigNode *cnode;
   ULONG arcres,mode,type,len,err;

   for(ftp=PktList->First;ftp && !exitprg;ftp=ftp->Next)
   {
      strcpy(pktname,dir);
      AddPart(pktname,ftp->Name,100);

      if(strncmp(ftp->Comment,"Echomail",8)==0)
      {
         jbcpos=9;

         jbstrcpy(buf,ftp->Comment,100);
         Parse4D(buf,&dest);

         for(cnode=CNodeList.First;cnode;cnode=cnode->Next)
            if(Compare4D(&cnode->Node,&dest)==0) break;

         if(cnode)
         {
            UBYTE temparcname[200];

            newpri=ChangeType(&dest,PKTS_ECHOMAIL);

            sprintf(basename,"%lu.%lu.%lu.%lu",dest.Zone,dest.Net,dest.Node,dest.Point);

            strcpy(buf,basename);
            MakeExtension(buf,ArcList);

            strcpy(temparcname,cfg_PacketDir);
            AddPart(temparcname,"CM_Temp.",150);
            strcat(temparcname,buf);

            strcpy(arcname,cfg_PacketDir);
            AddPart(arcname,buf,100);

            strncpy(newname,pktname,strlen(pktname)-6);
            newname[strlen(pktname)-6]=0;
            strcat(newname,"pkt");

            Rename(pktname,newname);

            if(cnode->Packer)
            {
               BOOL packsuccess;

               ExpandPacker(cnode->Packer->Packer,buf,temparcname,newname);

               LogWrite(2,TOSSINGINFO,"Packing %s for %lu:%lu/%lu.%lu to %s with %s",newname,dest.Zone,dest.Net,dest.Node,dest.Point,FilePart(arcname),cnode->Packer->Name);

               packsuccess=FALSE;

               if(IsInUse(arcname))
               {
                  LogWrite(2,TOSSINGINFO,"Bundle %s is already in use by another application",FilePart(arcname));
               }
               else
               {
                  PutStr("\n");
                  Rename(arcname,temparcname);
                  arcres=SystemTags(buf,TAG_DONE);
                  Rename(temparcname,arcname);
                  PutStr("\n");

                  UpdateFile(arcname,ArcList);

                  SetSignal(0L,SIGBREAKF_CTRL_C);

                  if(arcres==0)
                  {
                     packsuccess=TRUE;

                     DeleteFile(newname);

                     if(newpri != PKTS_ECHOMAIL)
                     {
                        LogWrite(2,TOSSINGINFO,"Changed priority for bundle to %lu:%lu/%lu.%lu from Normal to %s",dest.Zone,dest.Net,dest.Node,dest.Point,
                                                                                                                  prinames[newpri]);

                        LogWrite(4,MISCINFO,"");
                     }

                     strcpy(buf,cfg_PacketDir);
                     AddPart(buf,arcname,200);
                     AddFlow(buf,"#",&dest,newpri,TRUE);
                  }
                  else
                  {
                     LogWrite(1,SYSTEMERR,"Packer failed: %lu",arcres);
                     PutStr("\n");
                  }
               }

               if(!packsuccess)
               {
                  if(!isoutbound)
                  {
                     UBYTE tmpname[200];

                     strcpy(tmpname,newname);
                     strcat(tmpname,"tmp");
                     Rename(newname,tmpname);

                     MoveFile(tmpname,cfg_PacketDir);
                     strcpy(buf,cfg_PacketDir);
                     AddPart(buf,FilePart(tmpname),100);
                     SetComment(buf,ftp->Comment);
                  }
                  else
                  {
                     Rename(newname,pktname);
                  }
               }
            }
            else
            {
               if(MoveFile(newname,cfg_PacketDir))
               {
                  strcpy(buf,cfg_PacketDir);
                  AddPart(buf,FilePart(newname),200);

                  if(newpri != PKTS_ECHOMAIL)
                  {
                     LogWrite(4,TOSSINGINFO,"Changed priority for bundle to %lu:%lu/%lu.%lu from Normal to %s",dest.Zone,dest.Net,dest.Node,dest.Point,
                                                                                               prinames[newpri]);
                     LogWrite(4,MISCINFO,"");
                  }

                  LogWrite(2,TOSSINGINFO,"Sending %s unpacked to %lu:%lu/%lu.%lu",FilePart(newname),dest.Zone,dest.Net,dest.Node,dest.Point);
                  AddFlow(buf,"-",&dest,newpri,TRUE);
               }
               else
               {
                  LogWrite(1,SYSTEMERR,"Couldn't move %s to %s",newname,cfg_PacketDir);
               }
            }
         }
         else
         {
            LogWrite(1,TOSSINGERR,"Unknown node %lu:%lu/%lu.%lu",dest.Zone,dest.Net,dest.Node,dest.Point);
         }
      }

      if(strncmp(ftp->Comment,"Netmail",7)==0)
      {
         BOOL addsuccess;

         addsuccess=FALSE;

         jbcpos=8;

         jbstrcpy(buf,ftp->Comment,100);

         if(stricmp(buf,"Normal")==0) type=PKTS_NORMAL;
         if(stricmp(buf,"Direct")==0) type=PKTS_DIRECT;
         if(stricmp(buf,"Hold")==0)   type=PKTS_HOLD;
         if(stricmp(buf,"Crash")==0)  type=PKTS_CRASH;

         jbstrcpy(buf,ftp->Comment,100);
         Parse4D(buf,&dest);

         newpri=ChangeType(&dest,type);

         if(newpri != type)
         {
            LogWrite(4,TOSSINGINFO,"Changed priority for packet to %lu:%lu/%lu.%lu from %s to %s",dest.Zone,dest.Net,dest.Node,dest.Point,
                                                                                      prinames[type],prinames[newpri]);
         }

         switch(newpri)
         {
            case PKTS_NORMAL: ch='o';
                              break;
            case PKTS_CRASH:  ch='c';
                              break;
            case PKTS_DIRECT: ch='d';
                              break;
            case PKTS_HOLD:   ch='h';
                              break;
         }

         sprintf(buf2,"%lu.%lu.%lu.%lu.%lcut",dest.Zone,dest.Net,dest.Node,dest.Point,ch);
         strcpy(buf,cfg_Outbound);
         AddPart(buf,buf2,100);

         LogWrite(2,TOSSINGINFO,"Adding %s to %s",pktname,buf2);

         mode=MODE_NEWFILE;

         if(l=Lock(buf,SHARED_LOCK))
         {
            UnLock(l);
            mode=MODE_READWRITE;
         }

         if(IsInUse(buf))
         {
            LogWrite(2,TOSSINGINFO,"Packet %s is already in use by another application",buf);
         }
         else
         {
            if(ofh=Open(buf,mode))
            {
               if(ifh=Open(pktname,MODE_OLDFILE))
               {
                  if(mode==MODE_READWRITE)
                  {
                     Seek(ofh,-2,OFFSET_END);
                     Seek(ifh,sizeof(struct PktHeader),OFFSET_BEGINNING);
                  }

                  while(len=Read(ifh,copybuf,COPYBUFSIZE))
                     Write(ofh,copybuf,len);

                  err=IoErr();
                  Close(ifh);

                  if(err==0)
                  {
                     DeleteFile(pktname);
                     addsuccess=TRUE;
                  }
               }
               else
               {
                  LogWrite(1,SYSTEMERR,"Unable to open packet %s",FilePart(pktname));
               }
               Close(ofh);
            }
            else
            {
               LogWrite(1,SYSTEMERR,"Unable to open packet %s",buf2);
            }
         }

         if(!addsuccess)
         {
            if(!isoutbound)
            {
               MoveFile(pktname,cfg_PacketDir);
               strcpy(buf,cfg_PacketDir);
               AddPart(buf,FilePart(pktname),100);
               SetComment(buf,ftp->Comment);

               strcpy(newname,buf);
               newname[strlen(newname)-6]=0;
               strcat(newname,"pkttmp");
               Rename(buf,newname);
            }
         }
      }

      if(SetSignal(0L,0L)  & SIGBREAKF_CTRL_C)
         exitprg=TRUE;
   }
}

UBYTE *daynames[]={"mo","tu","we","th","fr","sa","su"};

MakeExtension(UBYTE *arc,struct jbList *arclist)
{
   struct DateStamp ds;
   struct FileToProcess *ftp,*foundfile;
   UWORD day,thisday;
   UBYTE buf[100],ext[10];
   int lastnum,count;
   BOOL kg;

   DateStamp(&ds);
   day=(6+ds.ds_Days)%7;

   foundfile=NULL;

   for(ftp=arclist->First;ftp;ftp=ftp->Next)
   {
      strcpy(buf,ftp->Name);
      buf[strlen(buf)-4]=0;

      if(strncmp(arc,ftp->Name,strlen(arc))==0)
      {
         strncpy(ext,&ftp->Name[strlen(ftp->Name)-3],3);
         ext[2]=0;

         if(ftp->Size == 0)
         {
            strcpy(buf,cfg_PacketDir);
            AddPart(buf,ftp->Name,100);
            DeleteFile(buf);
         }

         if(!foundfile)
         {
            foundfile=ftp;
         }
         else
         {
             if(CompareDates(&ftp->Date,&foundfile->Date)<0)
               foundfile=ftp;
         }
      }
   }

   lastnum=-1;

   if(foundfile)
   {
      if(cfg_Flags & CFG_WEEKDAYNAMING)
      {
         strncpy(ext,&foundfile->Name[strlen(foundfile->Name)-3],3);
         ext[2]=0;

         if(stricmp(ext,daynames[day])==0)
         {
            strcpy(buf,cfg_PacketDir);
            AddPart(buf,foundfile->Name,100);
            UpdateFile(buf,arclist);

            if(foundfile->Size == 0)
            {
               lastnum=day*10+foundfile->Name[strlen(foundfile->Name)-1]-'0';
               lastnum++;
               if(lastnum==70) lastnum=0;
            }
            else if(cfg_MaxBundleSize == 0 || foundfile->Size < cfg_MaxBundleSize)
            {
               lastnum=day*10+foundfile->Name[strlen(foundfile->Name)-1]-'0';
               sprintf(ext,".%s%lu",daynames[lastnum/10],lastnum%10);
               strcat(arc,ext);
LogWrite(1,DEBUG,"*** Adding to %s, %lu bytes (weekday naming, correct day, below size limit",arc,foundfile->Size);
               return;
            }
         }
      }
      else
      {
         for(thisday=0;thisday<7;thisday++)
            if(stricmp(ext,daynames[thisday])==0) break;

         if(thisday==7)
            thisday=0;

         strcpy(buf,cfg_PacketDir);
         AddPart(buf,foundfile->Name,100);
         UpdateFile(buf,arclist);

         if(foundfile->Size == 0)
         {
            lastnum=thisday*10+foundfile->Name[strlen(foundfile->Name)-1]-'0';
            lastnum++;
            if(lastnum==70) lastnum=0;
         }
         else if(cfg_MaxBundleSize == 0 || foundfile->Size < cfg_MaxBundleSize)
         {
            lastnum=thisday*10+foundfile->Name[strlen(foundfile->Name)-1]-'0';
            sprintf(ext,".%s%lu",daynames[lastnum/10],lastnum%10);
            strcat(arc,ext);
LogWrite(1,DEBUG,"*** Adding to %s, %lu bytes (no weekday naming, correct day, below size limit",arc,foundfile->Size);
            return;
         }
      }
   }

   if(lastnum==-1)
   {
      if(cfg_Flags & CFG_WEEKDAYNAMING)
         lastnum=day*10;

      else
         lastnum=0;
   }

   count=0;
   kg=TRUE;

   while(kg)
   {
      if(lastnum==70)
         lastnum=0;

      sprintf(buf,"%s.%s%lu",arc,daynames[lastnum/10],lastnum%10);

      for(ftp=arclist->First;ftp;ftp=ftp->Next)
         if(stricmp(buf,ftp->Name)==0 && ftp->Size>0) break;

      if(ftp==NULL)
      {
         kg=FALSE;
      }
      else
      {
         lastnum++;
         count++;

         if(count==70)
            kg=FALSE;
      }
   }

   if(count==70)
   {
      strcpy(arc,foundfile->Name);
LogWrite(1,DEBUG,"*** No empty slots, adding to %s, %lu bytes",arc,foundfile->Size);
      return;
   }

   sprintf(ext,".%s%lu",daynames[lastnum/10],lastnum%10);
   strcat(arc,ext);

LogWrite(1,DEBUG,"*** Adding to empty slot %s",arc);
}

AddFlow(UBYTE *filename,UBYTE *dowhat,struct Node4D *node,UBYTE pri,BOOL inoutbound)
{
   UBYTE buf[100],buf2[100],letter;
   UBYTE *action;

   switch(pri)
   {
      case PKTS_NORMAL:
      case PKTS_ECHOMAIL: letter='F';
                          break;
      case PKTS_HOLD:     letter='H';
                          break;
      case PKTS_DIRECT:   letter='D';
                          break;
      case PKTS_CRASH:    letter='C';
                          break;
      default:            letter='F';
   }

   sprintf(buf2,"%lu.%lu.%lu.%lu.%lcLO",node->Zone,node->Net,node->Node,node->Point,letter);
   strcpy(buf,cfg_Outbound);
   AddPart(buf,buf2,100);

   if(AddFlowFile(buf,dowhat,filename))
      return;

   if(inoutbound==TRUE)
   {
      action="";

      if(dowhat[0]=='-')
         action="Delete";

      if(dowhat[0]=='#')
         action="Truncate";

      sprintf(buf2,"Add to %s %s",buf,action);
      SetComment(filename,buf2);
      return;
   }
}

AddFlowFile(UBYTE *file,UBYTE *dowhat,UBYTE *arc)
{
   BPTR fh;
   UBYTE buf[300];

   if(!(fh=Open(file,MODE_READWRITE)))
   {
      LogWrite(1,SYSTEMERR,"Unable to add file to %s",file);
      return(FALSE);
   }

   while(FGets(fh,buf,299))
   {
      strip(buf);

      if(buf[0]=='#') strcpy(buf,&buf[1]);
      if(buf[0]=='~') strcpy(buf,&buf[1]);
      if(buf[0]=='^') strcpy(buf,&buf[1]);
      if(buf[0]=='-') strcpy(buf,&buf[1]);

      if(stricmp(buf,arc)==0)
      {
         Close(fh);
         return(TRUE);
      }
   }

   Seek(fh,0,OFFSET_END);

   FPrintf(fh,"%s%s\n",(long)dowhat,(long)arc);
   Close(fh);
   return(TRUE);
}

ChangeType(struct Node4D *dest,UBYTE pri)
{
   struct Change *change;
   UBYTE newpri;
   BOOL ispattern;

   newpri=pri;

   for(change=ChangeList.First;change;change=change->Next)
   {
      if(Compare4DPat(&change->Pattern,dest)==0)
      {
         ispattern=FALSE;

         if(pri == PKTS_ECHOMAIL && change->ChangeNormal == TRUE) ispattern=TRUE;
         if(pri == PKTS_NORMAL   && change->ChangeNormal == TRUE) ispattern=TRUE;
         if(pri == PKTS_CRASH    && change->ChangeCrash  == TRUE) ispattern=TRUE;
         if(pri == PKTS_DIRECT   && change->ChangeDirect == TRUE) ispattern=TRUE;
         if(pri == PKTS_HOLD     && change->ChangeHold   == TRUE) ispattern=TRUE;

         if(ispattern)
         {
            if(pri == PKTS_ECHOMAIL && change->DestPri == PKTS_NORMAL)
               newpri=pri;

            else
               newpri=change->DestPri;
         }
      }
   }

   return(newpri);
}

