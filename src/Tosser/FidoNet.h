#ifndef EXEC_TYPES_H
#include "exec/types.h"
#endif

#define SWAP(x) ((x >> 8) + ((x & 255) * 256))

struct PktHeader
{
   UWORD OrigNode;
   UWORD DestNode;

   UWORD Year;
   UWORD Month;
   UWORD Day;
   UWORD Hour;
   UWORD Minute;
   UWORD Second;

   UWORD Baud;
   UWORD PktType;          /* 0x0200 */

   UWORD OrigNet;
   UWORD DestNet;
   UBYTE ProdCodeLow;
   UBYTE RevisionMajor;
   UBYTE Password[8];

   UWORD QOrigZone;
   UWORD QDestZone;

   UWORD AuxNet;                                 /* H�r b�rjar det nya! */
   UWORD CWValidCopy;
   UBYTE ProdCodeHigh;
   UBYTE RevisionMinor;
   UWORD CapabilWord;

   UWORD OrigZone;
   UWORD DestZone;
   UWORD OrigPoint;
   UWORD DestPoint;
   ULONG ProdData;
};

struct PktMsgHeader
{
   UWORD PktType; /* 0x0200 */

   UWORD OrigNode;
   UWORD DestNode;
   UWORD OrigNet;
   UWORD DestNet;
   UWORD Attr;
   UWORD Cost;
/*
   Nullterminated strings without fixed length:

   UBYTE DateTime[20];
   UBYTE To[36];
   UBYTE From[36];
   UBYTE Subj[72];
*/
};

struct MsgHeader
{
	UBYTE From[36];
	UBYTE To[36];
	UBYTE Subject[72];
 	UBYTE DateTime[20];

	UWORD TimesRead;
	UWORD DestNode;
	UWORD OrigNode;
	UWORD Cost;
	UWORD OrigNet;
	UWORD DestNet;
	UWORD DestZone;
	UWORD OrigZone;
	UWORD DestPoint;
	UWORD OrigPoint;
	UWORD ReplyTo;
	UWORD Attributes;
	UWORD NextReply;
};

#define PVT             1
#define CRASH           2
#define RECD            4
#define SENT            8
#define FILEATTACH     16
#define INTRANSIT      32
#define ORPHAN         64
#define KILLSENT      128
#define LOCAL         256
#define HOLD          512
/*      UNUSED       1024 */
#define FILEREQ      2048
#define RREQ         4096
#define IRRR         8192
#define AUDIT       16384
#define UPDATEREQ   32768

struct StoredMsg
{
   UBYTE From[36];
   UBYTE To[36];
   UBYTE Subject[72];
   UBYTE DateTime[20];
   UWORD TimesRead;
   UWORD DestNode;
   UWORD OrigNode;
   UWORD Cost;
   UWORD OrigNet;
   UWORD DestNet;
   UWORD DestZone;
   UWORD OrigZone;
   UWORD DestPoint;
   UWORD OrigPoint;
   UWORD ReplyTo;
   UWORD Attr;
   UWORD NextReply;
   /* Text */
};

/* FSC-0045 */

struct PktHeader45
{
   UWORD OrigNode;
   UWORD DestNode;

   UWORD OrigPoint;
   UWORD DestPoint;

   UBYTE Reserved[8];

   UWORD SubVersion;
   UWORD Version;

   UWORD OrigNet;
   UWORD DestNet;

   UBYTE ProdCode;
   UBYTE Revision;

   UBYTE Password[8];

   UWORD OrigZone;
   UWORD DestZone;

   UBYTE OrigDomain[8];
   UBYTE DestDomain[8];

   UBYTE ProductSpecific[4];
};


