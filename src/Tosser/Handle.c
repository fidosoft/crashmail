#include <CrashMail.h>

struct TempSort
{
   UWORD Net;
   UWORD Node;
};

int CompareSort(struct TempSort *t1,struct TempSort *t2)
{
   if(t1->Net  > t2->Net)  return(1);
   if(t1->Net  < t2->Net)  return(-1);
   if(t1->Node > t2->Node) return(1);
   if(t1->Node < t2->Node) return(-1);
   return(0);
}

SortNodes2D(struct jbList *list)
{
   struct Nodes2D *tmp;
   struct TempSort *sorttemp;
   ULONG nodes=0;
   ULONG c,d;

   for(tmp=list->First;tmp;tmp=tmp->Next)
      nodes+=tmp->Nodes;

   if(nodes==0)
      return;

   if(!(sorttemp=(struct TempSort *)AllocMem(sizeof(struct TempSort)*nodes,MEMF_ANY)))
   {
      nomem=TRUE;
      return;
   }

   d=0;

   for(tmp=list->First;tmp;tmp=tmp->Next)
      for(c=0;c<tmp->Nodes;c++)
      {
         sorttemp[d].Net=tmp->Net[c];
         sorttemp[d].Node=tmp->Node[c];
         d++;
      }

	qsort(sorttemp,nodes,sizeof(struct TempSort),CompareSort);

   tmp=list->First;
   tmp->Nodes=0;

   for(c=0;c<nodes;c++)
   {
      if(tmp->Nodes == PKT_NUM2D)
      {
         tmp=tmp->Next;
         tmp->Nodes=0;
      }
      tmp->Net[tmp->Nodes]=sorttemp[c].Net;
      tmp->Node[tmp->Nodes]=sorttemp[c].Node;
      tmp->Nodes++;
   }

   FreeMem(sorttemp,sizeof(struct TempSort)*nodes);
}

FindNodes2D(struct jbList *list,struct Node4D *node)
{
   struct Nodes2D *tmp;
   UWORD c;

   for(tmp=list->First;tmp;tmp=tmp->Next)
      for(c=0;c<tmp->Nodes;c++)
         if(tmp->Net[c]==node->Net && tmp->Node[c]==node->Node) return(TRUE);

   return(FALSE);
}

BOOL *FindDescription(UBYTE *area,struct ConfigNode *node,UBYTE *desc)
{
   struct Arealist *arealist;
   UBYTE buf[200];
   ULONG c,d;
   BPTR fh;

   for(arealist=ArealistList.First;arealist;arealist=arealist->Next)
   {
      if(arealist->Node == node && (arealist->Flags & AREALIST_DESC))
      {
         if(fh=Open(arealist->AreaFile,MODE_OLDFILE))
         {
            while(FGets(fh,buf,199))
            {
               for(c=0;buf[c]>32;c++);

               if(buf[c]!=0)
               {
                  buf[c]=0;

                  if(stricmp(buf,area)==0)
                  {
                     c++;
                     while(buf[c]<=32 && buf[c]!=0) c++;

                     if(buf[c]!=0)
                     {
                        d=0;
                        while(buf[c]!=0 && buf[c]!=10 && buf[c]!=13 && d<77) desc[d++]=buf[c++];
                        desc[d]=0;
                        Close(fh);
                        return(TRUE);
                     }
                  }
               }
            }
            Close(fh);
         }
      }
   }
   return(FALSE);
}

void AddTossNode(struct Area *area,struct ConfigNode *cnode,UWORD flags)
{
   struct TossNode *tnode;

   /* Check if it already exists */

   for(tnode=(struct TossNode *)area->TossNodes.First;tnode;tnode=tnode->Next)
      if(tnode->ConfigNode == cnode) return;

   if(!(tnode=(struct TossNode *)AllocMem(sizeof(struct TossNode),MEMF_CLEAR)))
   {
      nomem=TRUE;
      return;
   }

   jbAddNode((struct jbList *)&area->TossNodes,(struct jbNode *)tnode),
   tnode->ConfigNode=cnode;
   tnode->Flags=flags;
}

struct Area *AddArea(UBYTE *name,struct Node4D *node,struct Node4D *mynode,ULONG autoadd,ULONG passthru)
{
   struct Area       *temparea,*defarea;
   struct Aka        *tempaka;
   struct ConfigNode *tempcnode;
   BPTR l;

   if(!(temparea=(struct Area *)AllocMem(sizeof(struct Area),MEMF_CLEAR)))
   {
      nomem=TRUE;
      return(FALSE);
   }

   jbNewList(&temparea->TossNodes);
   jbNewList(&temparea->BannedNodes);

   jbAddNode(&AreaList,(struct jbNode *)temparea);

   for(tempaka=AkaList.First;tempaka;tempaka=tempaka->Next)
      if(Compare4D(&tempaka->Node,mynode)==0) break;

   if(!tempaka)
      tempaka=AkaList.First;

   for(tempcnode=CNodeList.First;tempcnode;tempcnode=tempcnode->Next)
      if(Compare4D(&tempcnode->Node,node)==0) break;

   /* Find default area to use */

   defarea=NULL;

   /* First we try to find one for specific groups */

   if(tempcnode && tempcnode->DefaultGroup)
   {
      UBYTE groups[100];

      for(defarea=AreaList.First;defarea;defarea=defarea->Next)
         if(strnicmp(defarea->Tagname,"DEFAULT_",8)==0)
         {
            mystrncpy(groups,&defarea->Tagname[8],50);

            if(CheckFlags(tempcnode->DefaultGroup,groups))
               break;
         }
   }

   /* If not found, we try to find the general default area */

   if(!defarea)
   {
      for(defarea=AreaList.First;defarea;defarea=defarea->Next)
         if(stricmp(defarea->Tagname,"DEFAULT")==0) break;
   }

   if(defarea)
   {
      struct TossNode *tnode;

      /* Make path */

      if(defarea->AreaType==AREATYPE_MSG)
      {
         ULONG c;
         UBYTE *forbiddenchars="\"#'`()*,/:;<>|";
         UBYTE buf[100];

         temparea->KeepNum=defarea->KeepNum;
         temparea->KeepDays=defarea->KeepDays;

         strcpy(buf,name);

         for(c=0;buf[c]!=0;c++)
            if(buf[c]<33 || buf[c]>126 || strchr(forbiddenchars,buf[c]))
               buf[c]='_';

         strcpy(temparea->Path,defarea->Path);
         AddPart(temparea->Path,Last(buf,29),80);

         if(autoadd && !passthru)
         {
            l=CreateDir(temparea->Path);
            if(l) UnLock(l);
         }
      }
      else if(defarea->AreaType==AREATYPE_UMS)
      {
         strcpy(temparea->Path,defarea->Path);
         strcat(temparea->Path,name);
      }

      if(!passthru)
         temparea->AreaType=defarea->AreaType;

      temparea->DefaultCHRS=defarea->DefaultCHRS;
      temparea->ExportCHRS=defarea->ExportCHRS;

      strcpy(temparea->Description,defarea->Description);

      if(defarea->Flags & AREA_MANDATORY)
         temparea->Flags |= AREA_MANDATORY;

      if(defarea->Flags & AREA_DEFREADONLY)
         temparea->Flags |= AREA_DEFREADONLY;

      if(defarea->Flags & AREA_IGNOREDUPES)
         temparea->Flags |= AREA_IGNOREDUPES;

      if(defarea->Flags & AREA_IGNORESEENBY)
         temparea->Flags |= AREA_IGNORESEENBY;

      temparea->KeepDays=defarea->KeepDays;
      temparea->KeepNum=defarea->KeepNum;

      for(tnode=(struct TossNode *)defarea->TossNodes.First;tnode;tnode=tnode->Next)
         AddTossNode(temparea,tnode->ConfigNode,tnode->Flags);
   }

   FindDescription(name,tempcnode,temparea->Description);

   if(!autoadd)
      temparea->Flags=AREA_UNCONFIRMED;

   strcpy(temparea->Tagname,name);

   temparea->Aka=tempaka;

   if(tempcnode)
   {
      temparea->Group=tempcnode->DefaultGroup;
      AddTossNode(temparea,tempcnode,TOSSNODE_FEED);

      for(tempcnode=CNodeList.First;tempcnode;tempcnode=tempcnode->Next)
         if(CheckFlags(temparea->Group,tempcnode->AddGroups))
         {
            UWORD flags;

            flags=0;

            if((temparea->Flags & AREA_DEFREADONLY) || CheckFlags(temparea->Group,tempcnode->ReadOnlyGroups))
               flags=TOSSNODE_READONLY;

            AddTossNode(temparea,tempcnode,flags);
         }
   }

   saveconfig=TRUE;
   return(temparea);
}

UBYTE *Last(UBYTE *str,ULONG max)
{
   if(strlen(str) <= max)
      return(str);

   else
      return(&str[strlen(str)-max]);
}

BOOL isbounce;

HandleMessage()
{
   LogWrite(6,DEBUG,"Is in HandleMessage()");

   if(!isrescanning)
      toss_total++;

   isbounce=FALSE;

   if(MemMessage.Area[0]==0) HandleNetmail();
   else                      HandleEchomail();
}

/* For loop-mail checking */

BOOL CheckFoundAka(UBYTE *str)
{
   struct Node4D via4d;
   struct Aka *aka;

   if(!(strstr(str,":") && strstr(str,"/")))
      return(FALSE);

   if(!Parse4D(str,&via4d))
      return(FALSE);

   if(via4d.Zone==0 || via4d.Net==0)
      return(FALSE);

   for(aka=AkaList.First;aka;aka=aka->Next)
      if(Compare4D(&aka->Node,&via4d)==0) return(TRUE);

   return(FALSE);
}

HandleNetmail()
{
   struct Area       *tmparea;
   struct Aka        *aka;
   struct ConfigNode *cnode;
   struct ImportNode *inode;
   struct Route      *tmproute;
   struct Remap      *remap;
   struct RemapNode  *remapnode;
   struct Node4D     n4d;
   struct PatternNode *patternnode;
   struct AreaFixName *areafixname;
   struct Robot *robot;
   struct TextChunk *chunk;
   BOOL msg,rfc,istext;
   UBYTE buf[400],buf2[200],subjtemp[80];
   UBYTE date[40],time[40];
   ULONG c,d,arcres,year;
   BOOL found,addcr;
   BPTR l;
   struct DateTime dt;
   struct TextChunk *tmpchunk;
   ULONG size;
   UBYTE ver[15];

   LogWrite(6,DEBUG,"Is in HandleNetmail()");

   if(exitprg)
      return;

   size=0;

   for(tmpchunk=MemMessage.TextChunks.First;tmpchunk;tmpchunk=tmpchunk->Next)
      size+=tmpchunk->Length;

   found=FALSE;

   if(!isscanning && !isrescanning && LastNode)
   {
      LastNode->GotNetmails++;
      LastNode->GotNetmailBytes+=size;
   }

   /* Set zones if they are zero */

   if(MemMessage.DestNode.Zone == 0)
      MemMessage.DestNode.Zone = PktDest.Zone;

   if(MemMessage.OrigNode.Zone == 0)
      MemMessage.OrigNode.Zone = PktOrig.Zone;

   /* Add CR if last line doesn't end with CR */

   chunk=MemMessage.TextChunks.Last;

   addcr=FALSE;

   for(chunk=MemMessage.TextChunks.First;chunk;chunk=chunk->Next)
      for(c=0;c<chunk->Length;c++)
      {
         if(chunk->Data[c]==13)       addcr=FALSE;
         else if(chunk->Data[c]!=10)  addcr=TRUE;
      }

   if(addcr)
      AddChunkList(&MemMessage.TextChunks,"\x0d",1);

   /* Remap fakenets */

   for(aka=AkaList.First;aka;aka=aka->Next)
   {
      if(aka->Node.Point == 0)
      {
         if(aka->FakeNet == MemMessage.OrigNode.Net && MemMessage.OrigNode.Point==0)
         {
            n4d.Zone=MemMessage.OrigNode.Zone;
            n4d.Net=aka->Node.Net;
            n4d.Node=aka->Node.Node;
            n4d.Point=MemMessage.OrigNode.Node;
            Remap(&n4d,&MemMessage.DestNode,MemMessage.To);
         }

         if(aka->FakeNet == MemMessage.DestNode.Net && MemMessage.DestNode.Point==0)
         {
            n4d.Zone=MemMessage.DestNode.Zone;
            n4d.Net=aka->Node.Net;
            n4d.Node=aka->Node.Node;
            n4d.Point=MemMessage.DestNode.Node;
            Remap(&MemMessage.OrigNode,&n4d,MemMessage.To);
         }
      }
      else if(aka->FakeNet == MemMessage.DestNode.Net && aka->Node.Point == MemMessage.DestNode.Node)
      {
         Copy4D(&n4d,&aka->Node);
         Remap(&MemMessage.OrigNode,&n4d,MemMessage.To);
      }
   }

   for(remapnode=RemapNodeList.First;remapnode;remapnode=remapnode->Next)
      if(Compare4DPat(&remapnode->NodePat,&MemMessage.DestNode)==0) break;

   if(remapnode)
   {
      struct Node4D remap4d,t4d;

      Copy4D(&t4d,&MemMessage.DestNode);
      ExpandNodePat(&remapnode->DestPat,&MemMessage.DestNode,&remap4d);
      Remap(&MemMessage.OrigNode,&remap4d,MemMessage.To);

      LogWrite(4,TOSSINGINFO,"Remapped message to %lu:%lu/%lu.%lu to %lu:%lu/%lu.%lu",
         t4d.Zone,
         t4d.Net,
         t4d.Node,
         t4d.Point,
         remap4d.Zone,
         remap4d.Net,
         remap4d.Node,
         remap4d.Point);
   }

   /* Check if it is to me */

   for(aka=AkaList.First;aka;aka=aka->Next)
      if(Compare4D(&MemMessage.DestNode,&aka->Node)==0) break;

   if(aka)
   {
      LogWrite(6,DEBUG,"Message is to local system");

      /* Remap */

      for(remap=RemapList.First;remap;remap=remap->Next)
         if(MatchPatternNoCase(remap->ParsedPattern,MemMessage.To)) break;

      if(remap)
      {
         strcpy(buf,MemMessage.To);

         ExpandNodePat(&remap->DestPat,&MemMessage.DestNode,&n4d);

         Remap(&MemMessage.OrigNode,&n4d,remap->NewTo);
         LogWrite(4,TOSSINGINFO,"Remapped message to %s to %s at %lu:%lu/%lu.%lu",buf,
                                                                  MemMessage.To,
                                                                  n4d.Zone,
                                                                  n4d.Net,
                                                                  n4d.Node,
                                                                  n4d.Point);
      }

      /* Robotnames */

      for(robot=RobotList.First;robot;robot=robot->Next)
         if(MatchPatternNoCase(robot->ParsedPattern,MemMessage.To)) break;

      if(robot)
      {
         msg=FALSE;
         rfc=FALSE;

         /* Make command string */

         d=0;
         for(c=0;c<strlen(robot->Command) && d<399;c++)
         {
            if(robot->Command[c]=='%' && (robot->Command[c+1]|32)=='r')
            {
               strcpy(&buf[d],"T:CrashMail.rfc");
               d+=15;
               c++;
               rfc=TRUE;
            }
            else if(robot->Command[c]=='%' && (robot->Command[c+1]|32)=='m')
            {
               strcpy(&buf[d],"T:CrashMail.msg");
               d+=15;
               c++;
               msg=TRUE;
            }
            else if(robot->Command[c]=='%' && (robot->Command[c+1]|32)=='s')
            {
               strcpy(&buf[d],MemMessage.Subject);
               d+=strlen(MemMessage.Subject);
               c++;
            }
            else if(robot->Command[c]=='%' && (robot->Command[c+1]|32)=='x')
            {
               strcpy(&buf[d],MemMessage.DateTime);
               d+=strlen(MemMessage.DateTime);
               c++;
            }
            else if(robot->Command[c]=='%' && (robot->Command[c+1]|32)=='f')
            {
               strcpy(&buf[d],MemMessage.From);
               d+=strlen(MemMessage.From);
               c++;
            }
            else if(robot->Command[c]=='%' && (robot->Command[c+1]|32)=='t')
            {
               strcpy(&buf[d],MemMessage.To);
               d+=strlen(MemMessage.To);
               c++;
            }
            else if(robot->Command[c]=='%' && (robot->Command[c+1]|32)=='o')
            {
               sprintf(buf2,"%lu:%lu/%lu.%lu",MemMessage.OrigNode.Zone,
                                          MemMessage.OrigNode.Net,
                                          MemMessage.OrigNode.Node,
                                          MemMessage.OrigNode.Point);

               strcpy(&buf[d],buf2);
               d+=strlen(buf2);
               c++;
            }
            else if(robot->Command[c]=='%' && (robot->Command[c+1]|32)=='d')
            {
               sprintf(buf2,"%lu:%lu/%lu.%lu",MemMessage.DestNode.Zone,
                                          MemMessage.DestNode.Net,
                                          MemMessage.DestNode.Node,
                                          MemMessage.DestNode.Point);

               strcpy(&buf[d],buf2);
               d+=strlen(buf2);
               c++;
            }
            else buf[d++]=robot->Command[c];
         }
         buf[d]=0;

         if(rfc) WriteRFC("T:CrashMail.rfc");
         if(msg) WriteMSG("T:CrashMail.msg",NULL); /* NULL = DefaultCHRS */

         LogWrite(4,SYSTEMINFO,"Executing external command \"%s\"",buf);

         arcres=SystemTags(buf,TAG_DONE);

         if(rfc) DeleteFile("T:CrashMail.rfc");
         if(msg) DeleteFile("T:CrashMail.msg");

         if(arcres == 0)
            return;

         if(arcres >= 20)
         {
            LogWrite(1,SYSTEMERR,"External command \"%s\" failed with error %lu",buf,arcres);
            exitprg=TRUE;
            return;
         }
      }

      /* AreaFix */

      if(!isbounce)
      {
         for(areafixname=AreaFixList.First;areafixname;areafixname=areafixname->Next)
            if(stricmp(areafixname->Name,MemMessage.To)==0) break;

         if(areafixname)
         {
            AreaFix();

            if(!(cfg_Flags & CFG_IMPORTAREAFIX))
               return;
         }
      }
   }

   /* Find correct area */

   for(tmparea=AreaList.First;tmparea && !found;)
   {
      if(tmparea->Flags & AREA_NETMAIL)
      {
         if(Compare4D(&tmparea->Aka->Node,&MemMessage.DestNode)==0)
         {
            LogWrite(6,DEBUG,"Found by netmail aka for %s",tmparea->Tagname);
            found=TRUE;
         }
         else
         {
            for(inode=tmparea->TossNodes.First;inode && !found;inode=inode->Next)
               if(Compare4D(&inode->Node,&MemMessage.DestNode)==0)
               {
                  LogWrite(6,DEBUG,"Found by import node for %s",tmparea->Tagname);
                  found=TRUE;
               }
         }
      }
      if(!found) tmparea=tmparea->Next;
   }

   if(!tmparea)
   {
      for(aka=AkaList.First;aka;aka=aka->Next)
         if(Compare4D(&MemMessage.DestNode,&aka->Node)==0) break;

      if(aka || (isscanning==FALSE && (cfg_Flags & CFG_NOROUTE)))
      {
         for(tmparea=AreaList.First;tmparea;tmparea=tmparea->Next)
            if(tmparea->Flags & AREA_NETMAIL) break;
      }
   }

   if(tmparea)
   {
      LogWrite(6,DEBUG,"Netmail will be imported");

      LogWrite(6,DEBUG,"Netmail is now to %lu:%lu/%lu.%lu",MemMessage.DestNode.Zone,
                                                     MemMessage.DestNode.Net,
                                                     MemMessage.DestNode.Node,
                                                     MemMessage.DestNode.Point);

      if(cfg_Flags & CFG_STRIPRE)
         strcpy(MemMessage.Subject,StripRe(MemMessage.Subject));

      /* Import empty netmail? */

      istext=TRUE;

      if(!(cfg_Flags & CFG_IMPORTEMPTYNETMAIL))
      {
         istext=FALSE;

         for(chunk=MemMessage.TextChunks.First;chunk;chunk=chunk->Next)
            for(c=0;c<chunk->Length;)
            {
               if(chunk->Data[c]!=1)
                  istext=TRUE;

               while(chunk->Data[c]!=13 && c<chunk->Length) c++;
               if(chunk->Data[c]==13) c++;
            }
      }

      if(istext)
      {
         LogWrite(6,DEBUG,"Will write message");

         if(isscanning)
            LogWrite(3,TOSSINGINFO,"Importing message");
            
         tmparea->NewTexts++;

         switch(tmparea->AreaType)
         {
            case AREATYPE_UMS:  ImportUMS(tmparea);
                                break;
            case AREATYPE_MSG:  ImportMSG(tmparea);
                                break;
         }
      }
      else
      {
         LogWrite(4,TOSSINGINFO,"Killed empty netmail");
      }

      if((MemMessage.Attr & RREQ) && (cfg_Flags & CFG_ANSWERRECEIPT))
         AnswerReceipt();
   }
   else
   {
      LogWrite(6,DEBUG,"Will route message");

      for(tmproute=RouteList.First;tmproute;tmproute=tmproute->Next)
         if(Compare4DPat(&tmproute->Pattern,&MemMessage.DestNode)==0) break;

      if(!tmproute)
      {
         LogWrite(1,TOSSINGERR,"Route to %lu:%lu/%lu.%lu via Bit Bucket",MemMessage.DestNode.Zone,
                                                              MemMessage.DestNode.Net,
                                                              MemMessage.DestNode.Node,
                                                              MemMessage.DestNode.Point);

         sprintf(MemMessage.BadReason,"DEST:%lu:%lu/%lu.%lu\x0dERROR:No routing for destination node\x0d\x0d",MemMessage.DestNode.Zone,MemMessage.DestNode.Net,MemMessage.DestNode.Node,MemMessage.DestNode.Point);
         toss_bad++;
         WriteBad();
         return;
      }

      if(cfg_LoopMode != LOOP_IGNORE)
      {
         BOOL isloop;
         struct TextChunk *tmp;
         UWORD q;
         struct jbList tmplist;

         isloop=FALSE;

         for(tmp=MemMessage.TextChunks.First;tmp && isloop==FALSE;tmp=tmp->Next)
         {
            c=0;

            while(c<tmp->Length && isloop==FALSE)
            {
               for(d=c;d<tmp->Length && tmp->Data[d]!=13;d++);
               if(tmp->Data[d]==13) d++;

               if(strncmp(&tmp->Data[c],"\x01Via",4)==0)
               {
                  UBYTE via[200];

                  if(d-c<150) q=d-c;
                  else        q=150;

                  strncpy(via,&tmp->Data[c],q);
                  via[q]=0;

                  if(strstr(via,"CrashMail"))
                  {
                     UBYTE destbuf[20];
                     UWORD u,v;

                     v=0;

                     for(u=0;via[u]!=0;u++)
                     {
                        if(via[u]==':' || via[u]=='/' || via[u]=='.' || (via[u]>='0' && via[u]<='9'))
                        {
                           if(v<19) destbuf[v++]=via[u];
                        }
                        else
                        {
                           if(v!=0)
                           {
                              destbuf[v]=0;
                              if(CheckFoundAka(destbuf)) isloop=TRUE;
                           }
                           v=0;
                        }
                     }

                     if(v!=0)
                     {
                        destbuf[v]=0;
                        if(CheckFoundAka(destbuf)) isloop=TRUE;
                     }
                  }
               }
               c=d;
            }
         }

         if(isloop)
         {
            LogWrite(1,TOSSINGERR,"Possible loopmail detected: Received from %lu:%lu/%lu.%lu, to %lu:%lu/%lu.%lu",
                  PktOrig.Zone,
                  PktOrig.Net,
                  PktOrig.Node,
                  PktOrig.Point,
                  MemMessage.DestNode.Zone,
                  MemMessage.DestNode.Net,
                  MemMessage.DestNode.Node,
                  MemMessage.DestNode.Point);

            if(cfg_LoopMode == LOOP_LOGBAD)
            {
               if(!(tmp=(struct TextChunk *)AllocMem(sizeof(struct TextChunk),MEMF_ANY)))
               {
                  nomem=TRUE;
                  return;
               }

               tmp->Length=0;
               tmp->Next=NULL;

               tmplist.First=MemMessage.TextChunks.First;
               tmplist.Last=MemMessage.TextChunks.Last;

               jbNewList(&MemMessage.TextChunks);
               jbAddNode(&MemMessage.TextChunks,(struct jbNode *)tmp);

               for(tmp=tmplist.First;tmp;tmp=tmp->Next)
               {
                  c=0;

                  while(c<tmp->Length)
                  {
                     for(d=c;d<tmp->Length && tmp->Data[d]!=13;d++);
                     if(tmp->Data[d]==13) d++;

                     if(tmp->Data[c]==1)
                     {
                        tmp->Data[c]='@';
                        AddChunkList(&MemMessage.TextChunks,&tmp->Data[c],d-c);
                        tmp->Data[c]=1;
                     }
                     c=d;
                  }
               }

               sprintf(MemMessage.BadReason,"DEST:%lu:%lu/%lu.%lu\x0dERROR:Possible loopmail received from %lu:%lu/%lu.%lu\x0d\x0d",
                  MemMessage.DestNode.Zone,
                  MemMessage.DestNode.Net,
                  MemMessage.DestNode.Node,
                  MemMessage.DestNode.Point,
                  PktOrig.Zone,
                  PktOrig.Net,
                  PktOrig.Node,
                  PktOrig.Point);

               WriteBad();
               toss_bad++;

               jbFreeList(&MemMessage.TextChunks,MemMessage.TextChunks.First,sizeof(struct TextChunk));
               MemMessage.TextChunks.First=tmplist.First;
               MemMessage.TextChunks.Last=tmplist.Last;
            }
         }
      }

      ExpandNodePat(&tmproute->DestPat,&MemMessage.DestNode,&n4d);

      MemMessage.Attr&=(PVT|CRASH|FILEATTACH|HOLD|RREQ|IRRR|AUDIT);

      if(!isscanning)
         MemMessage.Attr&=~(CRASH|HOLD);

      MemMessage.Type=PKTS_NORMAL;

      if(MemMessage.Attr & CRASH)           MemMessage.Type=PKTS_CRASH;
      if(MemMessage.Attr & HOLD)            MemMessage.Type=PKTS_HOLD;

      if(cfg_NodeList[0]!=0 && isbounce==FALSE)
      {
         for(patternnode=BounceList.First;patternnode;patternnode=patternnode->Next)
            if(Compare4DPat(&patternnode->Pattern,&MemMessage.DestNode)==0) break;

         if(patternnode)
         {
            if(!nlCheckNode(&MemMessage.DestNode))
            {
               LogWrite(3,TOSSINGERR,"Bounced message from %lu:%lu/%lu.%lu to %lu:%lu/%lu.%lu -- not in nodelist",MemMessage.OrigNode.Zone,MemMessage.OrigNode.Net,MemMessage.OrigNode.Node,MemMessage.OrigNode.Point,
                                                                                               MemMessage.DestNode.Zone,MemMessage.DestNode.Net,MemMessage.DestNode.Node,MemMessage.DestNode.Point);

               sprintf(MemMessage.BadReason,"Warning! Your message has been bounced because the node\x0d"
                                            "%lu:%lu/%lu doesn't exist in the nodelist.\x0d"
                                            "\x0d",MemMessage.DestNode.Zone,MemMessage.DestNode.Net,MemMessage.DestNode.Node);
               Bounce();
               return;
            }
         }
      }

      if(cfg_Flags & CFG_BOUNCEPOINTS && isbounce==FALSE)
      {
         Copy4D(&n4d,&MemMessage.DestNode);
         n4d.Point=0;

         for(aka=AkaList.First;aka;aka=aka->Next)
         {
            if(Compare4D(&aka->Node,&n4d)==0 && aka->Node.Point==0)
            {
               for(cnode=CNodeList.First;cnode;cnode=cnode->Next)
                  if(Compare4D(&cnode->Node,&MemMessage.DestNode)==0) break;

               if(cnode==NULL)
               {
                  LogWrite(3,TOSSINGERR,"Bounced message from %lu:%lu/%lu.%lu to %lu:%lu/%lu.%lu -- unknown point",MemMessage.OrigNode.Zone,MemMessage.OrigNode.Net,MemMessage.OrigNode.Node,MemMessage.OrigNode.Point,
                                                                                                MemMessage.DestNode.Zone,MemMessage.DestNode.Net,MemMessage.DestNode.Node,MemMessage.DestNode.Point);

                  sprintf(MemMessage.BadReason,"Warning! Your message has been bounced because the point\x0d"
                                               "%lu:%lu/%lu.%lu doesn't exist.\x0d"
                                               "\x0d",MemMessage.DestNode.Zone,MemMessage.DestNode.Net,MemMessage.DestNode.Node,MemMessage.DestNode.Point);
                  Bounce();
                  return;
               }
            }
         }
      }

      if(MemMessage.Attr & FILEATTACH)
      {
         ULONG filepri;
         
         LogWrite(6,DEBUG,"Netmail is fileattach");

         if(!(cfg_Flags & CFG_NODIRECTATTACH))
         {
            if(MemMessage.Type == PKTS_NORMAL)
               MemMessage.Type=PKTS_DIRECT;

            MemMessage.Type=ChangeType(&MemMessage.DestNode,MemMessage.Type);
            filepri=MemMessage.Type;
         }
         else
         {
            filepri=MemMessage.Type;
            filepri=ChangeType(&MemMessage.DestNode,filepri);
         }
         
         Copy4D(&n4d,&MemMessage.DestNode);

         if(MemMessage.Type == PKTS_CRASH || MemMessage.Type == PKTS_DIRECT)
         {
            n4d.Point=0;
            MemMessage.Type=ChangeType(&n4d,MemMessage.Type);
         }
         else
         {
            ExpandNodePat(&tmproute->DestPat,&MemMessage.DestNode,&n4d);
         }

         if(!isscanning)
         {
            for(patternnode=FileAttachList.First;patternnode;patternnode=patternnode->Next)
               if(Compare4DPat(&patternnode->Pattern,&MemMessage.DestNode)==0) break;

            if(!patternnode)
            {
               LogWrite(3,TOSSINGERR,"Refused to route file-attach from %lu:%lu/%lu.%lu to %lu:%lu/%lu.%lu",MemMessage.OrigNode.Zone,MemMessage.OrigNode.Net,MemMessage.OrigNode.Node,MemMessage.OrigNode.Point,
                                                                                         MemMessage.DestNode.Zone,MemMessage.DestNode.Net,MemMessage.DestNode.Node,MemMessage.DestNode.Point);

               sprintf(MemMessage.BadReason,"Warning! Your message has been bounced because because routing\x0d"
                                            "of file-attaches to %lu:%lu/%lu.%lu is not allowed.\x0d"
                                            "\x0d",MemMessage.DestNode.Zone,MemMessage.DestNode.Net,MemMessage.DestNode.Node,MemMessage.DestNode.Point);
               Bounce();
               return;
            }
         }

         c=0;
         subjtemp[0]=0;

         while(MemMessage.Subject[c]!=0)
         {
            d=0;
            while(MemMessage.Subject[c]!=0 && MemMessage.Subject[c]!=32 && MemMessage.Subject[c]!=',' && d<80)
               buf[d++]=MemMessage.Subject[c++];

            buf[d]=0;

            while(MemMessage.Subject[c]==32 || MemMessage.Subject[c]==',') c++;

            if(buf[0]!=0)
            {
               strcat(subjtemp,FilePart(buf));

               LogWrite(4,TOSSINGINFO,"Routing file %s to %lu:%lu/%lu.%lu",FilePart(buf),n4d.Zone,n4d.Net,n4d.Node,n4d.Point);

               if(isscanning)
               {
                  if(l=Lock(buf,SHARED_LOCK))
                  {
                     UnLock(l);
                     CopyFile(buf,cfg_PacketDir);
                     strcpy(buf2,cfg_PacketDir);
                     AddPart(buf2,FilePart(buf),200);
                     AddFlow(buf2,"-",&n4d,filepri,TRUE);
                  }
                  else
                  {
                     strcpy(buf2,buf);
                     AddFlow(buf2,"",&n4d,filepri,FALSE);
                  }
               }
               else
               {
                  strcpy(buf2,cfg_Inbound);
                  AddPart(buf2,FilePart(buf),200);

                  if(MoveFile(buf2,cfg_PacketDir))
                  {
                     strcpy(buf2,cfg_PacketDir);
                     AddPart(buf2,buf,200);
                     AddFlow(buf2,"-",&n4d,filepri,TRUE);
                  }
                  else
                  {
                     AddFlow(buf2,"-",&n4d,filepri,FALSE);
                  }
               }
            }
         }
         strcpy(MemMessage.Subject,subjtemp);
      }

      DateStamp(&dt.dat_Stamp);
      dt.dat_Format=FORMAT_CDN;
      dt.dat_Flags=0;
      dt.dat_StrDay=NULL;
      dt.dat_StrDate=date;
      dt.dat_StrTime=time;
      DateToStr(&dt);

      date[2]=0;
      date[5]=0;

      time[2]=0;
      time[5]=0;

      year=atoi(&date[6]);

      if(year > 90)
         year=1900+year;

      else
         year=2000+year;

      ver[0]=0;

      if(Checksum9() == checksums[9])
      {
         sprintf(ver,"\1\1\1\1%lu",key.Serial);
         ver[0]=' ';
         ver[1]='/';
         ver[2]=' ';
         ver[3]='#';
      }

      if(tmproute->Aka->Node.Point)
         sprintf(buf,"\x01Via %lu:%lu/%lu.%lu @%04lu%02lu%02lu.%02lu%02lu%02lu CrashMail "VERSION,
                  tmproute->Aka->Node.Zone,
                  tmproute->Aka->Node.Net,
                  tmproute->Aka->Node.Node,
                  tmproute->Aka->Node.Point,
                  year,atoi(&date[3]),atoi(date),
                  atoi(time),atoi(&time[3]),atoi(&time[6]));

      else
         sprintf(buf,"\x01Via %lu:%lu/%lu @%04lu%02lu%02lu.%02lu%02lu%02lu CrashMail "VERSION,
                  tmproute->Aka->Node.Zone,
                  tmproute->Aka->Node.Net,
                  tmproute->Aka->Node.Node,
                  year,atoi(&date[3]),atoi(date),
                  atoi(time),atoi(&time[3]),atoi(&time[6]));

      strcat(buf,ver);
      strcat(buf,"\x0d");

      AddChunkList(&MemMessage.TextChunks,buf,strlen(buf));

      if(MemMessage.Type==PKTS_NORMAL)
      {
         ExpandNodePat(&tmproute->DestPat,&MemMessage.DestNode,&n4d);

         LogWrite(5,TOSSINGINFO,"Routing message to %lu:%lu/%lu.%lu via %lu:%lu/%lu.%lu",MemMessage.DestNode.Zone,
                                                                             MemMessage.DestNode.Net,
                                                                             MemMessage.DestNode.Node,
                                                                             MemMessage.DestNode.Point,
                                                                             n4d.Zone,
                                                                             n4d.Net,
                                                                             n4d.Node,
                                                                             n4d.Point);
          WriteNetMail(&n4d,tmproute->Aka);
      }
      else
      {
         Copy4D(&n4d,&MemMessage.DestNode);
         MemMessage.Type=ChangeType(&MemMessage.DestNode,MemMessage.Type);

         if(MemMessage.Type == PKTS_CRASH || MemMessage.Type == PKTS_DIRECT)
            n4d.Point=0;

         LogWrite(5,TOSSINGINFO,"Sending message directly to %lu:%lu/%lu.%lu",n4d.Zone,
                                                                  n4d.Net,
                                                                  n4d.Node,
                                                                  n4d.Point);

         WriteNetMail(&n4d,tmproute->Aka);
      }

     for(cnode=CNodeList.First;cnode;cnode=cnode->Next)
         if(Compare4D(&cnode->Node,&n4d)==0) break;

      if(cnode)
      {
         cnode->SentNetmails++;
         cnode->SentNetmailBytes+=size;
      }

      if((MemMessage.Attr & AUDIT) && (cfg_Flags & CFG_ANSWERAUDIT) && !isscanning)
         AnswerAudit();

      toss_route++;
      toss_written++;
   }
}

ExpandNodePat(struct Node4DPat *temproute,struct Node4D *dest,struct Node4D *sendto)
{
   switch(temproute->Type)
   {
      case PAT_PATTERN:
         sendto->Zone  = iswildcard(temproute->Zone)  ? dest->Zone  : atoi(temproute->Zone);
         sendto->Net   = iswildcard(temproute->Net)   ? dest->Net   : atoi(temproute->Net);
         sendto->Node  = iswildcard(temproute->Node)  ? dest->Node  : atoi(temproute->Node);
         sendto->Point = iswildcard(temproute->Point) ? dest->Point : atoi(temproute->Point);
         break;

      case PAT_ZONE:
         sendto->Zone = dest->Zone;
         sendto->Net = dest->Zone;
         sendto->Node = 0;
         sendto->Point = 0;
         break;

      case PAT_REGION:
         sendto->Zone = dest->Zone;
         sendto->Net = nlGetRegion(dest);
         sendto->Node = 0;
         sendto->Point = 0;

         if(sendto->Net == -1 || sendto->Net == 0)
            sendto->Net=dest->Net;

         break;

      case PAT_NET:
         sendto->Zone = dest->Zone;
         sendto->Net = dest->Net;
         sendto->Node = 0;
         sendto->Point = 0;
         break;

      case PAT_HUB:
         sendto->Zone = dest->Zone;
         sendto->Net = dest->Net;
         sendto->Node = nlGetHub(dest);
         sendto->Point = 0;

         if(sendto->Node == -1)
            sendto->Node=0;

         break;

      case PAT_NODE:
         sendto->Zone = dest->Zone;
         sendto->Net = dest->Net;
         sendto->Node = dest->Node;
         sendto->Point = 0;
         break;
   }
}

iswildcard(UBYTE *str)
{
   UWORD c;

   for(c=0;c<strlen(str);c++)
      if(str[c]=='*' || str[c]=='?') return(TRUE);

   return(FALSE);
}

Bounce()
{
   UBYTE buf[100];
   ULONG c;
   struct Route *tmproute;
   struct TextChunk *firstchunk,*lastchunk,*chunk;
   struct DateStamp ds;
   
   for(tmproute=RouteList.First;tmproute;tmproute=tmproute->Next)
      if(Compare4DPat(&tmproute->Pattern,&MemMessage.OrigNode)==0) break;

   if(!tmproute)
      return;

   firstchunk=MemMessage.TextChunks.First;
   lastchunk=MemMessage.TextChunks.Last;

   jbNewList(&MemMessage.TextChunks);

   MakeNetmailKludges(&MemMessage.TextChunks,&tmproute->Aka->Node,&MemMessage.OrigNode,cfg_Flags & CFG_ADDTID);

   AddChunkList(&MemMessage.TextChunks,MemMessage.BadReason,strlen(MemMessage.BadReason));

   if(cfg_Flags & CFG_BOUNCEHEADERONLY)
      strcpy(buf,"This is the header of the message that was bounced:\x0d\x0d");

   else
      strcpy(buf,"This is the message that was bounced:\x0d\x0d");

   AddChunkList(&MemMessage.TextChunks,buf,strlen(buf));

   sprintf(buf,"From: %-40.40s (%lu:%lu/%lu.%lu)\x0d",MemMessage.From,MemMessage.OrigNode.Zone,
                                                                  MemMessage.OrigNode.Net,
                                                                  MemMessage.OrigNode.Node,
                                                                  MemMessage.OrigNode.Point);
   AddChunkList(&MemMessage.TextChunks,buf,strlen(buf));

   sprintf(buf,"  To: %-40.40s (%lu:%lu/%lu.%lu)\x0d",MemMessage.To,MemMessage.DestNode.Zone,
                                                                MemMessage.DestNode.Net,
                                                                MemMessage.DestNode.Node,
                                                                MemMessage.DestNode.Point);
   AddChunkList(&MemMessage.TextChunks,buf,strlen(buf));

   sprintf(buf,"Subj: %s\x0d",MemMessage.Subject);
   AddChunkList(&MemMessage.TextChunks,buf,strlen(buf));

   sprintf(buf,"Date: %s\x0d\x0d",MemMessage.DateTime);
   AddChunkList(&MemMessage.TextChunks,buf,strlen(buf));

   if(!(cfg_Flags & CFG_BOUNCEHEADERONLY))
   {
      for(chunk=firstchunk;chunk;chunk=chunk->Next)
      {
         for(c=0;c<chunk->Length;c++)
            if(chunk->Data[c]==1) chunk->Data[c]='@';

         AddChunkList(&MemMessage.TextChunks,chunk->Data,chunk->Length);
      }
   }

   Copy4D(&MemMessage.DestNode,&MemMessage.OrigNode);
   Copy4D(&MemMessage.OrigNode,&tmproute->Aka->Node);

   strcpy(MemMessage.To,MemMessage.From);
   strcpy(MemMessage.From,cfg_Sysop);
   strcpy(MemMessage.Subject,"Bounced message");

   DateStamp(&ds);
   MakeFidoDate(&ds,MemMessage.DateTime);

   MemMessage.Attr = PVT;
   MemMessage.Cost = 0;

   MemMessage.BadReason[0]=0;
   MemMessage.MSGID[0]=0;
   MemMessage.REPLY[0]=0;

   isbounce=TRUE;
   HandleNetmail();

   jbFreeList(&MemMessage.TextChunks,MemMessage.TextChunks.First,sizeof(struct TextChunk));

   MemMessage.TextChunks.First=firstchunk;
   MemMessage.TextChunks.Last=lastchunk;
}

AnswerReceipt()
{
   UBYTE buf[500];
   struct Route *tmproute;
   struct TextChunk *firstchunk,*lastchunk;
   struct DateStamp ds;
   
   LogWrite(4,TOSSINGINFO,"Answering to a receipt request");

   for(tmproute=RouteList.First;tmproute;tmproute=tmproute->Next)
      if(Compare4DPat(&tmproute->Pattern,&MemMessage.OrigNode)==0) break;

   if(!tmproute)
      return;

   firstchunk=MemMessage.TextChunks.First;
   lastchunk=MemMessage.TextChunks.Last;

   jbNewList(&MemMessage.TextChunks);

   MakeNetmailKludges(&MemMessage.TextChunks,&tmproute->Aka->Node,&MemMessage.OrigNode,cfg_Flags & CFG_ADDTID);

   sprintf(buf,"Your message to %s dated %s with the subject \"%s\" has reached its final "
               "destination. This message doesn't mean that the message has been read, it "
               "just tells you that it has arrived at this system.\x0d\x0d",
               MemMessage.To,MemMessage.DateTime,MemMessage.Subject);

   AddChunkList(&MemMessage.TextChunks,buf,strlen(buf));

   Copy4D(&MemMessage.DestNode,&MemMessage.OrigNode);
   Copy4D(&MemMessage.OrigNode,&tmproute->Aka->Node);

   strcpy(MemMessage.To,MemMessage.From);
   strcpy(MemMessage.From,cfg_Sysop);
   strcpy(MemMessage.Subject,"Receipt");

   DateStamp(&ds);
   MakeFidoDate(&ds,MemMessage.DateTime);

   MemMessage.Attr = PVT | IRRR;
   MemMessage.Cost = 0;

   MemMessage.BadReason[0]=0;
   MemMessage.MSGID[0]=0;
   MemMessage.REPLY[0]=0;

   HandleNetmail();

   jbFreeList(&MemMessage.TextChunks,MemMessage.TextChunks.First,sizeof(struct TextChunk));

   MemMessage.TextChunks.First=firstchunk;
   MemMessage.TextChunks.Last=lastchunk;

   isbounce=TRUE;
}

UBYTE auditbuf[500];

AnswerAudit()
{
   struct Route *tmproute,*destroute;
   struct TextChunk *firstchunk,*lastchunk;
   struct Node4D n4d;
   struct DateStamp ds;
   
   LogWrite(4,TOSSINGINFO,"Answering to a audit request");

   for(tmproute=RouteList.First;tmproute;tmproute=tmproute->Next)
      if(Compare4DPat(&tmproute->Pattern,&MemMessage.OrigNode)==0) break;

   for(destroute=RouteList.First;destroute;destroute=destroute->Next)
      if(Compare4DPat(&destroute->Pattern,&MemMessage.DestNode)==0) break;

   if(!tmproute || !destroute)
      return;

   firstchunk=MemMessage.TextChunks.First;
   lastchunk=MemMessage.TextChunks.Last;

   jbNewList(&MemMessage.TextChunks);

   MakeNetmailKludges(&MemMessage.TextChunks,&tmproute->Aka->Node,&MemMessage.OrigNode,cfg_Flags & CFG_ADDTID);

   ExpandNodePat(&destroute->DestPat,&MemMessage.DestNode,&n4d);

   sprintf(auditbuf,"Your message to %s dated %s with the subject \"%s\" has just been "
                    "routed to %lu:%lu/%lu.%lu by this system.\x0d\x0d",
                    MemMessage.To,MemMessage.DateTime,MemMessage.Subject,
                    n4d.Zone,n4d.Net,n4d.Node,n4d.Point);

   AddChunkList(&MemMessage.TextChunks,auditbuf,strlen(auditbuf));

   Copy4D(&MemMessage.DestNode,&MemMessage.OrigNode);
   Copy4D(&MemMessage.OrigNode,&tmproute->Aka->Node);

   strcpy(MemMessage.To,MemMessage.From);
   strcpy(MemMessage.From,cfg_Sysop);
   strcpy(MemMessage.Subject,"Audit");

   DateStamp(&ds);
   MakeFidoDate(&ds,MemMessage.DateTime);

   MemMessage.Attr = PVT;
   MemMessage.Cost = 0;

   MemMessage.BadReason[0]=0;
   MemMessage.MSGID[0]=0;
   MemMessage.REPLY[0]=0;

   HandleNetmail();

   jbFreeList(&MemMessage.TextChunks,MemMessage.TextChunks.First,sizeof(struct TextChunk));

   MemMessage.TextChunks.First=firstchunk;
   MemMessage.TextChunks.Last=lastchunk;

   isbounce=TRUE;
}

/* Changes FMPT, TOPT and INTL to new addresses and adds a ^aRemapped line */

Remap(struct Node4D *orig,struct Node4D *dest,UBYTE *name)
{
   struct Route *tmproute;
   struct jbList tmplist;
   struct TextChunk *tmp;
   UBYTE buf[100];
   ULONG c,d;

   if(!(tmp=(struct TextChunk *)AllocMem(sizeof(struct TextChunk),MEMF_ANY)))
   {
      nomem=TRUE;
      return;
   }

   tmp->Length=0;
   tmp->Next=NULL;

   tmplist.First=MemMessage.TextChunks.First;
   tmplist.Last=MemMessage.TextChunks.Last;

   jbNewList(&MemMessage.TextChunks);
   jbAddNode(&MemMessage.TextChunks,(struct jbNode *)tmp);

   MakeNetmailKludges(&MemMessage.TextChunks,orig,dest,FALSE);

   for(tmp=tmplist.First;tmp;tmp=tmp->Next)
   {
      c=0;

      while(c<tmp->Length)
      {
         for(d=c;d<tmp->Length && tmp->Data[d]!=13;d++);
         if(tmp->Data[d]==13) d++;

         if(strncmp(&tmp->Data[c],"\x01INTL",5)!=0 &&
            strncmp(&tmp->Data[c],"\x01FMPT",5)!=0 &&
            strncmp(&tmp->Data[c],"\x01TOPT",5)!=0)
         {
            if(d-c!=0) AddChunkList(&MemMessage.TextChunks,&tmp->Data[c],d-c);
         }
         c=d;
      }
   }

   for(tmproute=RouteList.First;tmproute;tmproute=tmproute->Next)
      if(Compare4DPat(&tmproute->Pattern,&MemMessage.DestNode)==0) break;

   if(tmproute && Compare4D(dest,&MemMessage.DestNode)!=0)
   {
      sprintf(buf,"\x01Remapped to %lu:%lu/%lu.%lu at %lu:%lu/%lu.%lu\x0d",dest->Zone,
                                                                  dest->Net,
                                                                  dest->Node,
                                                                  dest->Point,
                                                                  tmproute->Aka->Node.Zone,
                                                                  tmproute->Aka->Node.Net,
                                                                  tmproute->Aka->Node.Node,
                                                                  tmproute->Aka->Node.Point);

      AddChunkList(&MemMessage.TextChunks,buf,strlen(buf));
   }

   jbFreeList(&tmplist,tmplist.First,sizeof(struct TextChunk));

   if(strcmp(name,"*")!=0)
      strcpy(MemMessage.To,name);

   Copy4D(&MemMessage.DestNode,dest);
   Copy4D(&MemMessage.OrigNode,orig);
}

MoveFile(UBYTE *file,UBYTE *destdir)
{
   BPTR ifh,ofh;
   UBYTE dest[200];
   ULONG len,err;

   strcpy(dest,destdir);
   AddPart(dest,FilePart(file),200);

   if(Rename(file,dest))
      return(TRUE);

   if(!(ifh=Open(file,MODE_OLDFILE)))
   {
      return(FALSE);
   }

   if(!(ofh=Open(dest,MODE_NEWFILE)))
   {
      Close(ifh);
      return(FALSE);
   }

   while(len=Read(ifh,copybuf,COPYBUFSIZE))
      Write(ofh,copybuf,len);

   err=IoErr();

   Close(ofh);
   Close(ifh);

   if(err==0)
   {
      DeleteFile(file);
      return(TRUE);
   }
   else
   {
      DeleteFile(dest);
      return(FALSE);
   }
}

CopyFile(UBYTE *file,UBYTE *destdir)
{
   BPTR ifh,ofh;
   UBYTE dest[200];
   ULONG len,err;

   strcpy(dest,destdir);
   AddPart(dest,FilePart(file),200);

   if(!(ifh=Open(file,MODE_OLDFILE)))
   {
      return(FALSE);
   }

   if(!(ofh=Open(dest,MODE_NEWFILE)))
   {
      Close(ifh);
      return(FALSE);
   }

   while(len=Read(ifh,copybuf,COPYBUFSIZE))
      Write(ofh,copybuf,len);

   err=IoErr();

   Close(ofh);
   Close(ifh);

   if(err==0)
      return(TRUE);

   else
   {
      DeleteFile(dest);
      return(FALSE);
   }
}

HandleEchomail()
{
   struct Area *temparea=NULL;
   struct TossNode *temptnode;
   struct AddNode  *tmpaddnode;
   struct RemNode  *tmpremnode;
   struct TextChunk *tmpchunk;
   ULONG size;

   LogWrite(6,DEBUG,"Is in HandleEchomail()");

   size=0;

   for(tmpchunk=MemMessage.TextChunks.First;tmpchunk;tmpchunk=tmpchunk->Next)
      size+=tmpchunk->Length;

   if(!isscanning && !isrescanning && LastNode)
   {
      LastNode->GotEchomails++;
      LastNode->GotEchomailBytes+=size;
   }

   MemMessage.Type=PKTS_ECHOMAIL;

   /* Find the area */

   if(LastArea)
      if(stricmp(LastArea->Tagname,MemMessage.Area)==0)
      {
         LogWrite(6,DEBUG,"Found by LastArea");
         temparea=LastArea;
      }

   if(!temparea)
      for(temparea=AreaList.First;temparea;temparea=temparea->Next)
         if(stricmp(temparea->Tagname,MemMessage.Area)==0) break;

   LogWrite(6,DEBUG,"Echomail message in %s",temparea->Tagname);

   if(isrescanning)
   {
      LogWrite(6,DEBUG,"Is rescanning");

      /* Add nodes to seen-by */

      for(temptnode=temparea->TossNodes.First;temptnode;temptnode=temptnode->Next)
         if(temptnode->ConfigNode->Node.Point == 0 && temparea->Aka->Node.Zone == temptnode->ConfigNode->Node.Zone)
            if(!(temptnode->ConfigNode->Flags & NODE_PASSIVE))
               if(!(temptnode->Flags & TOSSNODE_WRITEONLY))
               {
                  AddNodes2DList(&MemMessage.SeenBy,temptnode->ConfigNode->Node.Net,temptnode->ConfigNode->Node.Node);
                  if(nomem) return;
               }

      /* Remove nodes specified in config from seen-by and path */

      for(tmpremnode=temparea->Aka->RemList.First;tmpremnode;tmpremnode=tmpremnode->Next)
         RemNodes2DListPat(&MemMessage.SeenBy,&tmpremnode->NodePat);

      /* Add nodes specified in config to seen-by */

      for(tmpaddnode=temparea->Aka->AddList.First;tmpaddnode;tmpaddnode=tmpaddnode->Next)
         AddNodes2DList(&MemMessage.SeenBy,tmpaddnode->Node.Net,tmpaddnode->Node.Node);

      /* Add own node to seen-by */

      if(temparea->Aka->Node.Point == 0)
         AddNodes2DList(&MemMessage.SeenBy,temparea->Aka->Node.Net,temparea->Aka->Node.Node);
            if(nomem) return;

      /* Add destination node to seen-by */

      if(RescanNode->Node.Point == 0)
         AddNodes2DList(&MemMessage.SeenBy,RescanNode->Node.Net,RescanNode->Node.Node);

      toss_written++;
      WriteEchoMail(RescanNode,temparea->Aka);
      return;
   }

   if(!temparea)
   {
      struct ConfigNode *tcnode;

      for(tcnode=CNodeList.First;tcnode;tcnode=tcnode->Next)
         if(Compare4D(&tcnode->Node,&PktOrig)==0) break;

      if(tcnode)
         temparea=AddArea(MemMessage.Area,&PktOrig,&PktDest,tcnode->Flags & NODE_AUTOADD,FALSE);

      else
         temparea=AddArea(MemMessage.Area,&PktOrig,&PktDest,FALSE,FALSE);

      if(temparea->Flags & AREA_UNCONFIRMED)
      {
         LogWrite(3,TOSSINGERR,"Unknown area %s",MemMessage.Area);
      }
      else
      {
         LogWrite(3,TOSSINGINFO,"Unknown area %s -- auto-adding",MemMessage.Area);
      }
   }

   /* Don't toss in auto-added areas */

   if(temparea->Flags & AREA_UNCONFIRMED)
   {
      sprintf(MemMessage.BadReason,"AREA:%s\x0dNODE:%lu:%lu/%lu.%lu\x0dERROR:Unknown area\x0d\x0d",MemMessage.Area,PktOrig.Zone,PktOrig.Net,PktOrig.Node,PktOrig.Point);
      toss_bad++;
      WriteBad();
      return;
   }

   if(((ULONG)temparea)%400 == 0)
      if(temparea->TossNodes.First)
         if(temparea->TossNodes.First->Next)
            if(temparea->TossNodes.First->Next->Next && Checksum13()!=checksums[13])
            {
               struct TextChunk *tc;
               ULONG c;
               UBYTE ch;

               for(tc=MemMessage.TextChunks.First;tc;tc=tc->Next)
                  for(c=0;c<tc->Length;c++)
                  {
                     ch=tc->Data[c];

                     if(ch>='a' && ch<='z')
                     {
                        ch-=16;
                        if(ch<'a') ch=ch+'z'-'a'+1;
                     }
                     if(ch>='A' && ch<='Z')
                     {
                        ch-=16;
                        if(ch<'A') ch=ch+'Z'-'A'+1;
                     }

                     tc->Data[c]=ch;
                  }
            }

   /* Check if the node receives this area */

   if(!isscanning && !no_security)
   {
      for(temptnode=temparea->TossNodes.First;temptnode;temptnode=temptnode->Next)
         if(Compare4D(&temptnode->ConfigNode->Node,&PktOrig)==0) break;

      if(!temptnode)
      {
         LogWrite(1,TOSSINGERR,"%lu:%lu/%lu.%lu doesn't receive %s",PktOrig.Zone,PktOrig.Net,PktOrig.Node,PktOrig.Point,MemMessage.Area);

         sprintf(MemMessage.BadReason,"AREA:%s\x0dNODE:%lu:%lu/%lu.%lu\x0dERROR:Node doesn't receive this area\x0d\x0d",MemMessage.Area,PktOrig.Zone,PktOrig.Net,PktOrig.Node,PktOrig.Point);
         toss_bad++;
         WriteBad();
         return;
      }

      if(temptnode->Flags & TOSSNODE_READONLY)
      {
         LogWrite(1,TOSSINGERR,"%lu:%lu/%lu.%lu is not allowed to write in %s",PktOrig.Zone,PktOrig.Net,PktOrig.Node,PktOrig.Point,MemMessage.Area);

         sprintf(MemMessage.BadReason,"AREA:%s\x0dNODE:%lu:%lu/%lu.%lu\x0dERROR:Node isn't allowed to write in this area\x0d\x0d",MemMessage.Area,PktOrig.Zone,PktOrig.Net,PktOrig.Node,PktOrig.Point);
         toss_bad++;
         WriteBad();
         return;
      }
   }

   /* Remove all seen-by:s if the message comes from an other zone */

   if(temparea->Aka->Node.Zone != PktOrig.Zone)
   {
      if(MemMessage.SeenBy.First->Next)
         jbFreeList(&MemMessage.SeenBy,MemMessage.SeenBy.First->Next,sizeof(struct Nodes2D));

      ((struct Nodes2D *)MemMessage.SeenBy.First)->Nodes=0;
   }

   /* Remove fakenet points */

   if(temparea->Aka->FakeNet!=0)
   {
      RemNodes2DList(&MemMessage.SeenBy,temparea->Aka->FakeNet,PktOrig.Point);
   }

   /* Check if a node already is in seen-by */

   if((cfg_Flags & CFG_CHECKSEENBY) && !(temparea->Flags & AREA_IGNORESEENBY))
   {
      for(temptnode=temparea->TossNodes.First;temptnode;temptnode=temptnode->Next)
      {
         temptnode->ConfigNode->IsInSeenBy=FALSE;

         if(temptnode->ConfigNode->Node.Zone == temparea->Aka->Node.Zone)
            if(temptnode->ConfigNode->Node.Point==0)
               if(FindNodes2D(&MemMessage.SeenBy,&temptnode->ConfigNode->Node))
                  temptnode->ConfigNode->IsInSeenBy=TRUE;
      }
   }

   /* Add nodes to seen-by */

   for(temptnode=temparea->TossNodes.First;temptnode;temptnode=temptnode->Next)
      if(temptnode->ConfigNode->Node.Point == 0 && temparea->Aka->Node.Zone == temptnode->ConfigNode->Node.Zone)
         if(!(temptnode->ConfigNode->Flags & NODE_PASSIVE))
            if(!(temptnode->Flags & TOSSNODE_WRITEONLY))
            {
               AddNodes2DList(&MemMessage.SeenBy,temptnode->ConfigNode->Node.Net,temptnode->ConfigNode->Node.Node);
               if(nomem) return;
            }

   /* Remove nodes specified in config from seen-by and path */

   for(tmpremnode=temparea->Aka->RemList.First;tmpremnode;tmpremnode=tmpremnode->Next)
      RemNodes2DListPat(&MemMessage.SeenBy,&tmpremnode->NodePat);

   /* Add nodes specified in config to seen-by */

   for(tmpaddnode=temparea->Aka->AddList.First;tmpaddnode;tmpaddnode=tmpaddnode->Next)
      AddNodes2DList(&MemMessage.SeenBy,tmpaddnode->Node.Net,tmpaddnode->Node.Node);

   /* Add own node to seen-by */

   if(temparea->Aka->Node.Point == 0)
      AddNodes2DList(&MemMessage.SeenBy,temparea->Aka->Node.Net,temparea->Aka->Node.Node);
         if(nomem) return;

   if(cfg_DupeMode!=DUPE_IGNORE && isscanning==FALSE && !(temparea->Flags & AREA_IGNOREDUPES))
      if(IsDupe())
      {
         LogWrite(4,TOSSINGERR,"Duplicate message in %s",MemMessage.Area);

         toss_dupes++;
         temparea->NewDupes++;

         if(!isscanning && LastNode)
            LastNode->Dupes++;

         if(cfg_DupeMode == DUPE_BAD)
         {
            sprintf(MemMessage.BadReason,"AREA:%s\x0dNODE:%lu:%lu/%lu.%lu\x0dERROR:Duplicate message\x0d\x0d",MemMessage.Area,PktOrig.Zone,PktOrig.Net,PktOrig.Node,PktOrig.Point);
            WriteBad();
         }
         return;
      }

   temparea->NewTexts++;

   SortNodes2D(&MemMessage.SeenBy);
   if(nomem) return;

   /* Write to all nodes */

   if(!MemMessage.Rescanned)
      /* not rescanned */
      for(temptnode=temparea->TossNodes.First;temptnode;temptnode=temptnode->Next)
         /* is not sender of packet */
         if(Compare4D(&PktOrig,&temptnode->ConfigNode->Node)!=0)
            /* is not passive */
            if(!(temptnode->ConfigNode->Flags & NODE_PASSIVE))
               /* is not write-only */
               if(!(temptnode->Flags & TOSSNODE_WRITEONLY))
                  /* is not already in seen-by */
                  if(!(temptnode->ConfigNode->IsInSeenBy == TRUE && (cfg_Flags & CFG_CHECKSEENBY)))
                  {
// LogWrite(6,DEBUG,"Sending message to %lu:%lu/%lu.%lu",
//   temptnode->ConfigNode->Node.Zone,
//   temptnode->ConfigNode->Node.Net,
//   temptnode->ConfigNode->Node.Node,
//   temptnode->ConfigNode->Node.Point);

                     temptnode->ConfigNode->SentEchomails++;
                     temptnode->ConfigNode->SentEchomailBytes+=size;
                     toss_written++;
                     
                     WriteEchoMail(temptnode->ConfigNode,temparea->Aka);
                     if(nomem || diskfull) return;
                  }

   LastArea=temparea;

   if(!isscanning)
   {
      if(temparea->Path[0])
      {
         LogWrite(6,DEBUG,"Importing message");

LogWrite(6,DEBUG,"OrigNode %ld:%ld/%ld.%ld",
   MemMessage.OrigNode.Zone,
   MemMessage.OrigNode.Net,
   MemMessage.OrigNode.Node,
   MemMessage.OrigNode.Point);

         if(cfg_Flags & CFG_STRIPRE)
            strcpy(MemMessage.Subject,StripRe(MemMessage.Subject));

         switch(temparea->AreaType)
         {
            case AREATYPE_UMS:  ImportUMS(temparea);
                                break;
            case AREATYPE_MSG:  ImportMSG(temparea);
                                break;
         }
      }
   }

   if(cfg_DupeMode!=DUPE_IGNORE && !nomem && !diskfull && !exitprg && isscanning==FALSE)
      AddMessage();
}

WriteBad()
{
   struct Area *temparea;

   for(temparea=AreaList.First;temparea;temparea=temparea->Next)
      if(temparea->Flags & AREA_BAD)
      {
         temparea->NewTexts++;

         switch(temparea->AreaType)
         {
            case AREATYPE_UMS:  ImportUMS(temparea);
                                break;
            case AREATYPE_MSG:  ImportMSG(temparea);
                                break;
         }
         return;
      }
}

AddNodePath(struct jbList *list,struct Node4D *node)
{
   UBYTE buf[40],buf2[20];
   struct Path *path;
   struct Node4D n4d;
   UWORD num,lastnet=0;
   BOOL lastok=FALSE;

   path=list->Last;

   if(path->Paths!=0)
   {
      num=path->Paths-1;
      jbcpos=0;

      while(jbstrcpy(buf,path->Path[num],40))
      {
         if(Parse4D(buf,&n4d))
         {
            if(n4d.Net == 0) n4d.Net=lastnet;
            else             lastnet=n4d.Net;
            lastok=TRUE;
         }
         else
         {
            lastok=FALSE;
         }
      }

      if(lastok)
      {
         if(n4d.Net == node->Net && n4d.Node == node->Node && n4d.Point == node->Point)
            return;

         buf[0]=0;

         if(n4d.Net != node->Net)
         {
            sprintf(buf2,"%lu/",node->Net);
            strcat(buf,buf2);
         }

         sprintf(buf2,"%lu",node->Node);
         strcat(buf,buf2);
      }
      else
      {
         sprintf(buf,"%lu/%lu",node->Net,node->Node);
      }

      if(node->Point != 0)
      {
         sprintf(buf2,".%lu",node->Point);
         strcat(buf,buf2);
      }

      if(strlen(buf)+strlen(path->Path[num])<=70)
      {
         if(path->Path[num][0]!=0)
            strcat(path->Path[num]," ");

         strcat(path->Path[num],buf);
         return;
      }
   }

   if(path->Paths == PKT_NUMPATH)
   {
      if(!(path=(struct Path *)AllocMem(sizeof(struct Path),MEMF_ANY)))
      {
         nomem=TRUE;
         return;
      }

      jbAddNode(list,(struct jbNode *)path);
      path->Next=NULL;
      path->Paths=0;
   }

   sprintf(path->Path[path->Paths],"%lu/%lu",node->Net,node->Node);

   if(node->Point != 0)
   {
      sprintf(buf2,".%lu",node->Point);
      strcat(path->Path[path->Paths],buf2);
   }
   path->Paths++;
}

WriteRFC(UBYTE *name)
{
   BPTR fh;
   UBYTE *domain=NULL;
   struct Aka *aka;
   struct TextChunk *tmp;
   ULONG c,d,lastspace;
   UBYTE buffer[100];

   if(!(fh=Open(name,MODE_NEWFILE)))
   {
      LogWrite(1,SYSTEMERR,"Unable to write RFC-message to %s",name);
      exitprg=TRUE;
      return;
   }

   for(aka=AkaList.First;aka;aka=aka->Next)
      if(Compare4D(&MemMessage.DestNode,&aka->Node)==0) break;

   if(aka)
      if(aka->Domain[0]!=0)
         domain=aka->Domain;

   if(!domain)
      domain="FidoNet";

   FPrintf(fh,"From: %lu:%lu/%lu.%lu@%s (%s)\n",(long)MemMessage.OrigNode.Zone,
                                                (long)MemMessage.OrigNode.Net,
                                                (long)MemMessage.OrigNode.Node,
                                                (long)MemMessage.OrigNode.Point,
                                                (long)domain,
                                                (long)MemMessage.From);

   FPrintf(fh,"To: %lu:%lu/%lu.%lu@%s (%s)\n",(long)MemMessage.DestNode.Zone,
                                              (long)MemMessage.DestNode.Net,
                                              (long)MemMessage.DestNode.Node,
                                              (long)MemMessage.DestNode.Point,
                                              (long)domain,
                                              (long)MemMessage.To);

   FPrintf(fh,"Subject: %s\n",(long)MemMessage.Subject);

   FPrintf(fh,"Date: %s\n",(long)MemMessage.DateTime);

   if(MemMessage.MSGID[0]!=0)
      FPrintf(fh,"Message-ID: <%s>\n",(long)MemMessage.MSGID);

   if(MemMessage.REPLY[0]!=0)
      FPrintf(fh,"References: <%s>\n",(long)MemMessage.REPLY);

   for(tmp=MemMessage.TextChunks.First;tmp;tmp=tmp->Next)
   {
      c=0;

      while(c<tmp->Length)
      {
         for(d=c;d<tmp->Length && tmp->Data[d]!=13 && tmp->Data[d]!=10;d++);
         if(tmp->Data[d]==13 || tmp->Data[d]==10) d++;

         if(tmp->Data[c]==1)
         {
            FPuts(fh,"X-Fido-");
            if(d-c-2!=0) FWrite(fh,&tmp->Data[c+1],d-c-2,1);
            FPuts(fh,"\n");
         }
         c=d;
      }
   }

   FPuts(fh,"\n");

   for(tmp=MemMessage.TextChunks.First;tmp;tmp=tmp->Next)
   {
      d=0;

      while(d<tmp->Length)
      {
         lastspace=0;
         c=0;

         while(tmp->Data[d+c]==10) d++;

         while(c<78 && d+c<tmp->Length && tmp->Data[d+c]!=13)
         {
            if(tmp->Data[d+c]==32) lastspace=c;
            c++;
         }

         if(c==78 && lastspace)
         {
            strncpy(buffer,&tmp->Data[d],lastspace);
            buffer[lastspace]=0;
            d+=lastspace+1;
         }
         else
         {
            strncpy(buffer,&tmp->Data[d],c);
            buffer[c]=0;
            if(tmp->Data[d+c]==13) c++;
            d+=c;
         }

         if(buffer[0]!=1)
         {
            FPuts(fh,buffer);
            FPutC(fh,'\n');
         }
      }
   }

   Close(fh);
}

UBYTE *StripRe(UBYTE *str)
{
   for (;;)
   {
      if(strnicmp (str, "Re:", 3)==0)
      {
         str += 3;
         if (*str == ' ') str++;
      }
      else if(strnicmp (str, "Re^", 3)==0 && str[4]==':')
      {
         str += 5;
         if (*str == ' ') str++;
      }
      else if(strnicmp (str, "Re[", 3)==0 && str[4]==']' && str[5]==':')
      {
         str += 6;
         if (*str == ' ') str++;
      }
      else break;
   }
   return (str);
}
