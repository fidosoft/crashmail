/* sources/AreaFix.c */

AreaFix(void);
int CompareAlpha(struct SortStat **s1, struct SortStat **s2);
SortSortStat(struct jbList *list);
WriteResponse(void);
BOOL CheckFlags(UBYTE group,UBYTE *node);
struct Arealist *FindForward(UBYTE *tagname,UBYTE *nodeflags);
RemoteAreafix(UBYTE *area,struct ConfigNode *node);
Rescan(struct Area *area,ULONG num);
BOOL ParseRescan(UBYTE *areaname,UBYTE *node,ULONG max);

/* sources/jbio.c */

struct jbFile *jbOpen(UBYTE *name,UWORD mode,ULONG bufsize);
jbClose(struct jbFile *jb);
jbGetChar(struct jbFile *jb);
jbRead(struct jbFile *jb,const void *buf,ULONG bytes);
jbPutChar(struct jbFile *jb, UBYTE ch);
jbWrite(struct jbFile *jb,const void *buf, ULONG bytes);
jbPuts(struct jbFile *jb, UBYTE *str);
jbFGets(struct jbFile *jb,UBYTE *str,ULONG max);

/* sources/UMS.c */

void UMSStart(void);
void UMSClose(void);
void ImportUMS(struct Area *area);
MakeRFC(UBYTE *src, UBYTE *dest);
//Parse5D(UBYTE *buf, struct Node5D *node);
ULONG FindLink(UBYTE *msgid,UBYTE *notarea);
void Xlat(UBYTE *buf,UBYTE *chrs,ULONG len);
void WriteSeenbyBuf(UBYTE *dest,struct jbList *list,ULONG maxlen);
void WritePathBuf(UBYTE *dest,struct jbList *list,ULONG maxlen);
ExportAreaUMS(struct Area *area);
MakeFido(UBYTE *source,UBYTE *dest);
ConvertFido(UBYTE *source,UBYTE *dest);
//IsNum(UBYTE *str);
ExportUMSMsg(struct Area *area,ULONG num,UBYTE *reason);
RescanUMS(struct Area *area,ULONG num);

/* sources/jbList.c */

jbNewList(struct jbList *list);
jbAddNode(struct jbList *list, struct jbNode *node);
jbFreeList(struct jbList *list,struct jbNode *firstnode,ULONG sizenode);
jbFreeNum(struct jbList *list,ULONG num,ULONG size);
jbFreeNode(struct jbList *list,struct jbNode *node,ULONG size);

/* sources/CleanUp.c */

CleanUp(void);
CleanUpARexx(UBYTE level);
Free(void);
FreeConfig(void);
CleanUpConfig(void);
CheckConfig(void);
ConfigError(UBYTE *str,...);

/* sources/Toss.c */

Compare(UBYTE *str, UBYTE *recog);
HexToDec(char *hex);
ExaminePacker(UBYTE *name);
BOOL Toss(void);
BOOL TossBundle(UBYTE *file,ULONG size);
BOOL TossFile(UBYTE *file);

/* sources/Pkt.c */

stripspace(UBYTE *str);
ULONG ReadNull(UBYTE *buf, ULONG maxlen, struct jbFile *jb);
ULONG ReadCR(UBYTE *buf, ULONG maxlen, struct jbFile *jb);
AddSeenby(UBYTE *str,struct jbList *list);
AddNodes2DList(struct jbList *list,UWORD net,UWORD node);
RemNodes2DList(struct jbList *list,UWORD net,UWORD node);
RemNodes2DListPat(struct jbList *list,struct Node2DPat *pat);
AddPath(UBYTE *str,struct jbList *list);
RemLastPath(struct jbList *list);
ProcessKludge(UBYTE *kludge, ULONG klen);
ReadPkt(UBYTE *pkt,ULONG size);
CleanMemMessage(void);
WriteEchoMail(struct ConfigNode *node, struct Aka *aka);
WriteNodes2D(struct jbFile *jb,struct jbList *list);
WritePath(struct jbFile *jb,struct jbList *list);
WriteNetMail(struct Node4D *dest,struct Aka *aka);
CreatePkt(struct Node4D *dest,struct ConfigNode *node,struct Aka *aka);
BOOL WriteNull(struct jbFile *jb,UBYTE *str);
WriteMsgHeader(struct jbFile *jb,struct Node4D *dest,struct ConfigNode *node,struct Aka *aka,BOOL netmail);
FindPkt(struct Node4D *node,UBYTE type);
FinishPacket(struct Pkt *pkt);
ClosePackets(void);

/* sources/Scan.c */

BOOL Scan(void);
BOOL ScanList(UBYTE *file);
BOOL ScanArea(UBYTE *tagname);

/* sources/Config.c */

ConfigNoMem(void);
BOOL ReadConfig(UBYTE *cfg,BPTR lockfh);
CheckAreaPaths(void);
ResetConfig(void);
BOOL CorrectFlags(UBYTE *flags);
BOOL WriteConfig(UBYTE *cfg);
WriteString(BPTR fh,UBYTE *name,UBYTE *str);
WriteSwitch(BPTR fh,UBYTE *name,ULONG set);
WriteNode(BPTR fh,struct Node4D *n4d);
WriteNode4DPat(BPTR fh,struct Node4DPat *n4dpat);
WriteNode4DDestPat(BPTR fh,struct Node4DPat *n4dpat);
WriteNode2D(BPTR fh,struct Node4D *n4d);
WriteNode2DPat(BPTR fh,struct Node2DPat *n4dpat);
WriteCharsetName(BPTR fh,UBYTE *chrs);
SafeWrite(BPTR fh,UBYTE *str);
BOOL Check4DPatNodelist(struct Node4DPat *pat);

/* sources/MSG.c */

ImportMSG(struct Area *area);
WriteMSG(UBYTE *file,struct Area *area);
GetHighMsg(struct Area *area);
ExportAreaMSG(struct Area *area);
WriteHighWater(struct Area *area);
ExportAreaMSG(struct Area *area);
ExportMSGNum(struct Area *area,ULONG num);
RescanMSG(struct Area *area,ULONG max);

/* sources/Archive.c */

ArchiveOutbound(void);
PackOutbound(struct jbList *PktList,struct jbList *ArcList,UBYTE *dir,BOOL isoutbound);
MakeExtension(UBYTE *arc, struct jbList *arclist);
AddFlow(UBYTE *filename,UBYTE *dowhat,struct Node4D *node,UBYTE pri,BOOL inoutbound);
AddFlowFile(UBYTE *file,UBYTE *dowhat,UBYTE *arc);
ChangeType(struct Node4D *dest,UBYTE pri);
UpdateFile(UBYTE *file,struct jbList *list);

/* sources/main.c */

BOOL InitSession(void);
BOOL FinishSession(void);
Init(void);

/* sources/Dupe.c */

MakeDupeInfo(void);
AddMessage(void);
IsDupe(void);
dupecomp(UBYTE *d1,UBYTE *d2,ULONG len);
AddDupeBuf(UBYTE *dat,ULONG len);
WriteDupeBuf(void);
ReadDupeBuf(void);
InitDupe(void);

/* sources/rexx.c */

RexxLoop(void);
HandleRexxMessages(void);
ReplyRexxCmd(struct RexxMsg *Msg,LONG rc,UBYTE *s);
SendRexxMsg(UBYTE *s,struct RexxMsg *m,LONG flags);
RexxPassOn(struct MsgPort *port,UBYTE *command);

/* sources/Handle.c */

int CompareSort(struct TempSort *t1,struct TempSort *t2);
SortNodes2D(struct jbList *list);
FindNodes2D(struct jbList *list,struct Node4D *node);
struct Area *AddArea(UBYTE *name,struct Node4D *node,struct Node4D *mynode,ULONG autoadd,ULONG passthru);
void AddTossNode(struct Area *area,struct ConfigNode *cnode,UWORD flags);
HandleMessage(void);
HandleNetmail(void);
CheckNode(struct Node4D *node);
ExpandNodePat(struct Node4DPat *temproute,struct Node4D *dest,struct Node4D *sendto);
iswildcard(UBYTE *str);
Bounce(void);
AnswerReceipt(void);
AnswerAudit(void);
Remap(struct Node4D *orig,struct Node4D *dest,UBYTE *name);
MoveFile(UBYTE *file,UBYTE *destdir);
CopyFile(UBYTE *file,UBYTE *destdir);
HandleEchomail(void);
WriteBad(void);
AddNodePath(struct jbList *list,struct Node4D *node);
WriteRFC(UBYTE *name);
UBYTE *StripRe(UBYTE *str);
UBYTE *Last(UBYTE *str,ULONG max);

/* sources/misc.c */

BOOL Parse4D(UBYTE *buf, struct Node4D *node);
int Compare4D(struct Node4D *node1,struct Node4D *node2);
BOOL Parse4DPat(UBYTE *buf, struct Node4DPat *node);
BOOL Parse4DDestPat(UBYTE *buf, struct Node4DPat *node);
BOOL Parse2DPat(UBYTE *buf, struct Node2DPat *node);
int Compare4DPat(struct Node4DPat *nodepat,struct Node4D *node);
int Compare2DPat(struct Node2DPat *nodepat,UWORD net,UWORD node);
int NodeCompare(UBYTE *pat,UWORD num);
void Copy4D(struct Node4D *node1,struct Node4D *node2);
void Print4DPat(struct Node4DPat *pat,UBYTE *dest);
void Print4DDestPat(struct Node4DPat *pat,UBYTE *dest);
int CheckPacketPW(struct Node4D *node, UBYTE *pw);
ExpandPacker(UBYTE *cmd,UBYTE *dest,UBYTE *archive,UBYTE *file);
jbstrcpy(UBYTE *dest,UBYTE *src,ULONG maxlen);
ScanForPktArc(UBYTE *dir,struct jbList *pktlist,struct jbList *arclist);
ScanForPktArcShowBad(UBYTE *dir,struct jbList *pktlist,struct jbList *arclist);
ScanForNewOldArc(UBYTE *dir,struct jbList *arclist);
ScanForPktTmp(UBYTE *dir,struct jbList *pktlist);
ScanForNewPkt(UBYTE *dir,struct jbList *pktlist);
IsArcMail(UBYTE *file);
IsNewArcMail(UBYTE *file);
IsPkt(UBYTE *file);
IsPktTmp(UBYTE *file);
IsNewPkt(UBYTE *file);
DateCompareFTP(struct FileToProcess **f1, struct FileToProcess **f2);
SortFTPList(struct jbList *list);
strip(UBYTE *str);
WriteStats(void);
ReadStats(void);
AddChunkList(struct jbList *chunklist,UBYTE *buf,ULONG buflen);
MakeFidoDate(struct DateStamp *ds,UBYTE *dest);
MakeNetmailKludges(struct jbList *chunklist,struct Node4D *orig,struct Node4D *dest,ULONG addtid);
SafeDelete(UBYTE *file);
GetCharset(UBYTE *string,UBYTE *defchrs);
BOOL rawParse4DPat(UBYTE *buf, struct Node4DPat *node);
BOOL Parse5D(UBYTE *buf, struct Node5D *node);
void Print4D(struct Node4D *n4d,UBYTE *dest);

int os_GetHub(struct Node4D *node);
int os_GetRegion(struct Node4D *node);

void LogWrite(ULONG level,ULONG category,UBYTE *fmt,...);
void CloseLogWrite(void);
short InitLogWrite(void);

struct Aka *GetRouteAka(struct Node4D *node);
void mystrncpy(char *dest,char *src,long len);

BOOL SendAFList(short type,UBYTE *node);
BOOL RemoveArea(UBYTE *areaname);

