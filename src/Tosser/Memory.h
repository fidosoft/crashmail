#define PKT_MINREADLEN  200
#define PKT_CHUNKLEN  10000
#define PKT_NUM2D        50
#define PKT_NUMPATH      10

#define PKT_READLEN   10*1024
#define PKT_WRITELEN   5*1024

struct MemMessage
{
   struct Node4D OrigNode;
   struct Node4D DestNode;

   UBYTE Area[40];

   UBYTE To[36];
   UBYTE From[36];
   UBYTE Subject[72];
   UBYTE DateTime[20];

   UBYTE MSGID[80];
   UBYTE REPLY[80];

   UWORD Attr;
   UWORD Cost;

   UBYTE Type;
   BOOL Rescanned;

   UBYTE BadReason[300];

   struct jbList TextChunks;
   struct jbList SeenBy;
   struct jbList Path;
};

struct TextChunk
{
   struct TextChunk *Next;
   ULONG Length;
   UBYTE Data[PKT_CHUNKLEN];
};

struct Nodes2D
{
   struct Nodes2D *Next;
   UWORD Nodes;
   UWORD Net[PKT_NUM2D];
   UWORD Node[PKT_NUM2D];
};

struct Path
{
   struct Path *Next;
   UWORD Paths;
   UBYTE Path[PKT_NUMPATH][100];
};

struct Pkt
{
   struct Pkt *Next;
   struct jbFile *jb;
   struct Node4D Dest;
   struct Node4D Orig;
   UBYTE Type;
};


struct FileToProcess
{
   struct FileToToss *Next;
   UBYTE Name[100];
   UBYTE Comment[80];
   struct DateStamp Date;
   ULONG Size;
};

struct DiskAreaStatsCST1
{
   UBYTE Tagname[80];
   ULONG Texts;
   ULONG Dupes;
   ULONG FirstDay;
};

struct DiskAreaStats
{
   UBYTE Tagname[80];
   struct Node4D Aka;

   UBYTE Group;
   UBYTE fill_to_make_even; /* Just ignore this one */

   ULONG TotalTexts;
   UWORD Last8Days[8];
   ULONG Dupes;

   struct DateStamp FirstTime;
   ULONG LastDay;
};

struct DiskNodeStats
{
   struct Node4D Node;
   ULONG GotNetmails;
   ULONG GotNetmailBytes;
   ULONG SentNetmails;
   ULONG SentNetmailBytes;
   ULONG GotEchomails;
   ULONG GotEchomailBytes;
   ULONG SentEchomails;
   ULONG SentEchomailBytes;
   ULONG Dupes;
   ULONG FirstDay;
};


