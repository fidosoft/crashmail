#include <CrashMail.h>

ULONG sentmsg;
BOOL keepgoing;
BOOL reloadconfig;

RexxLoop()
{
   ULONG sigs;
   BPTR l;

   keepgoing=TRUE;
   sentmsg=0;

   if(!(notifysignal=AllocSignal(-1)))
   {
      LogWrite(1,SYSTEMERR,"Couldn't allocate signal for notify");
      exitprg=TRUE;
      CleanUp();
   }

   notifyrequest.nr_Name=(UBYTE *)argarray[ARG_SETTINGS];
   notifyrequest.nr_Flags=NRF_SEND_SIGNAL;
   notifyrequest.nr_stuff.nr_Signal.nr_Task=FindTask(NULL);
   notifyrequest.nr_stuff.nr_Signal.nr_SignalNum=notifysignal;

   if(StartNotify(&notifyrequest))
      startnotify=TRUE;

   while(keepgoing || sentmsg)
   {
      sigs=Wait((1<<rexxport->mp_SigBit) | (1<<notifysignal) | SIGBREAKF_CTRL_C);

      reloadconfig=FALSE;

      if(sigs & SIGBREAKF_CTRL_C)
         keepgoing=FALSE;

      if(sigs & (1<<notifysignal))
      {
         if(l=Lock((UBYTE *)argarray[ARG_SETTINGS],SHARED_LOCK))
         {
            UnLock(l);
            reloadconfig=TRUE;
         }
      }

      if(sigs & (1<<rexxport->mp_SigBit))
      {
         HandleRexxMessages();
         SetSignal(0L,SIGBREAKF_CTRL_C); /* Nollställa Ctrl-C */

         if(nomem || exitprg)
            keepgoing=FALSE;
      }

      if(reloadconfig)
      {
         BPTR fh;

         LogWrite(2,SYSTEMINFO,"Reloading configuration...");

         if(fh=Open((UBYTE *)argarray[ARG_SETTINGS],MODE_OLDFILE))
         {
            Free();
            ResetConfig();

            if(!ReadConfig((UBYTE *)argarray[ARG_SETTINGS],fh))
            {
               Printf("Couldn't read config file %s\n",(long)argarray[ARG_SETTINGS]);
               CleanUpConfig();
            }

            CheckConfig();
            Init();
         }
         else
         {
            LogWrite(2,SYSTEMERR,"Couldn't read config file %s",(long)argarray[ARG_SETTINGS]);
         }
      }
   }
   CleanUp();
}

HandleRexxMessages()
{
   struct RexxMsg *RexxMsg;
   ULONG err,max;
   UBYTE buf[100],buf2[100],buf3[10];
   BOOL ok;

   while(RexxMsg=(struct RexxMsg *)GetMsg(rexxport))
   {
      if(RexxMsg->rm_Node.mn_Node.ln_Type == NT_REPLYMSG)
      {
         if(RexxMsg->rm_Args[15])
            ReplyMsg((struct Message *)RexxMsg->rm_Args[15]);

         DeleteArgstring(RexxMsg->rm_Args[0]);
         DeleteRexxMsg(RexxMsg);
         sentmsg--;
      }
      else
      {
         RexxMsg->rm_Result1=0;
         RexxMsg->rm_Result2=0;

         jbcpos=0;
         err=0;

         if(jbstrcpy(buf,RexxMsg->rm_Args[0],100))
         {
            ok=TRUE;
            exitprg=FALSE;
            diskfull=FALSE;
            nomem=FALSE;

            if(stricmp(buf,"QUIT")==0)
            {
               keepgoing=FALSE;
            }
            else if(stricmp(buf,"TOSS")==0)
            {
               if(!Toss()) err=RC_ERROR;
            }
            else if(stricmp(buf,"SCAN")==0)
            {
               if(!Scan()) err=RC_ERROR;
            }
            else if(stricmp(buf,"TOSSFILE")==0)
            {
               if(jbstrcpy(buf,RexxMsg->rm_Args[0],100))
               {
                  if(!TossFile(buf)) err=RC_ERROR;
               }
               else
                  err=RC_ERROR;
            }
            else if(stricmp(buf,"SCANAREA")==0)
            {
               if(jbstrcpy(buf,RexxMsg->rm_Args[0],100))
               {
                  if(!ScanArea(buf)) err=RC_ERROR;
               }
               else
                  err=RC_ERROR;
            }
            else if(stricmp(buf,"SCANLIST")==0)
            {
               if(jbstrcpy(buf,RexxMsg->rm_Args[0],100))
               {
                  if(!ScanList(buf)) err=RC_ERROR;
               }
               else
                  err=RC_ERROR;
            }
            else if(stricmp(buf,"NOTIFY")==0 || stricmp(buf,"SENDQUERY")==0)
            {
               if(jbstrcpy(buf,RexxMsg->rm_Args[0],100))
               {
                  if(!SendAFList(SENDLIST_QUERY,buf))
                     err=RC_ERROR;
               }
               else
                  err=RC_ERROR;
            }
            else if(stricmp(buf,"SENDLIST")==0)
            {
               if(jbstrcpy(buf,RexxMsg->rm_Args[0],100))
               {
                  if(!SendAFList(SENDLIST_FULL,buf))
                     err=RC_ERROR;
               }
               else
                  err=RC_ERROR;
            }
            else if(stricmp(buf,"SENDUNLINKED")==0)
            {
               if(jbstrcpy(buf,RexxMsg->rm_Args[0],100))
               {
                  if(!SendAFList(SENDLIST_UNLINKED,buf))
                     err=RC_ERROR;
               }
               else
                  err=RC_ERROR;
            }
            else if(stricmp(buf,"SENDHELP")==0)
            {
               if(jbstrcpy(buf,RexxMsg->rm_Args[0],100))
               {
                  if(!SendAFList(SENDLIST_HELP,buf))
                     err=RC_ERROR;
               }
               else
                  err=RC_ERROR;
            }
            else if(stricmp(buf,"SENDINFO")==0)
            {
               if(jbstrcpy(buf,RexxMsg->rm_Args[0],100))
               {
                  if(!SendAFList(SENDLIST_INFO,buf))
                     err=RC_ERROR;
               }
               else
                  err=RC_ERROR;
            }
            else if(stricmp(buf,"REMOVE")==0)
            {
               if(jbstrcpy(buf,RexxMsg->rm_Args[0],100))
               {
                  if(!RemoveArea(buf))
                     err=RC_ERROR;
               }
               else
                  err=RC_ERROR;
            }
            else if(stricmp(buf,"RESCAN")==0)
            {
               err=RC_ERROR;

               if(jbstrcpy(buf,RexxMsg->rm_Args[0],100))
                  if(jbstrcpy(buf2,RexxMsg->rm_Args[0],100))
                  {
                     max=0;

                     if(jbstrcpy(buf3,RexxMsg->rm_Args[0],10))
                        max=atoi(buf3);

                     if(ParseRescan(buf,buf2,max))
                        err=0;
                  }
            }
            else if(stricmp(buf,"RELOADCONFIG")==0)
            {
               reloadconfig=TRUE;
            }
            else
            {
               ok=FALSE;
            }
         }
         else
         {
            ok=FALSE;
         }

         if(ok)
         {
            if(nomem)
            {
               LogWrite(1,SYSTEMERR,"*** Out of memory ***");
               err=20;
            }

            if(SetSignal(0L,0L)  & SIGBREAKF_CTRL_C)
            {
               LogWrite(1,SYSTEMERR,"*** User Break ***");
               err=5;
            }

            if(diskfull)
            {
               Fault(IoErr(),NULL,buf,100);
               LogWrite(1,SYSTEMERR,"Disk error: %s",buf);
               err=20;
            }

            ReplyRexxCmd(RexxMsg,err,NULL);
            ReplyMsg((struct Message *)RexxMsg);
         }
         else
         {
            if(SendRexxMsg(RexxMsg->rm_Args[0],RexxMsg,0))
            {
               sentmsg++;
            }
            else
            {
               ReplyRexxCmd(RexxMsg,RC_FATAL,NULL);
               ReplyMsg((struct Message *)RexxMsg);
            }
         }
      }
   }
}

ReplyRexxCmd(struct RexxMsg *Msg,LONG rc,UBYTE *s)
{
   Msg->rm_Result1=rc;

   if((Msg->rm_Action & (1<< RXFB_RESULT)) && s)
      Msg->rm_Result2=(LONG)CreateArgstring(s,strlen(s));

   else
      Msg->rm_Result2=0;
}

SendRexxMsg(UBYTE *s,struct RexxMsg *m,LONG flags)
{
   struct MsgPort *ARexxport;
   struct RexxMsg *RexxMsg;

   if(!(RexxMsg=(struct RexxMsg *)CreateRexxMsg(rexxport,"crashmail",rexxport->mp_Node.ln_Name)))
      return(NULL);

   if(RexxMsg->rm_Args[0]=(UBYTE *)CreateArgstring(s,strlen(s)))
   {
      RexxMsg->rm_Action = RXCOMM | flags;
      RexxMsg->rm_Args[15]=m;

      Forbid();

      if(ARexxport=(struct MsgPort *)FindPort("REXX"))
         PutMsg(ARexxport,(struct Message *)RexxMsg);

      Permit();

      if(ARexxport)
         return(RexxMsg);
   }

   if(RexxMsg->rm_Args[0])
      DeleteArgstring(RexxMsg->rm_Args[0]);

   DeleteRexxMsg(RexxMsg);
   return(NULL);
}

RexxPassOn(struct MsgPort *port,UBYTE *command)
{
   struct MsgPort *replyport;
   struct RexxMsg *RexxMsg;
   UBYTE res;

   if(!(replyport=(struct MsgPort *)CreatePort(NULL,0)))
   {
      return(NULL);
   }

   if(!(RexxMsg=(struct RexxMsg *)CreateRexxMsg(replyport,NULL,port->mp_Node.ln_Name)))
   {
      DeletePort(replyport);
      return(NULL);
   }

   if(!(RexxMsg->rm_Args[0]=(UBYTE *)CreateArgstring(command,strlen(command))))
   {
      DeleteRexxMsg(RexxMsg);
      DeletePort(replyport);
      return(NULL);
   }

   Printf("-> %s, waiting for result",command);
   Flush(Output());

   RexxMsg->rm_Action = RXCOMM;

   Forbid();
   PutMsg(port,(struct Message *)RexxMsg);
   Permit();

   WaitPort(replyport);

   while(RexxMsg=(struct RexxMsg *)GetMsg(replyport))
   {
      if(RexxMsg->rm_Node.mn_Node.ln_Type == NT_REPLYMSG)
      {
         res=RexxMsg->rm_Result1;
         DeleteArgstring(RexxMsg->rm_Args[0]);
         DeleteRexxMsg(RexxMsg);
      }
   }

   if(res == 0)
      Printf("\x0d-> %s, everything ok               \n",command);

   else
      Printf("\x0d-> %s, error %lu                   \n",command,res);

   DeletePort(replyport);
   return(res);
}