#include <exec/types.h>
#include <exec/memory.h>
#include <exec/tasks.h>
#include <dos/dos.h>
#include <dos/dostags.h>
#include <dos/notify.h>
#include <dos/dosextens.h>
#include <dos/datetime.h>
#include <dos/rdargs.h>
#include <utility/tagitem.h>
#include <stdarg.h>
#include <rexx/rxslib.h>
#include <rexx/errors.h>
#include <intuition/intuition.h>
#include <pd/nl.h>
#include <libraries/ums.h>
#include <exec/execbase.h>

#include <clib/exec_protos.h>
#include <clib/dos_protos.h>
#include <clib/rexxsyslib_protos.h>
#include <clib/alib_protos.h>
#include <clib/ums_protos.h>
#include <clib/utility_protos.h>
#include <clib/intuition_protos.h>

#include <stdarg.h>
#include <varargs.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include <cmshared/jbio.h>
#include <cmshared/jblist.h>
#include <cmshared/readkey.h>
#include <cmshared/nodelist.h>
#include <cmshared/config.h>

#include "FidoNet.h"
#include "Memory.h"

#include "proto.h"

#define VERSION_MAJOR 1
#define VERSION_MINOR 32

#define VERSION "1.32"

#define TID_VERSION "1.32"

extern struct ExecBase *SysBase;
extern struct StackSwapStruct stackswapstruct;
extern __asm void (*own_StackSwap)(register __A0 struct StackSwapStruct *newstack,register __A6 struct Library *library);

extern UBYTE *MonthNames[12];

extern struct PktHeader     PktHeader;
extern struct PktMsgHeader  PktMsgHeader;
extern struct MemMessage    MemMessage;
extern struct Node4D        PktOrig;
extern struct Node4D        PktDest;

extern struct jbList PktList;
extern struct jbList DeleteList;

extern struct Area *LastArea;
extern struct ConfigNode *LastNode;

extern BOOL nomem;
extern BOOL diskfull;
extern BOOL exitprg;
extern BOOL pkt_pw;
extern BOOL pkt_fake;
extern BOOL pkt_4d;
extern BOOL pkt_5d;

extern ULONG toss_total;
extern ULONG toss_bad;
extern ULONG toss_route;
extern ULONG toss_import;
extern ULONG toss_written;
extern ULONG toss_dupes;

extern BOOL scannedumsmail;

extern BOOL no_security;

extern ULONG scan_num;

extern UBYTE *dupebuf;
extern BOOL dupebufloaded;

extern struct NotifyRequest notifyrequest;
extern UBYTE notifysignal;
extern BOOL startnotify;
extern BOOL initlogtask;

extern struct Process *process;
extern APTR oldwin;
extern struct RDArgs *rdargs;

extern ULONG jbcpos;
extern BOOL isscanning;
extern BOOL isrescanning;
extern BOOL hasstarted;
extern BOOL saveconfig;

extern struct ConfigNode *RescanNode;

extern BOOL UMS;

extern struct Library *UMSBase;
extern UMSUserAccount UMSHandle;

extern struct Library *NodelistBase;
extern NodeList nodelist;

extern struct RxsLib *RexxSysBase;
extern struct MsgPort *rexxport;

extern BOOL nlopen;

#define COPYBUFSIZE 4000

extern UBYTE copybuf[COPYBUFSIZE];

#define ARG_SCAN            0
#define ARG_TOSS            1
#define ARG_TOSSFILE        2
#define ARG_SCANAREA        3
#define ARG_SCANLIST        4
#define ARG_SETTINGS        5
#define ARG_WAIT            6
#define ARG_QUIT            7
#define ARG_RESCAN          8
#define ARG_RESCANNODE      9
#define ARG_RESCANMAX      10
#define ARG_SENDQUERY      11
#define ARG_SENDLIST       12
#define ARG_SENDUNLINKED   13
#define ARG_SENDHELP       14
#define ARG_SENDINFO       15
#define ARG_REMOVE         16

extern ULONG argarray[];

#define crc32add(crc,c) (crc32tab[(unsigned char)crc ^ (unsigned char)c] ^ ((unsigned long)crc >> 8))
extern unsigned long crc32tab[];

#define UMSFLAG_TOEXPORT     1
#define UMSFLAG_EXPORTAREA   2
#define UMSFLAG_FINDLINK     8
#define UMSFLAG_EXPORTED    16
#define UMSFLAG_TORESCAN    32
#define UMSFLAG_RESCANAREA  64
#define UMSFLAG_RESCAN     128

#define STACKSIZE 20000

extern ULONG checksums[16];

extern ULONG UMS_MsgidConvertCache_Zone;
extern UBYTE UMS_MsgidConvertCache_Domain[100];

#define SYSTEMINFO   0
#define SYSTEMERR    1
#define TOSSINGINFO  2
#define TOSSINGERR   3
#define MISCINFO     4
#define DEBUG        5
#define AREAFIX      6
#define ACTIONINFO   7
#define USERERR      8

extern ULONG DayStatsWritten;

#define SENDLIST_FULL      1
#define SENDLIST_QUERY     2
#define SENDLIST_UNLINKED  3
#define SENDLIST_INFO      4
#define SENDLIST_HELP      5
