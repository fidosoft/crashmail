#include <CrashMail.h>

CleanUp()
{
   UBYTE buf[100];

   if(nomem)
      LogWrite(1,SYSTEMERR,"*** Out of memory ***");

   if(SetSignal(0L,0L)  & SIGBREAKF_CTRL_C)
      LogWrite(2,SYSTEMERR,"*** User Break ***");

   if(diskfull)
   {
      Fault(IoErr(),NULL,buf,100);
      LogWrite(1,SYSTEMERR,"Disk error: %s",buf);
   }

   if(hasstarted && cfg_LogLevel >= 2)
      LogWrite(1,SYSTEMINFO,"CrashMail end");

   Free();

   /* ARexxport */

   if(rexxport)     DeletePort(rexxport);
   if(RexxSysBase)  CloseLibrary((struct Library *)RexxSysBase);

   /* ReadArgs */

   if(rdargs) FreeArgs(rdargs);
   rdargs=NULL;

   /* Memory pool */

/*   if(memorypool)
      DeletePool(memorypool); */

   /* File notification */

   if(startnotify)
      EndNotify(&notifyrequest);

   if(notifysignal)
      FreeSignal(notifysignal);
 
   /* Restore stack -- No local variables allowed after this point! */

   if(stackswapstruct.stk_Lower)
   {
      /* This trick is needed, otherwise DICE will use stack... */

      own_StackSwap=(void *)((ULONG)SysBase-(ULONG)732);
      (*own_StackSwap)(&stackswapstruct,(struct Library *)SysBase);

       FreeMem(stackswapstruct.stk_Lower,STACKSIZE);
   }

   /* Error levels */

   if(SetSignal(0L,0L)  & SIGBREAKF_CTRL_C)
      _exit(5);

   if(nomem || exitprg)
      _exit(10);

   _exit(0);
}

Free()
{
   if(UMS) UMSClose();

   FreeConfig();

   /* MemMessage */

   jbFreeList(&MemMessage.TextChunks,MemMessage.TextChunks.First,sizeof(struct TextChunk));
   jbFreeList(&MemMessage.SeenBy,MemMessage.SeenBy.First,sizeof(struct Nodes2D));
   jbFreeList(&MemMessage.Path,MemMessage.Path.First,sizeof(struct Path));

   if(dupebuf) FreeMem(dupebuf,cfg_DupeSize);
   dupebuf=NULL;

   /* Packets */

   ClosePackets();

   jbFreeList(&PktList,PktList.First,sizeof(struct Pkt));
   jbFreeList(&DeleteList,DeleteList.First,sizeof(struct FileToProcess));

   if(cfg_Flags & CFG_UNATTENDED)
      process->pr_WindowPtr=oldwin;

   if(initlogtask)
   {
      CloseLogWrite();
      initlogtask=FALSE;
   }
}

CleanUpARexx(UBYTE level)
{
   if(rdargs) FreeArgs(rdargs);
   process->pr_WindowPtr=oldwin;
   if(RexxSysBase)  CloseLibrary((struct Library *)RexxSysBase);
   _exit(level);
}

CleanUpConfig(void)
{
   exitprg=TRUE;
   CleanUp();
}

extern struct jbFile *cfgfh;

ConfigNoMem()
{
   if(cfgfh) jbClose(cfgfh);
   nomem=TRUE;
   CleanUpConfig();
}

extern ULONG cfgline;

ConfigError(UBYTE *str,...)
{
   va_list args;
   UBYTE buf[200];

   if(cfgfh) jbClose(cfgfh);

   va_start(args,str);
   sprintf(buf,"Configuration error in line %lu: %s\n",cfgline,str);
   VFPrintf(Output(),buf,args);
   va_end(args);

   CleanUpConfig();
}

CheckConfig()
{
   BOOL Netmail=FALSE,Bad=FALSE;
   struct Area *area;
   struct PatternNode *patternnode;
   struct Route *route;
   struct Change *change;
   struct RemapNode *remapnode;
   UBYTE buf[100];

   for(patternnode=FileAttachList.First;patternnode;patternnode=patternnode->Next)
      if(!Check4DPatNodelist(&patternnode->Pattern))
      {
         Print4DPat(&patternnode->Pattern,buf);
         Printf("Nodelist needed for pattern \"%s\"\n",buf);
         CleanUpConfig();
      }

   for(patternnode=BounceList.First;patternnode;patternnode=patternnode->Next)
      if(!Check4DPatNodelist(&patternnode->Pattern))
      {
         Print4DPat(&patternnode->Pattern,buf);
         Printf("Nodelist needed for pattern \"%s\"\n",buf);
         CleanUpConfig();
      }

   for(route=RouteList.First;route;route=route->Next)
   {
      if(!Check4DPatNodelist(&route->Pattern))
      {
         Print4DPat(&route->Pattern,buf);
         Printf("Nodelist needed for pattern \"%s\"\n",buf);
         CleanUpConfig();
      }

      if(!Check4DPatNodelist(&route->DestPat))
      {
         Print4DDestPat(&route->DestPat,buf);
         Printf("Nodelist needed for pattern \"%s\"\n",buf);
         CleanUpConfig();
      }
   }

   for(change=ChangeList.First;change;change=change->Next)
      if(!Check4DPatNodelist(&change->Pattern))
      {
         Print4DPat(&change->Pattern,buf);
         Printf("Nodelist needed for pattern \"%s\"\n",buf);
         CleanUpConfig();
      }

   for(remapnode=RemapNodeList.First;remapnode;remapnode=remapnode->Next)
   {
      if(!Check4DPatNodelist(&remapnode->NodePat))
      {
         Print4DPat(&remapnode->NodePat,buf);
         Printf("Nodelist needed for pattern \"%s\"\n",buf);
         CleanUpConfig();
      }
      if(!Check4DPatNodelist(&remapnode->DestPat))
      {
         Print4DDestPat(&remapnode->DestPat,buf);
         Printf("Nodelist needed for pattern \"%s\"\n",buf);
         CleanUpConfig();
      }
   }

   for(area=AreaList.First;area;area=area->Next)
   {
      if(area->Flags & AREA_BAD)       Bad=TRUE;
      if(area->Flags & AREA_NETMAIL)   Netmail=TRUE;

      if(area->TossNodes.First && !(area->Flags & AREA_NETMAIL))
         if(area->TossNodes.First->Next)
            if(area->TossNodes.First->Next->Next && Checksum9()!=checksums[9])
            {
               Printf("Error with area \"%s\": Only two exports allowed when unregistered\n",area->Tagname);
               CleanUpConfig();
            }
   }

   if(!Netmail)
   {
      PutStr("No netmail area\n");
      CleanUpConfig();
   }

   if(!Bad)
   {
      PutStr("No area for bad mail\n");
      CleanUpConfig();
   }

   CheckAreaPaths();
}

CheckAreaPaths()
{
   struct Area *a1,*a2;

   for(a1=AreaList.First;a1;a1=a1->Next)
      for(a2=a1->Next;a2;a2=a2->Next)
         if(stricmp(a1->Path,a2->Path)==0 && a1->AreaType!=0 && a2->AreaType!=0)
         {
            Printf("The areas %s and %s both have the same path\n",(long)a1->Tagname,(long)a2->Tagname);
            CleanUpConfig();
         }
}
