#include <CrashPrefs.h>

struct List robotlist;
ULONG robotnum,currentrobot;

struct Hook RobotHelpHook = { {NULL}, HelpHookEntry,NULL,HELP_ROBOTNAMES};

void SetRobot(ULONG num)
{
   struct Robot *robot;
   ULONG c;

   if(currentrobot!=-1)
   {
      robot=RobotList.First;
      for(c=0;c<currentrobot;c++) robot=robot->Next;

      strcmpcpy(robot->Command,(UBYTE *)LT_GetAttributes(parthandle,50,TAG_END));
   }

   if(robotnum == 0)
   {
      LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,50,GA_Disabled,TRUE,TAG_END);
   }

   if(num!=-1)
   {
      if(robotnum)
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,FALSE,TAG_END);
      }
      else
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
      }

      robot=RobotList.First;
      for(c=0;c<num;c++) robot=robot->Next;

      if(num!=0)                 LT_SetAttributes(parthandle,12,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);

      if(num!=robotnum-1)        LT_SetAttributes(parthandle,13,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);

      LT_SetAttributes(parthandle,50,GA_Disabled,FALSE,GTST_String,robot->Command,TAG_END);
   }
   currentrobot=num;
}

void RefreshRobotlist(void)
{
   struct TempNode *tempnode;
   struct Robot *robot;

   LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
   FreeTempNodes(&robotlist);

   robotnum=0;

   for(robot=RobotList.First;robot;robot=robot->Next)
   {
      if(!(tempnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(&robotlist);
         return;
      }
      strcpy(tempnode->buf,robot->Pattern);
      tempnode->Node.ln_Name=tempnode->buf;
      AddTail(&robotlist,(struct Node *)tempnode);
      robotnum++;
   }

   LT_SetAttributes(parthandle,4711,GTLV_Labels,&robotlist,TAG_END);
}

BOOL CheckDupeRobot(UBYTE *buf,ULONG num)
{
   struct Robot *robot;
   ULONG c;
   UBYTE parsedbuf[402];

   for(robot=RobotList.First,c=0;robot;robot=robot->Next,c++)
      if(stricmp(robot->Pattern,buf)==0 && c!=num) break;

   if(robot)
   {
      rtEZRequestTags("You already have a robot with that name","_Ok",NULL,NULL,RT_Underscore,'_',
                                                                                RT_Window,partwin,
                                                                                RT_WaitPointer,TRUE,
                                                                                RT_LockWindow, TRUE,
                                                                                TAG_END);

      return(FALSE);
   }

   if(ParsePatternNoCase(buf,parsedbuf,402)==-1)
   {
      UBYTE reqbuf[100];

      sprintf(reqbuf,"\"%s\" is not a valid AmigaDOS pattern",buf);

      rtEZRequestTags(reqbuf,"_Ok",NULL,NULL,RT_Underscore,'_',
                                             RT_Window,partwin,
                                             RT_WaitPointer,TRUE,
                                             RT_LockWindow, TRUE,
                                             TAG_END);

      return(FALSE);
   }

   return(TRUE);
}

void AddRobot(void)
{
   struct Robot *robot;
   UBYTE buf[200];
   BOOL res;

   buf[0]=0;
   res=rtGetString(buf,199,"Add robot name",NULL,RT_Window,partwin,
                                                 RT_WaitPointer, TRUE,
                                                 RT_LockWindow, TRUE,
                                                 TAG_END);

   if(!res)
      return;

   if(!CheckDupeRobot(buf,-1))
      return;

   if(robot=(struct Robot *)AllocMem(sizeof(struct Robot),MEMF_CLEAR))
   {
      jbAddNode(&RobotList,(struct jbNode *)robot);
      strcpy(robot->Pattern,buf);
      RefreshRobotlist();
      SetRobot(robotnum-1);
      LT_SetAttributes(parthandle,4711,GTLV_Selected,currentrobot,GTLV_MakeVisible,currentrobot,TAG_END);
      saveconfig=TRUE;
   }
   else
   {
      DisplayBeep(NULL);
   }
}

void RemRobot(void)
{
   ULONG c,tmp;
   struct Robot *robot;
   UBYTE buf[100];
   BOOL res;

   robot=RobotList.First;
   for(c=0;c<currentrobot;c++) robot=robot->Next;

   sprintf(buf,"Do you really want to remove the robot \"%s\"?",robot->Pattern);

   res=rtEZRequestTags(buf,"_Remove|_Cancel",NULL,NULL,RT_Underscore,'_',
                                                       RT_Window,partwin,
                                                       RT_WaitPointer,TRUE,
                                                       RT_LockWindow, TRUE,
                                                       RTEZ_Flags, EZREQF_CENTERTEXT | EZREQF_NORETURNKEY,
                                                       TAG_END);

   if(!res)
      return;

   tmp=currentrobot;
   SetRobot(-1);
   jbFreeNode(&RobotList,(struct jbNode *)robot,sizeof(struct Robot));
   RefreshRobotlist();
   if(tmp == robotnum) tmp--;
   SetRobot(tmp);
   saveconfig=TRUE;
}


void PartRobots(void)
{
   ULONG class,code,tmp;
   struct IntuiMessage *IntuiMessage;
   struct TempNode *tempnode;
   APTR iaddress;
   ULONG c;
   struct Robot *robot;

   NewList(&robotlist);
   currentrobot=-1;

   parthandle = LT_CreateHandleTags(scr,LH_EditHook,&StrEditHook,
                                        LH_RawKeyFilter,FALSE,TAG_DONE);

   if(!parthandle)
   {
      FreeTempNodes(&robotlist);
      return;
   }

   LT_New(parthandle,
      LA_Type,      HORIZONTAL_KIND,
      LA_LabelText, "Robotnames",
      TAG_DONE);

      LT_New(parthandle,
         LA_Type,      VERTICAL_KIND,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      STRING_KIND,
            GTST_MaxChars,199,
            LA_Chars,20,
            LA_ID,        4712,
            LAST_Link,    4711,
            GA_Disabled,TRUE,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,       LISTVIEW_KIND,
            LA_Chars,20,
            LALV_LockSize, TRUE,
            LALV_Lines,    10,
            LALV_CursorKey,TRUE,
            LALV_Link,     4712,
            GTLV_Labels,&robotlist,
            LA_ID,4711,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      HORIZONTAL_KIND,
            LAGR_SameSize,TRUE,
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               LAGR_SameSize,TRUE,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Add...",
                  LA_Chars,10,
                  LA_ID,10,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Up",
                  GA_Disabled,TRUE,
                  LA_Chars,10,
                  LA_ID,12,
                  TAG_DONE);

            LT_EndGroup(parthandle);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               LAGR_SameSize,TRUE,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Remove...",
                  LA_Chars,10,
                  GA_Disabled,TRUE,
                  LA_ID,11,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Down",
                  LA_Chars,10,
                  GA_Disabled,TRUE,
                  LA_ID,13,
                  TAG_DONE);

            LT_EndGroup(parthandle);

         LT_EndGroup(parthandle);

      LT_EndGroup(parthandle);

      LT_New(parthandle,
         LA_Type,      VERTICAL_KIND,
         LA_ExtraSpace,TRUE,
         TAG_DONE);

            LT_New(parthandle,
               LA_Type,      STRING_KIND,
               LA_LabelText, "Command",
               LA_LabelPlace,PLACE_ABOVE,
               GA_Disabled,TRUE,
               LA_Chars,45,
               GTST_MaxChars,199,
               LA_ID,50,
               TAG_DONE);

      LT_EndGroup(parthandle);

   LT_EndGroup(parthandle);

   partwin=LT_Build(parthandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW|IDCMP_RAWKEY,
                               LAWN_Title,    "Robotnames",
                               WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                               LAWN_HelpHook, &RobotHelpHook,
                               TAG_END);

   if(!partwin)
   {
      LT_DeleteHandle(parthandle);
      DisplayBeep(NULL);
      FreeTempNodes(&robotlist);
      return;
   }

   RefreshRobotlist();

   if(robotnum)
   {
      LT_SetAttributes(parthandle,4711,GTLV_Selected,0,TAG_END);
      SetRobot(0);
   }

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(parthandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;

         LT_ReplyIMsg(IntuiMessage);

         switch(class)
         {
            case IDCMP_RAWKEY:         if(code == TAB) LT_Activate(parthandle,4712);
                                       break;
            case IDCMP_GADGETUP:       switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 10:
                                             AddRobot();
                                             break;
                                          case 11:
                                             RemRobot();
                                             break;
                                          case 12:
                                             if(currentrobot != 0)
                                             {
                                                tmp=currentrobot;
                                                SetRobot(-1);
                                                MoveUp(&RobotList,tmp);
                                                RefreshRobotlist();
                                                SetRobot(tmp-1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentrobot,
                                                   GTLV_MakeVisible,currentrobot,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 13:
                                             if(currentrobot != robotnum-1)
                                             {
                                                tmp=currentrobot;
                                                SetRobot(-1);
                                                MoveDown(&RobotList,tmp);
                                                RefreshRobotlist();
                                                SetRobot(tmp+1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentrobot,
                                                   GTLV_MakeVisible,currentrobot,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 4711:
                                             SetRobot(code);
                                             break;
                                          case 4712:
                                             if(CheckDupeRobot((UBYTE *)LT_GetAttributes(parthandle,4712,TAG_END),currentrobot))
                                             {
                                                robot=RobotList.First;
                                                for(c=0;c<currentrobot;c++) robot=robot->Next;
                                                strcmpcpy(robot->Pattern,(UBYTE *)LT_GetAttributes(parthandle,4712,TAG_END));

                                                tempnode=robotlist.lh_Head;
                                                for(c=0;c<currentrobot;c++) tempnode=tempnode->Node.ln_Succ;

                                                LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
                                                strcpy(tempnode->buf,(UBYTE *)LT_GetAttributes(parthandle,4712,TAG_END));
                                                LT_SetAttributes(parthandle,4711,GTLV_Labels,&robotlist,TAG_END);
                                             }
                                             else
                                             {
                                                LT_Activate(parthandle,4712);
                                             }
                                             break;
                                       }
                                       break;
            case IDCMP_CLOSEWINDOW:    SetRobot(-1);
                                       LT_DeleteHandle(parthandle);
                                       parthandle=NULL;
                                       FreeTempNodes(&robotlist);
                                       return;
         }
      }
      WaitPort(partwin->UserPort);
   }
}
