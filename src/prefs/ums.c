#include <CrashPrefs.h>

struct Hook UMSHelpHook = { {NULL}, HelpHookEntry,NULL,HELP_UMS};

void PartUMS(void)
{
   struct IntuiMessage *IntuiMessage;
   APTR iaddress;
   ULONG class,code;
   struct Node4D n4d;
   BOOL originbool,msgidbool,mausbool,ignorebool,emptybool;
   UBYTE nodebuf[40];

   originbool=FALSE;
   msgidbool=FALSE;
   mausbool=FALSE;
   ignorebool=FALSE;
   emptybool=FALSE;
   
   if(cfg_Flags & CFG_UMSKEEPORIGIN)
      originbool=TRUE;

   if(cfg_Flags & CFG_CHANGEUMSMSGID)
      msgidbool=TRUE;

   if(cfg_Flags & CFG_UMSMAUSGATE)
      mausbool=TRUE;

   if(cfg_Flags & CFG_UMSIGNOREORIGINDOMAIN)
      ignorebool=TRUE;

   if(cfg_Flags & CFG_UMSEMPTYTOALL)
      emptybool=TRUE;

   sprintf(nodebuf,"%lu:%lu/%lu.%lu",cfg_GatewayNode.Zone,
                                     cfg_GatewayNode.Net,
                                     cfg_GatewayNode.Node,
                                     cfg_GatewayNode.Point);

   parthandle = LT_CreateHandleTags(scr,LH_EditHook,&StrEditHookNext,
                                        LH_RawKeyFilter,FALSE,
                                        TAG_DONE);

   LT_New(parthandle,
      LA_Type, VERTICAL_KIND,
      TAG_DONE);

   LT_New(parthandle,
      LA_Type, VERTICAL_KIND,
      LA_LabelText, "Account",
      TAG_DONE);

      LT_New(parthandle,
         LA_Type,      STRING_KIND,
         LA_LabelText, "Login name",
         GTST_MaxChars,79,
         LA_Chars,     40,
         LA_ID,        1,
         GTST_String,  cfg_UMSName,
      TAG_DONE);

      LT_New(parthandle,
         LA_Type,      STRING_KIND,
         LA_LabelText, "Password",
         GTST_MaxChars,79,
         LA_Chars,     40,
         LA_ID,        2,
         GTST_String,  cfg_UMSPassword,
      TAG_DONE);

      LT_New(parthandle,
         LA_Type,      STRING_KIND,
         LA_LabelText, "Server",
         GTST_MaxChars,79,
         LA_Chars,     40,
         LA_ID,        3,
         GTST_String,  cfg_UMSServer,
      TAG_DONE);

   LT_EndGroup(parthandle);

   LT_New(parthandle,
      LA_Type, VERTICAL_KIND,
      LA_LabelText, "Miscellaneous",
      TAG_DONE);

      LT_New(parthandle,
         LA_Type,      STRING_KIND,
         LA_LabelText, "Default origin",
         GTST_MaxChars,79,
         LA_Chars,     40,
         LA_ID,        4,
         GTST_String,  cfg_DefaultOrigin,
      TAG_DONE);

   LT_EndGroup(parthandle);

   LT_New(parthandle,
      LA_Type, HORIZONTAL_KIND,
      LA_LabelText, "Gateway",
      TAG_DONE);

      LT_New(parthandle,
         LA_Type, VERTICAL_KIND,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      STRING_KIND,
            LA_LabelText, "Export Name",
            GTST_MaxChars,35,
            LA_Chars,     20,
            LA_ID,        5,
            GTST_String,  cfg_GatewayName,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      STRING_KIND,
            LA_LabelText, "Export Node",
            GTST_MaxChars,35,
            LA_Chars,     20,
            LA_ID,        6,
            GTST_String,  nodebuf,
         TAG_DONE);

      LT_EndGroup(parthandle);

      LT_New(parthandle,
         LA_Type, VERTICAL_KIND,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type, HORIZONTAL_KIND,
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      STRING_KIND,
               LA_LabelText, "Import Name",
               GTST_MaxChars,35,
               LA_Chars,     20,
               LA_ID,        11,
               GTST_String,  cfg_UMSGatewayName,
            TAG_DONE);

         LT_EndGroup(parthandle);

         LT_New(parthandle,
            LA_Type, HORIZONTAL_KIND,
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      CHECKBOX_KIND,
               LA_LabelText, "Operate as a Maus gateway",
               LA_ID,        9,
               LA_BOOL,      &mausbool,
            TAG_DONE);

         LT_EndGroup(parthandle);

      LT_EndGroup(parthandle);

   LT_EndGroup(parthandle);

   LT_New(parthandle,
      LA_Type, HORIZONTAL_KIND,
      LA_LabelText, "Switches",
      TAG_DONE);

      LT_New(parthandle,
         LA_Type, VERTICAL_KIND,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      CHECKBOX_KIND,
            LA_LabelText, "Keep origin when importing",
            LA_ID,        7,
            LA_BOOL,      &originbool,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      CHECKBOX_KIND,
            LA_LabelText, "Change MSGID to FidoNet-style",
            LA_ID,        8,
            LA_BOOL,      &msgidbool,
         TAG_DONE);

      LT_EndGroup(parthandle);

      LT_New(parthandle,
         LA_Type, VERTICAL_KIND,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      CHECKBOX_KIND,
            LA_LabelText, "Ignore domain on Origin line",
            LA_ID,        9,
            LA_BOOL,      &ignorebool,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      CHECKBOX_KIND,
            LA_LabelText, "Change empty To line to All",
            LA_ID,        12,
            LA_BOOL,      &emptybool,
         TAG_DONE);

      LT_EndGroup(parthandle);

   LT_EndGroup(parthandle);

   LT_EndGroup(parthandle);

   partwin=LT_Build(parthandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW|IDCMP_RAWKEY,
                               LAWN_Parent,   mainwin,
                               LAWN_HelpHook, &UMSHelpHook,
                               LAWN_Title,    "UMS",
                               WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                               TAG_END);

   if(!partwin)
   {
      LT_DeleteHandle(parthandle);
      DisplayBeep(NULL);
      return;
   }

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(parthandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;

         LT_ReplyIMsg(IntuiMessage);

         switch(class)
         {
            case IDCMP_RAWKEY:        if(code == TAB) LT_Activate(parthandle,1);
                                      break;

            case IDCMP_CLOSEWINDOW:   strcmpcpy(cfg_UMSName,(UBYTE *)LT_GetAttributes(parthandle,1,TAG_END));
                                      strcmpcpy(cfg_UMSPassword,(UBYTE *)LT_GetAttributes(parthandle,2,TAG_END));
                                      strcmpcpy(cfg_UMSServer,(UBYTE *)LT_GetAttributes(parthandle,3,TAG_END));
                                      strcmpcpy(cfg_DefaultOrigin,(UBYTE *)LT_GetAttributes(parthandle,4,TAG_END));
                                      strcmpcpy(cfg_GatewayName,(UBYTE *)LT_GetAttributes(parthandle,5,TAG_END));
                                      strcmpcpy(cfg_UMSGatewayName,(UBYTE *)LT_GetAttributes(parthandle,11,TAG_END));

                                      strcpy(nodebuf,(UBYTE *)LT_GetAttributes(parthandle,6,TAG_END));

                                      if(Parse4D(nodebuf,&n4d))
                                      {
                                         if(Compare4D(&cfg_GatewayNode,&n4d)!=0) saveconfig=TRUE;
                                         Copy4D(&cfg_GatewayNode,&n4d);
                                      }

                                      LT_DeleteHandle(parthandle);
                                      parthandle=NULL;
                                      cfg_Flags &= ~(CFG_UMSKEEPORIGIN|CFG_CHANGEUMSMSGID|CFG_UMSMAUSGATE|CFG_UMSIGNOREORIGINDOMAIN|CFG_UMSEMPTYTOALL);
                                      if(originbool) cfg_Flags |= CFG_UMSKEEPORIGIN;
                                      if(msgidbool) cfg_Flags |= CFG_CHANGEUMSMSGID;
                                      if(mausbool) cfg_Flags |= CFG_UMSMAUSGATE;
                                      if(ignorebool) cfg_Flags |= CFG_UMSIGNOREORIGINDOMAIN;
                                      if(emptybool) cfg_Flags |= CFG_UMSEMPTYTOALL;
                                      return;
            case IDCMP_GADGETUP:      switch(((struct Gadget *)iaddress)->GadgetID)
                                      {
                                         case 6:
                                          if(!Parse4D(nodebuf,&n4d))
                                          {
                                             DisplayBeep(NULL);
                                             LT_Activate(parthandle,6);
                                          }
                                          case 7:
                                          case 8:
                                             saveconfig=TRUE;
                                      }
                                      break;
         }
      }
      WaitPort(partwin->UserPort);
   }
}
