#include <CrashPrefs.h>

#include <cmshared/nodelist.h>

struct List routelist;

ULONG routenum,currentroute;
struct Route *routenode;

UBYTE routeakabuf[50];

UBYTE *loopmodes[]={"Ignore","Log","Log+Bad",NULL};
UBYTE testroute[30];

struct Hook RouteHelpHook = { {NULL}, HelpHookEntry,NULL,HELP_ROUTE};

void SetRoute(ULONG num)
{
   ULONG c;
   UBYTE buf[50];

   if(currentroute != -1)
   {
      struct Route *rt;
      struct Node4DPat n4dpat;

      rt=RouteList.First;
      for(c=0;c<currentroute;c++) rt=rt->Next;

      strcpy(buf,(UBYTE *)LT_GetAttributes(parthandle,50,TAG_END));

      if(Parse4DDestPat(buf,&n4dpat))
      {
         if(strcmp(rt->DestPat.Zone,n4dpat.Zone)!=0) saveconfig=TRUE;
         if(strcmp(rt->DestPat.Net,n4dpat.Net)!=0) saveconfig=TRUE;
         if(strcmp(rt->DestPat.Node,n4dpat.Node)!=0) saveconfig=TRUE;
         if(strcmp(rt->DestPat.Point,n4dpat.Point)!=0) saveconfig=TRUE;
         if(rt->DestPat.Type != n4dpat.Type) saveconfig=TRUE;
         
         Copy4DPat(&rt->DestPat,&n4dpat);
      }
   }

   if(routenum == 0)
   {
      LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,50,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,51,GA_Disabled,TRUE,TAG_END);
   }

   if(num!=-1)
   {
      if(routenum)
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,FALSE,TAG_END);
      }
      else
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
      }

      routenode=RouteList.First;
      for(c=0;c<num;c++) routenode=routenode->Next;

      if(num!=0)                 LT_SetAttributes(parthandle,12,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);

      if(num!=routenum-1)        LT_SetAttributes(parthandle,13,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);

      Print4DDestPat(&routenode->DestPat,buf);
      LT_SetAttributes(parthandle,50,GA_Disabled,FALSE,GTST_String,buf,TAG_END);

      sprintf(routeakabuf,"%lu:%lu/%lu.%lu",routenode->Aka->Node.Zone,routenode->Aka->Node.Net,routenode->Aka->Node.Node,routenode->Aka->Node.Point);
      LT_SetAttributes(parthandle,51,GA_Disabled,FALSE,GTTX_Text,routeakabuf,TAG_END);

      LT_SetAttributes(parthandle,4711,GTLV_Selected,num,TAG_END);
    }
   currentroute=num;
}

void RefreshRoutelist(void)
{
   struct TempNode *tempnode;
   struct Route *route;

   LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
   FreeTempNodes(&routelist);

   routenum=0;

   for(route=RouteList.First;route;route=route->Next)
   {
      if(!(tempnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(&routelist);
         return;
      }

      Print4DPat(&route->Pattern,tempnode->buf);
      tempnode->Node.ln_Name=tempnode->buf;
      AddTail(&routelist,(struct Node *)tempnode);
      routenum++;
   }

   LT_SetAttributes(parthandle,4711,GTLV_Labels,&routelist,TAG_END);
}

void AddRoute(void)
{
   struct Route *route;
   struct Node4DPat n4dpat;
   UBYTE buf[200];
   BOOL res;
   BOOL kg;

   kg=TRUE;
   buf[0]=0;

   while(kg)
   {
      res=rtGetString(buf,50,"Add route pattern",NULL,RT_Window,partwin,
                                                      RT_WaitPointer, TRUE,
                                                      RT_LockWindow, TRUE,
                                                      TAG_END);

      if(!res)
         return;

      kg=FALSE;

      if(!Parse4DPat(buf,&n4dpat))
      {
         rtEZRequestTags("Invalid node pattern","_Ok",NULL,NULL,RT_Underscore,'_',
                                                                RT_Window,partwin,
                                                                RT_WaitPointer, TRUE,
                                                                RT_LockWindow, TRUE,
                                                                TAG_END);
         kg=TRUE;
      }
   }

   if(!res)
      return;

   if(route=(struct Route *)AllocMem(sizeof(struct Route),MEMF_CLEAR))
   {
      jbAddNode(&RouteList,(struct jbNode *)route);
      Copy4DPat(&route->Pattern,&n4dpat);
      route->DestPat.Type=PAT_PATTERN;
      strcpy(route->DestPat.Zone,"0");
      strcpy(route->DestPat.Net,"0");
      strcpy(route->DestPat.Node,"0");
      strcpy(route->DestPat.Point,"0");
      route->Aka=AkaList.First;
      RefreshRoutelist();
      SetRoute(routenum-1);
      LT_SetAttributes(parthandle,4711,GTLV_Selected,currentroute,GTLV_MakeVisible,currentroute,TAG_END);
      saveconfig=TRUE;
   }
   else
   {
      DisplayBeep(NULL);
   }
}

void RemRoute(void)
{
   ULONG c,tmp;
   struct Route *route;
   UBYTE buf[100],buf2[100];
   BOOL res;

   route=RouteList.First;
   for(c=0;c<currentroute;c++) route=route->Next;

   Print4DPat(&route->Pattern,buf2);

   sprintf(buf,"Do you really want to remove the routing for \"%s\"?",
      buf2);
      
   res=rtEZRequestTags(buf,"_Remove|_Cancel",NULL,NULL,RT_Underscore,'_',
                                                       RT_Window,partwin,
                                                       RT_WaitPointer,TRUE,
                                                       RT_LockWindow, TRUE,
                                                       RTEZ_Flags, EZREQF_CENTERTEXT | EZREQF_NORETURNKEY,
                                                       TAG_END);

   if(!res)
      return;

   tmp=currentroute;
   SetRoute(-1);
   jbFreeNode(&RouteList,(struct jbNode *)route,sizeof(struct Route));
   RefreshRoutelist();
   if(tmp == routenum) tmp--;
   SetRoute(tmp);
   saveconfig=TRUE;
}

UBYTE testakabuf[50];
UBYTE testnodebuf[50];

iswildcard(UBYTE *str)
{
   UWORD c;

   for(c=0;c<strlen(str);c++)
      if(str[c]=='*' || str[c]=='?') return(TRUE);

   return(FALSE);
}

ExpandNodePat(struct Node4DPat *temproute,struct Node4D *dest,struct Node4D *sendto)
{
   switch(temproute->Type)
   {
      case PAT_PATTERN:
         sendto->Zone  = iswildcard(temproute->Zone)  ? dest->Zone  : atoi(temproute->Zone);
         sendto->Net   = iswildcard(temproute->Net)   ? dest->Net   : atoi(temproute->Net);
         sendto->Node  = iswildcard(temproute->Node)  ? dest->Node  : atoi(temproute->Node);
         sendto->Point = iswildcard(temproute->Point) ? dest->Point : atoi(temproute->Point);
         break;

      case PAT_ZONE:
         sendto->Zone = dest->Zone;
         sendto->Net = dest->Zone;
         sendto->Node = 0;
         sendto->Point = 0;
         break;

      case PAT_REGION:
         sendto->Zone = dest->Zone;
         sendto->Net = nlGetRegion(dest);
         sendto->Node = 0;
         sendto->Point = 0;

         if(sendto->Net == -1 || sendto->Net == 0)
            sendto->Net=dest->Net;

         break;

      case PAT_NET:
         sendto->Zone = dest->Zone;
         sendto->Net = dest->Net;
         sendto->Node = 0;
         sendto->Point = 0;
         break;

      case PAT_HUB:
         sendto->Zone = dest->Zone;
         sendto->Net = dest->Net;
         sendto->Node = nlGetHub(dest);
         sendto->Point = 0;

         if(sendto->Node == -1)
            sendto->Node=0;

         break;

      case PAT_NODE:
         sendto->Zone = dest->Zone;
         sendto->Net = dest->Net;
         sendto->Node = dest->Node;
         sendto->Point = 0;
         break;
   }
}

void TestRoute(void)
{
   struct Route *tmproute;
   struct Aka *aka;
   struct Node4D n4d,d4d;
   BOOL neednl;
   UBYTE errbuf[100];
   
   if(testroute[0]==0)
      return;

   Parse4D(testroute,&n4d);

   strcpy(testnodebuf,"No routing for node");
   strcpy(testakabuf,"No routing for node");

   for(aka=AkaList.First;aka;aka=aka->Next)
      if(Compare4D(&n4d,&aka->Node)==0)
      {
         strcpy(testnodebuf,"Imported");
         strcpy(testakabuf,"");
         LT_SetAttributes(parthandle,61,GTTX_Text,testnodebuf,TAG_END);
         LT_SetAttributes(parthandle,62,GTTX_Text,testakabuf,TAG_END);
         return;
      }

   neednl=FALSE;

   for(tmproute=RouteList.First;tmproute;tmproute=tmproute->Next)
   {
      if(tmproute->Pattern.Type == PAT_HUB || tmproute->Pattern.Type == PAT_REGION)
         neednl=TRUE;

      if(tmproute->DestPat.Type == PAT_HUB || tmproute->DestPat.Type == PAT_REGION)
         neednl=TRUE;
   }

   if(neednl)
   {
      if(cfg_NodeList[0]==0)
      {
         strcpy(testnodebuf,"Nodelist required");
         strcpy(testakabuf,"");
         LT_SetAttributes(parthandle,61,GTTX_Text,testnodebuf,TAG_END);
         LT_SetAttributes(parthandle,62,GTTX_Text,testakabuf,TAG_END);
         return;
      }
      else
      {
         if(!nlStart(errbuf))
         {
            strcpy(testnodebuf,"Nodelist error");
            strcpy(testakabuf,"");
            LT_SetAttributes(parthandle,61,GTTX_Text,testnodebuf,TAG_END);
            LT_SetAttributes(parthandle,62,GTTX_Text,testakabuf,TAG_END);
            return;
         }
      }
   }

   for(tmproute=RouteList.First;tmproute;tmproute=tmproute->Next)
      if(Compare4DPat(&tmproute->Pattern,&n4d)==0) break;

   if(tmproute)
   {
      ExpandNodePat(&tmproute->DestPat,&n4d,&d4d);
      sprintf(testakabuf,"%lu:%lu/%lu.%lu",tmproute->Aka->Node.Zone,tmproute->Aka->Node.Net,tmproute->Aka->Node.Node,tmproute->Aka->Node.Point);
      sprintf(testnodebuf,"%lu:%lu/%lu.%lu",d4d.Zone,d4d.Net,d4d.Node,d4d.Point);
   }

   if(neednl && cfg_NodeList[0]!=0)
      nlEnd();

   LT_SetAttributes(parthandle,61,GTTX_Text,testnodebuf,TAG_END);
   LT_SetAttributes(parthandle,62,GTTX_Text,testakabuf,TAG_END);
}

void PartRoute(void)
{
   ULONG class,code,tmp;
   struct IntuiMessage *IntuiMessage;
   struct TempNode *tempnode;
   APTR iaddress;
   ULONG c;
   UBYTE nodebuf[50];
   struct Node4DPat n4dpat;
   struct Node4D n4d;
   struct Aka *aka;

   NewList(&routelist);

   testroute[0]=0;

   currentroute=-1;

   parthandle = LT_CreateHandleTags(scr,LH_EditHook,&StrEditHook,
                                        LH_RawKeyFilter,FALSE,
                                        TAG_DONE);

   if(!parthandle)
   {
      FreeTempNodes(&routelist);
      return;
   }

   LT_New(parthandle,
      LA_Type,      HORIZONTAL_KIND,
      LA_LabelText, "Route",
      TAG_DONE);

      LT_New(parthandle,
         LA_Type,      VERTICAL_KIND,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      STRING_KIND,
            GTST_MaxChars,199,
            LA_ID,        4712,
            LAST_Link,    4711,
            GA_Disabled,TRUE,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,       LISTVIEW_KIND,
            LA_Chars,      20,
            LALV_Lines,    10,
            LALV_CursorKey,TRUE,
            LALV_Link,     4712,
            GTLV_Labels,   &routelist,
            LALV_LockSize, TRUE,
            LA_ID,4711,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      HORIZONTAL_KIND,
            LAGR_SameSize,TRUE,
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               LAGR_SameSize,TRUE,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Add...",
                  LA_Chars,10,
                  LA_ID,10,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Up",
                  GA_Disabled,TRUE,
                  LA_Chars,10,
                  LA_ID,12,
                  TAG_DONE);

            LT_EndGroup(parthandle);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               LAGR_SameSize,TRUE,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Remove...",
                  LA_Chars,10,
                  GA_Disabled,TRUE,
                  LA_ID,11,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Down",
                  LA_Chars,10,
                  GA_Disabled,TRUE,
                  LA_ID,13,
                  TAG_DONE);

            LT_EndGroup(parthandle);

         LT_EndGroup(parthandle);

      LT_EndGroup(parthandle);

      LT_New(parthandle,
         LA_Type,      VERTICAL_KIND,
         LA_ExtraSpace,TRUE,
         TAG_DONE);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               TAG_DONE);

            LT_New(parthandle,
               LA_Type,      STRING_KIND,
               LA_LabelText, "Route via",
               GTST_MaxChars,50,
               GA_Disabled,TRUE,
               LA_Chars,20,
               LA_ID,50,
               TAG_DONE);

            LT_New(parthandle,
               LA_Type,      TEXT_KIND,
               LA_LabelText, "Aka",
               GA_Disabled,TRUE,
               LA_Chars,20,
               LA_ID,51,
               LATX_Picker,TRUE,
               GTTX_Border,TRUE,
               TAG_DONE);

            LT_EndGroup(parthandle);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               LA_ExtraSpace,TRUE,
               LA_LabelText, "Test routing",
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      STRING_KIND,
                  LA_LabelText, "Destination",
                  GTST_MaxChars,50,
                  LA_Chars,20,
                  LA_ID,60,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      TEXT_KIND,
                  LA_LabelText, "Route via",
                  GTTX_Border,TRUE,
                  LA_Chars,20,
                  LA_ID,61,
                  TAG_DONE);


               LT_New(parthandle,
                  LA_Type,      TEXT_KIND,
                  LA_LabelText, "Aka",
                  GTTX_Border,TRUE,
                  LA_Chars,20,
                  LA_ID,62,
                  TAG_DONE);

            LT_EndGroup(parthandle);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               LA_ExtraSpace,TRUE,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      CYCLE_KIND,
                  LA_LabelText, "Loop-mail",
                  GTCY_Labels,  loopmodes,
                  LA_Chars,     20,
                  LA_ID,        70,
                  LA_UWORD,     &cfg_LoopMode,
                  TAG_DONE);

            LT_EndGroup(parthandle);

      LT_EndGroup(parthandle);

   LT_EndGroup(parthandle);

   partwin=LT_Build(parthandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW,
                               LAWN_Title,    "Routing",
                               WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                               LAWN_HelpHook, &RouteHelpHook,
                               TAG_END);

   if(!partwin)
   {
      LT_DeleteHandle(parthandle);
      DisplayBeep(NULL);
      FreeTempNodes(&routelist);
      return;
   }

   RefreshRoutelist();

   if(routenum)
   {
      LT_SetAttributes(parthandle,4711,GTLV_Selected,0,TAG_END);
      SetRoute(0);
   }

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(parthandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;

         LT_ReplyIMsg(IntuiMessage);

         switch(class)
         {
            case IDCMP_RAWKEY:         if(code == TAB) LT_Activate(parthandle,4712);
                                       break;
            case IDCMP_IDCMPUPDATE:    switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 51:
                                             aka=AskAka(partwin);

                                             if(aka)
                                             {
                                                saveconfig=TRUE;
                                                routenode->Aka=aka;
                                                sprintf(routeakabuf,"%lu:%lu/%lu.%lu",routenode->Aka->Node.Zone,routenode->Aka->Node.Net,routenode->Aka->Node.Node,routenode->Aka->Node.Point);
                                                LT_SetAttributes(parthandle,51,GA_Disabled,FALSE,GTTX_Text,routeakabuf,TAG_END);
                                                TestRoute();
                                             }
                                             break;
                                       }
                                       break;
            case IDCMP_GADGETUP:       switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 10:
                                             AddRoute();
                                             TestRoute();
                                             break;
                                          case 11:
                                             RemRoute();
                                             TestRoute();
                                             break;
                                          case 12:
                                             if(currentroute != 0)
                                             {
                                                tmp=currentroute;
                                                SetRoute(-1);
                                                MoveUp(&RouteList,tmp);
                                                RefreshRoutelist();
                                                SetRoute(tmp-1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentroute,
                                                   GTLV_MakeVisible,currentroute,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                                TestRoute();
                                             }
                                             break;
                                          case 13:
                                             if(currentroute != routenum-1)
                                             {
                                                tmp=currentroute;
                                                SetRoute(-1);
                                                MoveDown(&RouteList,tmp);
                                                RefreshRoutelist();
                                                SetRoute(tmp+1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentroute,
                                                   GTLV_MakeVisible,currentroute,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                                TestRoute();
                                             }
                                             break;
                                          case 50:
                                             if(!Parse4DDestPat(getgadgetstring((struct Gadget *)iaddress),&n4dpat))
                                             {
                                                DisplayBeep(NULL);
                                                LT_Activate(parthandle,50);
                                             }
                                             else
                                             {
                                                Copy4DPat(&routenode->DestPat,&n4dpat);
                                                Print4DDestPat(&n4dpat,nodebuf);
                                                LT_SetAttributes(parthandle,50,GTST_String,nodebuf,TAG_END);
                                                saveconfig=TRUE;
                                                TestRoute();
                                             }
                                             break;
                                          case 60:
                                             SetRoute(currentroute);

                                             if(Parse4D(getgadgetstring((struct Gadget *)iaddress),&n4d))
                                             {
                                                sprintf(testroute,"%lu:%lu/%lu.%lu",n4d.Zone,n4d.Net,n4d.Node,n4d.Point);
                                                LT_SetAttributes(parthandle,60,GTST_String,testroute,TAG_END);
                                                TestRoute();
                                             }
                                             else
                                             {
                                                DisplayBeep(NULL);
                                                LT_Activate(parthandle,60);
                                             }
                                             break;
                                          case 70:
                                             saveconfig=TRUE;
                                             break;
                                          case 4711:
                                             SetRoute(code);
                                             break;
                                          case 4712:
                                             if(!Parse4DPat(getgadgetstring((struct Gadget *)iaddress),&n4dpat))
                                             {
                                                DisplayBeep(NULL);
                                                LT_Activate(parthandle,4712);
                                             }
                                             else
                                             {
                                                Copy4DPat(&routenode->Pattern,&n4dpat);
                                                Print4DPat(&n4dpat,nodebuf);

                                                tempnode=routelist.lh_Head;
                                                for(c=0;c<currentroute;c++) tempnode=tempnode->Node.ln_Succ;

                                                LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
                                                strcpy(tempnode->buf,nodebuf);
                                                LT_SetAttributes(parthandle,4711,GTLV_Labels,&routelist,TAG_END);
                                                saveconfig=TRUE;
                                                TestRoute();
                                             }
                                             break;
                                       }
                                       break;
            case IDCMP_CLOSEWINDOW:    SetRoute(-1);
                                       LT_DeleteHandle(parthandle);
                                       parthandle=NULL;
                                       FreeTempNodes(&routelist);
                                       return;
         }
      }
      WaitPort(partwin->UserPort);
   }
}
