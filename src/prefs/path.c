#include <CrashPrefs.h>

struct Hook PathHelpHook = { {NULL}, HelpHookEntry,NULL,HELP_PATH};

#define PATHSIZE 39

UBYTE *pathnames[7]={"Inbound","Outbound","Packets","Statsfile","Temporary","Create packet","Nodelist"};
BOOL pathdir[7]={TRUE,TRUE,TRUE,FALSE,TRUE,TRUE,TRUE};
UBYTE *pathptrs[7]={cfg_Inbound,cfg_Outbound,cfg_PacketDir,cfg_StatsFile,cfg_TempDir,cfg_PacketCreate,cfg_NodeList};

UBYTE *nodelisttypes[]={"TrapList","Gotcha",NULL};

void PartPath(void)
{
   ULONG c;
   struct IntuiMessage *IntuiMessage;
   APTR iaddress;
   ULONG class,code,qualifier,gid;

   parthandle = LT_CreateHandleTags(scr,LH_EditHook,&StrEditHookNext,
                                        LH_RawKeyFilter,FALSE,
                                        TAG_DONE);

   if(!parthandle)
      return;

   LT_New(parthandle,
      LA_Type,      VERTICAL_KIND,
      LA_LabelText, "Paths",
      TAG_DONE);

   for(c=0;c<7;c++)
   {
      LT_New(parthandle,
         LA_Type,      STRING_KIND,
         LA_LabelText, pathnames[c],
         LA_Chars,     PATHSIZE,
         GTST_MaxChars,PATHSIZE,
         LA_ID,        c+1,
         GTST_String,  pathptrs[c],
         LAST_Picker,  TRUE,
      TAG_DONE);
   }

   LT_New(parthandle,
      LA_Type,      CYCLE_KIND,
      LA_LabelText, "Type",
      LA_Chars,     PATHSIZE,
      LA_ID,        10,
      GTCY_Labels,  nodelisttypes,
      GTCY_Active,  cfg_NodeListType-1,
      TAG_DONE);

   LT_EndGroup(parthandle);

   partwin=LT_Build(parthandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW|IDCMP_RAWKEY,
                               LAWN_Parent,   mainwin,
                               LAWN_HelpHook, &PathHelpHook,
                               LAWN_Title,    "Paths",
                               WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                               TAG_END);

   if(!partwin)
   {
      LT_DeleteHandle(parthandle);
      DisplayBeep(NULL);
      return;
   }

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(parthandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;
         qualifier=IntuiMessage->Qualifier;

         LT_ReplyIMsg(IntuiMessage);

         switch(class)
         {
            case IDCMP_RAWKEY:        if(code == TAB) LT_Activate(parthandle,1);
                                      break;

            case IDCMP_CLOSEWINDOW:   for(c=0;c<7;c++)
                                         strcmpcpy(pathptrs[c],(UBYTE *)LT_GetAttributes(parthandle,c+1,TAG_END));

                                      if(cfg_NodeListType-1 != LT_GetAttributes(parthandle,10,TAG_END)) 
                                      {
                                         saveconfig=TRUE;
                                         cfg_NodeListType=1+LT_GetAttributes(parthandle,10,TAG_END);
                                      }
                                       

                                      LT_DeleteHandle(parthandle);
                                      parthandle=NULL;
                                      return;

            case IDCMP_GADGETUP:      saveconfig=TRUE;
                                      break;

            case IDCMP_IDCMPUPDATE:   gid=((struct Gadget *)iaddress)->GadgetID;

                                      if(gid>=1 && gid<=7)
                                      {
                                         strcpy(pathptrs[gid-1],(UBYTE *)LT_GetAttributes(parthandle,gid,TAG_END));

                                         if(pathdir[gid-1])
                                            OpenReqDir(pathptrs[gid-1],PATHSIZE,"Please select directory",partwin);

                                         else
                                            OpenReq(pathptrs[gid-1],PATHSIZE,"Please select file...",partwin,FALSE);

                                         LT_SetAttributes(parthandle,gid,GTST_String,pathptrs[gid-1],TAG_END);
                                         saveconfig=TRUE;
                                      }
                                      break;
         }
      }
      WaitPort(partwin->UserPort);
   }
}
