#include <CrashPrefs.h>

void ChangeAreas(void);

#define CHARSET_ASCII 1

STRPTR *groups[]={"General","Nodes","AreaFix + Options","Banned",NULL};
STRPTR *areatypes[]={"Pass-through","UMS","*.msg",NULL};
STRPTR *areaflags[]={"Normal","Read-only","Write-only","Feed",NULL};

STRPTR *areaaddmodes[]={"Select from: All","Select from: With access",NULL};

STRPTR *areaexportchrs[]={"IBMPC","SWEDISH","MAC","ASCII","LATIN-1",NULL};
UBYTE *areaexportdata[]={AmigaToIbm,AmigaToSF7,AmigaToMac,(UBYTE *)CHARSET_ASCII,NULL,NULL};

STRPTR *areadefaultchrs[]={"IBMPC","SWEDISH","DEFAULT","MAC","LATIN-1",NULL};
UBYTE *areadefaultdata[]={IbmToAmiga,SF7ToAmiga,DefToAmiga,MacToAmiga,NULL,NULL};

struct List arealist,tossnodelist,bannednodelist;
ULONG areasnum,currentarea,currenttossnode,tossnodesnum,currentbannednode,bannednodesnum;
struct Area *areanode;

struct List areagrouplist;
struct List areaakalist;
struct List areanodelist;
struct List areanodeaccesslist;
struct List selareagrouplist;
struct List addbannedlist;

struct Menu *areamenus;

struct NewMenu areanewmenus[] = {
   {NM_TITLE,"Edit","",0,0,NULL},
   {NM_ITEM, "Change areas...","C",0,0,NULL},
   {NM_ITEM, (STRPTR)NM_BARLABEL,NULL,0,0,NULL},
   {NM_ITEM, "Duplicate area...","D",0,0,NULL},
   {NM_ITEM, (STRPTR)NM_BARLABEL,NULL,0,0,NULL},
   {NM_ITEM, "Add mandantory and new nodes...","A",0,0,NULL},
   {NM_ITEM, (STRPTR)NM_BARLABEL,NULL,0,0,NULL},
   {NM_ITEM, "Remove all areas in group...","R",0,0,NULL},
   {NM_ITEM, (STRPTR)NM_BARLABEL,NULL,0,0,NULL},
   {NM_ITEM, "Update descriptions...","P",0,0,NULL},
   {NM_TITLE,"Find","",0,0,NULL},
   {NM_ITEM, "Find...","F",0,0,NULL},
   {NM_ITEM, "Find next","N",0,0,NULL},
   {NM_ITEM, (STRPTR)NM_BARLABEL,NULL,0,0,NULL},
   {NM_ITEM, "Find unconfirmed","U",0,0,NULL},
   {NM_END,  "","",0,0,NULL}};

struct Hook AreaHelpHook = { {NULL}, HelpHookEntry,NULL,HELP_AREAS};
struct Hook ManyHelpHook = { {NULL}, HelpHookEntry,NULL,HELP_CHANGEMANY};

void RefreshTossNodelist(void)
{
   struct ConfigNode *cnode;
   struct TossNode *tossnode;
   struct ImportNode *importnode;
   struct TempNode *tnode;
   UBYTE buf[50];

   LT_SetAttributes(parthandle,5711,GTLV_Labels,~0,TAG_END);

   FreeTempNodes(&tossnodelist);
   tossnodesnum=0;

   if(areanode->Flags & AREA_NETMAIL)
   {
      for(importnode=areanode->TossNodes.First;importnode;importnode=importnode->Next)
      {
         if(!(tnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
         {
            FreeTempNodes(&tossnodelist);
            DisplayBeep(NULL);
            return;
         }

         sprintf(tnode->buf,"%lu:%lu/%lu.%lu",
            importnode->Node.Zone,
            importnode->Node.Net,
            importnode->Node.Node,
            importnode->Node.Point);

         tnode->Node.ln_Name=tnode->buf;
         AddTail(&tossnodelist,(struct Node *)tnode);
         tossnodesnum++;
      }
   }
   else
   {
      for(tossnode=areanode->TossNodes.First;tossnode;tossnode=tossnode->Next)
      {
         if(!(tnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
         {
            FreeTempNodes(&tossnodelist);
            DisplayBeep(NULL);
            return;
         }

         sprintf(buf,"%lu:%lu/%lu.%lu",
            tossnode->ConfigNode->Node.Zone,
            tossnode->ConfigNode->Node.Net,
            tossnode->ConfigNode->Node.Node,
            tossnode->ConfigNode->Node.Point);

         if(tossnode->Flags & TOSSNODE_READONLY) strcat(tnode->buf,"!");
         if(tossnode->Flags & TOSSNODE_WRITEONLY) strcat(tnode->buf,"@");
         if(tossnode->Flags & TOSSNODE_FEED) strcat(tnode->buf,"%");

         strcat(tnode->buf,buf);

         tnode->Node.ln_Name=tnode->buf;
         AddTail(&tossnodelist,(struct Node *)tnode);
         tossnodesnum++;
      }

      FreeTempNodes(&areanodelist);

      for(cnode=CNodeList.First;cnode;cnode=cnode->Next)
      {
         for(tossnode=areanode->TossNodes.First;tossnode;tossnode=tossnode->Next)
            if(tossnode->ConfigNode == cnode) break;

         if(!tossnode)
         {
            if(!(tnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
            {
               DisplayBeep(NULL);
               FreeTempNodes(&tossnodelist);
               FreeTempNodes(&areanodelist);
               return;
            }

            sprintf(tnode->buf,"%lu:%lu/%lu.%lu",cnode->Node.Zone,cnode->Node.Net,cnode->Node.Node,cnode->Node.Point);
            tnode->Node.ln_Name=tnode->buf;
            tnode->UserData=cnode;
            AddTail(&areanodelist,(struct Node *)tnode);
         }
      }
   }

   LT_SetAttributes(parthandle,5711,GTLV_Labels,&tossnodelist,TAG_END);
}

BOOL CheckFlags(UBYTE group,UBYTE *node);

void RefreshAreaNodeAccessList(void)
{
   struct ConfigNode *cnode;
   struct TossNode *tossnode;
   struct TempNode *tnode;

   FreeTempNodes(&areanodeaccesslist);

   for(cnode=CNodeList.First;cnode;cnode=cnode->Next)
   {
      for(tossnode=areanode->TossNodes.First;tossnode;tossnode=tossnode->Next)
         if(tossnode->ConfigNode == cnode) break;

      if(!tossnode && CheckFlags(areanode->Group,cnode->Groups))
      {
         if(!(tnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
         {
            DisplayBeep(NULL);
            FreeTempNodes(&areanodeaccesslist);
            return;
         }

         sprintf(tnode->buf,"%lu:%lu/%lu.%lu",cnode->Node.Zone,cnode->Node.Net,cnode->Node.Node,cnode->Node.Point);
         tnode->Node.ln_Name=tnode->buf;
         tnode->UserData=cnode;
         AddTail(&areanodeaccesslist,(struct Node *)tnode);
      }
   }
}

void SetTossNode(void)
{
   struct TossNode *tossnode;
   ULONG c;

   if(tossnodesnum)
   {
      LT_SetAttributes(parthandle,211,GA_Disabled,FALSE,TAG_DONE);

      if(!(areanode->Flags & AREA_NETMAIL))
      {
         tossnode=areanode->TossNodes.First;
         for(c=0;c<currenttossnode;c++) tossnode=tossnode->Next;

         c=0;
         if(tossnode->Flags & TOSSNODE_READONLY) c=1;
         if(tossnode->Flags & TOSSNODE_WRITEONLY) c=2;
         if(tossnode->Flags & TOSSNODE_FEED) c=3;

         LT_SetAttributes(parthandle,212,GA_Disabled,FALSE,TAG_DONE);
         LT_SetAttributes(parthandle,201,GA_Disabled,FALSE,TAG_DONE);
         LT_SetAttributes(parthandle,201,GTCY_Active,c,TAG_END);
      }
      else
      {
         LT_SetAttributes(parthandle,201,GA_Disabled,TRUE,TAG_DONE);
         LT_SetAttributes(parthandle,212,GA_Disabled,TRUE,TAG_DONE);
      }
   }
   else
   {
      LT_SetAttributes(parthandle,201,GA_Disabled,TRUE,TAG_DONE);
      LT_SetAttributes(parthandle,211,GA_Disabled,TRUE,TAG_DONE);
   }
}

void RefreshBannedNodelist(void)
{
   struct ConfigNode *cnode;
   struct TossNode *bannednode;
   struct TempNode *tnode;

   LT_SetAttributes(parthandle,6711,GTLV_Labels,~0,TAG_END);

   FreeTempNodes(&bannednodelist);
   bannednodesnum=0;

   for(bannednode=areanode->BannedNodes.First;bannednode;bannednode=bannednode->Next)
   {
      if(!(tnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         FreeTempNodes(&bannednodelist);
         DisplayBeep(NULL);
         return;
      }

      sprintf(tnode->buf,"%lu:%lu/%lu.%lu",
         bannednode->ConfigNode->Node.Zone,
         bannednode->ConfigNode->Node.Net,
         bannednode->ConfigNode->Node.Node,
         bannednode->ConfigNode->Node.Point);

      tnode->Node.ln_Name=tnode->buf;
      AddTail(&bannednodelist,(struct Node *)tnode);
      bannednodesnum++;
   }

   FreeTempNodes(&addbannedlist);

   for(cnode=CNodeList.First;cnode;cnode=cnode->Next)
   {
      for(bannednode=areanode->BannedNodes.First;bannednode;bannednode=bannednode->Next)
         if(bannednode->ConfigNode == cnode) break;

      if(!bannednode)
      {
         if(!(tnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
         {
            DisplayBeep(NULL);
            FreeTempNodes(&bannednodelist);
            FreeTempNodes(&addbannedlist);
            return;
         }

         sprintf(tnode->buf,"%lu:%lu/%lu.%lu",cnode->Node.Zone,cnode->Node.Net,cnode->Node.Node,cnode->Node.Point);
         tnode->Node.ln_Name=tnode->buf;
         tnode->UserData=cnode;
         AddTail(&addbannedlist,(struct Node *)tnode);
      }
   }

   LT_SetAttributes(parthandle,6711,GTLV_Labels,&bannednodelist,TAG_END);
}

void SetBannedNode(void)
{
   if((areanode->Flags & AREA_BAD) || (areanode->Flags & AREA_NETMAIL))
   {
      LT_SetAttributes(parthandle,510,GA_Disabled,TRUE,TAG_DONE);
      LT_SetAttributes(parthandle,511,GA_Disabled,TRUE,TAG_DONE);
   }
   else
   {
      LT_SetAttributes(parthandle,510,GA_Disabled,FALSE,TAG_DONE);

      if(bannednodesnum)
         LT_SetAttributes(parthandle,511,GA_Disabled,FALSE,TAG_DONE);

      else
        LT_SetAttributes(parthandle,511,GA_Disabled,TRUE,TAG_DONE);
   }
}

void RefreshArealist(void)
{
   struct TempNode *tempnode;
   struct Area *area;

   LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
   FreeTempNodes(&arealist);

   areasnum=0;

   for(area=AreaList.First;area;area=area->Next)
   {
      if(!(tempnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(&arealist);
         return;
      }
      strcpy(tempnode->buf,area->Tagname);
      tempnode->Node.ln_Name=tempnode->buf;
      AddTail(&arealist,(struct Node *)tempnode);
      areasnum++;
   }

   LT_SetAttributes(parthandle,4711,GTLV_Labels,&arealist,TAG_END);
}

UBYTE areaakabuf[50];
UBYTE areagroupbuf[100];

void SetAreaQuick(ULONG num)
{
   if(num!=0)                 LT_SetAttributes(parthandle,12,GA_Disabled,FALSE,TAG_END);
   else                       LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);

   if(num!=areasnum-1)        LT_SetAttributes(parthandle,13,GA_Disabled,FALSE,TAG_END);
   else                       LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);

   currentarea=num;
}

ULONG areaactivepage;

void SetArea(ULONG num)
{
   ULONG c;

   if(currentarea != -1)
   {
      if(areaactivepage == 0)
      {
         strcmpcpy(areanode->Path,(UBYTE *)LT_GetAttributes(parthandle,102,TAG_END));

         if(areanode->AreaType == AREATYPE_MSG)
         {
            NUMCMPCPY(areanode->KeepNum,106);
            NUMCMPCPY(areanode->KeepDays,107);
         }
      }
      if(areaactivepage == 2)
      {
         if((!(areanode->Flags & AREA_NETMAIL)) && stricmp(areanode->Tagname,"BAD")!=0)
            strcmpcpy(areanode->Description,(UBYTE *)LT_GetAttributes(parthandle,302,TAG_END));
      }
   }

   if(num!=-1)
   {
      areanode=AreaList.First;

      for(c=0;c<num;c++)
         areanode=areanode->Next;

      if(areanode->Flags & AREA_NETMAIL)
      {
         struct Area *a;
         ULONG netareas;

         netareas=0;

         for(a=AreaList.First;a;a=a->Next)
            if(a->Flags & AREA_NETMAIL) netareas++;

         if(netareas<=1)   LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
         else              LT_SetAttributes(parthandle,11,GA_Disabled,FALSE,TAG_END);

         LT_SetAttributes(parthandle,4712,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,104,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,199,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,201,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,300,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,301,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,302,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,303,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,210,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,211,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,212,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,310,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,311,GA_Disabled,TRUE,TAG_END);
         OnMenu(partwin,FULLMENUNUM(0,2,0));
         OffMenu(partwin,FULLMENUNUM(0,4,0));
      }
      else if(stricmp(areanode->Tagname,"BAD")==0)
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,104,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,199,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,300,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,301,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,302,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,303,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,201,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,210,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,211,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,212,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,310,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,311,GA_Disabled,TRUE,TAG_END);
         OffMenu(partwin,FULLMENUNUM(0,2,0));
         OffMenu(partwin,FULLMENUNUM(0,4,0));
      }
      else
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,101,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,104,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,199,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,201,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,300,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,301,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,302,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,303,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,210,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,211,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,212,GA_Disabled,FALSE,TAG_END);

         if(cfg_DupeMode == DUPE_IGNORE) LT_SetAttributes(parthandle,310,GA_Disabled,TRUE,TAG_END);
         else                            LT_SetAttributes(parthandle,310,GA_Disabled,FALSE,TAG_END);

         if(cfg_Flags & CFG_CHECKSEENBY) LT_SetAttributes(parthandle,311,GA_Disabled,FALSE,TAG_END);
         else                            LT_SetAttributes(parthandle,311,GA_Disabled,TRUE,TAG_END);

         OnMenu(partwin,FULLMENUNUM(0,2,0));
         OnMenu(partwin,FULLMENUNUM(0,4,0));
      }

      if(num!=0)                 LT_SetAttributes(parthandle,12,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);

      if(num!=areasnum-1)        LT_SetAttributes(parthandle,13,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);

      if(areanode->Flags & AREA_NETMAIL)
         LT_SetAttributes(parthandle,100,GTCB_Checked,TRUE,TAG_END);

      else
         LT_SetAttributes(parthandle,100,GTCB_Checked,FALSE,TAG_END);

      LT_SetAttributes(parthandle,101,GTCY_Active,areanode->AreaType,TAG_END);

      if(areanode->AreaType == 0)
      {
         LT_SetAttributes(parthandle,102,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,104,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,105,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,106,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,107,GA_Disabled,TRUE,TAG_END);
      }
      else
      {
         LT_SetAttributes(parthandle,102,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,104,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,105,GA_Disabled,FALSE,TAG_END);

         if(areanode->AreaType == AREATYPE_MSG)
         {
            LT_SetAttributes(parthandle,106,GA_Disabled,FALSE,TAG_END);
            LT_SetAttributes(parthandle,107,GA_Disabled,FALSE,TAG_END);
         }
         else
         {
            LT_SetAttributes(parthandle,106,GA_Disabled,TRUE,TAG_END);
            LT_SetAttributes(parthandle,107,GA_Disabled,TRUE,TAG_END);
         }
      }

      LT_SetAttributes(parthandle,102,GTST_String,areanode->Path,TAG_END);

      sprintf(areaakabuf,"%lu:%lu/%lu.%lu",
         areanode->Aka->Node.Zone,
         areanode->Aka->Node.Net,
         areanode->Aka->Node.Node,
         areanode->Aka->Node.Point);

      LT_SetAttributes(parthandle,103,GTTX_Text,areaakabuf,TAG_END);

      for(c=0;areaexportchrs[c];c++)
         if(areaexportdata[c]==areanode->ExportCHRS) break;

      LT_SetAttributes(parthandle,104,GTCY_Active,c,TAG_END);

      for(c=0;areadefaultchrs[c];c++)
         if(areadefaultdata[c]==areanode->DefaultCHRS) break;

      LT_SetAttributes(parthandle,105,GTCY_Active,c,TAG_END);

      LT_SetAttributes(parthandle,106,GTIN_Number,areanode->KeepNum,TAG_END);
      LT_SetAttributes(parthandle,107,GTIN_Number,areanode->KeepDays,TAG_END);

      if(areanode->Flags & AREA_MANDATORY) LT_SetAttributes(parthandle,300,GTCB_Checked,TRUE,TAG_END);
      else                                 LT_SetAttributes(parthandle,300,GTCB_Checked,FALSE,TAG_END);

      if(areanode->Flags & AREA_DEFREADONLY) LT_SetAttributes(parthandle,301,GTCB_Checked,TRUE,TAG_END);
      else                                   LT_SetAttributes(parthandle,301,GTCB_Checked,FALSE,TAG_END);

      if(areanode->Flags & AREA_UNCONFIRMED) LT_SetAttributes(parthandle,199,GTCB_Checked,TRUE,TAG_END);
      else                                   LT_SetAttributes(parthandle,199,GTCB_Checked,FALSE,TAG_END);

      if(areanode->Flags & AREA_IGNOREDUPES) LT_SetAttributes(parthandle,310,GTCB_Checked,TRUE,TAG_END);
      else                                   LT_SetAttributes(parthandle,310,GTCB_Checked,FALSE,TAG_END);

      if(areanode->Flags & AREA_IGNORESEENBY) LT_SetAttributes(parthandle,311,GTCB_Checked,TRUE,TAG_END);
      else                                    LT_SetAttributes(parthandle,311,GTCB_Checked,FALSE,TAG_END);

      LT_SetAttributes(parthandle,302,GTST_String,areanode->Description,TAG_END);

      if(areanode->Group) sprintf(areagroupbuf,"%lc: %s",areanode->Group,cfg_GroupNames[areanode->Group-'A']);
      else                strcpy(areagroupbuf,"");

      LT_SetAttributes(parthandle,303,GTTX_Text,areagroupbuf,TAG_END);

      currenttossnode=0;
      currentbannednode=0;
      RefreshTossNodelist();
      RefreshBannedNodelist();
      SetTossNode();
      SetBannedNode();
      LT_SetAttributes(parthandle,5711,GTLV_Selected,currenttossnode,GTLV_MakeVisible,currenttossnode,TAG_DONE);
      LT_SetAttributes(parthandle,6711,GTLV_Selected,currentbannednode,GTLV_MakeVisible,currentbannednode,TAG_DONE);
   }
 
   currentarea=num;
}

/********************************** Sort *******************************/

struct SortStat
{
   struct SortStat *Next;
   UBYTE buf[200];
   struct Area *area;
};

int CompareAlpha(struct SortStat **s1, struct SortStat **s2)
{
   return(stricmp((*s1)->buf,(*s2)->buf));
}

void SortAreas(void)
{
   struct jbList SortList;
   struct Area *area;
   ULONG nc,alloc;
   struct SortStat *ss,*buf,**work;
   BOOL res;

   res=rtEZRequestTags("How do you want to sort the areas?","_Tagname|_Group|_Cancel",NULL,NULL,RT_Underscore,'_',
         RT_Window,partwin,
         RT_WaitPointer,TRUE,
         RT_LockWindow, TRUE,
         TAG_END);

   if(!res)
      return;

   jbNewList(&SortList);

   for(area=AreaList.First;area;area=area->Next)
   {
      if(!(ss=(struct SortStat *)AllocMem(sizeof(struct SortStat),MEMF_CLEAR)))
      {
         jbFreeList(&SortList,SortList.First,sizeof(struct SortStat));
         return;
      }

      if(area->Flags & AREA_NETMAIL)
         sprintf(ss->buf,"A %s",area->Tagname);

      else if(area->Flags & AREA_BAD)
         sprintf(ss->buf,"B %s",area->Tagname);

      else if(area->Flags & AREA_DEFAULT)
         sprintf(ss->buf,"C %s",area->Tagname);

      else
      {
         if(res == 1)
         {
               sprintf(ss->buf,"D %s",area->Tagname);
         }
         else
         {
            if(area->Group)
               sprintf(ss->buf,"D%lc%s",area->Group,area->Tagname);

            else
               sprintf(ss->buf,"D %s",area->Tagname);
         }
      }

      ss->area=area;

      jbAddNode(&SortList,(struct jbNode *)ss);
   }

   nc=0;

   for(ss=SortList.First;ss;ss=ss->Next)
      nc++;

   if(nc==0)
      return;

   alloc=nc*4;

   if(!(buf=(struct SortStat **)AllocMem(alloc,MEMF_ANY)))
   {
      return;
   }

   work=buf;

   for(ss=SortList.First;ss;ss=ss->Next)
      *work++=ss;

   qsort(buf,nc,4,CompareAlpha);

   jbNewList(&AreaList);

   for(work=buf;nc--;)
   {
      jbAddNode(&AreaList,(struct jbNode *)(*work)->area);
      work++;
   }

   FreeMem(buf,alloc);
   jbFreeList(&SortList,SortList.First,sizeof(struct SortStat));

   RefreshArealist();
   LT_SetAttributes(parthandle,4711,GTLV_Selected,0,GTLV_MakeVisible,0,TAG_END);
   SetArea(0);
}


MakeSortStat(UBYTE *dest,UBYTE *before,UBYTE *after,UBYTE *desc)
{
   UBYTE p;
   UBYTE part1[80],part2[80];
   UBYTE *t1,*t2;

   if(strlen(desc)<=30)
   {
      sprintf(dest,"%s %-30s %s\x0d",before,desc,after);
   }
   else
   {
      p=35;
      while(desc[p]!=32 && p>0) p--;
      if(p==0) p=30;

      strncpy(part1,desc,p);
      part1[p]=0;

      if(desc[p]==32) p++;

      strncpy(part2,&desc[p],30);
      part2[30]=0;

      t1="                                                        ";
      t2="                                                        ";

      t1[strlen(before)]=0;
      t2[strlen(after)]=0;

      sprintf(dest,"%s %-30s %s\x0d"
                   "%s --> %-26s %s\x0d",before,part1,after,t1,part2,t2);
   }
}


/********************************** Main *******************************/

BOOL CheckDupeArea(UBYTE *buf,ULONG num)
{
   struct Area *area;
   ULONG c;

   for(area=AreaList.First,c=0;area;area=area->Next,c++)
      if(stricmp(buf,area->Tagname)==0 && c!=num) break;

   if(area)
   {
      rtEZRequestTags("You already have an area with that tagname","_Ok",NULL,NULL,
                                                                  RT_Underscore,'_',
                                                                  RT_Window,partwin,
                                                                  RT_WaitPointer, TRUE,
                                                                  RT_LockWindow, TRUE,
                                                                  TAG_END);
      return(FALSE);
   }

   return(TRUE);
}

UBYTE *Last(UBYTE *str,ULONG max)
{
   if(strlen(str) <= max)
      return(str);

   else
      return(&str[strlen(str)-max]);
}

void AddTossNode(struct Area *area,struct ConfigNode *cnode,UWORD flags)
{
   struct TossNode *tnode;

   /* Check if it already exists */

   for(tnode=(struct TossNode *)area->TossNodes.First;tnode;tnode=tnode->Next)
      if(tnode->ConfigNode == cnode) return;

   if(!(tnode=(struct TossNode *)AllocMem(sizeof(struct TossNode),MEMF_CLEAR)))
      return;

   jbAddNode((struct jbList *)&area->TossNodes,(struct jbNode *)tnode);
   tnode->ConfigNode=cnode;
   tnode->Flags=flags;
}

void AddArea(void)
{
   BOOL res;
   struct Area *area,*defarea;
   UBYTE buf[80];
   struct TossNode *tnode;

   for(defarea=AreaList.First;defarea;defarea=defarea->Next)
      if(stricmp(defarea->Tagname,"DEFAULT")==0) break;

   buf[0]=0;

   res=rtGetString(buf,79,"Add area",NULL,
            RTGS_TextFmt,"Enter the tagname of the area you want to add. Press\n"
                         "Echomail to make an echomail area or netmail to make\n"
                         "a netmail area",
            RTGS_Flags, GSREQF_CENTERTEXT,
            RT_Window,  partwin,
            RT_WaitPointer, TRUE,
            RT_LockWindow, TRUE,
            RT_Underscore,'_',
            RTGS_GadFmt,"_Echomail|_Netmail|_Cancel",
            TAG_END);

   if(!res)
      return;

   if(!CheckDupeArea(buf,-1))
      return;

   if(area=(struct Aka *)AllocMem(sizeof(struct Area),MEMF_CLEAR))
   {
      jbAddNode(&AreaList,(struct jbNode *)area);

      jbNewList(&area->TossNodes);
      jbNewList(&area->BannedNodes);

      strcpy(area->Tagname,buf);
      if(res == 2) area->Flags |= AREA_NETMAIL;

      area->Aka=AkaList.First;

      if(defarea)
      {
         if(defarea->AreaType==AREATYPE_MSG)
         {
            ULONG c;
            UBYTE *forbiddenchars="\"#'`()*,/:;<>|";
            UBYTE buf[100];

            area->KeepNum=defarea->KeepNum;
            area->KeepDays=defarea->KeepDays;

            for(c=0;buf[c]!=0;c++)
               if(buf[c]<33 || buf[c]>126 || strchr(forbiddenchars,buf[c]))
                  buf[c]='_';

            strcpy(area->Path,defarea->Path);
            AddPart(area->Path,Last(buf,29),80);
         }
         else if(defarea->AreaType==AREATYPE_UMS)
         {
            strcpy(area->Path,defarea->Path);
            strcat(area->Path,buf);
         }

         area->AreaType=defarea->AreaType;
         area->DefaultCHRS=defarea->DefaultCHRS;
         area->ExportCHRS=defarea->ExportCHRS;

         if(area->Flags & AREA_NETMAIL)
         {
            if(area->AreaType == 0)
               area->AreaType=AREATYPE_UMS;
         }
         else
         {
            strcpy(area->Description,defarea->Description);

            if(defarea->Flags & AREA_MANDATORY)
               area->Flags |= AREA_MANDATORY;

            if(defarea->Flags & AREA_DEFREADONLY)
               area->Flags |= AREA_DEFREADONLY;

            if(defarea->Flags & AREA_IGNOREDUPES)
               area->Flags |= AREA_IGNOREDUPES;

            if(defarea->Flags & AREA_IGNORESEENBY)
               area->Flags |= AREA_IGNORESEENBY;
         }

         area->KeepDays=defarea->KeepDays;
         area->KeepNum=defarea->KeepNum;

         for(tnode=(struct TossNode *)defarea->TossNodes.First;tnode;tnode=tnode->Next)
            AddTossNode(area,tnode->ConfigNode,tnode->Flags);
      }

      if(stricmp(area->Tagname,"DEFAULT")==0 || strnicmp(area->Tagname,"DEFAULT_",8)==0)
         area->Flags |= AREA_DEFAULT;

      RefreshArealist();
      SetArea(areasnum-1);
      LT_SetAttributes(parthandle,4711,GTLV_Selected,currentarea,GTLV_MakeVisible,currentarea,TAG_END);
      saveconfig=TRUE;
   }
   else
   {
      DisplayBeep(NULL);
   }
}

void RemArea(void)
{
   UBYTE buf[200];
   BOOL res;
   ULONG tmp;

   sprintf(buf,"Do you really want to remove the area \"%s\"?",areanode->Tagname);

   res=rtEZRequestTags(buf,"_Remove|_Cancel",NULL,NULL,RT_Underscore,'_',
                                                       RT_Window,partwin,
                                                       RT_WaitPointer,TRUE,
                                                       RT_LockWindow, TRUE,
                                                       RTEZ_Flags, EZREQF_CENTERTEXT | EZREQF_NORETURNKEY,
                                                       TAG_END);

   if(!res)
      return;

   if(areanode->Flags & AREA_NETMAIL) jbFreeList(&areanode->TossNodes,areanode->TossNodes.First,sizeof(struct ImportNode));
   else                               jbFreeList(&areanode->TossNodes,areanode->TossNodes.First,sizeof(struct TossNode));

   jbFreeList(&areanode->BannedNodes,areanode->BannedNodes.First,sizeof(struct BannedNode));

   tmp=currentarea;
   SetArea(-1);

   jbFreeNode(&AreaList,(struct jbNode *)areanode,sizeof(struct Area));

   if(tmp == areasnum-1)
      tmp--;

   RefreshArealist();
   SetArea(tmp);
   saveconfig=TRUE;
}

void DuplicateArea(void)
{
   BOOL res;
   struct Area *area;
   struct TossNode *tnode,*tmptnode;
   struct BannedNode *bnode,*tmpbnode;
   struct ImportNode *inode,*tmpinode;
   UBYTE buf[80];

   strcpy(buf,areanode->Tagname);

   res=rtGetString(buf,79,"Enter name for new area",NULL,
            RT_Window,  partwin,
            RT_WaitPointer, TRUE,
            RT_LockWindow, TRUE,
            TAG_END);

   if(!res)
      return;

   if(!CheckDupeArea(buf,-1))
      return;

   if(!(area=AllocMem(sizeof(struct Area),MEMF_CLEAR)))
   {
      DisplayBeep(NULL);
      return;
   }

   CopyMem(areanode,area,sizeof(struct Area));

   area->Next=NULL;
   strcpy(area->Tagname,buf);

   if(area->AreaType == 0)
      area->Path[0]=0;

   else
      strncat(area->Path,"2",79);

   jbNewList(&area->TossNodes);
   jbNewList(&area->BannedNodes);

   if(area->Flags & AREA_NETMAIL)
   {
      for(inode=areanode->TossNodes.First;inode;inode=inode->Next)
      {
         if(!(tmpinode=AllocMem(sizeof(struct ImportNode),MEMF_CLEAR)))
         {
            jbFreeList(&area->TossNodes,area->TossNodes.First,sizeof(struct ImportNode));
            FreeMem(area,sizeof(struct Area));
         }
         Copy4D(&tmpinode->Node,&inode->Node);
         jbAddNode(&area->TossNodes,(struct jbNode *)tmpinode);
      }
   }
   else
   {
      for(tnode=areanode->TossNodes.First;tnode;tnode=tnode->Next)
      {
         if(!(tmptnode=AllocMem(sizeof(struct TossNode),MEMF_CLEAR)))
         {
            jbFreeList(&area->TossNodes,area->TossNodes.First,sizeof(struct TossNode));
            FreeMem(area,sizeof(struct Area));
         }
         tmptnode->ConfigNode=tnode->ConfigNode;
         tmptnode->Flags=tnode->Flags;
         jbAddNode(&area->TossNodes,(struct jbNode *)tmptnode);
      }

      for(bnode=areanode->BannedNodes.First;bnode;bnode=bnode->Next)
      {
         if(!(tmpbnode=AllocMem(sizeof(struct BannedNode),MEMF_CLEAR)))
         {
            jbFreeList(&area->TossNodes,area->TossNodes.First,sizeof(struct TossNode));
            jbFreeList(&area->BannedNodes,area->BannedNodes.First,sizeof(struct BannedNode));
            FreeMem(area,sizeof(struct Area));
         }
         tmpbnode->ConfigNode=bnode->ConfigNode;
         jbAddNode(&area->BannedNodes,(struct jbNode *)tmpbnode);
      }
   }

   jbAddNode(&AreaList,(struct jbNode *)area);
   RefreshArealist();
   SetArea(areasnum-1);
   LT_SetAttributes(parthandle,4711,GTLV_Selected,currentarea,GTLV_MakeVisible,currentarea,TAG_END);
   saveconfig=TRUE;
}

void RemoveGroup(void)
{
   BOOL res;
   struct Area *a,*a2;
   ULONG num,gr;
   UBYTE buf[100];

   res=AskList(&areagrouplist,"Select group",partwin);

   if(res==-1)
      return;

   num=0;

   for(a=AreaList.First;a;a=a->Next)
      if(a->Group == res+'A')
         if(!(a->Flags & AREA_BAD) && !(a->Flags & AREA_NETMAIL) && !(a->Flags & AREA_DEFAULT))
            num++;

   gr=res+'A';

   if(num == 0)
   {
      sprintf(buf,"There are no areas in that group");

      res=rtEZRequestTags(buf,"_Proceed",NULL,NULL,RT_Underscore,'_',
                                                   RT_Window,partwin,
                                                   RT_WaitPointer,TRUE,
                                                   RT_LockWindow, TRUE,
                                                   TAG_END);
   }
   else
   {
      sprintf(buf,"This would remove %lu areas. Are you sure?",num);

      res=rtEZRequestTags(buf,"_Remove|_Cancel",NULL,NULL,RT_Underscore,'_',
                                                          RT_Window,partwin,
                                                          RT_WaitPointer,TRUE,
                                                          RT_LockWindow, TRUE,
                                                          RTEZ_Flags, EZREQF_CENTERTEXT | EZREQF_NORETURNKEY,
                                                          TAG_END);
      if(!res)
         return;

      SetArea(-1);

      a=AreaList.First;

      while(a)
      {
         a2=a->Next;

         if(a->Group == gr)
         {
            if(!(a->Flags & AREA_BAD) && !(a->Flags & AREA_NETMAIL) && !(a->Flags & AREA_DEFAULT))
            {
               if(a->Flags & AREA_NETMAIL)
                  jbFreeList(&a->TossNodes,a->TossNodes.First,sizeof(struct ImportNode));

               else
                  jbFreeList(&a->TossNodes,a->TossNodes.First,sizeof(struct TossNode));

               jbFreeList(&a->BannedNodes,a->BannedNodes.First,sizeof(struct BannedNode));

               jbFreeNode(&AreaList,(struct jbNode *)a,sizeof(struct Area));
            }
         }
         a=a2;
      }

      RefreshArealist();
      LT_SetAttributes(parthandle,4711,GTLV_Selected,0,TAG_END);
      SetArea(0);
      saveconfig=TRUE;
   }
}

void UpdateDescriptions(void)
{
   struct Arealist *arealist;
   UBYTE buf[200];
   ULONG c,d;
   BPTR fh;
   struct Area *area;
   BOOL res;
   
   res=rtEZRequestTags("This will change the descriptions for your areas\nDo you want to continue?","_Ok|_Cancel",NULL,NULL,
         RT_Underscore,'_',
         RT_Window,partwin,
         RT_WaitPointer,TRUE,
         RT_LockWindow, TRUE,
         TAG_END);

   if(!res)
      return;

   LT_LockWindow(partwin);

   for(arealist=ArealistList.First;arealist;arealist=arealist->Next)
   {
      if(arealist->Flags & AREALIST_DESC)
      {
         if(fh=Open(arealist->AreaFile,MODE_OLDFILE))
         {
            while(FGets(fh,buf,199))
            {
               for(c=0;buf[c]>32;c++);

               if(buf[c]!=0)
               {
                  buf[c]=0;

                  for(area=AreaList.First;area;area=area->Next)
                  {
                     if(stricmp(buf,area->Tagname)==0)
                     {
                        c++;
                        while(buf[c]<=32 && buf[c]!=0) c++;

                        if(buf[c]!=0)
                        {
                           d=0;
                           while(buf[c]!=0 && buf[c]!=10 && buf[c]!=13 && d<77) area->Description[d++]=buf[c++];
                           area->Description[d]=0;
                        }
                     }
                  }
               }
            }

            Close(fh);
         }
      }
   }

   LT_UnlockWindow(partwin);
   SetArea(0);
   saveconfig=TRUE;
}

void AddMandantoryNewNodes(void)
{
   struct ConfigNode *cnode;
   struct TossNode *tnode,*tmptnode;
   ULONG added;
   UBYTE buf[100];

   added=0;

   for(cnode=CNodeList.First;cnode;cnode=cnode->Next)
   {
      for(tnode=areanode->TossNodes.First;tnode;tnode=tnode->Next)
         if(tnode->ConfigNode == cnode) break;

      if(!tnode)
      {
         if(areanode->Group && (areanode->Flags & AREA_MANDATORY) && CheckFlags(areanode->Group,cnode->Groups))
         {
            if(tmptnode=AllocMem(sizeof(struct TossNode),MEMF_CLEAR))
            {
               tmptnode->ConfigNode=cnode;

               if((areanode->Flags & AREA_DEFREADONLY) || CheckFlags(areanode->Group,cnode->ReadOnlyGroups))
                  tmptnode->Flags|=TOSSNODE_READONLY;

               jbAddNode(&areanode->TossNodes,(struct jbNode *)tmptnode);
               added++;
            }
         }
         else if(areanode->Group && CheckFlags(areanode->Group,cnode->Groups))
         {
            if(tmptnode=AllocMem(sizeof(struct TossNode),MEMF_CLEAR))
            {
               tmptnode->ConfigNode=cnode;

               if((areanode->Flags & AREA_DEFREADONLY) || CheckFlags(areanode->Group,cnode->ReadOnlyGroups))
                  tmptnode->Flags|=TOSSNODE_READONLY;

               jbAddNode(&areanode->TossNodes,(struct jbNode *)tmptnode);
               added++;
            }
         }
      }
   }

   if(added)
   {
      sprintf(buf,"%lu nodes were added",added);
      saveconfig=TRUE;
   }
   else
      strcpy(buf,"No nodes were added");

   rtEZRequestTags(buf,"Ok",NULL,NULL,
      RT_Underscore,'_',
      RT_Window,partwin,
      RT_WaitPointer,TRUE,
      RT_LockWindow, TRUE,
      RTEZ_Flags, EZREQF_CENTERTEXT,
      TAG_END);

   RefreshArealist();
   SetArea(currentarea);
   saveconfig=TRUE;
}


UBYTE findbuf[100];

void FindNext(void);

void Find(void)
{
   BOOL res;

   res=rtGetString(findbuf,99,"Enter string to find",NULL,
            RT_Window,      partwin,
            RT_WaitPointer, TRUE,
            RT_LockWindow,  TRUE,
            TAG_END);

   if(!res)
      return;

   FindNext();
}

UBYTE MakeSmall(UBYTE *src,UBYTE *dest)
{
   ULONG c;

   for(c=0;c<strlen(src);c++)
      dest[c]=ToLower(src[c]);

   dest[c]=0;
}

void FindNext(void)
{
   struct Area *a;
   ULONG num;
   UBYTE findtemp[100];
   UBYTE tagtemp[100];

   if(!findbuf[0])
   {
      Find();
      return;
   }

   a=areanode->Next;
   num=currentarea+1;

   MakeSmall(findbuf,findtemp);

   while(a)
   {
      MakeSmall(a->Tagname,tagtemp);

      if(strstr(tagtemp,findtemp))
      {
         break;
      }
      else
      {
         a=a->Next;
         num++;
      }
   }

   if(a)
   {
      LT_SetAttributes(parthandle,4711,GTLV_Selected,num,GTLV_MakeVisible,num,TAG_END);
      SetArea(num);
   }
   else
   {
      DisplayBeep(NULL);
   }
}

void FindUnconfirmed(void)
{
   struct Area *a;
   ULONG num;

   a=areanode->Next;
   num=currentarea+1;

   while(a)
   {
      if(a->Flags & AREA_UNCONFIRMED)
      {
         break;
      }
      else
      {
         a=a->Next;
         num++;
      }
   }

   if(a)
   {
      LT_SetAttributes(parthandle,4711,GTLV_Selected,num,GTLV_MakeVisible,num,TAG_END);
      SetArea(num);
   }
   else
   {
      DisplayBeep(NULL);
   }
}

void PartAreas(void)
{
   ULONG class,code,num;
   APTR iaddress;
   struct MenuItem *menuitem;
   struct IntuiMessage *IntuiMessage;
   struct Aka *aka;
   LONG c,res;
   struct TempNode *tempnode;
   struct TossNode *tossnode;
   struct BannedNode *bannednode;
   struct ImportNode *importnode;
   struct ConfigNode *cnode;
   struct Node4D n4d;
   UBYTE buf[50];
   ULONG tmp;

   NewList(&arealist);
   NewList(&tossnodelist);
   NewList(&bannednodelist);
   NewList(&areaakalist);
   NewList(&areagrouplist);
   NewList(&selareagrouplist);
   NewList(&areanodelist);
   NewList(&areanodeaccesslist);
   NewList(&addbannedlist);

   for(c=-1;c<26;c++)
   {
      if(!(tempnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(&selareagrouplist);
         return;
      }

      if(c==-1) strcpy(tempnode->buf,"<No group>");
      else      sprintf(tempnode->buf,"%lc: %s",'A'+c,cfg_GroupNames[c]);

      tempnode->Node.ln_Name=tempnode->buf;
      AddTail(&selareagrouplist,(struct Node *)tempnode);
   }

   for(c=0;c<26;c++)
   {
      if(!(tempnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(&areagrouplist);
         FreeTempNodes(&selareagrouplist);
         return;
      }

      sprintf(tempnode->buf,"%lc: %s",'A'+c,cfg_GroupNames[c]);
      tempnode->Node.ln_Name=tempnode->buf;
      AddTail(&areagrouplist,(struct Node *)tempnode);
   }

   for(aka=AkaList.First;aka;aka=aka->Next)
   {
      if(!(tempnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(&areaakalist);
         FreeTempNodes(&areagrouplist);
         FreeTempNodes(&selareagrouplist);
         return;
      }
      sprintf(tempnode->buf,"%lu:%lu/%lu.%lu",aka->Node.Zone,aka->Node.Net,aka->Node.Node,aka->Node.Point);
      tempnode->Node.ln_Name=tempnode->buf;
      AddTail(&areaakalist,(struct Node *)tempnode);
   }

   parthandle = LT_CreateHandleTags(scr,LH_EditHook,&StrEditHook,LH_RawKeyFilter,FALSE,TAG_DONE);

   if(!parthandle)
   {
      FreeTempNodes(&arealist);
      FreeTempNodes(&tossnodelist);
      FreeTempNodes(&bannednodelist);
      FreeTempNodes(&areaakalist);
      FreeTempNodes(&areagrouplist);
      FreeTempNodes(&selareagrouplist);
      FreeTempNodes(&areanodelist);
      FreeTempNodes(&areanodeaccesslist);
      return;
   }

   areamenus=LT_LayoutMenus(parthandle,areanewmenus,GTMN_FrontPen,0L,
                                                    GTMN_TextAttr,scr->Font,
                                                    GTMN_NewLookMenus,TRUE,
                                                    LH_MenuGlyphs,TRUE,
                                                    TAG_DONE);
	if(!areamenus)
   {
      FreeTempNodes(&arealist);
      FreeTempNodes(&tossnodelist);
      FreeTempNodes(&bannednodelist);
      FreeTempNodes(&areaakalist);
      FreeTempNodes(&areagrouplist);
      FreeTempNodes(&selareagrouplist);
      FreeTempNodes(&areanodelist);
      FreeTempNodes(&areanodeaccesslist);
      LT_DeleteHandle(parthandle);
      DisplayBeep(NULL);
      return;
   }

   LT_New(parthandle,
      LA_Type,      HORIZONTAL_KIND,
      LA_LabelText, "Areas",
      TAG_DONE);

      LT_New(parthandle,
         LA_Type,      VERTICAL_KIND,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      STRING_KIND,
            GTST_MaxChars,79,
            LA_ID,        4712,
            LAST_Link,    4711,
            GA_Disabled,TRUE,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,       LISTVIEW_KIND,
            LA_Chars,      22,
            GTLV_Labels,   &arealist,
            LALV_LockSize, TRUE,
            LALV_Lines,    12,
            LALV_CursorKey,TRUE,
            LALV_Link,     4712,
            LA_ID,4711,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      HORIZONTAL_KIND,
            LAGR_Spread,  TRUE,
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      BUTTON_KIND,
               LA_LabelText, "Add...",
               LA_Chars,10,
               LA_ID,10,
               TAG_DONE);

            LT_New(parthandle,
               LA_Type,      BUTTON_KIND,
               LA_LabelText, "Remove...",
               LA_Chars,10,
               GA_Disabled,TRUE,
               LA_ID,11,
               TAG_DONE);

         LT_EndGroup(parthandle);

         LT_New(parthandle,
            LA_Type,      HORIZONTAL_KIND,
            LAGR_Spread,  TRUE,
            TAG_DONE);

            LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Up",
                  GA_Disabled,TRUE,
                  LA_Chars,10,
                  LA_ID,12,
                  TAG_DONE);

            LT_New(parthandle,
               LA_Type,      BUTTON_KIND,
               LA_LabelText, "Down",
               LA_Chars,10,
               GA_Disabled,TRUE,
               LA_ID,13,
               TAG_DONE);

         LT_EndGroup(parthandle);

         LT_New(parthandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Sort areas...",
            LA_Chars,21,
            LA_ID,14,
            TAG_DONE);

      LT_EndGroup(parthandle);

      LT_New(parthandle,
         LA_Type,      VERTICAL_KIND,
         LA_ExtraSpace,TRUE,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      HORIZONTAL_KIND,
            TAG_DONE);

            LT_New(parthandle,
               LA_LabelText,     "Page",
               LA_Type,          CYCLE_KIND,
               LA_Chars,         30,
               LA_ID,            98,
               GTCY_Labels,      groups,
               TAG_DONE);

         LT_EndGroup(parthandle);

         LT_New(parthandle,
             LA_Type,      VERTICAL_KIND,
             LA_ID,        99,
             LAGR_ActivePage,0,
             TAG_DONE);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      VERTICAL_KIND,
                  LA_LabelText, "General",
                  TAG_DONE);

                  LT_New(parthandle,
                     LA_LabelText,     "Netmail",
                     LA_Type,          CHECKBOX_KIND,
                     LA_ID,            100,
                     LA_NoKey,         TRUE,
                     GA_Disabled,      TRUE,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_LabelText,     "Unconfirmed",
                     LA_Type,          CHECKBOX_KIND,
                     LA_ID,            199,
                     GA_Disabled,      TRUE,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_LabelText,     "Type",
                     LA_Type,          CYCLE_KIND,
                     LA_ID,            101,
                     GTCY_Labels,      areatypes,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_LabelText,     "Path",
                     LA_Chars,         20,
                     LA_Type,          STRING_KIND,
                     LA_ID,            102,
                     GTST_MaxChars,    79,
                     LAST_Picker,      TRUE,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_LabelText,     "Aka",
                     LA_Chars,         20,
                     LA_Type,          TEXT_KIND,
                     LA_ID,            103,
                     LATX_Picker,      TRUE,
                     GTTX_Border,      TRUE,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_LabelText,     "Export CHRS",
                     LA_Type,          CYCLE_KIND,
                     LA_ID,            104,
                     GTCY_Labels,      areaexportchrs,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_LabelText,     "Default CHRS",
                     LA_Type,          CYCLE_KIND,
                     LA_ID,            105,
                     GTCY_Labels,      areadefaultchrs,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_LabelText,     "Keep number",
                     LA_Type,          INTEGER_KIND,
                     LA_ID,            106,
                     GTIN_EditHook,    &StrEditHook,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_LabelText,     "Keep days",
                     LA_Type,          INTEGER_KIND,
                     LA_ID,            107,
                     GTIN_EditHook,    &StrEditHook, 
                     TAG_DONE);

               LT_EndGroup(parthandle);

            LT_EndGroup(parthandle);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      VERTICAL_KIND,
                  LA_LabelText, "Nodes",
                  TAG_DONE);

                  LT_New(parthandle,
                     LA_Type,      VERTICAL_KIND,
                     TAG_DONE);

                     LT_New(parthandle,
                        LA_Type,       LISTVIEW_KIND,
                        LA_Lines,      10,
                        LALV_Link,     NIL_LINK,
                        LA_ID,         5711,
                        LA_Chars,      31,
                        LALV_LockSize, TRUE,
                        TAG_DONE);

                  LT_EndGroup(parthandle);

                  LT_New(parthandle,
                     LA_Type,          CYCLE_KIND,
                     LA_ID,            201,
                     GTCY_Labels,      areaflags,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_Type,          XBAR_KIND,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_Type,          CYCLE_KIND,
                     GTCY_Labels,      areaaddmodes,
                     LA_ID,            212,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_Type,      HORIZONTAL_KIND,
                     TAG_DONE);

                     LT_New(parthandle,
                        LA_Type,    BUTTON_KIND,
                        LA_LabelText,"Add...",
                        LA_ID,      210,
                        LA_Chars,   15,
                        TAG_DONE);

                     LT_New(parthandle,
                        LA_Type,    BUTTON_KIND,
                        LA_LabelText,"Remove",
                        LA_ID,      211,
                        LA_Chars,   15,
                        TAG_DONE);

                  LT_EndGroup(parthandle);

               LT_EndGroup(parthandle);

            LT_EndGroup(parthandle);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      VERTICAL_KIND,
                  LA_LabelText, "AreaFix",
                  TAG_DONE);

                  LT_New(parthandle,
                     LA_LabelText,     "Mandatory",
                     LA_Type,          CHECKBOX_KIND,
                     LA_ID,            300,
                     GTCY_Labels,      areatypes,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_LabelText,     "Default read-only",
                     LA_Type,          CHECKBOX_KIND,
                     LA_ID,            301,
                     GTCY_Labels,      areatypes,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_LabelText,     "Description",
                     LA_Chars,         20,
                     LA_Type,          STRING_KIND,
                     LA_ID,            302,
                     GTST_MaxChars,    79,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_LabelText,     "Group",
                     LA_Chars,         20,
                     LA_Type,          TEXT_KIND,
                     LA_ID,            303,
                     LATX_LockSize,    TRUE,
                     LATX_Picker,      TRUE,
                     GTTX_Border,      TRUE,
                     TAG_DONE);

               LT_EndGroup(parthandle);

               LT_New(parthandle,
                  LA_Type,      VERTICAL_KIND,
                  LA_LabelText, "Options",
                  TAG_DONE);

                  LT_New(parthandle,
                     LA_LabelText,     "Ignore dupes",
                     LA_Type,          CHECKBOX_KIND,
                     LA_ID,            310,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_LabelText,     "Ignore SEEN-BY",
                     LA_Type,          CHECKBOX_KIND,
                     LA_ID,            311,
                     TAG_DONE);

               LT_EndGroup(parthandle);

            LT_EndGroup(parthandle);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      VERTICAL_KIND,
                  LA_LabelText, "Banned",
                  TAG_DONE);

                  LT_New(parthandle,
                     LA_Type,       LISTVIEW_KIND,
                     LA_Lines,      10,
                     LALV_Link,     NIL_LINK,
                     LA_ID,         6711,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_Type,      HORIZONTAL_KIND,
                     TAG_DONE);

                     LT_New(parthandle,
                        LA_Type,    BUTTON_KIND,
                        LA_LabelText,"Add...",
                        LA_Chars,   10,
                        LA_ID,      510,
                        TAG_DONE);

                     LT_New(parthandle,
                        LA_Type,    BUTTON_KIND,
                        LA_LabelText,"Remove",
                        LA_Chars,   10,
                        LA_ID,      511,
                        TAG_DONE);

                  LT_EndGroup(parthandle);

               LT_EndGroup(parthandle);

            LT_EndGroup(parthandle);

         LT_EndGroup(parthandle);

      LT_EndGroup(parthandle);

   LT_EndGroup(parthandle);

   partwin=LT_Build(parthandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW|IDCMP_RAWKEY,
                               LAWN_Title,    "Areas",
                               LAWN_Menu,     areamenus,
                               LAWN_HelpHook, &AreaHelpHook,
                               WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                               TAG_END);

   RefreshArealist();

   if(!partwin)
   {
      ClearMenuStrip(parthandle->Window);
      LT_DeleteHandle(parthandle);
	   FreeMenus(areamenus);
      DisplayBeep(NULL);
      FreeTempNodes(&arealist);
      FreeTempNodes(&tossnodelist);
      FreeTempNodes(&bannednodelist);
      FreeTempNodes(&areaakalist);
      FreeTempNodes(&areagrouplist);
      FreeTempNodes(&selareagrouplist);
      FreeTempNodes(&areanodelist);
      FreeTempNodes(&areanodeaccesslist);
      return;
   }


   if(areasnum)
   {
      areaactivepage=0;
      currentarea=-1;
      LT_SetAttributes(parthandle,4711,GTLV_Selected,0,TAG_END);
      SetArea(0);
   }

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(parthandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;

         LT_ReplyIMsg(IntuiMessage);

         switch(class)
         {
            case IDCMP_RAWKEY:         if(code == TAB) LT_Activate(parthandle,4712);
                                       break;
            case IDCMP_IDCMPUPDATE:    switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 102:
                                             strcmpcpy(areanode->Path,(UBYTE *)LT_GetAttributes(parthandle,102,TAG_END));

                                             if(OpenReqDir(areanode->Path,79,"Please select path",partwin))
                                             {
                                                saveconfig=TRUE;
                                                LT_SetAttributes(parthandle,102,GTST_String,areanode->Path,TAG_END);
                                             }
                                             break;
                                          case 303:
                                             res=AskList(&selareagrouplist,"Select group",partwin);

                                             if(res!=-1)
                                             {
                                                if(res == 0) areanode->Group=0;
                                                else         areanode->Group=res+'A'-1;
                                                SetArea(currentarea);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 103:
                                             res=AskList(&areaakalist,"Select aka",partwin);

                                             if(res!=-1)
                                             {
                                                saveconfig=TRUE;
                                                aka=AkaList.First;
                                                for(c=0;c<res;c++) aka=aka->Next;
                                                areanode->Aka=aka;
                                                SetArea(currentarea);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 5711:
                                             if(!(areanode->Flags & AREA_NETMAIL))
                                             {
                                                tossnode=areanode->TossNodes.First;
                                                for(c=0;c<currenttossnode;c++) tossnode=tossnode->Next;

                                                code=LT_GetAttributes(parthandle,201,TAG_END);
                                                code++;
                                                if(code == 4) code=0;

                                                tossnode->Flags=0;

                                                if(code == 1) tossnode->Flags|=TOSSNODE_READONLY;
                                                if(code == 2) tossnode->Flags|=TOSSNODE_WRITEONLY;
                                                if(code == 3) tossnode->Flags|=TOSSNODE_FEED;

                                                RefreshTossNodelist();
                                                SetTossNode();
                                                saveconfig=TRUE;
                                             }
                                             break;
                                        }
                                       break;
            case IDCMP_MENUPICK:       num=code;
                                       while(num!=MENUNULL)
                                       {
                                          if(MENUNUM(num)==0)
                                          {
                                             switch(ITEMNUM(num))
                                             {
                                                case 0: SetArea(-1);
                                                        LT_LockWindow(partwin);
                                                        ChangeAreas();
                                                        LT_UnlockWindow(partwin);
                                                        SetArea(0);
                                                        LT_SetAttributes(parthandle,4711,GTLV_Selected,0,GTLV_MakeVisible,0,TAG_END);
                                                        break;
                                                case 2: DuplicateArea();
                                                        break;
                                                case 4: AddMandantoryNewNodes();
                                                        break;
                                                case 6: RemoveGroup();
                                                        break;
                                                case 8: UpdateDescriptions();
                                                        break;
                                             }
                                          }
                                          if(MENUNUM(num)==1)
                                          {
                                             switch(ITEMNUM(num))
                                             {
                                                case 0: Find();
                                                        break;
                                                case 1: FindNext();
                                                        break;
                                                case 3: FindUnconfirmed();
                                                        break;
                                             }
                                          }
                                          menuitem=ItemAddress(areamenus,num);
                                          num=menuitem->NextSelect;
                                       }
                                       break;
            case IDCMP_GADGETUP:       switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 10:
                                             AddArea();
                                             break;
                                          case 11:
                                             RemArea();
                                             break;
                                          case 12:
                                             if(currentarea != 0)
                                             {
                                                tmp=currentarea;
                                                MoveUp(&AreaList,tmp);
                                                RefreshArealist();
                                                SetAreaQuick(tmp-1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentarea,
                                                   GTLV_MakeVisible,currentarea,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 13:
                                             if(currentarea != areasnum-1)
                                             {
                                                tmp=currentarea;
                                                MoveDown(&AreaList,tmp);
                                                RefreshArealist();
                                                SetAreaQuick(tmp+1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentarea,
                                                   GTLV_MakeVisible,currentarea,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 14:
                                             SortAreas();
                                             saveconfig=TRUE;
                                             break;
                                          case 98:
                                             SetArea(currentarea);
                                             LT_SetAttributes(parthandle,99,LAGR_ActivePage,code,TAG_END);
                                             areaactivepage=code;
                                             break;
                                          case 101:
                                             if(code == 0 && ((areanode->Flags & AREA_NETMAIL) || stricmp(areanode->Tagname,"BAD")==0))
                                             {
                                                DisplayBeep(NULL);
                                             }
                                             else
                                             {
                                                areanode->AreaType=code;
                                                SetArea(currentarea);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 102:
                                             struct Area *a;

                                             strcpy(areanode->Path,getgadgetstring((struct Gadget *)iaddress));
                                             saveconfig=TRUE;

                                             for(a=AreaList.First;a;a=a->Next)
                                                if(stricmp(a->Path,areanode->Path)==0 && a!=areanode) break;

                                             if(a)
                                             {
                                                rtEZRequestTags("Warning! You already have an area with that path","_Ok",NULL,NULL,RT_Underscore,'_',
                                                                                   RT_Window,partwin,
                                                                                   RT_WaitPointer,TRUE,
                                                                                   RT_LockWindow, TRUE,
                                                                                   TAG_END);
                                             }
                                             break;
                                          case 104:
                                             areanode->ExportCHRS=areaexportdata[code];
                                             saveconfig=TRUE;
                                             break;
                                          case 105:
                                             areanode->DefaultCHRS=areadefaultdata[code];
                                             saveconfig=TRUE;
                                             break;
                                          case 199:
                                             if(areanode->Flags & AREA_UNCONFIRMED) areanode->Flags&=~(AREA_UNCONFIRMED);
                                             else                                   areanode->Flags|=AREA_UNCONFIRMED;
                                             break;
                                           case 201:
                                             if(!(areanode->Flags & AREA_NETMAIL))
                                             {
                                                tossnode=areanode->TossNodes.First;
                                                for(c=0;c<currenttossnode;c++) tossnode=tossnode->Next;

                                                tossnode->Flags=0;

                                                if(code == 1) tossnode->Flags|=TOSSNODE_READONLY;
                                                if(code == 2) tossnode->Flags|=TOSSNODE_WRITEONLY;
                                                if(code == 3) tossnode->Flags|=TOSSNODE_FEED;

                                                RefreshTossNodelist();
                                                SetTossNode();
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 210:
                                             if(areanode->Flags & AREA_NETMAIL)
                                             {
                                                buf[0]=0;

                                                res=rtGetString(buf,50,"Add import node",NULL,RT_Window,partwin,
                                                        RT_WaitPointer, TRUE,
                                                        RT_LockWindow, TRUE,
                                                        TAG_END);

                                                if(res)
                                                {
                                                   if(!Parse4D(buf,&n4d))
                                                   {
                                                      rtEZRequestTags("Invalid node number","_Ok",NULL,NULL,RT_Underscore,'_',
                                                            RT_Window,partwin,
                                                            RT_WaitPointer, TRUE,
                                                            RT_LockWindow, TRUE,
                                                            TAG_END);
                                                   }
                                                   else
                                                   {
                                                      if(importnode=AllocMem(sizeof(struct ImportNode),MEMF_CLEAR))
                                                      {
                                                         Copy4D(&importnode->Node,&n4d);
                                                         jbAddNode(&areanode->TossNodes,(struct jbNode *)importnode);
                                                         RefreshTossNodelist();
                                                         currenttossnode=tossnodesnum-1;
                                                         SetTossNode();
                                                         LT_SetAttributes(parthandle,5711,GTLV_Selected,currenttossnode,GTLV_MakeVisible,currenttossnode,TAG_DONE);
                                                         saveconfig=TRUE;
                                                      }
                                                      else
                                                      {
                                                         DisplayBeep(NULL);
                                                      }
                                                   }
                                                }
                                             }
                                             else
                                             {
                                                if(tossnodesnum >= 2 && Checksum9() != checksums[9])
                                                {
                                                   rtEZRequestTags("You can only have two nodes per area\n"
                                                                   "in the unregistered version of CrashMail","_Ok",NULL,NULL,RT_Underscore,'_',
                                                                        RT_Window,partwin,
                                                                        RT_WaitPointer, TRUE,
                                                                        RT_LockWindow, TRUE,
                                                                        TAG_END);
                                                }
                                                else
                                                {
                                                   struct List *addtnlist;

                                                   RefreshAreaNodeAccessList();
                                                   if(LT_GetAttributes(parthandle,212,TAG_END)==0) addtnlist=&areanodelist;
                                                   else                                            addtnlist=&areanodeaccesslist;

                                                   res=AskList(addtnlist,"Select node",partwin);

                                                   if(res!=-1)
                                                   {
                                                      tempnode=addtnlist->lh_Head;
                                                      for(c=0;c<res;c++) tempnode=tempnode->Node.ln_Succ;

                                                      cnode=tempnode->UserData;

                                                      if(tossnode=AllocMem(sizeof(struct TossNode),MEMF_CLEAR))
                                                      {
                                                         tossnode->ConfigNode=cnode;
                                                         if(areanode->Flags & AREA_DEFREADONLY) tossnode->Flags|=TOSSNODE_READONLY;
                                                         jbAddNode(&areanode->TossNodes,(struct jbNode *)tossnode);
                                                         RefreshTossNodelist();
                                                         currenttossnode=tossnodesnum-1;
                                                         SetTossNode();
                                                         LT_SetAttributes(parthandle,5711,GTLV_Selected,currenttossnode,GTLV_MakeVisible,currenttossnode,TAG_DONE);
                                                         saveconfig=TRUE;
                                                      }
                                                      else
                                                      {
                                                         DisplayBeep(NULL);
                                                      }
                                                   }
                                                }
                                             }
                                             break;
                                          case 211:
                                             if(areanode->Flags & AREA_NETMAIL)
                                                jbFreeNum(&areanode->TossNodes,currenttossnode,sizeof(struct ImportNode));

                                             else
                                                jbFreeNum(&areanode->TossNodes,currenttossnode,sizeof(struct TossNode));

                                             if(currenttossnode==tossnodesnum-1) currenttossnode--;
                                             RefreshTossNodelist();
                                             SetTossNode();
                                             LT_SetAttributes(parthandle,5711,GTLV_Selected,currenttossnode,GTLV_MakeVisible,currenttossnode,TAG_DONE);
                                             saveconfig=TRUE;
                                             break;
                                          case 300:
                                             if(areanode->Flags & AREA_MANDATORY) areanode->Flags&=~(AREA_MANDATORY);
                                             else                                  areanode->Flags|=AREA_MANDATORY;
                                             saveconfig=TRUE;
                                             break;
                                          case 301:
                                             if(areanode->Flags & AREA_DEFREADONLY) areanode->Flags&=~(AREA_DEFREADONLY);
                                             else                                   areanode->Flags|=AREA_DEFREADONLY;
                                             saveconfig=TRUE;
                                             break;
                                          case 310:
                                             if(areanode->Flags & AREA_IGNOREDUPES) areanode->Flags&=~(AREA_IGNOREDUPES);
                                             else                                   areanode->Flags|=AREA_IGNOREDUPES;
                                             saveconfig=TRUE;
                                             break;
                                          case 311:
                                             if(areanode->Flags & AREA_IGNORESEENBY) areanode->Flags&=~(AREA_IGNORESEENBY);
                                             else                                    areanode->Flags|=AREA_IGNORESEENBY;
                                             saveconfig=TRUE;
                                             break;
                                          case 4711:
                                             SetArea(code);
                                             break;
                                          case 4712:
                                             if(!CheckDupeArea(getgadgetstring((struct Gadget *)iaddress),currentarea))
                                             {
                                                LT_Activate(parthandle,4712);
                                             }
                                             else
                                             {
                                                strcpy(areanode->Tagname,getgadgetstring((struct Gadget *)iaddress));

                                                tempnode=arealist.lh_Head;
                                                for(c=0;c<currentarea;c++) tempnode=tempnode->Node.ln_Succ;

                                                LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
                                                strcpy(tempnode->buf,areanode->Tagname);
                                                LT_SetAttributes(parthandle,4711,GTLV_Labels,&arealist,TAG_END);

                                                if(areanode->Flags & AREA_DEFAULT)
                                                   areanode->Flags ^= AREA_DEFAULT;

                                                if(stricmp(areanode->Tagname,"DEFAULT")==0 || strnicmp(areanode->Tagname,"DEFAULT_",8)==0)
                                                   areanode->Flags |= AREA_DEFAULT;

                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 5711:
                                             currenttossnode=code;
                                             SetTossNode();
                                             break;
                                          case 510:
                                             res=AskList(&addbannedlist,"Select node",partwin);

                                             if(res!=-1)
                                             {
                                                 tempnode=addbannedlist.lh_Head;
                                                 for(c=0;c<res;c++) tempnode=tempnode->Node.ln_Succ;

                                                 cnode=tempnode->UserData;

                                                 if(bannednode=AllocMem(sizeof(struct BannedNode),MEMF_CLEAR))
                                                 {
                                                    bannednode->ConfigNode=cnode;
                                                    jbAddNode(&areanode->BannedNodes,(struct jbNode *)bannednode);
                                                    RefreshBannedNodelist();
                                                    currentbannednode=bannednodesnum-1;
                                                    SetBannedNode();
                                                    LT_SetAttributes(parthandle,6711,GTLV_Selected,currenttossnode,GTLV_MakeVisible,currenttossnode,TAG_DONE);
                                                    saveconfig=TRUE;
                                                 }
                                                 else
                                                 {
                                                    DisplayBeep(NULL);
                                                 }
                                             }
                                             break;
                                          case 511:
                                             if(!(areanode->Flags & AREA_NETMAIL))
                                             {
                                                jbFreeNum(&areanode->BannedNodes,currentbannednode,sizeof(struct BannedNode));
                                                if(currentbannednode==bannednodesnum-1) currentbannednode--;
                                                RefreshBannedNodelist();
                                                SetBannedNode();
                                                LT_SetAttributes(parthandle,6711,GTLV_Selected,currentbannednode,GTLV_MakeVisible,currenttossnode,TAG_DONE);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 6711:
                                             currentbannednode=code;
                                             break;
                                       }
                                       break;

            case IDCMP_CLOSEWINDOW:    SetArea(-1);
                                       LT_DeleteHandle(parthandle);
                                       parthandle=NULL;
                                       FreeTempNodes(&arealist);
                                       FreeTempNodes(&tossnodelist);
                                       FreeTempNodes(&bannednodelist);
                                       FreeTempNodes(&areaakalist);
                                       FreeTempNodes(&areagrouplist);
                                       FreeTempNodes(&selareagrouplist);
                                       FreeTempNodes(&areanodelist);
                                       FreeTempNodes(&areanodeaccesslist);
                                       return;
         }
      }
      WaitPort(partwin->UserPort);
   }
}

