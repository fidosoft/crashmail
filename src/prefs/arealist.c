#include <CrashPrefs.h>

struct List arealistlist;

ULONG arealistnum,currentarealist;

struct Hook ArealistHelpHook = { {NULL}, HelpHookEntry,NULL,HELP_AREALIST};
ULONG tmparealistflags;
UBYTE tmpgroup;

UBYTE groupbuf[200];

void SetArealist(ULONG num)
{
   struct Arealist *arealist;
   ULONG c;

   if(currentarealist!=-1)
   {
      arealist=ArealistList.First;
      for(c=0;c<currentarealist;c++) arealist=arealist->Next;

      strcmpcpy(arealist->AreaFile,(UBYTE *)LT_GetAttributes(parthandle,50,TAG_END));
      arealist->Flags=tmparealistflags;
      arealist->Group=tmpgroup;
   }

   if(arealistnum == 0)
   {
      LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,50,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,51,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,52,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,53,GA_Disabled,TRUE,TAG_END);
   }

   if(num!=-1)
   {
      if(arealistnum)
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,FALSE,TAG_END);
      }
      else
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
      }

      arealist=ArealistList.First;
      for(c=0;c<num;c++) arealist=arealist->Next;

      tmparealistflags=arealist->Flags;
      tmpgroup=arealist->Group;

      if(num!=0)                 LT_SetAttributes(parthandle,12,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);

      if(num!=arealistnum-1)     LT_SetAttributes(parthandle,13,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);

      LT_SetAttributes(parthandle,50,GA_Disabled,FALSE,GTST_String,arealist->AreaFile,TAG_END);
      LT_SetAttributes(parthandle,51,GA_Disabled,FALSE,TAG_END);
      LT_SetAttributes(parthandle,52,GA_Disabled,FALSE,TAG_END);
      LT_SetAttributes(parthandle,53,GA_Disabled,FALSE,TAG_END);

      if(arealist->Flags & AREALIST_FORWARD) LT_SetAttributes(parthandle,51,GTCB_Checked,TRUE,TAG_END);
      else                                   LT_SetAttributes(parthandle,51,GTCB_Checked,FALSE,TAG_END);

      if(arealist->Flags & AREALIST_DESC)    LT_SetAttributes(parthandle,52,GTCB_Checked,TRUE,TAG_END);
      else                                   LT_SetAttributes(parthandle,52,GTCB_Checked,FALSE,TAG_END);

      if(arealist->Group) sprintf(groupbuf,"%lc: %s",arealist->Group,cfg_GroupNames[arealist->Group-'A']);
      else                strcpy(groupbuf,"<No group>");

      LT_SetAttributes(parthandle,53,GTTX_Text,groupbuf,TAG_END);
   }
   currentarealist=num;
}

void RefreshArealistlist(void)
{
   struct TempNode *tempnode;
   struct Arealist *arealist;

   LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
   FreeTempNodes(&arealistlist);

   arealistnum=0;

   for(arealist=ArealistList.First;arealist;arealist=arealist->Next)
   {
      if(!(tempnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(&arealistlist);
         return;
      }
      sprintf(tempnode->buf,"%lu:%lu/%lu.%lu",
         arealist->Node->Node.Zone,
         arealist->Node->Node.Net,
         arealist->Node->Node.Node,
         arealist->Node->Node.Point);

      tempnode->Node.ln_Name=tempnode->buf;
      AddTail(&arealistlist,(struct Node *)tempnode);
      arealistnum++;
   }

   LT_SetAttributes(parthandle,4711,GTLV_Labels,&arealistlist,TAG_END);
}

void AddArealist(void)
{
   struct Arealist *arealist;
   struct ConfigNode *cnode;

   cnode=AskCNode(partwin);

   if(!cnode)
      return;

   if(arealist=(struct Arealist *)AllocMem(sizeof(struct Arealist),MEMF_CLEAR))
   {
      jbAddNode(&ArealistList,(struct jbNode *)arealist);
      arealist->Node=cnode;
      RefreshArealistlist();
      SetArealist(arealistnum-1);
      LT_SetAttributes(parthandle,4711,GTLV_Selected,currentarealist,GTLV_MakeVisible,currentarealist,TAG_END);
      saveconfig=TRUE;
   }
   else
   {
      DisplayBeep(NULL);
   }
}

void RemArealist(void)
{
   ULONG c,tmp;
   struct Arealist *arealist;
   UBYTE buf[120];
   BOOL res;

   arealist=ArealistList.First;
   for(c=0;c<currentarealist;c++) arealist=arealist->Next;

   sprintf(buf,"Do you really want to remove the arealist\nfrom %lu:%lu/%lu.%lu?",
         arealist->Node->Node.Zone,
         arealist->Node->Node.Net,
         arealist->Node->Node.Node,
         arealist->Node->Node.Point);

   res=rtEZRequestTags(buf,"_Remove|_Cancel",NULL,NULL,RT_Underscore,'_',
                                                       RT_Window,partwin,
                                                       RT_WaitPointer,TRUE,
                                                       RT_LockWindow, TRUE,
                                                       RTEZ_Flags, EZREQF_CENTERTEXT | EZREQF_NORETURNKEY,
                                                       TAG_END);

   if(!res)
      return;

   tmp=currentarealist;
   SetArealist(-1);
   currentarealist=-1;
   jbFreeNode(&ArealistList,(struct jbNode *)arealist,sizeof(struct Arealist));
   RefreshArealistlist();
   if(tmp == arealistnum) tmp--;
   SetArealist(tmp);
   saveconfig=TRUE;
}

UBYTE arealistfrom[100];
UBYTE arealistgroup[100];

UBYTE TestArealist(UBYTE *area)
{
   struct Arealist *arealist,*resarealist;
   UBYTE buf[200],desc[200];
   ULONG c,d;
   BPTR fh;

   LT_LockWindow(partwin);

   resarealist=FALSE;

   for(arealist=ArealistList.First;arealist && !resarealist;arealist=arealist->Next)
   {
      if(arealist->Flags & AREALIST_FORWARD)
      {
         if(fh=Open(arealist->AreaFile,MODE_OLDFILE))
         {
            while(FGets(fh,buf,199))
            {
               for(c=0;buf[c]>32;c++);
               buf[c]=0;

               if(stricmp(buf,area)==0)
                  resarealist=arealist;
            }
            Close(fh);
         }
         else
         {
            sprintf(buf,"Unable to open \"%s\"",arealist->AreaFile);

            rtEZRequestTags(buf,"Ok",NULL,NULL,RT_Underscore,'_',
                                               RT_Window,partwin,
                                               RT_WaitPointer,TRUE,
                                               TAG_END);
         }
      }
   }

   if(resarealist)
   {
      arealist=resarealist;

      sprintf(arealistfrom,"%lu:%lu/%lu.%lu",
            arealist->Node->Node.Zone,
            arealist->Node->Node.Net,
            arealist->Node->Node.Node,
            arealist->Node->Node.Point);

      if(arealist->Group)
         sprintf(arealistgroup,"%lc: %s",arealist->Group,cfg_GroupNames[arealist->Group-'A']);

      else
         strcpy(arealistgroup,"Impossible to subscribe");
   }
   else
   {
      strcpy(arealistfrom,"Not found");
      strcpy(arealistgroup,"");
   }

   desc[0]=0;

   for(arealist=ArealistList.First;arealist && desc[0]==0;arealist=arealist->Next)
   {
      if(arealist->Flags & AREALIST_DESC)
      {
         if(fh=Open(arealist->AreaFile,MODE_OLDFILE))
         {
            while(FGets(fh,buf,199) && desc[0]==0)
            {
               for(c=0;buf[c]>32;c++);

               if(buf[c]!=0)
               {
                  buf[c]=0;

                  if(stricmp(buf,area)==0)
                  {
                     c++;
                     while(buf[c]<=32 && buf[c]!=0) c++;

                     if(buf[c]!=0)
                     {
                        d=0;
                        while(buf[c]!=0 && buf[c]!=10 && buf[c]!=13 && d<77) desc[d++]=buf[c++];
                        desc[d]=0;
                     }
                  }
               }
            }
            Close(fh);
         }
         else
         {
            sprintf(buf,"Unable to open \"%s\"",arealist->AreaFile);

            rtEZRequestTags(buf,"Ok",NULL,NULL,RT_Underscore,'_',
                                               RT_Window,partwin,
                                               RT_WaitPointer,TRUE,
                                               TAG_END);
         }
      }
   }

   if(desc[0]==0)
      strcpy(desc,"<Not found>");

   LT_UnlockWindow(partwin);

   LT_SetAttributes(parthandle,61,GTTX_Text,arealistfrom,TAG_END);
   LT_SetAttributes(parthandle,62,GTTX_Text,arealistgroup,TAG_END);
   LT_SetAttributes(parthandle,63,GTTX_Text,desc,TAG_END);
}

void PartArealist(void)
{
   ULONG class,code,tmp;
   struct IntuiMessage *IntuiMessage;
   APTR iaddress;
   UBYTE grp;
   
   NewList(&arealistlist);

   currentarealist=-1;

   parthandle = LT_CreateHandleTags(scr,LH_EditHook,&StrEditHook,LH_RawKeyFilter,FALSE,TAG_DONE);

   if(!parthandle)
   {
      FreeTempNodes(&arealistlist);
      return;
   }

   LT_New(parthandle,
      LA_Type,      HORIZONTAL_KIND,
      LA_LabelText, "Arealists",
      TAG_DONE);

      LT_New(parthandle,
         LA_Type,      VERTICAL_KIND,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,       LISTVIEW_KIND,
            LA_Chars,20,
            LALV_LockSize, TRUE,
            LALV_Lines,    12,
            LALV_CursorKey,TRUE,
            LALV_Link,  NIL_LINK,
            GTLV_Labels,&arealistlist,
            LA_ID,4711,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      HORIZONTAL_KIND,
            LAGR_SameSize,TRUE,
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               LAGR_SameSize,TRUE,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Add...",
                  LA_Chars,10,
                  LA_ID,10,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Up",
                  GA_Disabled,TRUE,
                  LA_Chars,10,
                  LA_ID,12,
                  TAG_DONE);

            LT_EndGroup(parthandle);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               LAGR_SameSize,TRUE,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Remove...",
                  LA_Chars,10,
                  GA_Disabled,TRUE,
                  LA_ID,11,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Down",
                  LA_Chars,10,
                  GA_Disabled,TRUE,
                  LA_ID,13,
                  TAG_DONE);

            LT_EndGroup(parthandle);

         LT_EndGroup(parthandle);

      LT_EndGroup(parthandle);

      LT_New(parthandle,
         LA_Type,      VERTICAL_KIND,
         LA_ExtraSpace,TRUE,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      VERTICAL_KIND,
            LA_ExtraSpace,TRUE,
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      STRING_KIND,
               LA_LabelText, "Area list",
               GA_Disabled,TRUE,
               LA_Chars,30,
               GTST_MaxChars,79,
               LAST_Picker,TRUE,
               LA_ID,50,
               TAG_DONE);

            LT_New(parthandle,
               LA_LabelText,     "Group",
               LA_Chars,         30,
               LA_Type,          TEXT_KIND,
               LA_ID,            53,
               LATX_LockSize,    TRUE,
               LATX_Picker,      TRUE,
               GTTX_Border,      TRUE,
               TAG_DONE);

        LT_EndGroup(parthandle);

        LT_New(parthandle,
            LA_Type,      VERTICAL_KIND,
            LA_ExtraSpace,TRUE,
            TAG_DONE);

            LT_New(parthandle,
               LA_LabelText,     "Forward requests",
               LA_Type,          CHECKBOX_KIND,
               LA_ID,            51,
               TAG_DONE);

            LT_New(parthandle,
               LA_LabelText,     "Get descriptions",
               LA_Type,          CHECKBOX_KIND,
               LA_ID,            52,
               TAG_DONE);

         LT_EndGroup(parthandle);

         LT_New(parthandle,
            LA_Type,      VERTICAL_KIND,
            LA_ExtraSpace,TRUE,
            LA_LabelText, "Test arealists",
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      STRING_KIND,
               LA_LabelText, "Area",
               GTST_MaxChars,50,
               LA_Chars,20,
               LA_ID,60,
               TAG_DONE);

            LT_New(parthandle,
               LA_Type,      TEXT_KIND,
               LA_LabelText, "Node",
               GTTX_Border,TRUE,
               LA_Chars,20,
               LA_ID,61,
               TAG_DONE);


            LT_New(parthandle,
               LA_Type,      TEXT_KIND,
               LA_LabelText, "Group",
               GTTX_Border,TRUE,
               LA_Chars,20,
               LA_ID,62,
               TAG_DONE);

            LT_New(parthandle,
               LA_Type,      TEXT_KIND,
               LA_LabelText, "Description",
               GTTX_Border,TRUE,
               LA_Chars,20,
               LA_ID,63,
               TAG_DONE);

         LT_EndGroup(parthandle);

      LT_EndGroup(parthandle);

   LT_EndGroup(parthandle);

   partwin=LT_Build(parthandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW|IDCMP_RAWKEY,
                               LAWN_Title,    "Arealists",
                               WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                               LAWN_HelpHook, &ArealistHelpHook,
                               TAG_END);

   if(!partwin)
   {
      LT_DeleteHandle(parthandle);
      DisplayBeep(NULL);
      FreeTempNodes(&arealistlist);
      return;
   }

   RefreshArealistlist();

   if(arealistnum)
   {
      LT_SetAttributes(parthandle,4711,GTLV_Selected,0,TAG_END);
      SetArealist(0);
   }

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(parthandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;

         LT_ReplyIMsg(IntuiMessage);

         switch(class)
         {
            case IDCMP_RAWKEY:         if(code == TAB) LT_Activate(parthandle,50);
                                       break;
            case IDCMP_IDCMPUPDATE:    switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 50:
                                            UBYTE buf[200];

                                            strcpy(buf,(UBYTE *)LT_GetAttributes(parthandle,50,TAG_END));

                                            if(OpenReq(buf,79,"Please select file",partwin,FALSE))
                                            {
                                               saveconfig=TRUE;
                                               LT_SetAttributes(parthandle,50,GTST_String,buf,TAG_END);
                                               SetArealist(currentarealist);
                                            }
                                            break;
                                          case 53:
                                             grp=AskGroup(partwin,"<No group>");

                                             if(grp!=-1)
                                             {
                                                tmpgroup=grp;
                                                SetArealist(currentarealist);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                       }
                                       break;
             case IDCMP_GADGETUP:      switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 10:
                                             AddArealist();
                                             break;
                                          case 11:
                                             RemArealist();
                                             break;
                                          case 12:
                                             if(currentarealist != 0)
                                             {
                                                tmp=currentarealist;
                                                SetArealist(-1);
                                                MoveUp(&ArealistList,tmp);
                                                RefreshArealistlist();
                                                SetArealist(tmp-1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentarealist,
                                                   GTLV_MakeVisible,currentarealist,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 13:
                                             if(currentarealist != arealistnum-1)
                                             {
                                                tmp=currentarealist;
                                                SetArealist(-1);
                                                MoveDown(&ArealistList,tmp);
                                                RefreshArealistlist();
                                                SetArealist(tmp+1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentarealist,
                                                   GTLV_MakeVisible,currentarealist,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 50:
                                             SetArealist(currentarealist);
                                             saveconfig=TRUE;
                                             break;
                                          case 51:
                                             if(tmparealistflags & AREALIST_FORWARD) tmparealistflags&=~(AREALIST_FORWARD);
                                             else                                    tmparealistflags|=AREALIST_FORWARD;
                                             saveconfig=TRUE;
                                             break;
                                          case 52:
                                             if(tmparealistflags & AREALIST_DESC)  tmparealistflags&=~(AREALIST_DESC);
                                             else                                  tmparealistflags|=AREALIST_DESC;
                                             saveconfig=TRUE;
                                             break;
                                          case 60:
                                             SetArealist(currentarealist);
                                             TestArealist(getgadgetstring((struct Gadget *)iaddress));
                                             break;
                                          case 4711:
                                             SetArealist(code);
                                             break;
                                       }
                                       break;

            case IDCMP_CLOSEWINDOW:    SetArealist(-1);
                                       LT_DeleteHandle(parthandle);
                                       parthandle=NULL;
                                       FreeTempNodes(&arealistlist);
                                       return;
         }
      }
      WaitPort(partwin->UserPort);
   }
}
