#include <CrashPrefs.h>

UBYTE *localchrs[]={"IBMPC","LATIN-1","SWEDISH","MAC",NULL};
UBYTE *localdata[]={IbmToAmiga,NULL,SF7ToAmiga,MacToAmiga};

UBYTE *dupemodes[]={"Ignore","Bad","Kill",NULL};

UBYTE *loglevels[]={"1 - Minimum",
                    "2 - Small",
                    "3 - Normal",
                    "4 - Extra",
                    "5 - Extreme",
                    "6 - Debug"};

struct Hook GeneralHelpHook = { {NULL}, HelpHookEntry,NULL,HELP_GENERAL};

void GhostDupe(ULONG mode)
{
   if(mode == DUPE_IGNORE)
   {
      LT_SetAttributes(parthandle,8,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,9,GA_Disabled,TRUE,TAG_END);
   }
   else
   {
      LT_SetAttributes(parthandle,8,GA_Disabled,FALSE,TAG_END);
      LT_SetAttributes(parthandle,9,GA_Disabled,FALSE,TAG_END);
   }
}

__geta4 LONG logdispfunc(struct Gadget *gad,WORD num)
{
   return((LONG)loglevels[num-1]);
}

void PartGeneral(void)
{
   struct IntuiMessage *IntuiMessage;
   APTR iaddress;
   ULONG c,class,code,qualifier,gid;

   ULONG chrsulong;
   ULONG maxpktulong,maxbundleulong,dupesizeulong;

   parthandle = LT_CreateHandleTags(scr,LH_EditHook,&StrEditHookNext,
                                        LH_RawKeyFilter,FALSE,
                                        TAG_DONE);

   if(!parthandle)
      return;

   maxpktulong=cfg_MaxPktSize/1024;
   maxbundleulong=cfg_MaxBundleSize/1024;
   dupesizeulong=cfg_DupeSize/1024;

   for(c=0;localchrs[c];c++)
      if(localdata[c]==cfg_LocalCharset) break;

   chrsulong=c;

   LT_New(parthandle,
      LA_Type, VERTICAL_KIND,
      TAG_DONE);

      LT_New(parthandle,
         LA_Type,      VERTICAL_KIND,
         LA_LabelText, "Variables",
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      VERTICAL_KIND,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      STRING_KIND,
            LA_LabelText, "Sysop",
            LA_Chars,     40,
            LA_ID,        1,
            GTST_String,  cfg_Sysop,
            GTST_MaxChars,39,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      INTEGER_KIND,
            LA_LabelText, "Default zone",
            LA_ID,        2,
            GTIN_EditHook,&StrEditHookNext,
            GTIN_Number,  cfg_DefaultZone,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      CYCLE_KIND,
            LA_LabelText, "Local charset",
            LA_ID,        5,
            GTCY_Labels,  localchrs,
            GTCY_Active,   c,
            TAG_DONE);

         LT_New(parthandle,
            LAST_Picker,  TRUE,
            LA_Type,      STRING_KIND,
            LA_LabelText, "Dupefile",
            LA_Chars,     40,
            LA_ID,        8,
            GTST_String,  cfg_DupeFile,
            GTST_MaxChars,39,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      INTEGER_KIND,
            LA_LabelText, "Size of dupefile (in KB)",
            LA_Chars,     40,
            LA_ID,        9,
            GTIN_EditHook,&StrEditHookNext,
            GTIN_Number,  cfg_DupeSize/1024,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      CYCLE_KIND,
            LA_LabelText, "Dupemode",
            LA_ID,        10,
            GTCY_Labels,  dupemodes,
            GTCY_Active,  cfg_DupeMode,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      STRING_KIND,
            LA_LabelText, "Logfile",
            LA_Chars,     40,
            LA_ID,        3,
            LAST_Picker,  TRUE,
            GTST_MaxChars,39,
            GTST_String,  cfg_LogFile,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      SLIDER_KIND,
            LA_ID,        4,
            LA_LabelText, "Loglevel:",
            LA_Chars,     30,
            GTSL_DispFunc,logdispfunc,
            GTSL_LevelFormat,"%s   ",
            LASL_FullCheck,TRUE,
            GTSL_Max,6,
            GTSL_Min,1,
            PGA_Freedom, LORIENT_HORIZ,
            GTSL_Level,  cfg_LogLevel,
            TAG_DONE);

         LT_EndGroup(parthandle);

         LT_New(parthandle,
            LA_Type,      HORIZONTAL_KIND,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      VERTICAL_KIND,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      INTEGER_KIND,
            LA_LabelText, "Max seconds in log buffer",
            LA_Chars,     10,
            LA_ID,        11,
            GTIN_EditHook,&StrEditHookNext,
            GTIN_Number,  cfg_LogBufferSecs,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      INTEGER_KIND,
            LA_LabelText, "Max packet size (in KB)",
            LA_Chars,     10,
            LA_ID,        6,
            GTIN_EditHook,&StrEditHookNext,
            GTIN_Number,  cfg_MaxPktSize/1024,
            TAG_DONE);

         LT_EndGroup(parthandle);

         LT_New(parthandle,
            LA_Type,      VERTICAL_KIND,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      INTEGER_KIND,
            LA_LabelText, "Max lines in log buffer",
            LA_Chars,     10,
            LA_ID,        12,
            GTIN_EditHook,&StrEditHookNext,
            GTIN_Number,  cfg_LogBufferLines,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      INTEGER_KIND,
            LA_LabelText, "Max bundle size (in KB)",
            LA_Chars,     10,
            LA_ID,        7,
            GTIN_EditHook,&StrEditHookNext,
            GTIN_Number,  cfg_MaxBundleSize/1024,
            TAG_DONE);

         LT_EndGroup(parthandle);

         LT_EndGroup(parthandle);

      LT_EndGroup(parthandle);

   LT_EndGroup(parthandle);

   partwin=LT_Build(parthandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW|IDCMP_RAWKEY,
                               LAWN_Parent,   mainwin,
                               LAWN_HelpHook, &GeneralHelpHook,
                               LAWN_Title,    "General",
                               WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                               TAG_END);

   if(!partwin)
   {
      LT_DeleteHandle(parthandle);
      DisplayBeep(NULL);
      return;
   }

/*

   ShowLogLevel(cfg_LogLevel);
*/

   GhostDupe(cfg_DupeMode);

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(parthandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;
         qualifier=IntuiMessage->Qualifier;

         LT_ReplyIMsg(IntuiMessage);

         switch(class)
         {
            case IDCMP_RAWKEY:        if(code == TAB) LT_Activate(parthandle,1);
                                      break;
            case IDCMP_CLOSEWINDOW:   NUMCMPCPY(cfg_DefaultZone,2);
                                      NUMCMPCPY(cfg_LogLevel,4);
                                      NUMCMPCPY(cfg_DupeMode,10);
                                      NUMCMPCPY(cfg_LogBufferSecs,11);
                                      NUMCMPCPY(cfg_LogBufferLines,12);

                                      if(cfg_LocalCharset!=localdata[LT_GetAttributes(parthandle,5,TAG_END)])    saveconfig=TRUE;
                                      cfg_LocalCharset=localdata[LT_GetAttributes(parthandle,5,TAG_END)];

                                      if(cfg_MaxPktSize!=1024*LT_GetAttributes(parthandle,6,TAG_END))    saveconfig=TRUE;
                                      cfg_MaxPktSize=1024*LT_GetAttributes(parthandle,6,TAG_END);

                                      if(cfg_MaxBundleSize!=1024*LT_GetAttributes(parthandle,7,TAG_END))    saveconfig=TRUE;
                                      cfg_MaxBundleSize=1024*LT_GetAttributes(parthandle,7,TAG_END);

                                      if(cfg_DupeSize!=1024*LT_GetAttributes(parthandle,9,TAG_END))    saveconfig=TRUE;
                                      cfg_DupeSize=1024*LT_GetAttributes(parthandle,9,TAG_END);

                                      strcmpcpy(cfg_Sysop,(UBYTE *)LT_GetAttributes(parthandle,1,TAG_END));
                                      strcmpcpy(cfg_LogFile,(UBYTE *)LT_GetAttributes(parthandle,3,TAG_END));
                                      strcmpcpy(cfg_DupeFile,(UBYTE *)LT_GetAttributes(parthandle,8,TAG_END));

                                      LT_DeleteHandle(parthandle);
                                      parthandle=NULL;
                                      return;

            case IDCMP_GADGETUP:      gid=((struct Gadget *)iaddress)->GadgetID;

                                      switch(gid)
                                      {
                                         case 10: GhostDupe(LT_GetAttributes(parthandle,10,TAG_END));
                                                  break;
                                      }

                                      break;

            case IDCMP_IDCMPUPDATE:   gid=((struct Gadget *)iaddress)->GadgetID;

                                      if(gid == 3)
                                      {
                                         strcpy(cfg_LogFile,(UBYTE *)LT_GetAttributes(parthandle,3,TAG_END));

                                         if(OpenReq(cfg_LogFile,40,"Please select file",partwin,FALSE))
                                            saveconfig=TRUE;

                                         LT_SetAttributes(parthandle,3,GTST_String,cfg_LogFile,TAG_END);
                                      }
                                      if(gid == 8)
                                      {
                                         strcpy(cfg_DupeFile,(UBYTE *)LT_GetAttributes(parthandle,8,TAG_END));

                                         if(OpenReq(cfg_DupeFile,40,"Please select file",partwin,FALSE))
                                            saveconfig=TRUE;

                                         LT_SetAttributes(parthandle,8,GTST_String,cfg_DupeFile,TAG_END);
                                      }
                                      break;
         }
      }
      WaitPort(partwin->UserPort);
   }
}
