#include <CrashPrefs.h>

void strcmpcpy(UBYTE *dest,UBYTE *src)
{
   if(strcmp(dest,src)==0)
      return;

   saveconfig=TRUE;
   strcpy(dest,src);
}

/* Routines for parsing and comparing node numbers and node patterns in
   2D and 4D. Even special handling for DestPat:s! */

Parse4D(UBYTE *buf, struct Node4D *node)
{
   ULONG c=0,val=0;
   BOOL GotZone=FALSE,GotNet=FALSE,GotNode=FALSE;

   node->Zone=0;
   node->Net=0;
   node->Node=0;
   node->Point=0;

	for(c=0;c<strlen(buf);c++)
	{
		if(buf[c]==':')
		{
         if(GotZone || GotNet || GotNode) return(FALSE);
			node->Zone=val;
         GotZone=TRUE;
			val=0;
	   }
		else if(buf[c]=='/')
		{
         if(GotNet || GotNode) return(FALSE);
         node->Net=val;
         GotNet=TRUE;
			val=0;
		}
		else if(buf[c]=='.')
		{
         if(GotNode) return(FALSE);
         node->Node=val;
         GotNode=TRUE;
			val=0;
		}
		else if(buf[c]>='0' && buf[c]<='9')
		{
         val*=10;
         val+=buf[c]-'0';
		}
		else return(FALSE);
	}
   if(GotZone && !GotNet)  node->Net=val;
   else if(GotNode)        node->Point=val;
   else                    node->Node=val;

   return(TRUE);
}

int Compare4D(struct Node4D *node1,struct Node4D *node2)
{
   if(node1->Zone!=0 && node2->Zone!=0)
   {
      if(node1->Zone > node2->Zone) return(1);
      if(node1->Zone < node2->Zone) return(-1);
   }

   if(node1->Net  > node2->Net) return(1);
   if(node1->Net  < node2->Net) return(-1);

   if(node1->Node > node2->Node) return(1);
   if(node1->Node < node2->Node) return(-1);

   if(node1->Point > node2->Point) return(1);
   if(node1->Point < node2->Point) return(-1);

   return(0);
}

Parse4DPat(UBYTE *buf, struct Node4DPat *node)
{
   BOOL res;

   node->Zone[0]=0;
   node->Net[0]=0;
   node->Node[0]=0;
   node->Point[0]=0;

   if(strnicmp(buf,"ZONE ",5)==0)
   {
      node->Type=PAT_ZONE;
      res=rawParse4DPat(&buf[5],node);
      strcpy(node->Zone,node->Node);
      node->Net[0]=0;
      node->Node[0]=0;
      node->Point[0]=0;
   }
   else if(strnicmp(buf,"REGION ",7)==0)
   {
      node->Type=PAT_REGION;
      res=rawParse4DPat(&buf[7],node);
      node->Node[0]=0;
      node->Point[0]=0;
   }
   else if(strnicmp(buf,"NET ",4)==0)
   {
      node->Type=PAT_NET;
      res=rawParse4DPat(&buf[4],node);
      node->Node[0]=0;
      node->Point[0]=0;
   }
   else if(strnicmp(buf,"HUB ",4)==0)
   {
      node->Type=PAT_HUB;
      res=rawParse4DPat(&buf[4],node);
      node->Point[0]=0;
   }
   else if(strnicmp(buf,"NODE ",5)==0)
   {
      node->Type=PAT_NODE;
      res=rawParse4DPat(&buf[5],node);
      node->Point[0]=0;
   }
   else
   {
      node->Type=PAT_PATTERN;
      res=rawParse4DPat(buf,node);
   }

   return(res);
}

Parse4DDestPat(UBYTE *buf, struct Node4DPat *node)
{
   BOOL res;

   node->Zone[0]=0;
   node->Net[0]=0;
   node->Node[0]=0;
   node->Point[0]=0;

   res=TRUE;

   if(stricmp(buf,"ZONE")==0)
   {
      node->Type=PAT_ZONE;
   }
   else if(stricmp(buf,"REGION")==0)
   {
      node->Type=PAT_REGION;
   }
   else if(stricmp(buf,"NET")==0)
   {
      node->Type=PAT_NET;
   }
   else if(stricmp(buf,"HUB")==0)
   {
      node->Type=PAT_HUB;
   }
   else if(stricmp(buf,"NODE")==0)
   {
      node->Type=PAT_NODE;
   }
   else
   {
      node->Type=PAT_PATTERN;
      res=rawParse4DPat(buf,node);
   }

   return(res);
}

rawParse4DPat(UBYTE *buf, struct Node4DPat *node)
{
   ULONG c=0,tempc=0;
   UBYTE temp[10];
   BOOL GotZone=FALSE,GotNet=FALSE,GotNode=FALSE;

   strcpy(node->Zone,"*");
   strcpy(node->Net,"*");
   strcpy(node->Node,"*");
   strcpy(node->Point,"*");

   if(strcmp(buf,"*")==0)
      return(TRUE);

	for(c=0;c<strlen(buf);c++)
	{
		if(buf[c]==':')
		{
         if(GotZone || GotNet || GotNode) return(FALSE);
         strcpy(node->Zone,temp);
         GotZone=TRUE;
			tempc=0;
	   }
		else if(buf[c]=='/')
		{
         if(GotNet || GotNode) return(FALSE);
         strcpy(node->Net,temp);
         GotNet=TRUE;
			tempc=0;
		}
		else if(buf[c]=='.')
		{
         if(GotNode) return(FALSE);
         strcpy(node->Node,temp);
         node->Point[0]=0;
         GotNode=TRUE;
			tempc=0;
		}
		else if((buf[c]>='0' && buf[c]<='9') || buf[c]=='*' || buf[c]=='?')
		{
         if(tempc<9)
         {
            temp[tempc++]=buf[c];
            temp[tempc]=0;
         }
		}
		else return(FALSE);
	}

   if(GotZone && !GotNet)
   {
      strcpy(node->Net,temp);
   }
   else if(GotNode)
   {
      strcpy(node->Point,temp);
   }
   else
   {
      strcpy(node->Node,temp);
      strcpy(node->Point,"0");
   }

   return(TRUE);
}

BOOL Parse2DPat(UBYTE *buf, struct Node2DPat *node)
{
   ULONG c=0,tempc=0;
   UBYTE temp[10];
   BOOL GotNet=FALSE;

   strcpy(node->Net,"*");
   strcpy(node->Node,"*");

   if(strcmp(buf,"*")==0)
      return(TRUE);

	for(c=0;c<strlen(buf);c++)
	{
		if(buf[c]=='/')
		{
         if(GotNet) return(FALSE);
         strcpy(node->Net,temp);
         GotNet=TRUE;
			tempc=0;
		}
		else if((buf[c]>='0' && buf[c]<='9') || buf[c]=='*' || buf[c]=='?')
		{
         if(tempc<9)
         {
            temp[tempc++]=buf[c];
            temp[tempc]=0;
         }
		}
		else return(FALSE);
	}

   strcpy(node->Node,temp);
   return(TRUE);
}

int Compare4DPat(struct Node4DPat *nodepat,struct Node4D *node)
{
   int num;

   switch(nodepat->Type)
   {
      case PAT_PATTERN:
         if(node->Zone!=0)
            if(NodeCompare(nodepat->Zone, node->Zone )!=0) return(1);

         if(NodeCompare(nodepat->Net,  node->Net  )!=0) return(1);
         if(NodeCompare(nodepat->Node, node->Node )!=0) return(1);
         if(NodeCompare(nodepat->Point,node->Point)!=0) return(1);
         return(0);

      case PAT_ZONE:
         if(NodeCompare(nodepat->Zone,node->Zone)!=0) return(1);
         return(0);

      case PAT_REGION:
         if(cfg_NodeList[0]==0)
            return(1);

         num=nlGetRegion(node);

         if(num == -1)
            return(1);

         if(node->Zone!=0)
            if(NodeCompare(nodepat->Zone,node->Zone)!=0) return(1);

         if(NodeCompare(nodepat->Net,num)!=0) return(1);
         return(0);

      case PAT_NET:
         if(node->Zone!=0)
            if(NodeCompare(nodepat->Zone,node->Zone)!=0) return(1);

         if(NodeCompare(nodepat->Net,node->Net)!=0) return(1);
         return(0);

      case PAT_HUB:
         if(cfg_NodeList[0]==0)
            return(1);

         num=nlGetHub(node);

         if(num == -1)
            return(1);

         if(node->Zone!=0)
            if(NodeCompare(nodepat->Zone,node->Zone)!=0) return(1);

         if(NodeCompare(nodepat->Net,node->Net)!=0) return(1);
         if(NodeCompare(nodepat->Node,num)!=0) return(1);
         return(0);

      case PAT_NODE:
         if(node->Zone!=0)
            if(NodeCompare(nodepat->Zone, node->Zone )!=0) return(1);

         if(NodeCompare(nodepat->Net,  node->Net  )!=0) return(1);
         if(NodeCompare(nodepat->Node, node->Node )!=0) return(1);
         return(0);
   }
}

int Compare2DPat(struct Node2DPat *nodepat,UWORD net,UWORD node)
{
      if(NodeCompare(nodepat->Net,  net )!=0) return(1);
      if(NodeCompare(nodepat->Node, node)!=0) return(1);
      return(0);
}

int NodeCompare(UBYTE *pat,UWORD num)
{
   UBYTE buf[10],c;
   sprintf(buf,"%lu",num);

   if(pat[0]==0) return(0);

   for(c=0;c<strlen(pat);c++)
   {
      if(pat[c]=='*') return(0);
      if(pat[c]!=buf[c] && pat[c]!='?') return(1);
      if(buf[c]==0) return(1);
   }

   if(buf[c]!=0)
      return(1);

   return(0);
}

Copy4D(struct Node4D *node1,struct Node4D *node2)
{
   node1->Zone=node2->Zone;
   node1->Net=node2->Net;
   node1->Node=node2->Node;
   node1->Point=node2->Point;
}

Copy4DPat(struct Node4DPat *node1,struct Node4DPat *node2)
{
   node1->Type=node2->Type;
   strcpy(node1->Zone,node2->Zone);
   strcpy(node1->Net,node2->Net);
   strcpy(node1->Node,node2->Node);
   strcpy(node1->Point,node2->Point);
}

Copy2DPat(struct Node2DPat *node1,struct Node2DPat *node2)
{
   strcpy(node1->Net,node2->Net);
   strcpy(node1->Node,node2->Node);
}

Print4DPat(struct Node4DPat *pat,UBYTE *dest)
{
   switch(pat->Type)
   {
      case PAT_PATTERN:
         sprintf(dest,"%s:%s/%s.%s",pat->Zone,pat->Net,pat->Node,pat->Point);
         break;
      case PAT_ZONE:
         sprintf(dest,"ZONE %s",pat->Zone);
         break;
      case PAT_REGION:
         sprintf(dest,"REGION %s:%s",pat->Zone,pat->Net);
         break;
      case PAT_NET:
         sprintf(dest,"NET %s:%s",pat->Zone,pat->Net);
         break;
      case PAT_HUB:
         sprintf(dest,"HUB %s:%s/%s",pat->Zone,pat->Net,pat->Node);
         break;
      case PAT_NODE:
         sprintf(dest,"NODE %s:%s/%s",pat->Zone,pat->Net,pat->Node);
         break;
   }
}

Print4DDestPat(struct Node4DPat *pat,UBYTE *dest)
{
   switch(pat->Type)
   {
      case PAT_PATTERN:
         sprintf(dest,"%s:%s/%s.%s",pat->Zone,pat->Net,pat->Node,pat->Point);
         break;
      case PAT_ZONE:
         sprintf(dest,"ZONE");
         break;
      case PAT_REGION:
         sprintf(dest,"REGION");
         break;
      case PAT_NET:
         sprintf(dest,"NET");
         break;
      case PAT_HUB:
         sprintf(dest,"HUB");
         break;
      case PAT_NODE:
         sprintf(dest,"NODE");
         break;
   }
}

BOOL Check4DPatNodelist(struct Node4DPat *pat)
{
   if(cfg_NodeList[0])
      return(TRUE);

   if(pat->Type == PAT_HUB || pat->Type == PAT_REGION)
      return(FALSE);

   return(TRUE);
}

/* ReqTools filerequester */

BOOL OpenReq(UBYTE *file,ULONG varsize,UBYTE *title,struct Window *win,BOOL save)
{
   ULONG flags=0;
   APTR req;
   UBYTE buf[200],dir[200];
   APTR res;

   if(!(req=rtAllocRequest(RT_FILEREQ,TAG_END)))
   {
      DisplayBeep(NULL);
      return(FALSE);
   }

   strcpy(buf,FilePart(file));

   strcpy(dir,file);
   *(PathPart(dir))=0;

   rtChangeReqAttr(req,RTFI_Dir,dir,TAG_END);

   if(save) flags=FREQF_SAVE;

   res = rtFileRequest((struct rtFileRequester *)req,buf,title,RT_Window,      win,
                                                               RT_WaitPointer, TRUE,
                                                               RT_LockWindow,  TRUE,
                                                               RTFI_Flags,     flags,
                                                               TAG_END);

   if(res)
   {
      strncpy(file,((struct rtFileRequester *)req)->Dir,varsize-1);
      file[varsize-1]=0;
      AddPart(file,buf,varsize-1);
   }

   rtFreeRequest(req);

   if(res) return(TRUE);
   return(FALSE);
}

BOOL OpenReqDir(UBYTE *file,ULONG varsize,UBYTE *title,struct Window *win)
{
   APTR req;
   UBYTE buf[200];
   APTR res;

   if(!(req=rtAllocRequest(RT_FILEREQ,TAG_END)))
   {
      DisplayBeep(NULL);
      return(FALSE);
   }

   rtChangeReqAttr(req,RTFI_Dir,file,TAG_END);

   res = rtFileRequest((struct rtFileRequester *)req,buf,title,RT_Window,      win,
                                                               RT_WaitPointer, TRUE,
                                                               RT_LockWindow,  TRUE,
                                                               RTFI_Flags,     FREQF_NOFILES,
                                                               TAG_END);

   if(res)
   {
      strncpy(file,((struct rtFileRequester *)req)->Dir,varsize-1);
      file[varsize-1]=0;
   }

   rtFreeRequest(req);

   if(res) return(TRUE);
   return(FALSE);
}

struct Menu *asklistmenus;

struct NewMenu asklistnewmenus[] = {
   {NM_TITLE,"Find","",0,0,NULL},
   {NM_ITEM, "Find...","F",0,0,NULL},
   {NM_ITEM, "Find next","N",0,0,NULL},
   {NM_END,  "","",0,0,NULL}};

UBYTE askfindbuf[100];
struct Window *askwin;
struct LayoutHandle *askhandle;
LONG asklistres;

void AskFindNext(struct List *l);

void AskFind(struct List *l)
{
   BOOL res;

   res=rtGetString(askfindbuf,99,"Enter string to find",NULL,
            RT_Window,      askwin,
            RT_WaitPointer, TRUE,
            RT_LockWindow,  TRUE,
            TAG_END);

   if(!res)
      return;

   AskFindNext(l);
}

UBYTE MakeSmall(UBYTE *src,UBYTE *dest);

void AskFindNext(struct List *l)
{
   struct Node *n;
   ULONG num,i;
   UBYTE findtemp[100];
   UBYTE tagtemp[100];

   if(!askfindbuf[0])
   {
      AskFind(l);
      return;
   }

   n=l->lh_Head;
   for(i=0;i<asklistres;i++) n=n->ln_Succ;

   num=asklistres+1;
   n=n->ln_Succ;

   MakeSmall(askfindbuf,findtemp);

   while(n->ln_Succ)
   {
      MakeSmall(n->ln_Name,tagtemp);

      if(strstr(tagtemp,findtemp))
      {
         break;
      }
      else
      {
         n=n->ln_Succ;
         num++;
      }
   }

   if(n->ln_Succ)
   {
      LT_SetAttributes(askhandle,4711,GTLV_Selected,num,GTLV_MakeVisible,num,TAG_END);
      asklistres=num;
   }
   else
   {
      DisplayBeep(NULL);
   }
}

ULONG AskList(struct List *list,UBYTE *title,struct Window *win)
{
   struct Node *node;
   struct IntuiMessage *IntuiMessage;
   ULONG class,code,num;
   APTR iaddress;
   struct MenuItem *menuitem;

   LT_LockWindow(win);

   askhandle = LT_CreateHandleTags(win->WScreen,TAG_DONE);

   if(!askhandle)
   {
      DisplayBeep(NULL);
      LT_UnlockWindow(win);
      return;
   }

   asklistmenus=LT_LayoutMenus(askhandle,asklistnewmenus,GTMN_FrontPen,0L,
                                                         GTMN_TextAttr,win->WScreen,
                                                         GTMN_NewLookMenus,TRUE,
                                                         LH_MenuGlyphs,TRUE,
                                                         TAG_DONE);

   if(!asklistmenus)
   {
      LT_DeleteHandle(askhandle);
      DisplayBeep(NULL);
      LT_UnlockWindow(win);
      return;
   }

   LT_New(askhandle,
      LA_Type,      VERTICAL_KIND,
      TAG_DONE);

      LT_New(askhandle,
         LA_Type,       LISTVIEW_KIND,
         LALV_Lines,    15,
         LALV_CursorKey,TRUE,
         LALV_Link,     NIL_LINK,
         LALV_LockSize, TRUE,
         LA_Chars,      40,
         LA_ID,4711,
         TAG_DONE);

      LT_New(askhandle,
         LA_Type,      XBAR_KIND,
         TAG_END);

      LT_New(askhandle,
         LA_Type,      HORIZONTAL_KIND,
         LAGR_SameSize,TRUE,
         TAG_DONE);

         LT_New(askhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Ok",
            LA_Chars,19,
            GA_Disabled,   TRUE,
            LABT_ReturnKey,TRUE,
            LA_ID,10,
            TAG_DONE);

         LT_New(askhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Cancel",
            LA_Chars,19,
            LA_ID,11,
            TAG_DONE);

      LT_EndGroup(askhandle);

   LT_EndGroup(askhandle);

   askwin=LT_Build(askhandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW,
                             LAWN_Title,    title,
                             WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                             LAWN_Menu,     asklistmenus,
                             TAG_END);

   if(!askwin)
   {
      LT_DeleteHandle(askhandle);
	   FreeMenus(asklistmenus);
      DisplayBeep(NULL);
      LT_UnlockWindow(win);
      return;
   }


  LT_SetAttributes(askhandle,4711,GTLV_Labels,list,TAG_END);

   asklistres=-1;

   num=0;

   for(node=list->lh_Head;node->ln_Succ;node=node->ln_Succ)
      num++;

   if(num)
   {
      LT_SetAttributes(askhandle,10,GA_Disabled,FALSE,TAG_END);
      LT_SetAttributes(askhandle,4711,GTLV_Selected,0,TAG_END);
      asklistres=0;
   }

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(askhandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;

         LT_ReplyIMsg(IntuiMessage);

         switch(class)
         {
            case IDCMP_MENUPICK:       num=code;
                                       while(num!=MENUNULL)
                                       {
                                          if(MENUNUM(num)==0)
                                          {
                                             switch(ITEMNUM(num))
                                             {
                                                case 0: AskFind(list);
                                                        break;
                                                case 1: AskFindNext(list);
                                                        break;
                                             }
                                          }
                                          menuitem=ItemAddress(asklistmenus,num);
                                          num=menuitem->NextSelect;
                                       }
                                       break;

             case IDCMP_GADGETUP:      switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 10:
                                             LT_DeleteHandle(askhandle);
                                             LT_UnlockWindow(win);
                                             return(asklistres);
                                          case 11:
                                             LT_DeleteHandle(askhandle);
                                             LT_UnlockWindow(win);
                                             return(-1);
                                          case 4711:
                                             asklistres=code;
                                             LT_SetAttributes(askhandle,10,GA_Disabled,FALSE,TAG_END);
                                             break;
                                       }
                                       break;
            case IDCMP_IDCMPUPDATE:    switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 4711:
                                             LT_DeleteHandle(askhandle);
                                             LT_UnlockWindow(win);
                                       	   FreeMenus(asklistmenus);
                                             return(asklistres);
                                       }
                                       break;
            case IDCMP_CLOSEWINDOW:    LT_DeleteHandle(askhandle);
                                       LT_UnlockWindow(win);
                                       return(-1);
         }
      }
      WaitPort(askwin->UserPort);
   }
}

int EditGroups(UBYTE *groups,UBYTE *title,struct Window *win)
{
   struct IntuiMessage *IntuiMessage;
   ULONG class,code;
   APTR iaddress;
   struct List list;
   struct TempNode *tempnodes;
   ULONG c,d;

   NewList(&list);

   if(!(tempnodes=AllocMem(26*sizeof(struct TempNode),MEMF_CLEAR)))
   {
      DisplayBeep(NULL);
      return(FALSE);
   }

   for(c=0;c<26;c++)
   {
      if(CheckFlags('A'+c,groups))
      {
         sprintf(tempnodes[c].buf,"*%lc: %s",'A'+c,cfg_GroupNames[c]);
         tempnodes[c].UserData=(APTR)TRUE;
      }
      else
      {
         sprintf(tempnodes[c].buf,"%lc: %s",'A'+c,cfg_GroupNames[c]);
         tempnodes[c].UserData=(APTR)FALSE;
      }

      tempnodes[c].Node.ln_Name=tempnodes[c].buf;
      AddTail(&list,(struct Node *)&tempnodes[c]);
   }

   LT_LockWindow(win);

   askhandle = LT_CreateHandleTags(win->WScreen,TAG_DONE);

   if(!askhandle)
   {
      DisplayBeep(NULL);
      LT_UnlockWindow(win);
      FreeMem(tempnodes,26*sizeof(struct TempNode));
      return(FALSE);
   }

   LT_New(askhandle,
      LA_Type,      VERTICAL_KIND,
      TAG_DONE);

      LT_New(askhandle,
         LA_Type,       LISTVIEW_KIND,
         LALV_Lines,    15,
         LALV_CursorKey,TRUE,
         LALV_Link,     NIL_LINK,
         LALV_LockSize, TRUE,
         GTLV_Labels,   &list,
         LA_Chars,      40,
         LA_ID,4711,
         TAG_DONE);

      LT_New(askhandle,
         LA_Type,      XBAR_KIND,
         TAG_END);

      LT_New(askhandle,
         LA_Type,      HORIZONTAL_KIND,
         LAGR_SameSize,TRUE,
         TAG_DONE);

         LT_New(askhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Ok",
            LA_Chars,19,
            LABT_ReturnKey,TRUE,
            LA_ID,10,
            TAG_DONE);

         LT_New(askhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Cancel",
            LA_Chars,19,
            LA_ID,11,
            TAG_DONE);

      LT_EndGroup(askhandle);

   LT_EndGroup(askhandle);

   askwin=LT_Build(askhandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW,
                             LAWN_Title,    title,
                             WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                             TAG_END);

   if(!askwin)
   {
      LT_DeleteHandle(askhandle);
      DisplayBeep(NULL);
      LT_UnlockWindow(win);
      FreeMem(tempnodes,26*sizeof(struct TempNode));
      return(FALSE);
   }

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(askhandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;

         LT_ReplyIMsg(IntuiMessage);

         switch(class)
         {
              case IDCMP_GADGETUP:     switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 10:
                                             LT_DeleteHandle(askhandle);
                                             LT_UnlockWindow(win);

                                             d=0;

                                             for(c=0;c<26;c++)
                                                if(tempnodes[c].UserData) groups[d++]='A'+c;

                                             groups[d]=0;

                                             FreeMem(tempnodes,26*sizeof(struct TempNode));
                                             return(TRUE);

                                          case 11:
                                             LT_DeleteHandle(askhandle);
                                             LT_UnlockWindow(win);
                                             FreeMem(tempnodes,26*sizeof(struct TempNode));
                                             return(FALSE);
                                       }
                                       break;
            case IDCMP_IDCMPUPDATE:    switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 4711:
                                             LT_SetAttributes(askhandle,4711,GTLV_Labels,~0,TAG_END);

                                             if(tempnodes[code].UserData)
                                             {
                                                tempnodes[code].UserData=(APTR)FALSE;
                                                sprintf(tempnodes[code].buf,"%lc: %s",'A'+code,cfg_GroupNames[code]);
                                             }
                                             else
                                             {
                                                tempnodes[code].UserData=(APTR)TRUE;
                                                sprintf(tempnodes[code].buf,"*%lc: %s",'A'+code,cfg_GroupNames[code]);
                                             }

                                             LT_SetAttributes(askhandle,4711,GTLV_Labels,&list,TAG_END);
                                             break;
                                       }
                                       break;
            case IDCMP_CLOSEWINDOW:    LT_DeleteHandle(askhandle);
                                       LT_UnlockWindow(win);
                                       FreeMem(tempnodes,26*sizeof(struct TempNode));
                                       return(FALSE);
         }
      }
      WaitPort(askwin->UserPort);
   }
}

int MakeCNodeList(struct List *list)
{
   struct TempNode *tempnode;
   struct ConfigNode *cnode;

   NewList(list);

   for(cnode=(struct ConfigNode *)CNodeList.First;cnode;cnode=cnode->Next)
   {
      if(!(tempnode=AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(list);
         return(FALSE);
      }

      PrintFull4D(&cnode->Node,tempnode->buf);
      tempnode->Node.ln_Name=tempnode->buf;
      tempnode->UserData=cnode;

      AddTail(list,(struct Node *)tempnode);
   }

   return(TRUE);
}

struct ConfigNode *AskCNode(struct Window *win)
{
   struct List list;
   long res,c;
   struct ConfigNode *cnode;

   if(!MakeCNodeList(&list))
      return(NULL);

   res=AskList(&list,"Select node",win);
   FreeTempNodes(&list);

   if(res == -1)
      return(NULL);

   cnode=(struct ConfigNode *)CNodeList.First;

   for(c=0;c<res;c++)
      cnode=cnode->Next;

   return(cnode);
}

int MakeAkaList(struct List *list)
{
   struct TempNode *tempnode;
   struct Aka *aka;

   NewList(list);

   for(aka=(struct Aka *)AkaList.First;aka;aka=aka->Next)
   {
      if(!(tempnode=AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(list);
         return(FALSE);
      }

      PrintFull4D(&aka->Node,tempnode->buf);
      tempnode->Node.ln_Name=tempnode->buf;
      tempnode->UserData=aka;

      AddTail(list,(struct Node *)tempnode);
   }

   return(TRUE);
}

struct Aka *AskAka(struct Window *win)
{
   struct List list;
   long res,c;
   struct Aka *aka;

   if(!MakeAkaList(&list))
      return(NULL);

   res=AskList(&list,"Select aka",win);
   FreeTempNodes(&list);

   if(res == -1)
      return(NULL);

   aka=(struct Aka *)AkaList.First;

   for(c=0;c<res;c++)
      aka=aka->Next;

   return(aka);
}

UBYTE AskGroup(struct Window *win,UBYTE *nullname)
{
   struct List list;
   long res,c;
   UBYTE ret;
   struct TempNode *tempnodes,*tn;

   NewList(&list);

   if(!(tempnodes=AllocMem(27*sizeof(struct TempNode),MEMF_CLEAR)))
   {
      DisplayBeep(NULL);
      return(255);
   }

   if(nullname)
   {
      strcpy(tempnodes[0].buf,nullname);
      tempnodes[0].UserData=0;
      tempnodes[0].Node.ln_Name=tempnodes[0].buf;
      AddTail(&list,(struct Node *)&tempnodes[0]);
   }

   for(c=1;c<27;c++)
   {
      sprintf(tempnodes[c].buf,"%lc: %s",'A'+c-1,cfg_GroupNames[c-1]);
      tempnodes[c].UserData=(APTR)('A'+c-1);
      tempnodes[c].Node.ln_Name=tempnodes[c].buf;
      AddTail(&list,(struct Node *)&tempnodes[c]);
   }

   res=AskList(&list,"Select group",win);

   if(res != -1)
   {
      tn=(struct TempNode *)list.lh_Head;

      for(c=0;c<res;c++)
         tn=(struct TempNode *)tn->Node.ln_Succ;

      ret=(UBYTE)tn->UserData;
   }
   else
   {
      ret=255;
   }

   FreeMem(tempnodes,27*sizeof(struct TempNode));

   return(ret);
}


void jbSwapNodes(struct jbList *list,struct jbNode *n1)
{
   struct jbNode *prev,*tmp,*n2;

   prev=NULL;
   n2=n1->Next;

   for(tmp=list->First;tmp;tmp=tmp->Next)
      if(tmp->Next == n1) prev=tmp;

   if(prev)
      prev->Next=n2;

   n1->Next=n2->Next;
   n2->Next=n1;

   if(list->First == n1) list->First=n2;
   if(list->Last == n2)  list->Last=n1;
}

void MoveDown(struct jbList *list,ULONG num)
{
   struct jbNode *jbn;
   ULONG c;

   jbn=list->First;

   for(c=0;c<num;c++)
     jbn=jbn->Next;

   jbSwapNodes(list,jbn);
}

void MoveUp(struct jbList *list,ULONG num)
{
   struct jbNode *jbn;
   ULONG c;

   jbn=list->First;

   for(c=0;c<num-1;c++)
     jbn=jbn->Next;

   jbSwapNodes(list,jbn);
}


void PrintFull4D(struct Node4D *node,UBYTE *buf)
{
   sprintf(buf,"%lu:%lu/%lu.%lu",
      node->Zone,node->Net,node->Node,node->Point);
}

BOOL CheckFlags(UBYTE group,UBYTE *node)
{
   UBYTE c;

   for(c=0;c<strlen(node);c++)
   {
      if(group==node[c])
         return(TRUE);
    }

   return(FALSE);
}

