#include <CrashPrefs.h>

struct List charsetlist;
ULONG charsetsnum,currentcharset;

UBYTE *aliaschrs[]={IbmToAmiga,SF7ToAmiga,MacToAmiga,NULL};
UBYTE *aliaschrsname[]={"IBMPC","SWEDISH","MAC","LATIN-1",NULL};

struct Hook CharsetHelpHook = { {NULL}, HelpHookEntry,NULL,HELP_CHARSET};

void SetCharset(ULONG num)
{
   struct CharsetAlias *charset;
   ULONG c;

   charset=CharsetList.First;

   if(charsetsnum == 0)
   {
      LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,69,GA_Disabled,TRUE,TAG_END);
   }

   if(num!=-1)
   {
      for(c=0;c<num;c++)
         charset=charset->Next;

      if(charsetsnum)
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,FALSE,TAG_END);
      }
      else
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
      }

      if(num!=0)                 LT_SetAttributes(parthandle,12,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);

      if(num!=charsetsnum-1)     LT_SetAttributes(parthandle,13,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);

      LT_SetAttributes(parthandle,69,GA_Disabled,FALSE,TAG_END);

      for(c=0;aliaschrs[c];c++)
         if(aliaschrs[c]==charset->CHRS) break;

      LT_SetAttributes(parthandle,69,GTCY_Active,c,TAG_END);
   }

   currentcharset=num;
}

void RefreshCharsetlist(void)
{
   struct TempNode *tempnode;
   struct CharsetAlias *charset;

   LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
   FreeTempNodes(&charsetlist);

   charsetsnum=0;

   for(charset=CharsetList.First;charset;charset=charset->Next)
   {
      if(!(tempnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(&charsetlist);
         return;
      }
      strcpy(tempnode->buf,charset->Name);
      tempnode->Node.ln_Name=tempnode->buf;
      AddTail(&charsetlist,(struct Node *)tempnode);
      charsetsnum++;
   }

   LT_SetAttributes(parthandle,4711,GTLV_Labels,&charsetlist,TAG_END);
}

BOOL CheckDupeCharset(UBYTE *buf,ULONG num)
{
   struct CharsetAlias *charset;
   ULONG c;

   if(strncmp(buf,"IBMPC",5)==0     ||
      strncmp(buf,"SWEDISH",7)==0   ||
      strncmp(buf,"MAC",3)==0       ||
      strncmp(buf,"LATIN-1",7)==0     )
   {
      rtEZRequestTags("That charset is already recognized by CrashMail","_Ok",NULL,NULL,RT_Underscore,'_',
                                                                                 RT_Window,partwin,
                                                                                 RT_WaitPointer,TRUE,
                                                                                 RT_LockWindow, TRUE,
                                                                                 TAG_END);

      return(FALSE);
   }

   for(c=0,charset=CharsetList.First;charset;charset=charset->Next,c++)
      if(stricmp(charset->Name,buf)==0 && c!=num) break;

   if(charset)
   {
      rtEZRequestTags("You already have a charset alias with that name","_Ok",NULL,NULL,RT_Underscore,'_',
                                                                                 RT_Window,partwin,
                                                                                 RT_WaitPointer,TRUE,
                                                                                 RT_LockWindow, TRUE,
                                                                                 TAG_END);

      return(FALSE);
   }

   return(TRUE);
}

void AddCharset(void)
{
   struct CharsetAlias *charset;
   UBYTE buf[100];
   BOOL res;

   buf[0]=0;
   res=rtGetString(buf,30,"Add charset alias",NULL,RT_Window,partwin,
                                                   RT_WaitPointer, TRUE,
                                                   RT_LockWindow, TRUE,
                                                   TAG_END);

   if(!res)
      return;

   if(!CheckDupeCharset(buf,-1))
      return;

   if(charset=(struct CharsetAlias *)AllocMem(sizeof(struct CharsetAlias),MEMF_CLEAR))
   {
      jbAddNode(&CharsetList,(struct jbNode *)charset);
      strcpy(charset->Name,buf);
      RefreshCharsetlist();
      SetCharset(charsetsnum-1);
      LT_SetAttributes(parthandle,4711,GTLV_Selected,currentcharset,GTLV_MakeVisible,currentcharset,TAG_END);
      saveconfig=TRUE;
   }
   else
   {
      DisplayBeep(NULL);
   }
}

void RemCharset(void)
{
   ULONG c,tmp;
   struct CharsetAlias *charset;
   UBYTE buf[100];
   BOOL res;

   charset=CharsetList.First;
   for(c=0;c<currentcharset;c++) charset=charset->Next;

   sprintf(buf,"Do you really want to remove the charset alias \"%s\"?",charset->Name);

   res=rtEZRequestTags(buf,"_Remove|_Cancel",NULL,NULL,RT_Underscore,'_',
                                                       RT_Window,partwin,
                                                       RT_WaitPointer,TRUE,
                                                       RT_LockWindow, TRUE,
                                                       RTEZ_Flags, EZREQF_CENTERTEXT | EZREQF_NORETURNKEY,
                                                       TAG_END);

   if(!res)
      return;

   tmp=currentcharset;
   jbFreeNode(&CharsetList,(struct jbNode *)charset,sizeof(struct CharsetAlias));
   RefreshCharsetlist();
   if(tmp == charsetsnum) tmp--;
   SetCharset(tmp);
   saveconfig=TRUE;
}

void PartCharset(void)
{
   ULONG class,code,tmp;
   struct IntuiMessage *IntuiMessage;
   struct TempNode *tempnode;
   struct CharsetAlias *charset;
   APTR iaddress;
   ULONG c;

   NewList(&charsetlist);
   currentcharset=-1;

   RefreshCharsetlist();

   parthandle = LT_CreateHandleTags(scr,LH_EditHook,&StrEditHook,LH_RawKeyFilter,FALSE,TAG_DONE);

   if(!parthandle)
   {
      FreeTempNodes(&charsetlist);
      return;
   }

   LT_New(parthandle,
      LA_Type,      VERTICAL_KIND,
      LA_LabelText, "Charset alias",
      TAG_DONE);

      LT_New(parthandle,
         LA_Type,      STRING_KIND,
         GTST_MaxChars,30,
         LA_ID,        4712,
         LAST_Link,    4711,
         GA_Disabled,TRUE,
         TAG_DONE);

      LT_New(parthandle,
         LA_Type,       LISTVIEW_KIND,
         LALV_Lines,    10,
         LALV_CursorKey,TRUE,
         LALV_Link,4712,
         GTLV_Labels,&charsetlist,
         LA_ID,4711,
         TAG_DONE);

      LT_New(parthandle,
         LA_Type,       CYCLE_KIND,
         GTCY_Labels,aliaschrsname,
         LA_ExtraSpace,TRUE,
         LA_LabelText, "Real charset",
         LA_LabelPlace,PLACE_ABOVE,
         GA_Disabled,TRUE,
         LA_ID,69,
         TAG_DONE);

      LT_New(parthandle,
         LA_Type,      HORIZONTAL_KIND,
         LAGR_SameSize,TRUE,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      VERTICAL_KIND,
            LAGR_SameSize,TRUE,
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      BUTTON_KIND,
               LA_LabelText, "Add...",
               LA_Chars,20,
               LA_ID,10,
               TAG_DONE);

            LT_New(parthandle,
               LA_Type,      BUTTON_KIND,
               LA_LabelText, "Up",
               GA_Disabled,TRUE,
               LA_Chars,20,
               LA_ID,12,
               TAG_DONE);

         LT_EndGroup(parthandle);

         LT_New(parthandle,
            LA_Type,      VERTICAL_KIND,
            LAGR_SameSize,TRUE,
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      BUTTON_KIND,
               LA_LabelText, "Remove...",
               LA_Chars,20,
               GA_Disabled,TRUE,
               LA_ID,11,
               TAG_DONE);

            LT_New(parthandle,
               LA_Type,      BUTTON_KIND,
               LA_LabelText, "Down",
               LA_Chars,20,
               GA_Disabled,TRUE,
               LA_ID,13,
               TAG_DONE);

         LT_EndGroup(parthandle);

      LT_EndGroup(parthandle);

   LT_EndGroup(parthandle);

   partwin=LT_Build(parthandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW|IDCMP_RAWKEY,
                               LAWN_Title,    "Charset alias",
                               LAWN_HelpHook, &CharsetHelpHook,
                               WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                               TAG_END);

   if(!partwin)
   {
      LT_DeleteHandle(parthandle);
      DisplayBeep(NULL);
      FreeTempNodes(&charsetlist);
      return;
   }

   if(charsetsnum)
   {
      LT_SetAttributes(parthandle,4711,GTLV_Selected,0,TAG_END);
      SetCharset(0);
   }

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(parthandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;

         LT_ReplyIMsg(IntuiMessage);

         switch(class)
         {
            case IDCMP_RAWKEY:         if(code == TAB) LT_Activate(parthandle,4712);
                                       break;
            case IDCMP_GADGETUP:       switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 10:
                                             AddCharset();
                                             break;
                                          case 11:
                                             RemCharset();
                                             break;
                                          case 12:
                                             if(currentcharset != 0)
                                             {
                                                tmp=currentcharset;
                                                MoveUp(&CharsetList,tmp);
                                                RefreshCharsetlist();
                                                SetCharset(tmp-1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentcharset,
                                                   GTLV_MakeVisible,currentcharset,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 13:
                                             if(currentcharset != charsetsnum-1)
                                             {
                                                tmp=currentcharset;
                                                MoveDown(&CharsetList,tmp);
                                                RefreshCharsetlist();
                                                SetCharset(tmp+1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentcharset,
                                                   GTLV_MakeVisible,currentcharset,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 69:
                                             charset=CharsetList.First;
                                             for(c=0;c<currentcharset;c++) charset=charset->Next;
                                             charset->CHRS=aliaschrs[code];
                                             break;
                                          case 4711:
                                             SetCharset(code);
                                             break;
                                          case 4712:
                                             if(CheckDupeCharset(getgadgetstring((struct Gadget *)iaddress),currentcharset))
                                             {
                                                tempnode=charsetlist.lh_Head;
                                                for(c=0;c<currentcharset;c++) tempnode=tempnode->Node.ln_Succ;
                                                LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
                                                strcpy(tempnode->buf,getgadgetstring((struct Gadget *)iaddress));
                                                LT_SetAttributes(parthandle,4711,GTLV_Labels,&charsetlist,TAG_END);

                                                charset=CharsetList.First;
                                                for(c=0;c<currentcharset;c++) charset=charset->Next;
                                                strcpy(charset->Name,tempnode->buf);
                                             }
                                             else
                                             {
                                                LT_Activate(parthandle,4712);
                                             }
                                             break;
                                       }
                                       break;
            case IDCMP_CLOSEWINDOW:    LT_DeleteHandle(parthandle);
                                       parthandle=NULL;
                                       FreeTempNodes(&charsetlist);
                                       return;
         }
      }
      WaitPort(partwin->UserPort);
   }
}
