#include <CrashPrefs.h>

void CleanUp(int);

BOOL NewConfig(void)
{
   BOOL res;
   BOOL kg;
   UBYTE buf[40];

   UBYTE basepath[40];
   UBYTE Sysop[40];
   struct Node4D Feed4D;
   struct Node4D Aka4D;
   BOOL UMS;

   struct Aka *aka;
   struct ConfigNode *cnode;
   struct Packer *packer;
   struct Area *area;
   struct Route *route;

   struct AreaFixName *areafix;

   strcpy(Sysop,cfg_Sysop);

   res=rtGetString(Sysop,40,"Create new configuration",NULL,
      RTGL_TextFmt,   "In order to create a new configuration,\n"
                      "you must answer a few questions.\n\n"
                      "What is your name?",
      RTGL_Flags,     GLREQF_CENTERTEXT,
      RT_Window,      mainwin,
      RT_WaitPointer, TRUE,
      RT_LockWindow,  TRUE,
      TAG_END);

   if(!res)
      return(FALSE);

   kg=TRUE;
   buf[0]=0;

   while(kg)
   {
      res=rtGetString(buf,40,"Create new configuration",NULL,
         RTGL_TextFmt,   "Enter your main node address.\n\n"
                         "(Other akas can be configured later)",
         RTGL_Flags,     GLREQF_CENTERTEXT,
         RT_Window,      mainwin,
         RT_WaitPointer, TRUE,
         RT_LockWindow,  TRUE,
         TAG_END);

      if(!res)
         return(FALSE);

      kg=FALSE;

      if(!Parse4D(buf,&Aka4D))
      {
         rtEZRequestTags("Invalid node number","_Ok",NULL,NULL,RT_Underscore,'_',
                                                               RT_Window,mainwin,
                                                               RT_WaitPointer, TRUE,
                                                               RT_LockWindow, TRUE,
                                                               TAG_END);
         kg=TRUE;
      }
   }

   buf[0]=0;
   kg=TRUE;

   while(kg)
   {
      res=rtGetString(buf,40,"Create new configuration",NULL,
         RTGL_TextFmt,   "Enter your main mail feed.\n\n"
                         "(Other nodes and downlinks can be configured later)",
         RTGL_Flags,     GLREQF_CENTERTEXT,
         RT_Window,      mainwin,
         RT_WaitPointer, TRUE,
         RT_LockWindow,  TRUE,
         TAG_END);

      if(!res)
         return(FALSE);

      kg=FALSE;

      if(!Parse4D(buf,&Feed4D))
      {
         rtEZRequestTags("Invalid node number","_Ok",NULL,NULL,RT_Underscore,'_',
                                                               RT_Window,mainwin,
                                                               RT_WaitPointer, TRUE,
                                                               RT_LockWindow, TRUE,
                                                               TAG_END);
         kg=TRUE;
      }
   }

   res=rtEZRequestTags("Which messagebase do you want to use?","*._MSG|_UMS|Cancel",NULL,NULL,
         RT_Underscore,'_',
         RT_Window,mainwin,
         RT_WaitPointer,TRUE,
         RT_LockWindow, TRUE,
         TAG_END);

   if(!res)
      return(FALSE);

   if(res == 1) UMS=FALSE;
   else         UMS=TRUE;

   if(UMS)
   {
      strcpy(basepath,"fidonet.");

      res=rtGetString(basepath,80,"Create new configuration",NULL,
         RTGL_TextFmt,   "Enter the prefix you want to have for auto-added UMS areas",
         RTGL_Flags,     GLREQF_CENTERTEXT,
         RT_Window,      mainwin,
         RT_WaitPointer, TRUE,
         RT_LockWindow,  TRUE,
         TAG_END);

      if(!res)
         return(FALSE);
   }
   else
   {
      strcpy(basepath,"MAIL:Areas");

      res=rtGetString(basepath,80,"Create new configuration",NULL,
         RTGL_TextFmt,   "Enter directory where auto-added *.msg areas\n"
                         "should be created",
         RTGL_Flags,     GLREQF_CENTERTEXT,
         RT_Window,      mainwin,
         RT_WaitPointer, TRUE,
         RT_LockWindow,  TRUE,
         TAG_END);

      if(!res)
         return(FALSE);
   }

   strcpy(cfg_GroupNames[0],"Default");
   strcpy(cfg_Sysop,Sysop);
   strcpy(cfg_AreaFixHelp,"PROGDIR:AreafixHelp.doc");

   strcpy(cfg_GatewayName,"UUCP");

   cfg_GatewayNode.Zone=1;
   cfg_GatewayNode.Net=1;
   cfg_GatewayNode.Node=31;
   cfg_GatewayNode.Point=0;

   cfg_DupeMode=DUPE_BAD;
   cfg_DupeSize=64*1024;

   cfg_Flags|=CFG_UMSKEEPORIGIN|CFG_CHANGEUMSMSGID;
   cfg_Flags|=CFG_IMPORTAREAFIX|CFG_AREAFIXREMOVE|CFG_ALLOWRESCAN;
   cfg_Flags|=CFG_AUTOADD|CFG_STRIPRE|CFG_ANSWERRECEIPT;
   cfg_Flags|=CFG_FORCEINTL|CFG_ANSWERAUDIT;
   cfg_Flags|=CFG_CHECKSEENBY|CFG_PATH3D;
   cfg_Flags|=CFG_IMPORTSEENBY;
   cfg_Flags|=CFG_BOUNCEPOINTS;
   cfg_Flags|=CFG_ADDTID;
   cfg_Flags|=CFG_MSGHIGHWATER|CFG_MSGWRITEBACK;

   if(!(packer=(struct Packer *)AllocMem(sizeof(struct Packer),MEMF_CLEAR)))
      CleanUp(10);

   strcpy(packer->Name,"LHA");
   strcpy(packer->Packer,"LhA a %a %f");
   strcpy(packer->Unpacker,"LhA x -m -M %a #?.pkt");
   strcpy(packer->Recog,"\?\?-lh?-");

   jbAddNode(&PackerList,(struct jbNode *)packer);

   if(!(packer=(struct Packer *)AllocMem(sizeof(struct Packer),MEMF_CLEAR)))
      CleanUp(10);

   strcpy(packer->Name,"LZH");
   strcpy(packer->Packer,"LhA -0 a %a %f");
   strcpy(packer->Unpacker,"LhA x -m -M %a #?.pkt");
   strcpy(packer->Recog,"\?\?-lh?-");

   jbAddNode(&PackerList,(struct jbNode *)packer);

   if(!(packer=(struct Packer *)AllocMem(sizeof(struct Packer),MEMF_CLEAR)))
      CleanUp(10);

   strcpy(packer->Name,"ZIP");
   strcpy(packer->Packer,"Zip %a %f");
   strcpy(packer->Unpacker,"UnZip -j %a");
   strcpy(packer->Recog,"PK");

   jbAddNode(&PackerList,(struct jbNode *)packer);

   if(!(aka=(struct Aka *)AllocMem(sizeof(struct Aka),MEMF_CLEAR)))
      CleanUp(10);

   Copy4D(&aka->Node,&Aka4D);
   strcpy(aka->Domain,"FidoNet");
   jbNewList(&aka->AddList);
   jbNewList(&aka->RemList);

   jbAddNode(&AkaList,(struct jbNode *)aka);

   if(!(cnode=(struct ConfigNode *)AllocMem(sizeof(struct ConfigNode),MEMF_CLEAR)))
      CleanUp(10);

   Copy4D(&cnode->Node,&Feed4D);
   cnode->Packer=(struct Packer *)PackerList.First;
   cnode->DefaultGroup='A';
   cnode->Flags |= NODE_AUTOADD | NODE_PACKNETMAIL;
   jbNewList(&cnode->RemoteAFList);

   jbAddNode(&CNodeList,(struct jbNode *)cnode);

   if(!(area=(struct Area *)AllocMem(sizeof(struct Area),MEMF_CLEAR)))
      CleanUp(10);

   if(UMS)
   {
      area->AreaType=AREATYPE_UMS;
      strcpy(area->Path,"");
   }
   else
   {
      area->AreaType=AREATYPE_MSG;
      strcpy(area->Path,basepath);
      AddPart(area->Path,"NETMAIL",80);
   }

   area->Flags=AREA_NETMAIL;
   area->Aka=aka;
   jbNewList(&area->TossNodes);
   strcpy(area->Tagname,"NETMAIL");
   strcpy(area->Description,"");
   area->DefaultCHRS=DefToAmiga;
   area->ExportCHRS=NULL;

   jbAddNode(&AreaList,(struct jbNode *)area);

   if(!(area=(struct Area *)AllocMem(sizeof(struct Area),MEMF_CLEAR)))
      CleanUp(10);

   if(UMS)
   {
      area->AreaType=AREATYPE_UMS;
      sprintf(area->Path,"%sBAD",basepath);
   }
   else
   {
      area->AreaType=AREATYPE_MSG;
      strcpy(area->Path,basepath);
      AddPart(area->Path,"BAD",80);
   }

   area->Flags=AREA_BAD;
   area->Aka=aka;
   jbNewList(&area->TossNodes);
   strcpy(area->Tagname,"BAD");
   strcpy(area->Description,"Bad messages");
   area->DefaultCHRS=DefToAmiga;
   area->ExportCHRS=NULL;

   jbAddNode(&AreaList,(struct jbNode *)area);

   if(!(area=(struct Area *)AllocMem(sizeof(struct Area),MEMF_CLEAR)))
      CleanUp(10);

   if(UMS)
      area->AreaType=AREATYPE_UMS;

   else
      area->AreaType=AREATYPE_MSG;

   area->Aka=aka;
   jbNewList(&area->TossNodes);
   strcpy(area->Path,basepath);
   strcpy(area->Tagname,"DEFAULT");
   strcpy(area->Description,"<No description>");
   area->DefaultCHRS=DefToAmiga;
   area->ExportCHRS=NULL;

   jbAddNode(&AreaList,(struct jbNode *)area);

   if(!(route=(struct Route *)AllocMem(sizeof(struct Route),MEMF_CLEAR)))
      CleanUp(10);

   strcpy(route->Pattern.Zone,"*");
   strcpy(route->Pattern.Net,"*");
   strcpy(route->Pattern.Node,"*");
   strcpy(route->Pattern.Point,"*");

   sprintf(route->DestPat.Zone,"%lu",Feed4D.Zone);
   sprintf(route->DestPat.Net,"%lu",Feed4D.Net);
   sprintf(route->DestPat.Node,"%lu",Feed4D.Node);
   sprintf(route->DestPat.Point,"%lu",Feed4D.Point);

   route->Aka=aka;

   jbAddNode(&RouteList,(struct jbNode *)route);

   if(!(areafix=(struct AreaFixName *)AllocMem(sizeof(struct AreaFixName),MEMF_CLEAR)))
      CleanUp(10);

   strcpy(areafix->Name,"AreaFix");

   jbAddNode(&AreaFixList,(struct jbNode *)areafix);

   if(!(areafix=(struct AreaFixName *)AllocMem(sizeof(struct AreaFixName),MEMF_CLEAR)))
      CleanUp(10);

   strcpy(areafix->Name,"AreaMgr");

   jbAddNode(&AreaFixList,(struct jbNode *)areafix);

   if(!(areafix=(struct AreaFixName *)AllocMem(sizeof(struct AreaFixName),MEMF_CLEAR)))
      CleanUp(10);

   strcpy(areafix->Name,"CrashMail");

   jbAddNode(&AreaFixList,(struct jbNode *)areafix);

   rtEZRequestTags("A new configuration has been created. All netmail\n"
                   "will be routed to your main feed. LHA will be used\n"
                   "to pack mail. You may change these settings at any time","_Ok",NULL,NULL,
      RTEZ_Flags,     GLREQF_CENTERTEXT,
      RT_Underscore,'_',
      RT_Window,mainwin,
      RT_WaitPointer, TRUE,
      RT_LockWindow, TRUE,
      TAG_END);

   saveconfig=TRUE;
   return(TRUE);
}
