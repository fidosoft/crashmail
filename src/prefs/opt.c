#include <CrashPrefs.h>

#define SWITCHES 21

struct Hook OptionsHelpHook = { {NULL}, HelpHookEntry,NULL,HELP_OPTIONS};

UBYTE *optnames[]={/* "Auto-add areas", */ "Strip Re: on import","Honor Receipt reqs",
                   "No routing","Force INTL on export","Honor Audit reqs",
                   /* "Pack netmail", */ "Check SEEN-BY","Path 3D",
                   "Import SEEN-BY","Import empty netmail","Unattended mode",
                   "No direct f/a","Bounce unknown points","Check packet dest",
                   "Weekday naming","Add TID kludge","Codepage 865",
                   "Translate *.msg","Use *.msg HighWater","Write back *.msg",
                   "Keep HW when tossing","Bounce header only"};

ULONG optflags[]={/* CFG_AUTOADD, */ CFG_STRIPRE,CFG_ANSWERRECEIPT,
                  CFG_NOROUTE,CFG_FORCEINTL,CFG_ANSWERAUDIT,
                  /* CFG_PACKNETMAIL, */ CFG_CHECKSEENBY,CFG_PATH3D,
                  CFG_IMPORTSEENBY,CFG_IMPORTEMPTYNETMAIL,CFG_UNATTENDED,
                  CFG_NODIRECTATTACH,CFG_BOUNCEPOINTS,CFG_CHECKPKTDEST,
                  CFG_WEEKDAYNAMING,CFG_ADDTID,CFG_CODEPAGE865,
                  CFG_XLATMSG,CFG_MSGHIGHWATER,CFG_MSGWRITEBACK,
                  CFG_NOTOUCHHWTOSSING,CFG_BOUNCEHEADERONLY};

BOOL bools[SWITCHES];

void MakeOpt(ULONG start,ULONG end,UBYTE *header)
{
   ULONG c,d,st;

   st=end-start;

   LT_New(parthandle,
      LA_Type,      HORIZONTAL_KIND,
      LA_LabelText, header,
      TAG_DONE);

   for(c=0;c<3;c++)
   {
      LT_New(parthandle,
         LA_Type,      VERTICAL_KIND,
         TAG_DONE);

      for(d=0;d<(st+2)/3;d++)
      {
         if(d*3+c+start < end)
         {
            LT_New(parthandle,
               LA_Type,      CHECKBOX_KIND,
               LA_LabelText, optnames[start+d*3+c],
               LA_ID,        start+d*3+c+1,
               LA_BOOL,      &bools[start+d*3+c],
             TAG_DONE);
         }
      }

      LT_EndGroup(parthandle);
   }

   LT_EndGroup(parthandle);
}

void PartOptions(void)
{
   struct IntuiMessage *IntuiMessage;
   APTR iaddress;
   ULONG class,code,qualifier,c;

   for(c=0;c<SWITCHES;c++)
   {
      if(cfg_Flags & optflags[c]) bools[c]=TRUE;
      else                        bools[c]=FALSE;
   }

   parthandle = LT_CreateHandleTags(scr,LH_EditHook,&StrEditHook,
                                        TAG_DONE);

   if(!parthandle)
      return;

   LT_New(parthandle,
      LA_Type,      VERTICAL_KIND,
      TAG_DONE);

   MakeOpt(0,SWITCHES,"Options");

   LT_EndGroup(parthandle);

   partwin=LT_Build(parthandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW,
                               LAWN_Parent,   mainwin,
                               LAWN_HelpHook, &OptionsHelpHook,
                               LAWN_Title,    "Options",
                               WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                               TAG_END);

   if(!partwin)
   {
      LT_DeleteHandle(parthandle);
      DisplayBeep(NULL);
      return;
   }

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(parthandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;
         qualifier=IntuiMessage->Qualifier;

         LT_ReplyIMsg(IntuiMessage);

         switch(class)
         {
            case IDCMP_CLOSEWINDOW:   LT_DeleteHandle(parthandle);
                                      parthandle=NULL;

                                      for(c=0;c<SWITCHES;c++)
                                      {
                                         if(bools[c]) cfg_Flags|=optflags[c];
                                         else         cfg_Flags&=~optflags[c];
                                      }
                                      return;

            case IDCMP_GADGETUP:      saveconfig=TRUE;
                                      break;
         }
      }
      WaitPort(partwin->UserPort);
   }
}
