#include <CrashPrefs.h>

struct List bouncelist;
ULONG bouncenum,currentbounce;

struct Hook BounceHelpHook = { {NULL}, HelpHookEntry,NULL,HELP_BOUNCE};

void SetBounce(ULONG num)
{
   if(bouncenum == 0)
   {
      LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);
   }

   if(num!=-1)
   {
      if(bouncenum)
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,FALSE,TAG_END);
      }
      else
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
      }

      if(num!=0)                 LT_SetAttributes(parthandle,12,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);

      if(num!=bouncenum-1)       LT_SetAttributes(parthandle,13,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);
   }

   currentbounce=num;
}

void RefreshBouncelist(void)
{
   struct TempNode *tempnode;
   struct PatternNode *pat;

   LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
   FreeTempNodes(&bouncelist);

   bouncenum=0;

   for(pat=BounceList.First;pat;pat=pat->Next)
   {
      if(!(tempnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(&bouncelist);
         return;
      }

      Print4DPat(&pat->Pattern,tempnode->buf);

      tempnode->Node.ln_Name=tempnode->buf;
      AddTail(&bouncelist,(struct Node *)tempnode);
      bouncenum++;
   }

   LT_SetAttributes(parthandle,4711,GTLV_Labels,&bouncelist,TAG_END);
}

void AddBounce(void)
{
   struct Node4DPat n4dpat;
   struct PatternNode *pat;
   UBYTE buf[100];
   BOOL res;
   BOOL

   kg=TRUE;
   buf[0]=0;

   while(kg)
   {
      res=rtGetString(buf,50,"Add bounce pattern",NULL,RT_Window,partwin,
                                                       RT_WaitPointer, TRUE,
                                                       RT_LockWindow, TRUE,
                                                       TAG_END);

      if(!res)
         return;

      kg=FALSE;

      if(!Parse4DPat(buf,&n4dpat))
      {
         rtEZRequestTags("Invalid node pattern","_Ok",NULL,NULL,RT_Underscore,'_',
                                                                RT_Window,partwin,
                                                                RT_WaitPointer, TRUE,
                                                                RT_LockWindow, TRUE,
                                                                TAG_END);
         kg=TRUE;
      }
   }

   if(pat=(struct PatternNode *)AllocMem(sizeof(struct PatternNode),MEMF_CLEAR))
   {
      jbAddNode(&BounceList,(struct jbNode *)pat);
      Copy4DPat(&pat->Pattern,&n4dpat);
      RefreshBouncelist();
      SetBounce(bouncenum-1);
      LT_SetAttributes(parthandle,4711,GTLV_Selected,currentbounce,GTLV_MakeVisible,currentbounce,TAG_END);
      saveconfig=TRUE;
   }
   else
   {
      DisplayBeep(NULL);
   }
}

void RemBounce(void)
{
   ULONG c,tmp;
   struct PatternNode *pat;
   UBYTE buf[100],buf2[100];
   BOOL res;

   pat=BounceList.First;
   for(c=0;c<currentbounce;c++) pat=pat->Next;

   Print4DPat(&pat->Pattern,buf2);
   sprintf(buf,"Do you really want to remove the pattern \"%s\"?",buf2);

   res=rtEZRequestTags(buf,"_Remove|_Cancel",NULL,NULL,RT_Underscore,'_',
                                                       RT_Window,partwin,
                                                       RT_WaitPointer,TRUE,
                                                       RT_LockWindow, TRUE,
                                                       RTEZ_Flags, EZREQF_CENTERTEXT | EZREQF_NORETURNKEY,
                                                       TAG_END);

   if(!res)
      return;

   tmp=currentbounce;
   jbFreeNode(&BounceList,(struct jbNode *)pat,sizeof(struct PatternNode));
   RefreshBouncelist();
   if(tmp == bouncenum) tmp--;
   SetBounce(tmp);
   saveconfig=TRUE;
}

void PartBounce(void)
{
   ULONG class,code,tmp;
   struct IntuiMessage *IntuiMessage;
   struct TempNode *tempnode;
   struct PatternNode *pat;
   APTR iaddress;
   ULONG c;
   struct Node4DPat n4dpat;

   NewList(&bouncelist);
   currentbounce=-1;

   RefreshBouncelist();

   parthandle = LT_CreateHandleTags(scr,LH_EditHook,&StrEditHook,LH_RawKeyFilter,FALSE,TAG_DONE);

   if(!parthandle)
   {
      FreeTempNodes(&bouncelist);
      return;
   }

   LT_New(parthandle,
      LA_Type,      VERTICAL_KIND,
      LA_LabelText, "Bounce nodes",
      TAG_DONE);

      LT_New(parthandle,
         LA_Type,      STRING_KIND,
         GTST_MaxChars,30,
         LA_ID,        4712,
         LAST_Link,    4711,
         GA_Disabled,TRUE,
         TAG_DONE);

      LT_New(parthandle,
         LA_Type,       LISTVIEW_KIND,
         LALV_Lines,    10,
         LALV_CursorKey,TRUE,
         LALV_Link,4712,
         GTLV_Labels,&bouncelist,
         LA_ID,4711,
         TAG_DONE);

      LT_New(parthandle,
         LA_Type,      HORIZONTAL_KIND,
         LAGR_SameSize,TRUE,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      VERTICAL_KIND,
            LAGR_SameSize,TRUE,
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      BUTTON_KIND,
               LA_LabelText, "Add...",
               LA_Chars,20,
               LA_ID,10,
               TAG_DONE);

            LT_New(parthandle,
               LA_Type,      BUTTON_KIND,
               LA_LabelText, "Up",
               GA_Disabled,TRUE,
               LA_Chars,20,
               LA_ID,12,
               TAG_DONE);

         LT_EndGroup(parthandle);

         LT_New(parthandle,
            LA_Type,      VERTICAL_KIND,
            LAGR_SameSize,TRUE,
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      BUTTON_KIND,
               LA_LabelText, "Remove...",
               LA_Chars,20,
               GA_Disabled,TRUE,
               LA_ID,11,
               TAG_DONE);

            LT_New(parthandle,
               LA_Type,      BUTTON_KIND,
               LA_LabelText, "Down",
               LA_Chars,20,
               GA_Disabled,TRUE,
               LA_ID,13,
               TAG_DONE);

         LT_EndGroup(parthandle);

      LT_EndGroup(parthandle);

   LT_EndGroup(parthandle);

   partwin=LT_Build(parthandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW|IDCMP_RAWKEY,
                               LAWN_Title,    "Bounce nodes",
                               WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                               LAWN_HelpHook, &BounceHelpHook,
                               TAG_END);

   if(!partwin)
   {
      LT_DeleteHandle(parthandle);
      DisplayBeep(NULL);
      FreeTempNodes(&bouncelist);
      return;
   }

   if(bouncenum)
   {
      LT_SetAttributes(parthandle,4711,GTLV_Selected,0,TAG_END);
      SetBounce(0);
   }

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(parthandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;

         LT_ReplyIMsg(IntuiMessage);

         switch(class)
         {
            case IDCMP_RAWKEY:         if(code == TAB) LT_Activate(parthandle,4712);
                                       break;
            case IDCMP_GADGETUP:       switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 10:
                                             AddBounce();
                                             break;
                                          case 11:
                                             RemBounce();
                                             break;
                                          case 12:
                                             if(currentbounce != 0)
                                             {
                                                tmp=currentbounce;
                                                MoveUp(&BounceList,tmp);
                                                RefreshBouncelist();
                                                SetBounce(tmp-1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentbounce,
                                                   GTLV_MakeVisible,currentbounce,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 13:
                                             if(currentbounce != bouncenum-1)
                                             {
                                                tmp=currentbounce;
                                                MoveDown(&BounceList,tmp);
                                                RefreshBouncelist();
                                                SetBounce(tmp+1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentbounce,
                                                   GTLV_MakeVisible,currentbounce,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 4711:
                                             SetBounce(code);
                                             break;
                                          case 4712:
                                             if(!Parse4DPat(getgadgetstring((struct Gadget *)iaddress),&n4dpat))
                                             {
                                                DisplayBeep(NULL);
                                                LT_Activate(parthandle,4712);
                                             }
                                             else
                                             {
                                                tempnode=bouncelist.lh_Head;
                                                for(c=0;c<currentbounce;c++) tempnode=tempnode->Node.ln_Succ;

                                                LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
                                                Print4DPat(&n4dpat,tempnode->buf);
                                                LT_SetAttributes(parthandle,4711,GTLV_Labels,&bouncelist,TAG_END);

                                                pat=BounceList.First;
                                                for(c=0;c<currentbounce;c++) pat=pat->Next;
                                                Copy4DPat(&pat->Pattern,&n4dpat);
                                             }
                                             break;

                                       }
                                       break;
            case IDCMP_CLOSEWINDOW:    LT_DeleteHandle(parthandle);
                                       parthandle=NULL;
                                       FreeTempNodes(&bouncelist);
                                       return;
         }
      }
      WaitPort(partwin->UserPort);
   }
}
