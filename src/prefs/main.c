#include <CrashPrefs.h>

UBYTE *ver="\0$VER: CrashPrefs "VERSION" ("__COMMODORE_DATE__")";
UBYTE *config_version="CrashPrefs "VERSION;

UBYTE req_buf[300];

struct Screen *scr;
struct Menu *menus;

struct LayoutHandle *mainhandle;
struct Window *mainwin;

struct LayoutHandle *parthandle;
struct Window *partwin;

void _waitwbmsg(void);
routine_that_is_never_called() { _waitwbmsg(); }
extern struct WBStartup *_WBMsg;

extern struct ExecBase *SysBase;
extern struct Library *GadToolsBase;

struct Library *KeymapBase;
struct Library *AmigaGuideBase;
struct Library *ReqToolsBase;
struct Library *GTLayoutBase;

struct NewAmigaGuide nag;
AMIGAGUIDECONTEXT handle;
BOOL amigaguide,amigaguideerr;

/* Fakade charsets */

UBYTE *AmigaToIbm=1;
UBYTE *AmigaToSF7=2;
UBYTE *AmigaToMac=3;
UBYTE *IbmToAmiga=4;
UBYTE *SF7ToAmiga=5;
UBYTE *DefToAmiga=6;
UBYTE *MacToAmiga=6;

UBYTE cfg_Settings[100]="";
UBYTE cfg_PubScreen[100]="";
UBYTE cfg_Helpfile[100]="";

void FreeTempNodes(struct List *list)
{
   struct TempNode *tn;

   while(tn=(struct TempNode *)RemHead(list))
      FreeMem(tn,sizeof(struct TempNode));
}

/* Annat configtjafs */

void CleanUp(int);
void FreeAmigaGuide(void);

CleanUpConfig(void)
{
   CleanUp(10);
}

BOOL saveconfig;

extern struct jbFile *cfgfh;
extern ULONG cfgline;

void ConfigNoMem(void)
{
   if(cfgfh) jbClose(cfgfh);
   PrintFault(ERROR_NO_FREE_STORE,"");
   CleanUpConfig();
}

ConfigError(UBYTE *str,...)
{
   va_list args;
   UBYTE buf[200];

   if(cfgfh) jbClose(cfgfh);

   va_start(args,str);
   sprintf(buf,"Error in configuration on line %lu: %s",cfgline,str);

   if(mainwin)
   {
      rtEZRequestTags(buf,"_Quit",NULL,args,RT_Underscore,'_',RT_Window,mainwin,
                                                              RT_WaitPointer, TRUE,
                                                              RT_LockWindow, TRUE,
                                                              TAG_END);
   }
   else
   {
      rtEZRequestTags(buf,"_Quit",NULL,args,RT_Underscore,'_',TAG_END);
   }

   va_end(args);
   CleanUpConfig();
}

__asm void (*own_StackSwap)(register __A0 struct StackSwapStruct *newstack,register __A6 struct Library *library);

struct StackSwapStruct stackswapstruct;

void CleanUp(int err)
{
   FreeConfig();
   FreeAmigaGuide();

   if(mainhandle)
   {
      ClearMenuStrip(mainhandle->Window);
      LT_DeleteHandle(mainhandle);
   }

   if(menus)
	   FreeMenus(menus);

   if(scr)
      UnlockPubScreen(NULL,scr);

   if(KeymapBase)   CloseLibrary(KeymapBase);
   if(ReqToolsBase) CloseLibrary(ReqToolsBase);
   if(GTLayoutBase) CloseLibrary(GTLayoutBase);

   if(stackswapstruct.stk_Lower)
   {
      /* This trick is needed, otherwise DICE will use stack... */

      own_StackSwap=(void *)((ULONG)SysBase-(ULONG)732);
      (*own_StackSwap)(&stackswapstruct,(struct Library *)SysBase);

      FreeMem(stackswapstruct.stk_Lower,STACKSIZE);
   }

   _exit(err);
}

UBYTE *Context[] = {
   "CRASHPREFS",
   "CRASHPREFS_GENERAL",
   "CRASHPREFS_OPTIONS",
   "CRASHPREFS_PATH",
   "CRASHPREFS_AKA",
   "CRASHPREFS_NODES",
   "CRASHPREFS_AREAS",
   "CRASHPREFS_UMS",
   "CRASHPREFS_GROUPS",
   "CRASHPREFS_PACKERS",
   "CRASHPREFS_CHARSET",
   "CRASHPREFS_BOUNCE",
   "CRASHPREFS_FILEATTACH",
   "CRASHPREFS_REMAP",
   "CRASHPREFS_ROUTE",
   "CRASHPREFS_CHANGE",
   "CRASHPREFS_ROBOTNAMES",
   "CRASHPREFS_AREAFIX",
   "CRASHPREFS_AREALIST",
   "CRASHPREFS_CHANGEMANY",
   NULL };

void ShowAmigaGuideHelp(ULONG num);
void HandleAmigaGuide(void);

void FreeAmigaGuide(void)
{
   struct AmigaGuideMsg *agm;

   if(AmigaGuideBase)
   {
      if(handle)
      {
         if(amigaguide || amigaguideerr)
         {
            while(agm = (struct AmigaGuideMsg *)GetAmigaGuideMsg(handle))
            {
                ReplyAmigaGuideMsg( agm );
            }
         }
         else
         {
            /* if AG is not ready wait until we get the ActiveToolID before
            ** we exit
            */
            while(!amigaguide)
            {
               HandleAmigaGuide();        /* clear amigaguide messages */
               Delay(5);                  /* 100 ms */
            }
         }
         CloseAmigaGuide(handle);
      }
      CloseLibrary(AmigaGuideBase);
   }
}

void HandleAmigaGuide(void)
{
   struct AmigaGuideMsg *agm;

   if(AmigaGuideBase)
   {
      while(agm = (struct AmigaGuideMsg *) GetAmigaGuideMsg(handle))
      {
         switch(agm->agm_Type)
         {
            case ActiveToolID:
                  amigaguide=TRUE;
                  break;
            case ToolStatusID:
                  if(agm->agm_Pri_Ret)
                     amigaguideerr=TRUE;

                  break;
         }
         ReplyAmigaGuideMsg( agm );
      }
   }
}

void LoadAmigaGuide(UBYTE *filename)
{
   ULONG sig,retsig;
   static UBYTE filenamebuf[200];

   strcpy(filenamebuf,filename);

   amigaguide=FALSE;
   amigaguideerr=FALSE;

   nag.nag_Name=filenamebuf;
   nag.nag_BaseName="CrashMail";
   nag.nag_Context=Context;
   nag.nag_Screen=scr;

   if(AmigaGuideBase=(struct Library *)OpenLibrary("amigaguide.library",34))
   {
      sig=0;

      if(handle=(AMIGAGUIDECONTEXT)OpenAmigaGuideAsyncA(&nag,NULL))
         sig = AmigaGuideSignal(handle);

      if(sig)
      {
         HandleAmigaGuide();

         while(!amigaguide && !amigaguideerr)
         {
            retsig=Wait(sig);
            HandleAmigaGuide();
         }
      }
   }
}

void ShowAmigaGuideHelp(ULONG num)
{
   if(amigaguide==FALSE && amigaguideerr==FALSE)
   {
      if(AmigaGuideBase=(struct Library *)OpenLibrary("amigaguide.library",34))
      {
         if(cfg_Helpfile[0]!=0)
         {
            LoadAmigaGuide(cfg_Helpfile);
         }
         else
         {
            LoadAmigaGuide("PROGDIR:CrashMail.guide");
            if(!amigaguide) LoadAmigaGuide("HELP:english/CrashMail.guide");
         }
      }
   }

   if(amigaguide)
   {
      SetAmigaGuideContextA(handle,num,NULL);
      SendAmigaGuideContextA(handle,NULL);
   }
   else
   {
      DisplayBeep(NULL);
   }
}

__asm __geta4 ULONG HelpHookEntry(register __a0 struct Hook *hook,register __a2 struct HelpMsg *helpmsg,register __a1 struct IBox *ibox)
{
   ShowAmigaGuideHelp((LONG)hook->h_Data);
}

struct Hook MainHelpHook = { {NULL}, HelpHookEntry,NULL,HELP_MAIN};

/* EditHook */

#define IEQUALIFIER_SHIFT     (IEQUALIFIER_LSHIFT|IEQUALIFIER_RSHIFT)

__asm __geta4 ULONG StrEditHookEntry (register __a0 struct Hook *hook,register __a2 struct SGWork *sgw,register __a1 ULONG *msg)
{
   int qual,rawcode;

   if (msg[0] == SGH_KEY)
   {
      rawcode = sgw->IEvent->ie_Code;
      qual = sgw->IEvent->ie_Qualifier;
      if (sgw->EditOp == EO_INSERTCHAR   || sgw->EditOp == EO_REPLACECHAR  || sgw->EditOp == EO_BADFORMAT    ||
         (sgw->EditOp == EO_NOOP && sgw->Actions == (SGA_USE|SGA_BEEP) && sgw->NumChars == (sgw->StringInfo->MaxChars - 1)))
      {
         if ((qual & IEQUALIFIER_RCOMMAND) || sgw->Code == 27)
         {
            sgw->Actions &= ~(SGA_USE|SGA_BEEP|SGA_REDISPLAY);
            sgw->IEvent->ie_Qualifier &= ~IEQUALIFIER_RCOMMAND;

            if (!(qual & IEQUALIFIER_REPEAT))
            {
               sgw->Actions |= SGA_REUSE|SGA_END;
               /* sgw->Code = KEYB_SHORTCUT; */
            }
         }
      }

      return (TRUE);
   }
   return (FALSE);
}

struct Hook StrEditHook = { { NULL }, StrEditHookEntry, NULL, NULL };

#define IEQUALIFIER_SHIFT     (IEQUALIFIER_LSHIFT|IEQUALIFIER_RSHIFT)

__asm __geta4 ULONG StrEditHookEntryNext (register __a0 struct Hook *hook,register __a2 struct SGWork *sgw,register __a1 ULONG *msg)
{
   int qual,rawcode;

   if (msg[0] == SGH_KEY)
   {
      rawcode = sgw->IEvent->ie_Code;
      qual = sgw->IEvent->ie_Qualifier;
      if (sgw->EditOp == EO_INSERTCHAR   || sgw->EditOp == EO_REPLACECHAR  || sgw->EditOp == EO_BADFORMAT    ||
         (sgw->EditOp == EO_NOOP && sgw->Actions == (SGA_USE|SGA_BEEP) && sgw->NumChars == (sgw->StringInfo->MaxChars - 1)))
      {
         if ((qual & IEQUALIFIER_RCOMMAND) || sgw->Code == 27)
         {
            sgw->Actions &= ~(SGA_USE|SGA_BEEP|SGA_REDISPLAY);
            sgw->IEvent->ie_Qualifier &= ~IEQUALIFIER_RCOMMAND;

            if (!(qual & IEQUALIFIER_REPEAT))
            {
               sgw->Actions |= SGA_REUSE|SGA_END;
               /* sgw->Code = KEYB_SHORTCUT; */
            }
         }
      }
      if(sgw->EditOp == EO_ENTER)
      {
         sgw->Actions=SGA_END;

         if(sgw->IEvent->ie_Qualifier & IEQUALIFIER_SHIFT)
            sgw->Actions |= SGA_PREVACTIVE;

         else
            sgw->Actions |= SGA_NEXTACTIVE;
      }
      return(TRUE);
   }
   return (FALSE);
}

struct Hook StrEditHookNext = { { NULL }, StrEditHookEntryNext, NULL, NULL };

myputs(UBYTE *str)
{
   Write(Output(),str,strlen(str));
}

UBYTE *abouttemplate="CrashMail Preferences "VERSION"\nCopyright "COPYRIGHT" Johan Billing\n"
                     "All rights reserved.\n"
                     "\n"
                     "%s\n"
                     "\n"
                     "This program has been released as ShareWare.";

UBYTE aboutbuf[1000];

void AskQuit(void)
{
   ULONG res;

   if(saveconfig)
   {
      res=rtEZRequestTags("The settings have been changed. Do you really want to quit?","_Save & quit|_Quit|_Cancel",NULL,NULL,RT_Underscore,'_',
                                                                                                                               RT_Window,mainhandle->Window,
                                                                                                                               RT_WaitPointer,TRUE,
                                                                                                                               RT_LockWindow, TRUE,
                                                                                                                               RTEZ_Flags, EZREQF_CENTERTEXT,
                                                                                                                               TAG_END);
      if(res==0)
         return;

      if(res==1)
      {
         LT_LockWindow(mainhandle->Window);
         WriteConfig(req_buf);
         LT_UnlockWindow(mainhandle->Window);
      }
   }

   CleanUp(0);
}

void CheckUnconfirmed(void)
{
   struct Area *a;

   for(a=AreaList.First;a;a=a->Next)
      if(a->Flags & AREA_UNCONFIRMED) break;

   if(a)
   {
      rtEZRequestTags("There are auto-added areas in your configuration.\n"
                      "that are unconfirmed. You should go to the Areas\n"
                      "section and either conmfirm then or delete them.",
                           "Proceed",NULL,NULL,
                           RT_Underscore,'_',
                           RT_Window,mainwin,
                           RT_WaitPointer,TRUE,
                           RT_LockWindow, TRUE,
                           RTEZ_Flags, EZREQF_CENTERTEXT,
                           TAG_END);
   }
}

struct NewMenu newmenus[] = { {NM_TITLE,"Project","",0,0,NULL},
                              {NM_ITEM, "Open...","O",0,0,NULL},
                              {NM_ITEM, "Save","S",0,0,NULL},
                              {NM_ITEM, "Save As","A",0,0,NULL},
                              {NM_ITEM, (STRPTR)NM_BARLABEL,NULL,0,0,NULL},
                              {NM_ITEM, "About...","?",0,0,NULL},
                              {NM_ITEM, (STRPTR)NM_BARLABEL,NULL,0,0,NULL},
                              {NM_ITEM, "Quit","Q",0,0,NULL},
                              {NM_TITLE,"Other","",0,0,NULL},
                              {NM_ITEM, "Import TrapToss configuration...",NULL,0,0,NULL},
                              {NM_ITEM, "Import Areas.bbs...",NULL,0,0,NULL},
                              {NM_ITEM, (STRPTR)NM_BARLABEL,NULL,0,0,NULL},
                              {NM_ITEM, "Export Areas.bbs...",NULL,0,0,NULL},
                              {NM_END,  "","",0,0,NULL}};

void (*mainfuncs[])(void)={PartGeneral,PartOptions,PartPath,
                           PartAka,    PartNode,   PartAreas,
                           PartUMS,    PartGroups, PartPackers,
                           PartCharset,PartBounce, PartFileAttach,
                           PartRemap,  PartRoute,  PartChange,
                           PartRobots, PartAreaFix,PartArealist};

#define SETTINGS  0
#define PUBSCREEN 1
#define HELPFILE  2

BOOL nomem,diskfull;

void realmain(void);

void _main(void)
{
   if(SysBase->LibNode.lib_Version<37)
   {
      myputs("Sorry! This program requires Kickstart 2.04 or higher\n");
      _exit(0);
   }

   if(!(stackswapstruct.stk_Lower = AllocMem(STACKSIZE,MEMF_PUBLIC)))
   {
      Printf("Failed to allocate memory for new stack\n");
      CleanUp(10);
   }

   stackswapstruct.stk_Upper   = (ULONG)stackswapstruct.stk_Lower + STACKSIZE;
   stackswapstruct.stk_Pointer = (APTR)stackswapstruct.stk_Upper;

   /* This trick is needed, otherwise DICE will use stack when swapping... */

   own_StackSwap=(void *)((ULONG)SysBase-(ULONG)732);
   (*own_StackSwap)(&stackswapstruct,(struct Library *)SysBase);

   realmain();
}

void realmain(void)
{
   APTR lock;
   ULONG txtwid=0,num;
   struct IntuiMessage *IntuiMessage;
   APTR iaddress;
   ULONG class,code,qualifier,gid;
   struct MenuItem *menuitem;
   BOOL hasconfig;
   ULONG res;
   UBYTE buf[50];

   if(!(KeymapBase=OpenLibrary("keymap.library",37)))
      CleanUp(10);

   if(!(ReqToolsBase=OpenLibrary(REQTOOLSNAME,REQTOOLSVERSION)))
   {
      Printf("CrashPrefs needs "REQTOOLSNAME" %lu or higher\n",REQTOOLSVERSION);
      CleanUp(10);
   }

   if(!(GTLayoutBase=OpenLibrary("gtlayout.library",8)))
   {
      Printf("CrashPrefs needs gtlayout.library 8 or higher\n");
      CleanUp(10);
   }

   if(_WBMsg)
   {
      struct DiskObject *dobj;
      UBYTE *what;
      BPTR olddir;

      olddir=CurrentDir(_WBMsg->sm_ArgList->wa_Lock);

      if(!(dobj=GetDiskObject(_WBMsg->sm_ArgList->wa_Name)))
      {
         CurrentDir(olddir);
         CleanUp(10);
      }

      if(what=FindToolType(dobj->do_ToolTypes,"SETTINGS"))
      {
         strncpy(cfg_Settings,what,98);
         cfg_Settings[99]=0;
      }

      if(what=FindToolType(dobj->do_ToolTypes,"PUBSCREEN"))
      {
         strncpy(cfg_PubScreen,what,98);
         cfg_PubScreen[99]=0;
      }

      if(what=FindToolType(dobj->do_ToolTypes,"HELPFILE"))
      {
         strncpy(cfg_Helpfile,what,98);
         cfg_Helpfile[99]=0;
      }

      FreeDiskObject(dobj);
      CurrentDir(olddir);
   }
   else
   {
      struct RDArgs *rdargs;
      UBYTE *argstr="SETTINGS,PUBSCREEN=PS/K,HELPFILE/K";
      ULONG argarray[]={NULL,NULL,NULL};

      if(!(rdargs=ReadArgs(argstr,argarray,NULL)))
      {
         PrintFault(IoErr(),"");
         CleanUp(10);
      }

      if(argarray[SETTINGS]) strcpy(cfg_Settings,(UBYTE *)argarray[SETTINGS]);
      if(argarray[PUBSCREEN]) strcpy(cfg_PubScreen,(UBYTE *)argarray[PUBSCREEN]);
      if(argarray[HELPFILE]) strcpy(cfg_Helpfile,(UBYTE *)argarray[HELPFILE]);

      FreeArgs(rdargs);
   }

   if(!(scr=LockPubScreen(cfg_PubScreen)))
   {
      if(!(scr=LockPubScreen(NULL)))
      {
         Printf("Couldn't find screen\n");
         CleanUp(10);
      }
   }

   mainhandle = LT_CreateHandleTags(scr,LH_AutoActivate,FALSE,TAG_DONE);

   if(!mainhandle)
   {
      DisplayBeep(NULL);
      CleanUp(10);
   }

   menus=LT_LayoutMenus(mainhandle,newmenus,GTMN_FrontPen,0L,
                                            GTMN_TextAttr,scr->Font,
                                            GTMN_NewLookMenus,TRUE,
                                            LH_MenuGlyphs,TRUE,
                                            TAG_DONE);

	if(!menus)
   {
      DisplayBeep(NULL);
      CleanUp(0);
   }

   LT_New(mainhandle,
      LA_Type,      VERTICAL_KIND,
      LA_LabelText, "Sections",
      TAG_DONE);

      LT_New(mainhandle,
         LA_Type,      HORIZONTAL_KIND,
         TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "General...",
            LA_Chars,     15,
            LA_ID,        1,
         TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Options...",
            LA_Chars,     15,
            LA_ID,        2,
         TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Paths...",
            LA_Chars,     15,
            LA_ID,        3,
         TAG_DONE);

      LT_EndGroup(mainhandle);

      LT_New(mainhandle,
         LA_Type,      HORIZONTAL_KIND,
         TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Aka...",
            LA_Chars,     15,
            LA_ID,        4,
         TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Nodes...",
            LA_Chars,     15,
            LA_ID,        5,
         TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Areas...",
            LA_Chars,     15,
            LA_ID,        6,
         TAG_DONE);

      LT_EndGroup(mainhandle);

      LT_New(mainhandle,
         LA_Type,      HORIZONTAL_KIND,
         TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "UMS...",
            LA_Chars,     15,
            LA_ID,        7,
         TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Groups...",
            LA_Chars,     15,
            LA_ID,        8,
            TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Packers...",
            LA_Chars,     15,
            LA_ID,        9,
         TAG_DONE);

      LT_EndGroup(mainhandle);

      LT_New(mainhandle,
         LA_Type,      HORIZONTAL_KIND,
         TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Charset...",
            LA_Chars,     15,
            LA_ID,        10,
         TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Bounce...",
            LA_Chars,     15,
            LA_ID,        11,
            TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Fileattach...",
            LA_Chars,     15,
            LA_ID,        12,
         TAG_DONE);

      LT_EndGroup(mainhandle);

      LT_New(mainhandle,
         LA_Type,      HORIZONTAL_KIND,
         TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Remap...",
            LA_Chars,     15,
            LA_ID,        13,
         TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Route...",
            LA_Chars,     15,
            LA_ID,        14,
            TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Change...",
            LA_Chars,     15,
            LA_ID,        15,
         TAG_DONE);

      LT_EndGroup(mainhandle);

      LT_New(mainhandle,
         LA_Type,      HORIZONTAL_KIND,
         TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Robotnames...",
            LA_Chars,     15,
            LA_ID,        16,
         TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "AreaFix...",
            LA_Chars,     15,
            LA_ID,        17,
            TAG_DONE);

         LT_New(mainhandle,
            LA_Type,      BUTTON_KIND,
            LA_LabelText, "Arealists...",
            LA_Chars,     15,
            LA_ID,        18,
         TAG_DONE);

      LT_EndGroup(mainhandle);

   LT_EndGroup(mainhandle);

   mainwin=LT_Build(mainhandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW,
                               LAWN_Menu,     menus,
                               LAWN_HelpHook, &MainHelpHook,
                               LAWN_Title,    "CrashPrefs "VERSION" � "COPYRIGHT" Johan Billing",
                               WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                               TAG_END);

   if(!mainwin)
   {
      DisplayBeep(NULL);
      CleanUp(0);
   }

   ReadKey();

   if(Checksum9() != checksums[9])
   {
      sprintf(aboutbuf,abouttemplate,"Unregistered version");
   }
   else
   {
      sprintf(buf,"Registered to %s",key.Name);
      sprintf(aboutbuf,abouttemplate,buf);
   }

   strcpy(req_buf,cfg_Settings);

   ResetConfig();

   hasconfig=FALSE;

   while(!hasconfig)
   {
      lock=rtLockWindow(mainwin);

      if(req_buf[0]!=0)
      {
         res=ReadConfig(req_buf,NULL);
      }
      else
      {
         strcpy(req_buf,"PROGDIR:CrashMail.prefs");

         res=ReadConfig(req_buf,NULL);

         if(!res)
         {
            if(res=ReadConfig("MAIL:CrashMail.prefs",NULL))
               strcpy(req_buf,"MAIL:CrashMail.prefs");
         }
      }

      rtUnlockWindow(mainwin,lock);

      if(res)
      {
         hasconfig=TRUE;
      }
      else
      {
         BOOL has2;
         UBYTE *tmp=req_buf;
         has2=FALSE;

         while(!has2)
         {
            res=rtEZRequestTags("Settings file \"%s\" not found.\nWhat do you want to do?","_Load another|_Create new config|_Quit",NULL,&tmp,
                  RT_Underscore,'_',
                  RT_Window,mainwin,
                  RT_WaitPointer,TRUE,
                  RT_LockWindow, TRUE,
                  TAG_END);

            if(!res)
               CleanUp(0);

            if(res==1)
               if(OpenReq(req_buf,300,"Open settings file",mainwin,FALSE)) has2=TRUE;

            if(res==2)
               if(NewConfig())
               {
                  has2=TRUE;
                  hasconfig=TRUE;
               }
         }
      }
   }

   CheckUnconfirmed();

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(mainhandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;
         qualifier=IntuiMessage->Qualifier;

         LT_ReplyIMsg(IntuiMessage);

         switch(class)
         {
            case IDCMP_CLOSEWINDOW:   AskQuit();
                                      break;
            case IDCMP_GADGETUP:      gid=((struct Gadget *)iaddress)->GadgetID;

                                      if(gid > 0 && gid<=20)
                                      {
                                         LT_LockWindow(mainwin);
                                         (*mainfuncs[gid-1])();
                                         LT_UnlockWindow(mainwin);
                                      }

                                      break;

            case IDCMP_MENUPICK:      num=code;
                                      while(num!=MENUNULL)
                                      {
                                         if(MENUNUM(num)==0)
                                         {
                                            switch(ITEMNUM(num))
                                            {
                                               case 0: if(OpenReq(req_buf,300,"Open...",mainwin,FALSE))
                                                       {
                                                          FreeConfig();
                                                          ResetConfig();
                                                          lock=rtLockWindow(mainwin);
                                                          ReadConfig(req_buf,NULL);
                                                          CheckUnconfirmed();
                                                          rtUnlockWindow(mainwin,lock);
                                                       }
                                                       break;
                                               case 1: lock=rtLockWindow(mainwin);
                                                       WriteConfig(req_buf);
                                                       rtUnlockWindow(mainwin,lock);
                                                       saveconfig=FALSE;
                                                       break;
                                               case 2: if(OpenReq(req_buf,300,"Save As...",mainwin,TRUE))
                                                       {
                                                          lock=rtLockWindow(mainwin);
                                                          WriteConfig(req_buf);
                                                          rtUnlockWindow(mainwin,lock);
                                                          saveconfig=FALSE;
                                                       }
                                                       break;
                                               case 4: rtEZRequestTags(aboutbuf,"_Ok",NULL,NULL,RT_Underscore,'_',
                                                                                                RT_Window,mainwin,
                                                                                                RT_WaitPointer,TRUE,
                                                                                                RT_LockWindow, TRUE,
                                                                                                RTEZ_Flags, EZREQF_CENTERTEXT,
                                                                                                TAG_END);
                                                       break;
                                               case 6: AskQuit();
                                                       break;
                                            }
                                         }
                                         if(MENUNUM(num)==1)
                                         {
                                            switch(ITEMNUM(num))
                                            {
                                               case 0: ImportTrapToss();
                                                       break;
                                               case 1: ImportAreasBBS();
                                                       break;
                                               case 3: WriteAreasBBS();
                                                       break;
                                            }
                                         }
                                         menuitem=ItemAddress(menus,num);
                                         num=menuitem->NextSelect;
                                      }
                                      break;
         }
      }
      WaitPort(mainwin->UserPort);
   }
}


/* help */
/* tab aktivera stringgadget under listview... */