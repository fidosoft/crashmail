#include <exec/types.h>
#include <exec/execbase.h>
#include <exec/memory.h>
#include <exec/libraries.h>
#include <dos/dos.h>
#include <intuition/intuition.h>
#include <intuition/gadgetclass.h>
#include <libraries/gadtools.h>
#include <libraries/amigaguide.h>
#include <intuition/intuitionbase.h>
#include <intuition/gadgetclass.h>
#include <intuition/sghooks.h>
#include <intuition/imageclass.h>
#include <libraries/altclass.h>
#include <libraries/reqtools.h>
#include <libraries/gtlayout.h>
#include <graphics/gfxbase.h>
#include <workbench/startup.h>
#include <pd/nl.h>

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <stdarg.h>
#include <varargs.h>

#include <clib/alib_protos.h>
#include <clib/exec_protos.h>
#include <clib/dos_protos.h>
#include <clib/intuition_protos.h>
#include <clib/gadtools_protos.h>
#include <clib/graphics_protos.h>
#include <clib/utility_protos.h>
#include <clib/keymap_protos.h>
#include <clib/amigaguide_protos.h>
#include <clib/reqtools_protos.h>
#include <clib/icon_protos.h>

#define VERSION "1.32"
#define COPYRIGHT "1995-1998"

#define STACKSIZE 20000

#define NUMCMPCPY(x,y) if(x!=LT_GetAttributes(parthandle,y,TAG_END)) saveconfig=TRUE;x=LT_GetAttributes(parthandle,y,TAG_END)

#include <cmshared/jblist.h>
#include <cmshared/jbio.h>
#include <cmshared/config.h>
#include <cmshared/readkey.h>
#include <cmshared/nodelist.h>

Free(void);
FreeConfig(void);
BOOL ReadConfig(UBYTE *cfg,BPTR lockfh);
ResetConfig(void);
BOOL WriteConfig(UBYTE *cfg);
void strcmpcpy(UBYTE *dest,UBYTE *src);
Parse4D(UBYTE *buf, struct Node4D *node);
int Compare4D(struct Node4D *node1,struct Node4D *node2);
Parse4DPat(UBYTE *buf, struct Node4DPat *node);
rawParse4DPat(UBYTE *buf, struct Node4DPat *node);
Parse4DDestPat(UBYTE *buf, struct Node4DPat *node);
int Compare4DPat(struct Node4DPat *nodepat,struct Node4D *node);
int NodeCompare(UBYTE *pat,UWORD num);
Print4DPat(struct Node4DPat *pat,UBYTE *dest);
Print4DDestPat(struct Node4DPat *pat,UBYTE *dest);
Copy4D(struct Node4D *node1,struct Node4D *node2);
Copy4DPat(struct Node4DPat *node1,struct Node4DPat *node2);
Copy2DPat(struct Node2DPat *node1,struct Node2DPat *node2);
BOOL Check4DPatNodelist(struct Node4DPat *pat);
BOOL OpenReq(UBYTE *file,ULONG varsize,UBYTE *title,struct Window *win,BOOL save);
BOOL OpenReqDir(UBYTE *file,ULONG varsize,UBYTE *title,struct Window *win);

void MoveUp(struct jbList *list,ULONG num);
void MoveDown(struct jbList *list,ULONG num);

extern struct IntuitionBase *IntuitionBase;

#define HELP_MAIN         0
#define HELP_GENERAL      1
#define HELP_OPTIONS      2
#define HELP_PATH         3
#define HELP_AKA          4
#define HELP_NODES        5
#define HELP_AREAS        6
#define HELP_UMS          7
#define HELP_GROUPS       8
#define HELP_PACKERS      9
#define HELP_CHARSET     10
#define HELP_BOUNCE      11
#define HELP_FILEATTACH  12
#define HELP_REMAP       13
#define HELP_ROUTE       14
#define HELP_CHANGE      15
#define HELP_ROBOTNAMES  16
#define HELP_AREAFIX     17
#define HELP_AREALIST    18
#define HELP_CHANGEMANY  19

#define AverageWidth(x) (TextLength(x,"ABCDEFGHIJKLMNOPQRSTUVWXYZ",26)/26)
#define StringHeight(x) (3+((struct RastPort *)x)->Font->tf_YSize+3)
#define ButtonHeight(x) (3+((struct RastPort *)x)->Font->tf_YSize+3)

#define getgadgetstring(g) ((struct StringInfo *)(g)->SpecialInfo)->Buffer
#define getgadgetvalue(g)  ((struct StringInfo *)(g)->SpecialInfo)->LongInt

extern struct Screen *scr;
extern struct Window *mainwin;

extern struct LayoutHandle *parthandle;
extern struct Window *partwin;

extern struct ExecBase *SysBase;
extern struct Library *GadToolsBase;
extern struct Library *GTLayoutBase;

void PartGeneral(void);
void PartUMS(void);
void PartPath(void);
void PartOptions(void);
void PartAka(void);
void PartNode(void);
void PartAreas(void);
void PartPackers(void);
void PartCharset(void);
void PartGroups(void);
void PartRemap(void);
void PartRobots(void);
void PartBounce(void);
void PartFileAttach(void);
void PartRoute(void);
void PartChange(void);
void PartAreaFix(void);
void PartArealist(void);

BOOL NewConfig(void);

ULONG AskList(struct List *list,UBYTE *title,struct Window *win);
UBYTE AskGroup(struct Window *win,UBYTE *nullname);
struct Aka *AskAka(struct Window *win);
struct ConfigNode *AskCNode(struct Window *win);
int EditGroups(UBYTE *groups,UBYTE *title,struct Window *win);

BOOL CheckFlags(UBYTE group,UBYTE *node);


struct TempNode
{
   struct Node Node;
   UBYTE buf[200];
   APTR UserData;
};

void FreeTempNodes(struct List *list);

extern struct Hook StrEditHook;
extern struct Hook StrEditHookNext;

__asm __geta4 ULONG HelpHookEntry(register __a0 struct Hook *hook,register __a2 struct HelpMsg *helpmsg,register __a1 struct IBox *ibox);
__asm __geta4 ULONG HelpHookEntryWin(register __a0 struct Hook *hook,register __a2 struct HelpMsg *helpmsg,register __a1 struct IBox *ibox);

#define TAB          66

void ImportTrapToss(void);
void ImportAreasBBS(void);
void WriteAreasBBS(void);

extern struct Library *NodelistBase;
extern NodeList nodelist;

BOOL Parse2DPat(UBYTE *buf,struct Node2DPat *pat);
void PrintFull4D(struct Node4D *node,UBYTE *buf);

