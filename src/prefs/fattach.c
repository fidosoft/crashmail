#include <CrashPrefs.h>

struct List fattachlist;
ULONG fattachnum,currentfattach;

struct Hook FAttachHelpHook = { {NULL}, HelpHookEntry,NULL,HELP_FILEATTACH};

void SetFileAttach(ULONG num)
{
   if(fattachnum == 0)
   {
      LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);
   }

   if(num!=-1)
   {
      if(fattachnum)
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,FALSE,TAG_END);
      }
      else
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
      }

      if(num!=0)                 LT_SetAttributes(parthandle,12,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);

      if(num!=fattachnum-1)       LT_SetAttributes(parthandle,13,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);
   }

   currentfattach=num;
}

void RefreshFileAttachlist(void)
{
   struct TempNode *tempnode;
   struct PatternNode *pat;

   LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
   FreeTempNodes(&fattachlist);

   fattachnum=0;

   for(pat=FileAttachList.First;pat;pat=pat->Next)
   {
      if(!(tempnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(&fattachlist);
         return;
      }

      Print4DPat(&pat->Pattern,tempnode->buf);

      tempnode->Node.ln_Name=tempnode->buf;
      AddTail(&fattachlist,(struct Node *)tempnode);
      fattachnum++;
   }

   LT_SetAttributes(parthandle,4711,GTLV_Labels,&fattachlist,TAG_END);
}

void AddFileAttach(void)
{
   struct Node4DPat n4dpat;
   struct PatternNode *pat;
   UBYTE buf[100];
   BOOL res;
   BOOL

   kg=TRUE;
   buf[0]=0;

   while(kg)
   {
      res=rtGetString(buf,50,"Add file attach pattern",NULL,RT_Window,partwin,
                                                       RT_WaitPointer, TRUE,
                                                       RT_LockWindow, TRUE,
                                                       TAG_END);

      if(!res)
         return;

      kg=FALSE;

      if(!Parse4DPat(buf,&n4dpat))
      {
         rtEZRequestTags("Invalid node pattern","_Ok",NULL,NULL,RT_Underscore,'_',
                                                                RT_Window,partwin,
                                                                RT_WaitPointer, TRUE,
                                                                RT_LockWindow, TRUE,
                                                                TAG_END);
         kg=TRUE;
      }
   }

   if(pat=(struct PatternNode *)AllocMem(sizeof(struct PatternNode),MEMF_CLEAR))
   {
      jbAddNode(&FileAttachList,(struct jbNode *)pat);
      Copy4DPat(&pat->Pattern,&n4dpat);
      RefreshFileAttachlist();
      SetFileAttach(fattachnum-1);
      LT_SetAttributes(parthandle,4711,GTLV_Selected,currentfattach,GTLV_MakeVisible,currentfattach,TAG_END);
      saveconfig=TRUE;
   }
   else
   {
      DisplayBeep(NULL);
   }
}

void RemFileAttach(void)
{
   ULONG c,tmp;
   struct PatternNode *pat;
   UBYTE buf[100],buf2[100];
   BOOL res;

   pat=FileAttachList.First;
   for(c=0;c<currentfattach;c++) pat=pat->Next;

   Print4DPat(&pat->Pattern,buf2);
   sprintf(buf,"Do you really want to remove the pattern \"%s\"?",buf2);

   res=rtEZRequestTags(buf,"_Remove|_Cancel",NULL,NULL,RT_Underscore,'_',
                                                       RT_Window,partwin,
                                                       RT_WaitPointer,TRUE,
                                                       RT_LockWindow, TRUE,
                                                       RTEZ_Flags, EZREQF_CENTERTEXT | EZREQF_NORETURNKEY,
                                                       TAG_END);

   if(!res)
      return;

   tmp=currentfattach;
   jbFreeNode(&FileAttachList,(struct jbNode *)pat,sizeof(struct PatternNode));
   RefreshFileAttachlist();
   if(tmp == fattachnum) tmp--;
   SetFileAttach(tmp);
   saveconfig=TRUE;
}

void PartFileAttach(void)
{
   ULONG class,code,tmp;
   struct IntuiMessage *IntuiMessage;
   struct TempNode *tempnode;
   struct PatternNode *pat;
   APTR iaddress;
   ULONG c;
   struct Node4DPat n4dpat;

   NewList(&fattachlist);
   currentfattach=-1;

   RefreshFileAttachlist();

   parthandle = LT_CreateHandleTags(scr,LH_EditHook,&StrEditHook,LH_RawKeyFilter,FALSE,TAG_DONE);

   if(!parthandle)
   {
      FreeTempNodes(&fattachlist);
      return;
   }

   LT_New(parthandle,
      LA_Type,      VERTICAL_KIND,
      LA_LabelText, "Patterns for allowed File Attach",
      TAG_DONE);

      LT_New(parthandle,
         LA_Type,      STRING_KIND,
         GTST_MaxChars,30,
         LA_ID,        4712,
         LAST_Link,    4711,
         GA_Disabled,TRUE,
         TAG_DONE);

      LT_New(parthandle,
         LA_Type,       LISTVIEW_KIND,
         LALV_Lines,    10,
         LALV_CursorKey,TRUE,
         LALV_Link,4712,
         GTLV_Labels,&fattachlist,
         LA_ID,4711,
         TAG_DONE);

      LT_New(parthandle,
         LA_Type,      HORIZONTAL_KIND,
         LAGR_SameSize,TRUE,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      VERTICAL_KIND,
            LAGR_SameSize,TRUE,
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      BUTTON_KIND,
               LA_LabelText, "Add...",
               LA_Chars,20,
               LA_ID,10,
               TAG_DONE);

            LT_New(parthandle,
               LA_Type,      BUTTON_KIND,
               LA_LabelText, "Up",
               GA_Disabled,TRUE,
               LA_Chars,20,
               LA_ID,12,
               TAG_DONE);

         LT_EndGroup(parthandle);

         LT_New(parthandle,
            LA_Type,      VERTICAL_KIND,
            LAGR_SameSize,TRUE,
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      BUTTON_KIND,
               LA_LabelText, "Remove...",
               LA_Chars,20,
               GA_Disabled,TRUE,
               LA_ID,11,
               TAG_DONE);

            LT_New(parthandle,
               LA_Type,      BUTTON_KIND,
               LA_LabelText, "Down",
               LA_Chars,20,
               GA_Disabled,TRUE,
               LA_ID,13,
               TAG_DONE);

         LT_EndGroup(parthandle);

      LT_EndGroup(parthandle);

   LT_EndGroup(parthandle);

   partwin=LT_Build(parthandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW|IDCMP_RAWKEY,
                               LAWN_Title,    "FileAttach nodes",
                               WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                               LAWN_HelpHook, &FAttachHelpHook,
                               TAG_END);

   if(!partwin)
   {
      LT_DeleteHandle(parthandle);
      DisplayBeep(NULL);
      FreeTempNodes(&fattachlist);
      return;
   }

   if(fattachnum)
   {
      LT_SetAttributes(parthandle,4711,GTLV_Selected,0,TAG_END);
      SetFileAttach(0);
   }

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(parthandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;

         LT_ReplyIMsg(IntuiMessage);

         switch(class)
         {
            case IDCMP_RAWKEY:         if(code == TAB) LT_Activate(parthandle,4712);
                                       break;
            case IDCMP_GADGETUP:       switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 10:
                                             AddFileAttach();
                                             break;
                                          case 11:
                                             RemFileAttach();
                                             break;
                                          case 12:
                                             if(currentfattach != 0)
                                             {
                                                tmp=currentfattach;
                                                MoveUp(&FileAttachList,tmp);
                                                RefreshFileAttachlist();
                                                SetFileAttach(tmp-1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentfattach,
                                                   GTLV_MakeVisible,currentfattach,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 13:
                                             if(currentfattach != fattachnum-1)
                                             {
                                                tmp=currentfattach;
                                                MoveDown(&FileAttachList,tmp);
                                                RefreshFileAttachlist();
                                                SetFileAttach(tmp+1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentfattach,
                                                   GTLV_MakeVisible,currentfattach,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 4711:
                                             SetFileAttach(code);
                                             break;
                                          case 4712:
                                             if(!Parse4DPat(getgadgetstring((struct Gadget *)iaddress),&n4dpat))
                                             {
                                                DisplayBeep(NULL);
                                                LT_Activate(parthandle,4712);
                                             }
                                             else
                                             {
                                                tempnode=fattachlist.lh_Head;
                                                for(c=0;c<currentfattach;c++) tempnode=tempnode->Node.ln_Succ;

                                                LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
                                                Print4DPat(&n4dpat,tempnode->buf);
                                                LT_SetAttributes(parthandle,4711,GTLV_Labels,&fattachlist,TAG_END);

                                                pat=FileAttachList.First;
                                                for(c=0;c<currentfattach;c++) pat=pat->Next;
                                                Copy4DPat(&pat->Pattern,&n4dpat);
                                             }
                                             break;

                                       }
                                       break;
            case IDCMP_CLOSEWINDOW:    LT_DeleteHandle(parthandle);
                                       parthandle=NULL;
                                       FreeTempNodes(&fattachlist);
                                       return;
         }
      }
      WaitPort(partwin->UserPort);
   }
}
