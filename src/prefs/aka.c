#include <CrashPrefs.h>

struct List akalist;
struct List akaaddlist;
struct List akaremlist;

ULONG akanum,currentaka,totakaadd,totakarem,currentakaadd,currentakarem;
struct Aka *akanode;

struct Hook AkaHelpHook = { {NULL}, HelpHookEntry,NULL,HELP_AKA};

void RefreshAkaAddlist(void)
{
   struct AddNode *addnode;
   struct TempNode *tnode;

   LT_SetAttributes(parthandle,5711,GTLV_Labels,~0,TAG_END);

   FreeTempNodes(&akaaddlist);
   totakaadd=0;

   if(akanode->AddList.First)
   {
      for(addnode=akanode->AddList.First;addnode;addnode=addnode->Next)
      {
         if(!(tnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
         {
            DisplayBeep(NULL);
            return;
         }

         sprintf(tnode->buf,"%lu/%lu",addnode->Node.Net,addnode->Node.Node);
         tnode->Node.ln_Name=tnode->buf;
         AddTail(&akaaddlist,(struct Node *)tnode);
         totakaadd++;
      }
   }

   LT_SetAttributes(parthandle,5711,GTLV_Labels,&akaaddlist,TAG_END);
}

void RefreshAkaRemlist(void)
{
   struct RemNode *remnode;
   struct TempNode *tnode;

   LT_SetAttributes(parthandle,6711,GTLV_Labels,~0,TAG_END);

   FreeTempNodes(&akaremlist);
   totakarem=0;

   if(akanode->RemList.First)
   {
      for(remnode=akanode->RemList.First;remnode;remnode=remnode->Next)
      {
         if(!(tnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
         {
            DisplayBeep(NULL);
            return;
         }
         sprintf(tnode->buf,"%s/%s",remnode->NodePat.Net,remnode->NodePat.Node);
         tnode->Node.ln_Name=tnode->buf;
         AddTail(&akaremlist,(struct Node *)tnode);
         totakarem++;
       }
   }

   LT_SetAttributes(parthandle,6711,GTLV_Labels,&akaremlist,TAG_END);
}


void SetAkaAdd(void)
{
   RefreshAkaAddlist();

   if(totakaadd)
   {
      LT_SetAttributes(parthandle,5712,GA_Disabled,FALSE,TAG_DONE);
      LT_SetAttributes(parthandle,501,GA_Disabled,FALSE,TAG_DONE);
      LT_SetAttributes(parthandle,5711,GTLV_Selected,currentakaadd,GTLV_MakeVisible,0,TAG_DONE);
   }
   else
   {
      LT_SetAttributes(parthandle,5712,GA_Disabled,TRUE,TAG_DONE);
      LT_SetAttributes(parthandle,501,GA_Disabled,TRUE,TAG_DONE);
   }
}

void SetAkaRem(void)
{
   RefreshAkaRemlist();

   if(totakarem)
   {
      LT_SetAttributes(parthandle,6712,GA_Disabled,FALSE,TAG_DONE);
      LT_SetAttributes(parthandle,601,GA_Disabled,FALSE,TAG_DONE);
      LT_SetAttributes(parthandle,6711,GTLV_Selected,currentakarem,GTLV_MakeVisible,currentakarem,TAG_DONE);
   }
   else
   {
      LT_SetAttributes(parthandle,6712,GA_Disabled,TRUE,TAG_DONE);
      LT_SetAttributes(parthandle,601,GA_Disabled,TRUE,TAG_DONE);
   }
}

void SetAka(ULONG num)
{
   ULONG c;

   if(currentaka!=-1)
   {
      NUMCMPCPY(akanode->FakeNet,61);
      strcmpcpy(akanode->Domain,(UBYTE *)LT_GetAttributes(parthandle,60,TAG_END));
   }

   if(akanum == 0)
   {
      LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,50,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,51,GA_Disabled,TRUE,TAG_END);
   }

   akanode=NULL;

   if(num!=-1)
   {
      if(akanum)
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,FALSE,TAG_END);
      }
      else
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
      }

      if(akanum == 1)
         LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);

      akanode=AkaList.First;
      for(c=0;c<num;c++) akanode=akanode->Next;

      if(num!=0)                 LT_SetAttributes(parthandle,12,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);

      if(num!=akanum-1)          LT_SetAttributes(parthandle,13,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);

      LT_SetAttributes(parthandle,50,GA_Disabled,FALSE,TAG_END);
      LT_SetAttributes(parthandle,51,GA_Disabled,FALSE,TAG_END);

      LT_SetAttributes(parthandle,60,GTST_String,akanode->Domain,TAG_END);
      LT_SetAttributes(parthandle,61,GTIN_Number,akanode->FakeNet,TAG_END);

      currentakaadd=0;
      currentakarem=0;

      SetAkaAdd();
      SetAkaRem();
   }

   currentaka=num;
}

void RefreshAkalist(void)
{
   struct TempNode *tempnode;
   struct Aka *aka;

   LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
   FreeTempNodes(&akalist);

   akanum=0;

   for(aka=AkaList.First;aka;aka=aka->Next)
   {
      if(!(tempnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(&akalist);
         return;
      }

      sprintf(tempnode->buf,"%lu:%lu/%lu.%lu",aka->Node.Zone,aka->Node.Net,aka->Node.Node,aka->Node.Point);
      tempnode->Node.ln_Name=tempnode->buf;
      AddTail(&akalist,(struct Node *)tempnode);
      akanum++;
   }

   LT_SetAttributes(parthandle,4711,GTLV_Labels,&akalist,TAG_END);
}

BOOL CheckDupeAka(UBYTE *buf,ULONG num)
{
   struct Node4D n4d;
   struct Aka *aka;
   ULONG c;

   if(!Parse4D(buf,&n4d))
   {
      rtEZRequestTags("Invalid node number","_Ok",NULL,NULL,RT_Underscore,'_',
                                                            RT_Window,partwin,
                                                            RT_WaitPointer, TRUE,
                                                            RT_LockWindow, TRUE,
                                                            TAG_END);
      return(FALSE);
   }

   for(aka=AkaList.First,c=0;aka;aka=aka->Next,c++)
      if(Compare4D(&aka->Node,&n4d)==0 && c!=num) break;

   if(aka)
   {
      rtEZRequestTags("You already have that aka","_Ok",NULL,NULL,RT_Underscore,'_',
                                                                  RT_Window,partwin,
                                                                  RT_WaitPointer, TRUE,
                                                                  RT_LockWindow, TRUE,
                                                                  TAG_END);
      return(FALSE);
   }

   return(TRUE);
}

void AddAka(void)
{
   struct Aka *aka;
   struct Node4D n4d;
   UBYTE buf[200];
   BOOL res;
   BOOL kg;

   kg=TRUE;
   buf[0]=0;

   while(kg)
   {
      res=rtGetString(buf,50,"Add aka",NULL,RT_Window,partwin,
                                            RT_WaitPointer, TRUE,
                                            RT_LockWindow, TRUE,
                                            TAG_END);

      if(!res)
         return;

      if(CheckDupeAka(buf,-1))
         kg=FALSE;
   }

   Parse4D(buf,&n4d);

   if(aka=(struct Aka *)AllocMem(sizeof(struct Aka),MEMF_CLEAR))
   {
      jbAddNode(&AkaList,(struct jbNode *)aka);
      Copy4D(&aka->Node,&n4d);
      jbNewList(&aka->AddList);
      jbNewList(&aka->RemList);
      RefreshAkalist();
      SetAka(akanum-1);
      LT_SetAttributes(parthandle,4711,GTLV_Selected,currentaka,GTLV_MakeVisible,currentaka,TAG_END);
      saveconfig=TRUE;
   }
   else
   {
      DisplayBeep(NULL);
   }
}

void RemAka(void)
{
   struct Aka *newaka;
   struct Area *area,*area2;
   struct Route *route,*route2;
   UBYTE buf[200];

   ULONG areas=0,routes=0,res;

   for(area=AreaList.First;area;area=area->Next)
      if(area->Aka == akanode) areas++;

   for(route=RouteList.First;route;route=route->Next)
      if(route->Aka == akanode) routes++;

   sprintf(buf,"Do you really want to remove the aka %lu:%lu/%lu.%lu?\n\nIt is used for %lu areas and %lu routings",
      akanode->Node.Zone,akanode->Node.Net,akanode->Node.Node,akanode->Node.Point,areas,routes);

   res=rtEZRequestTags(buf,"_Remove|_Cancel",NULL,NULL,RT_Underscore,'_',
                                                       RT_Window,partwin,
                                                       RT_WaitPointer,TRUE,
                                                       RT_LockWindow, TRUE,
                                                       RTEZ_Flags, EZREQF_CENTERTEXT | EZREQF_NORETURNKEY,
                                                       TAG_END);

   if(!res)
      return;

   if(routes || areas)
   {
      res=rtEZRequestTags("What do you want to do with the areas and routings\nthat use this aka?\n\n(You can keep the aka by pressing Cancel)",
                          "_Change aka|_Remove|_Cancel",NULL,NULL,RT_Underscore,'_',
                                                                  RT_Window,partwin,
                                                                  RT_WaitPointer,TRUE,
                                                                  RT_LockWindow, TRUE,
                                                                  RTEZ_Flags, EZREQF_CENTERTEXT,
                                                                  TAG_END);

      if(!res)
         return;

      if(res == 1)
      {
         struct List tmpakalist;
         ULONG c,res;
         struct TempNode *tempnode;
         struct Aka *aka;

         NewList(&tmpakalist);

         for(aka=AkaList.First;aka;aka=aka->Next)
         {
            if(aka!=akanode)
            {
               if(!(tempnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
               {
                  DisplayBeep(NULL);
                  FreeTempNodes(&tmpakalist);
                  return;
               }
               sprintf(tempnode->buf,"%lu:%lu/%lu.%lu",aka->Node.Zone,aka->Node.Net,aka->Node.Node,aka->Node.Point);
               tempnode->Node.ln_Name=tempnode->buf;
               tempnode->UserData=aka;
               AddTail(&tmpakalist,(struct Node *)tempnode);
            }
         }

         res=AskList(&tmpakalist,"Select new aka",partwin);

         if(res==-1)
         {
            FreeTempNodes(&tmpakalist);
            return;
         }

         tempnode=tmpakalist.lh_Head;
         for(c=0;c<res;c++) tempnode=tempnode->Node.ln_Succ;

         newaka=tempnode->UserData;

         for(area=AreaList.First;area;area=area->Next)
            if(area->Aka == akanode) area->Aka=newaka;

         for(route=RouteList.First;route;route=route->Next)
            if(route->Aka == akanode) route->Aka=newaka;

         FreeTempNodes(&tmpakalist);
      }

      if(res == 2)
      {
         ULONG netmails;

         netmails=0;

         for(area=AreaList.First;area;area=area->Next)
            if((area->Flags & AREA_NETMAIL) && area->Aka!=akanode) netmails++;

         if(akanode!= (struct Aka *)AkaList.First)
            newaka=(struct Aka *)AkaList.First;

         else
            newaka=(struct Aka *)AkaList.First->Next;

         area=AreaList.First;

         while(area)
         {
            area2=area->Next;

            if(area->Aka == akanode)
            {
               if(area->Flags & AREA_BAD)
               {
                  area->Aka=newaka;
               }
               else if(area->Flags & AREA_DEFAULT)
               {
                  area->Aka=newaka;
               }
               else if((area->Flags & AREA_NETMAIL) && netmails == 0)
               {
                  area->Aka=newaka;
               }
               else
               {
                  if(area->Flags & AREA_NETMAIL)
                     jbFreeList(&area->TossNodes,area->TossNodes.First,sizeof(struct ImportNode));

                  else
                     jbFreeList(&area->TossNodes,area->TossNodes.First,sizeof(struct TossNode));

                  jbFreeNode(&AreaList,(struct jbNode *)area,sizeof(struct Area));
               }
            }
            area=area2;
         }

         if(netmails==0)
         {
            UBYTE buf[200];

            sprintf(buf,"Every CrashMail configuration needs at least one\n"
                        "netmail area. Therefore the aka of your last\n"
                        "netmail area was changed to %lu:%lu/%lu.%lu",
                           newaka->Node.Zone,
                           newaka->Node.Net,
                           newaka->Node.Node,
                           newaka->Node.Point);

            res=rtEZRequestTags(buf,"Ok",NULL,NULL,RT_Underscore,'_',
                     RT_Window,partwin,
                     RT_WaitPointer,TRUE,
                     RT_LockWindow, TRUE,
                     RTEZ_Flags, EZREQF_CENTERTEXT,
                     TAG_END);
         }

         route=RouteList.First;

         while(route)
         {
            route2=route->Next;

            if(route->Aka == akanode)
               jbFreeNode(&RouteList,(struct jbNode *)route,sizeof(struct Route));

            route=route2;
         }
      }
   }

   jbFreeList(&akanode->AddList,akanode->AddList.First,sizeof(struct AddNode));
   jbFreeList(&akanode->RemList,akanode->RemList.First,sizeof(struct RemNode));
   jbFreeNode(&AkaList,(struct jbNode *)akanode,sizeof(struct Aka));

   if(currentaka == akanum-1)
      currentaka--;

   RefreshAkalist();
   SetAka(currentaka);
   saveconfig=TRUE;
}

void PartAka(void)
{
   ULONG class,code,tmp;
   struct IntuiMessage *IntuiMessage;
   struct TempNode *tempnode;
   APTR iaddress;
   ULONG c;
   UBYTE nodebuf[50];
   struct Node2DPat n2dpat;
   struct Node4D n4d;
   struct AddNode *addnode;
   struct RemNode *remnode;
   BOOL kg,res;
   UBYTE buf[100];

   NewList(&akalist);
   NewList(&akaaddlist);
   NewList(&akaremlist);

   currentaka=-1;

   parthandle = LT_CreateHandleTags(scr,LH_EditHook,&StrEditHook,
                                        LH_RawKeyFilter,FALSE,TAG_DONE);

   if(!parthandle)
   {
      FreeTempNodes(&akalist);
      return;
   }

   LT_New(parthandle,
      LA_Type,      HORIZONTAL_KIND,
      LA_LabelText, "Aka",
      TAG_DONE);

      LT_New(parthandle,
         LA_Type,      VERTICAL_KIND,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      STRING_KIND,
            GTST_MaxChars,199,
            LA_ID,        4712,
            LAST_Link,    4711,
            GA_Disabled,TRUE,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,       LISTVIEW_KIND,
            LA_Chars,      20,
            LALV_Lines,    11,
            LALV_LockSize, TRUE,
            LALV_CursorKey,TRUE,
            LALV_Link,     4712,
            GTLV_Labels,   &akalist,
            LA_ID,4711,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      HORIZONTAL_KIND,
            LAGR_SameSize,TRUE,
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               LAGR_SameSize,TRUE,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Add...",
                  LA_Chars,10,
                  LA_ID,10,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Up",
                  GA_Disabled,TRUE,
                  LA_Chars,10,
                  LA_ID,12,
                  TAG_DONE);

            LT_EndGroup(parthandle);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               LAGR_SameSize,TRUE,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Remove...",
                  LA_Chars,10,
                  GA_Disabled,TRUE,
                  LA_ID,11,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      BUTTON_KIND,
                  LA_LabelText, "Down",
                  LA_Chars,10,
                  GA_Disabled,TRUE,
                  LA_ID,13,
                  TAG_DONE);

            LT_EndGroup(parthandle);

         LT_EndGroup(parthandle);

      LT_EndGroup(parthandle);

      LT_New(parthandle,
         LA_Type,      VERTICAL_KIND,
         LA_ExtraSpace,TRUE,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      VERTICAL_KIND,
            LA_ExtraSpace,TRUE,
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      STRING_KIND,
               LA_LabelText, "Domain",
               GTST_MaxChars,50,
               LA_Chars,35,
               LA_ID,60,
               TAG_DONE);

            LT_New(parthandle,
               LA_Type,      INTEGER_KIND,
               LA_LabelText, "FakeNet",
               LA_Chars,35,
               LA_ID,61,
               TAG_DONE);

         LT_EndGroup(parthandle);

         LT_New(parthandle,
            LA_Type,    HORIZONTAL_KIND,
            LA_LabelText,"SEEN-BY modifications",
            LA_ExtraSpace, TRUE,
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,    VERTICAL_KIND,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      STRING_KIND,
                  GTST_MaxChars,199,
                  LA_ID,        5712,
                  LAST_Link,    5711,
                  GA_Disabled,TRUE,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,       LISTVIEW_KIND,
                  LA_LabelText,  "Add SEEN-BY",
                  LALV_Lines,    5,
                  LALV_Link,     5712,
                  GTLV_Labels,   &akaaddlist,
                  LA_ID,5711,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      HORIZONTAL_KIND,
                  LAGR_SameSize,TRUE,
                  TAG_DONE);

                  LT_New(parthandle,
                     LA_Type,      BUTTON_KIND,
                     LA_LabelText, "Add...",
                     LA_Chars,9,
                     LA_ID,500,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_Type,      BUTTON_KIND,
                     LA_LabelText, "Remove",
                     GA_Disabled,TRUE,
                     LA_Chars,9,
                     LA_ID,501,
                     TAG_DONE);

               LT_EndGroup(parthandle);

            LT_EndGroup(parthandle);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               LA_ExtraSpace,TRUE,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      STRING_KIND,
                  GTST_MaxChars,199,
                  LA_ID,        6712,
                  LAST_Link,    6711,
                  GA_Disabled,TRUE,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,       LISTVIEW_KIND,
                  LA_LabelText,  "Strip SEEN-BY",
                  LALV_Lines,    5,
                  LALV_Link,     6712,
                  GTLV_Labels,   &akaremlist,
                  LA_ID,6711,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      HORIZONTAL_KIND,
                  LAGR_SameSize,TRUE,
                  TAG_DONE);

                  LT_New(parthandle,
                     LA_Type,      BUTTON_KIND,
                     LA_LabelText, "Add...",
                     LA_Chars,9,
                     LA_ID,600,
                     TAG_DONE);

                  LT_New(parthandle,
                     LA_Type,      BUTTON_KIND,
                     LA_LabelText, "Remove",
                     GA_Disabled,TRUE,
                     LA_Chars,9,
                     LA_ID,601,
                     TAG_DONE);

                LT_EndGroup(parthandle);

            LT_EndGroup(parthandle);

         LT_EndGroup(parthandle);

      LT_EndGroup(parthandle);

   LT_EndGroup(parthandle);

   partwin=LT_Build(parthandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW|IDCMP_RAWKEY,
                               LAWN_Title,    "Aka",
                               LAWN_HelpHook, &AkaHelpHook,
                               WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                               TAG_END);

   if(!partwin)
   {
      LT_DeleteHandle(parthandle);
      DisplayBeep(NULL);
      FreeTempNodes(&akalist);
      FreeTempNodes(&akaaddlist);
      FreeTempNodes(&akaremlist);
      return;
   }

   RefreshAkalist();

   if(akanum)
   {
      LT_SetAttributes(parthandle,4711,GTLV_Selected,0,TAG_END);
      SetAka(0);
   }

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(parthandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;

         LT_ReplyIMsg(IntuiMessage);

         switch(class)
         {
            case IDCMP_RAWKEY:         if(code == TAB) LT_Activate(parthandle,4712);
                                       break;
            case IDCMP_GADGETUP:       switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 10:
                                             AddAka();
                                             break;
                                          case 11:
                                             RemAka();
                                             break;
                                          case 12:
                                             if(currentaka != 0)
                                             {
                                                tmp=currentaka;
                                                MoveUp(&AkaList,tmp);
                                                RefreshAkalist();
                                                SetAka(tmp-1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentaka,
                                                   GTLV_MakeVisible,currentaka,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 13:
                                             if(currentaka != akanum-1)
                                             {
                                                tmp=currentaka;
                                                MoveDown(&AkaList,tmp);
                                                RefreshAkalist();
                                                SetAka(tmp+1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentaka,
                                                   GTLV_MakeVisible,currentaka,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 60:
                                             saveconfig=TRUE;
                                             break;
                                          case 61:
                                             saveconfig=TRUE;
                                             break;
                                          case 500:
                                             buf[0]=0;
                                             kg=TRUE;

                                             while(kg)
                                             {
                                                res=rtGetString(buf,99,"Add SEEN-BY",NULL,RT_Window,
                                                                                             partwin,
                                                                                             RT_WaitPointer, TRUE,
                                                                                             RT_LockWindow, TRUE,
                                                                                             TAG_END);

                                                kg=FALSE;

                                                if(res)
                                                {
                                                   if(!Parse4D(buf,&n4d))
                                                   {
                                                      rtEZRequestTags("Invalid node number","_Ok",NULL,NULL,RT_Underscore,'_',
                                                                                                             RT_Window,partwin,
                                                                                                             RT_WaitPointer, TRUE,
                                                                                                             RT_LockWindow, TRUE,
                                                                                                             TAG_END);
                                                      kg=TRUE;
                                                   }
                                                }
                                             }

                                             if(res)
                                             {
                                                if(addnode=(struct AddNode *)AllocMem(sizeof(struct AddNode),MEMF_CLEAR))
                                                {
                                                   Copy4D(&addnode->Node,&n4d);
                                                   jbAddNode(&akanode->AddList,(struct jbNode *)addnode);

                                                   currentakaadd=totakaadd;
                                                   RefreshAkaAddlist();
                                                   SetAkaAdd();
                                                   saveconfig=TRUE;
                                                }
                                                else
                                                {
                                                   DisplayBeep(NULL);
                                                }
                                             }
                                             break;
                                          case 501:
                                             jbFreeNum(&akanode->AddList,currentakaadd,sizeof(struct AddNode));
                                             if(currentakaadd==totakaadd-1) currentakaadd--;
                                             RefreshAkaAddlist();
                                             SetAkaAdd();
                                             saveconfig=TRUE;
                                             break;
                                          case 600:
                                             buf[0]=0;
                                             kg=TRUE;

                                             while(kg)
                                             {
                                                res=rtGetString(buf,99,"Remove SEEN-BY",NULL,RT_Window,
                                                                                             partwin,
                                                                                             RT_WaitPointer, TRUE,
                                                                                             RT_LockWindow, TRUE,
                                                                                             TAG_END);

                                                kg=FALSE;

                                                if(res)
                                                {
                                                   if(!Parse2DPat(buf,&n2dpat))
                                                   {
                                                      rtEZRequestTags("Invalid node pattern","_Ok",NULL,NULL,RT_Underscore,'_',
                                                                                                             RT_Window,partwin,
                                                                                                             RT_WaitPointer, TRUE,
                                                                                                             RT_LockWindow, TRUE,
                                                                                                             TAG_END);
                                                      kg=TRUE;
                                                   }
                                                }
                                             }

                                             if(res)
                                             {
                                                if(remnode=(struct RemNode *)AllocMem(sizeof(struct RemNode),MEMF_CLEAR))
                                                {
                                                   Copy2DPat(&remnode->NodePat,&n2dpat);
                                                   jbAddNode(&akanode->RemList,(struct jbNode *)remnode);

                                                   currentakarem=totakarem;
                                                   RefreshAkaRemlist();
                                                   SetAkaRem();
                                                   saveconfig=TRUE;
                                                }
                                                else
                                                {
                                                   DisplayBeep(NULL);
                                                }
                                             }
                                             break;
                                          case 601:
                                             jbFreeNum(&akanode->RemList,currentakarem,sizeof(struct RemNode));
                                             if(currentakarem==totakarem-1) currentakarem--;
                                             RefreshAkaRemlist();
                                             SetAkaRem();
                                             saveconfig=TRUE;
                                             break;
                                          case 4711:
                                             SetAka(code);
                                             break;
                                          case 4712:
                                             if(!Parse4D(getgadgetstring((struct Gadget *)iaddress),&n4d))
                                             {
                                                DisplayBeep(NULL);
                                                LT_Activate(parthandle,4712);
                                             }
                                             else
                                             {
                                                if(!CheckDupeAka(getgadgetstring((struct Gadget *)iaddress),currentaka))
                                                {
                                                   LT_Activate(parthandle,4712);
                                                }
                                                else
                                                {
                                                   Copy4D(&akanode->Node,&n4d);
                                                   sprintf(nodebuf,"%lu:%lu/%lu.%lu",n4d.Zone,n4d.Net,n4d.Node,n4d.Point);

                                                   tempnode=akalist.lh_Head;
                                                   for(c=0;c<currentaka;c++) tempnode=tempnode->Node.ln_Succ;

                                                   LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
                                                   strcpy(tempnode->buf,nodebuf);
                                                   LT_SetAttributes(parthandle,4711,GTLV_Labels,&akalist,TAG_END);
                                                   saveconfig=TRUE;
                                                }
                                             }
                                             break;
                                          case 5711:
                                             currentakaadd=code;
                                             break;
                                          case 5712:
                                             if(!Parse4D(getgadgetstring((struct Gadget *)iaddress),&n4d))
                                             {
                                                DisplayBeep(NULL);
                                                LT_Activate(parthandle,5712);
                                             }
                                             else
                                             {
                                                addnode=akanode->AddList.First;
                                                for(c=0;c<currentakaadd;c++) addnode=addnode->Next;

                                                Copy4D(&addnode->Node,&n4d);
                                                sprintf(nodebuf,"%lu/%lu",n4d.Net,n4d.Node);

                                                tempnode=akaaddlist.lh_Head;
                                                for(c=0;c<currentakaadd;c++) tempnode=tempnode->Node.ln_Succ;

                                                LT_SetAttributes(parthandle,5711,GTLV_Labels,~0,TAG_END);
                                                strcpy(tempnode->buf,nodebuf);
                                                LT_SetAttributes(parthandle,5711,GTLV_Labels,&akaaddlist,TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 6711:
                                             currentakarem=code;
                                             break;
                                          case 6712:
                                             if(!Parse2DPat(getgadgetstring((struct Gadget *)iaddress),&n2dpat))
                                             {
                                                DisplayBeep(NULL);
                                                LT_Activate(parthandle,6712);
                                             }
                                             else
                                             {
                                                remnode=akanode->RemList.First;
                                                for(c=0;c<currentakarem;c++) remnode=remnode->Next;

                                                Copy2DPat(&remnode->NodePat,&n2dpat);
                                                sprintf(nodebuf,"%s/%s",n2dpat.Net,n2dpat.Node);

                                                tempnode=akaremlist.lh_Head;
                                                for(c=0;c<currentakarem;c++) tempnode=tempnode->Node.ln_Succ;

                                                LT_SetAttributes(parthandle,6711,GTLV_Labels,~0,TAG_END);
                                                strcpy(tempnode->buf,nodebuf);
                                                LT_SetAttributes(parthandle,6711,GTLV_Labels,&akaremlist,TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                       }
                                       break;
            case IDCMP_CLOSEWINDOW:    SetAka(-1);
                                       LT_DeleteHandle(parthandle);
                                       parthandle=NULL;
                                       FreeTempNodes(&akalist);
                                       FreeTempNodes(&akaaddlist);
                                       FreeTempNodes(&akaremlist);
                                       return;
         }
      }
      WaitPort(partwin->UserPort);
   }
}
