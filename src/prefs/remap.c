#include <CrashPrefs.h>

struct List remaplist;
ULONG remapnum,currentremap;

struct List remapnodelist;
ULONG  remapnodenum,currentremapnode;

struct Hook RemapHelpHook = { {NULL}, HelpHookEntry,NULL,HELP_REMAP};

STRPTR remappages[]={"Names","Nodes",NULL};

ULONG remapactivepage;
ULONG tmpcurrentremap,tmpcurrentremapnode;

void SetRemap(ULONG num)
{
   struct Remap *remap;
   ULONG c;
   UBYTE buf[50];

   if(currentremap!=-1)
   {
      UBYTE buf[50];
      struct Node4DPat n4dpat;

      remap=RemapList.First;
      for(c=0;c<currentremap;c++) remap=remap->Next;

      strcmpcpy(remap->NewTo,(UBYTE *)LT_GetAttributes(parthandle,50,TAG_END));

      strcpy(buf,(UBYTE *)LT_GetAttributes(parthandle,51,TAG_END));

      if(Parse4DDestPat(buf,&n4dpat))
      {
         if(strcmp(remap->DestPat.Zone, n4dpat.Zone)!=0)  saveconfig=TRUE;
         if(strcmp(remap->DestPat.Net,  n4dpat.Net)!=0)   saveconfig=TRUE;
         if(strcmp(remap->DestPat.Node, n4dpat.Node)!=0)  saveconfig=TRUE;
         if(strcmp(remap->DestPat.Point,n4dpat.Point)!=0) saveconfig=TRUE;

         if(remap->DestPat.Type != n4dpat.Type) saveconfig=TRUE;

         Copy4DPat(&remap->DestPat,&n4dpat);
      }
   }

   if(remapnum == 0)
   {
      LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,50,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,51,GA_Disabled,TRUE,TAG_END);
   }
   if(num!=-1)
   {
      if(remapnum)
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,FALSE,TAG_END);
      }
      else
      {
         LT_SetAttributes(parthandle,11,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,4712,GA_Disabled,TRUE,TAG_END);
      }

      remap=RemapList.First;
      for(c=0;c<num;c++) remap=remap->Next;

      if(num!=0)                 LT_SetAttributes(parthandle,12,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,12,GA_Disabled,TRUE,TAG_END);

      if(num!=remapnum-1)        LT_SetAttributes(parthandle,13,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,13,GA_Disabled,TRUE,TAG_END);

      Print4DDestPat(&remap->DestPat,buf);
      LT_SetAttributes(parthandle,50,GA_Disabled,FALSE,GTST_String,remap->NewTo,TAG_END);
      LT_SetAttributes(parthandle,51,GA_Disabled,FALSE,GTST_String,buf,TAG_END);
   }
   currentremap=num;
}

void SetRemapNode(ULONG num)
{
   struct RemapNode *remapnode;
   ULONG c;
   UBYTE buf[50];

   if(currentremapnode!=-1)
   {
      UBYTE buf[50];
      struct Node4DPat n4dpat;

      remapnode=RemapNodeList.First;
      for(c=0;c<currentremapnode;c++) remapnode=remapnode->Next;

      strcpy(buf,(UBYTE *)LT_GetAttributes(parthandle,151,TAG_END));

      if(Parse4DDestPat(buf,&n4dpat))
      {
         if(strcmp(remapnode->DestPat.Zone, n4dpat.Zone)!=0)  saveconfig=TRUE;
         if(strcmp(remapnode->DestPat.Net,  n4dpat.Net)!=0)   saveconfig=TRUE;
         if(strcmp(remapnode->DestPat.Node, n4dpat.Node)!=0)  saveconfig=TRUE;
         if(strcmp(remapnode->DestPat.Point,n4dpat.Point)!=0) saveconfig=TRUE;

         if(remapnode->DestPat.Type != n4dpat.Type) saveconfig=TRUE;

         Copy4DPat(&remapnode->DestPat,&n4dpat);
      }
   }

   if(remapnodenum == 0)
   {
      LT_SetAttributes(parthandle,14712,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,111,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,112,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,113,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,150,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,151,GA_Disabled,TRUE,TAG_END);
   }
   if(num!=-1)
   {
      if(remapnodenum)
      {
         LT_SetAttributes(parthandle,111,GA_Disabled,FALSE,TAG_END);
         LT_SetAttributes(parthandle,14712,GA_Disabled,FALSE,TAG_END);
      }
      else
      {
         LT_SetAttributes(parthandle,111,GA_Disabled,TRUE,TAG_END);
         LT_SetAttributes(parthandle,14712,GA_Disabled,TRUE,TAG_END);
      }

      remapnode=RemapNodeList.First;
      for(c=0;c<num;c++) remapnode=remapnode->Next;

      if(num!=0)                 LT_SetAttributes(parthandle,112,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,112,GA_Disabled,TRUE,TAG_END);

      if(num!=remapnodenum-1)    LT_SetAttributes(parthandle,113,GA_Disabled,FALSE,TAG_END);
      else                       LT_SetAttributes(parthandle,113,GA_Disabled,TRUE,TAG_END);

      Print4DDestPat(&remapnode->DestPat,buf);

      LT_SetAttributes(parthandle,151,GA_Disabled,FALSE,GTST_String,buf,TAG_END);
   }
   currentremapnode=num;
}

void RefreshRemaplist(void)
{
   struct TempNode *tempnode;
   struct Remap *remap;

   LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
   FreeTempNodes(&remaplist);

   remapnum=0;

   for(remap=RemapList.First;remap;remap=remap->Next)
   {
      if(!(tempnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(&remaplist);
         return;
      }
      strcpy(tempnode->buf,remap->Pattern);
      tempnode->Node.ln_Name=tempnode->buf;
      AddTail(&remaplist,(struct Node *)tempnode);
      remapnum++;
   }

   LT_SetAttributes(parthandle,4711,GTLV_Labels,&remaplist,TAG_END);
}

void RefreshRemapNodelist(void)
{
   struct TempNode *tempnode;
   struct RemapNode *remapnode;

   LT_SetAttributes(parthandle,14711,GTLV_Labels,~0,TAG_END);
   FreeTempNodes(&remapnodelist);

   remapnodenum=0;

   for(remapnode=RemapNodeList.First;remapnode;remapnode=remapnode->Next)
   {
      if(!(tempnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(&remapnodelist);
         return;
      }

      Print4DPat(&remapnode->NodePat,tempnode->buf);

      tempnode->Node.ln_Name=tempnode->buf;
      AddTail(&remapnodelist,(struct Node *)tempnode);
      remapnodenum++;
   }

   LT_SetAttributes(parthandle,14711,GTLV_Labels,&remapnodelist,TAG_END);
}

BOOL CheckDupeRemap(UBYTE *buf,ULONG num)
{
   struct Remap *remap;
   ULONG c;
   UBYTE parsedbuf[402];

   for(remap=RemapList.First,c=0;remap;remap=remap->Next,c++)
      if(stricmp(remap->Pattern,buf)==0 && c!=num) break;

   if(remap)
   {
      rtEZRequestTags("You already have a remap with that pattern","_Ok",NULL,NULL,RT_Underscore,'_',
                                                                                   RT_Window,partwin,
                                                                                   RT_WaitPointer,TRUE,
                                                                                   RT_LockWindow, TRUE,
                                                                                   TAG_END);

      return(FALSE);
   }

   if(ParsePatternNoCase(buf,parsedbuf,402)==-1)
   {
      UBYTE reqbuf[100];

      sprintf(reqbuf,"\"%s\" is not a valid AmigaDOS pattern",buf);

      rtEZRequestTags(reqbuf,"_Ok",NULL,NULL,RT_Underscore,'_',
                                             RT_Window,partwin,
                                             RT_WaitPointer,TRUE,
                                             RT_LockWindow, TRUE,
                                             TAG_END);

      return(FALSE);
   }

   return(TRUE);
}

BOOL CheckDupeRemapNode(UBYTE *buf,ULONG num)
{
   struct RemapNode *remapnode;
   ULONG c;
   struct Node4DPat n4dpat;

   if(!Parse4DPat(buf,&n4dpat))
   {
      UBYTE reqbuf[100];

      sprintf(reqbuf,"\"%s\" is not a valid node pattern",buf);

      rtEZRequestTags(reqbuf,"_Ok",NULL,NULL,RT_Underscore,'_',
                                             RT_Window,partwin,
                                             RT_WaitPointer,TRUE,
                                             RT_LockWindow, TRUE,
                                             TAG_END);

      return(FALSE);
   }

   for(remapnode=RemapNodeList.First,c=0;remapnode;remapnode=remapnode->Next,c++)
   {
      BOOL same;

      same=TRUE;

      if(strcmp(remapnode->NodePat.Zone, n4dpat.Zone)!=0)  same=FALSE;
      if(strcmp(remapnode->NodePat.Net,  n4dpat.Net)!=0)   same=FALSE;
      if(strcmp(remapnode->NodePat.Node, n4dpat.Node)!=0)  same=FALSE;
      if(strcmp(remapnode->NodePat.Point,n4dpat.Point)!=0) same=FALSE;

      if(same && c!=num) break;
  }

   if(remapnode)
   {
      rtEZRequestTags("You already have a remap with that pattern","_Ok",NULL,NULL,RT_Underscore,'_',
                                                                                   RT_Window,partwin,
                                                                                   RT_WaitPointer,TRUE,
                                                                                   RT_LockWindow, TRUE,
                                                                                   TAG_END);

      return(FALSE);
   }

   return(TRUE);
}

void AddRemap(void)
{
   struct Remap *remap;
   UBYTE buf[200];
   BOOL res;

   buf[0]=0;

   res=rtGetString(buf,199,"Add remap pattern",NULL,RT_Window,partwin,
                                                    RT_WaitPointer, TRUE,
                                                    RT_LockWindow, TRUE,
                                                    TAG_END);

   if(!res)
      return;

   if(!CheckDupeRemap(buf,-1))
      return;

   if(remap=(struct Remap *)AllocMem(sizeof(struct Remap),MEMF_CLEAR))
   {
      jbAddNode(&RemapList,(struct jbNode *)remap);
      strcpy(remap->Pattern,buf);
      remap->DestPat.Type=PAT_PATTERN;
      strcpy(remap->DestPat.Zone,"0");
      strcpy(remap->DestPat.Net,"0");
      strcpy(remap->DestPat.Node,"0");
      strcpy(remap->DestPat.Point,"0");
      RefreshRemaplist();
      SetRemap(remapnum-1);
      LT_SetAttributes(parthandle,4711,GTLV_Selected,currentremap,GTLV_MakeVisible,currentremap,TAG_END);
      saveconfig=TRUE;
   }
   else
   {
      DisplayBeep(NULL);
   }
}

void AddRemapNode(void)
{
   struct RemapNode *remapnode;
   UBYTE buf[200];
   BOOL res;

   buf[0]=0;

   res=rtGetString(buf,199,"Add remap pattern",NULL,RT_Window,partwin,
                                                    RT_WaitPointer, TRUE,
                                                    RT_LockWindow, TRUE,
                                                    TAG_END);

   if(!res)
      return;

   if(!CheckDupeRemapNode(buf,-1))
      return;

   if(remapnode=(struct RemapNode *)AllocMem(sizeof(struct RemapNode),MEMF_CLEAR))
   {
      remapnode->DestPat.Type=PAT_PATTERN;
      strcpy(remapnode->DestPat.Zone,"0");
      strcpy(remapnode->DestPat.Net,"0");
      strcpy(remapnode->DestPat.Node,"0");
      strcpy(remapnode->DestPat.Point,"0");

      jbAddNode(&RemapNodeList,(struct jbNode *)remapnode);
      Parse4DPat(buf,&remapnode->NodePat);
      RefreshRemapNodelist();
      SetRemapNode(remapnodenum-1);
      LT_SetAttributes(parthandle,14711,GTLV_Selected,currentremapnode,GTLV_MakeVisible,currentremapnode,TAG_END);
      saveconfig=TRUE;
   }
   else
   {
      DisplayBeep(NULL);
   }
}

void RemRemap(void)
{
   ULONG c,tmp;
   struct Remap *remap;
   UBYTE buf[100];
   BOOL res;

   remap=RemapList.First;
   for(c=0;c<currentremap;c++) remap=remap->Next;

   sprintf(buf,"Do you really want to remove the remap \"%s\"?",remap->Pattern);

   res=rtEZRequestTags(buf,"_Remove|_Cancel",NULL,NULL,RT_Underscore,'_',
                                                       RT_Window,partwin,
                                                       RT_WaitPointer,TRUE,
                                                       RT_LockWindow, TRUE,
                                                       RTEZ_Flags, EZREQF_CENTERTEXT | EZREQF_NORETURNKEY,
                                                       TAG_END);

   if(!res)
      return;

   tmp=currentremap;
   SetRemap(-1);
   jbFreeNode(&RemapList,(struct jbNode *)remap,sizeof(struct Remap));
   RefreshRemaplist();
   if(tmp == remapnum) tmp--;
   SetRemap(tmp);
   saveconfig=TRUE;
}

void RemRemapNode(void)
{
   ULONG c,tmp;
   struct RemapNode *remapnode;
   UBYTE buf[100],buf2[100];
   BOOL res;

   remapnode=RemapNodeList.First;
   for(c=0;c<currentremapnode;c++) remapnode=remapnode->Next;

   Print4DPat(&remapnode->NodePat,buf2);
   sprintf(buf,"Do you really want to remove the remap \"%s\"?",
      buf2);

   res=rtEZRequestTags(buf,"_Remove|_Cancel",NULL,NULL,RT_Underscore,'_',
                                                       RT_Window,partwin,
                                                       RT_WaitPointer,TRUE,
                                                       RT_LockWindow, TRUE,
                                                       RTEZ_Flags, EZREQF_CENTERTEXT | EZREQF_NORETURNKEY,
                                                       TAG_END);

   if(!res)
      return;

   tmp=currentremapnode;
   SetRemapNode(-1);
   jbFreeNode(&RemapNodeList,(struct jbNode *)remapnode,sizeof(struct RemapNode));
   RefreshRemapNodelist();
   if(tmp == remapnodenum) tmp--;
   SetRemapNode(tmp);
   saveconfig=TRUE;
}

void PartRemap(void)
{
   ULONG class,code,tmp;
   struct IntuiMessage *IntuiMessage;
   struct TempNode *tempnode;
   APTR iaddress;
   ULONG c;
   struct Node4DPat n4dpat;
   struct Remap *remap;
   struct RemapNode *remapnode;

   NewList(&remaplist);
   currentremap=-1;

   NewList(&remapnodelist);
   currentremapnode=-1;

   parthandle = LT_CreateHandleTags(scr,LH_EditHook,&StrEditHook,
                                        LH_RawKeyFilter,FALSE,TAG_DONE);

   if(!parthandle)
   {
      FreeTempNodes(&remaplist);
      FreeTempNodes(&remapnodelist);
      return;
   }

   LT_New(parthandle,
      LA_Type,      VERTICAL_KIND,
      TAG_DONE);

      LT_New(parthandle,
         LA_Type,      VERTICAL_KIND,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      CYCLE_KIND,
            LA_LabelText, "Page",
            LA_Type,          CYCLE_KIND,
            LA_ID,            98,
            LA_Chars,         30,
            GTCY_Labels,      remappages,
            TAG_DONE);

      LT_EndGroup(parthandle);

      LT_New(parthandle,
         LA_Type,          VERTICAL_KIND,
         LA_ID,            99,
         LAGR_ActivePage,  0,
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,      VERTICAL_KIND,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      HORIZONTAL_KIND,
            LA_LabelText, "Remap names",
            TAG_DONE);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      STRING_KIND,
                  GTST_MaxChars,199,
                  LA_ID,        4712,
                  LAST_Link,    4711,
                  GA_Disabled,TRUE,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,       LISTVIEW_KIND,
                  LA_Chars,      20,
                  LALV_LockSize, TRUE,
                  LALV_Lines,    10,
                  LALV_Link,     4712,
                  GTLV_Labels,   &remaplist,
                  LA_ID,4711,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      HORIZONTAL_KIND,
                  LAGR_SameSize,TRUE,
                  TAG_DONE);

                  LT_New(parthandle,
                     LA_Type,      VERTICAL_KIND,
                     LAGR_SameSize,TRUE,
                     TAG_DONE);

                     LT_New(parthandle,
                        LA_Type,      BUTTON_KIND,
                        LA_LabelText, "Add...",
                        LA_Chars,10,
                        LA_ID,10,
                        TAG_DONE);

                     LT_New(parthandle,
                        LA_Type,      BUTTON_KIND,
                        LA_LabelText, "Up",
                        GA_Disabled,TRUE,
                        LA_Chars,10,
                        LA_ID,12,
                        TAG_DONE);

                  LT_EndGroup(parthandle);

                  LT_New(parthandle,
                     LA_Type,      VERTICAL_KIND,
                     LAGR_SameSize,TRUE,
                     TAG_DONE);

                     LT_New(parthandle,
                        LA_Type,      BUTTON_KIND,
                        LA_LabelText, "Remove...",
                        LA_Chars,10,
                        GA_Disabled,TRUE,
                        LA_ID,11,
                        TAG_DONE);

                     LT_New(parthandle,
                        LA_Type,      BUTTON_KIND,
                        LA_LabelText, "Down",
                        LA_Chars,10,
                        GA_Disabled,TRUE,
                        LA_ID,13,
                        TAG_DONE);

                  LT_EndGroup(parthandle);

               LT_EndGroup(parthandle);

            LT_EndGroup(parthandle);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               LA_ExtraSpace,TRUE,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      STRING_KIND,
                  LA_LabelText, "New name",
                  GA_Disabled,TRUE,
                  LA_Chars,30,
                  GTST_MaxChars,35,
                  LA_ID,50,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      STRING_KIND,
                  LA_LabelText, "New node",
                  GA_Disabled,TRUE,
                  LA_Chars,30,
                  GTST_MaxChars,40,
                  LA_ID,51,
                  TAG_DONE);

            LT_EndGroup(parthandle);

         LT_EndGroup(parthandle);

         LT_EndGroup(parthandle);

         LT_New(parthandle,
            LA_Type,      VERTICAL_KIND,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      HORIZONTAL_KIND,
            LA_LabelText, "Remap nodes",
            TAG_DONE);

           LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      STRING_KIND,
                  GTST_MaxChars,199,
                  LA_ID,        14712,
                  LAST_Link,    14711,
                  GA_Disabled,TRUE,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,       LISTVIEW_KIND,
                  LA_Chars,      20,
                  LALV_LockSize, TRUE,
                  LALV_Lines,    10,
                  LALV_Link,     14712,
                  GTLV_Labels,   &remaplist,
                  LA_ID,14711,
                  TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      HORIZONTAL_KIND,
                  LAGR_SameSize,TRUE,
                  TAG_DONE);

                  LT_New(parthandle,
                     LA_Type,      VERTICAL_KIND,
                     LAGR_SameSize,TRUE,
                     TAG_DONE);

                     LT_New(parthandle,
                        LA_Type,      BUTTON_KIND,
                        LA_LabelText, "Add...",
                        LA_Chars,10,
                        LA_ID,110,
                        TAG_DONE);

                     LT_New(parthandle,
                        LA_Type,      BUTTON_KIND,
                        LA_LabelText, "Up",
                        GA_Disabled,TRUE,
                        LA_Chars,10,
                        LA_ID,112,
                        TAG_DONE);

                  LT_EndGroup(parthandle);

                  LT_New(parthandle,
                     LA_Type,      VERTICAL_KIND,
                     LAGR_SameSize,TRUE,
                     TAG_DONE);

                     LT_New(parthandle,
                        LA_Type,      BUTTON_KIND,
                        LA_LabelText, "Remove...",
                        LA_Chars,10,
                        GA_Disabled,TRUE,
                        LA_ID,111,
                        TAG_DONE);

                     LT_New(parthandle,
                        LA_Type,      BUTTON_KIND,
                        LA_LabelText, "Down",
                        LA_Chars,10,
                        GA_Disabled,TRUE,
                        LA_ID,113,
                        TAG_DONE);

                  LT_EndGroup(parthandle);

               LT_EndGroup(parthandle);

            LT_EndGroup(parthandle);

            LT_New(parthandle,
               LA_Type,      VERTICAL_KIND,
               LA_ExtraSpace,TRUE,
               TAG_DONE);

               LT_New(parthandle,
                  LA_Type,      STRING_KIND,
                  LA_LabelText, "New node",
                  GA_Disabled,TRUE,
                  LA_Chars,30,
                  GTST_MaxChars,40,
                  LA_ID,151,
                  TAG_DONE);

            LT_EndGroup(parthandle);

         LT_EndGroup(parthandle);

         LT_EndGroup(parthandle);

      LT_EndGroup(parthandle);

   LT_EndGroup(parthandle);

   partwin=LT_Build(parthandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW|IDCMP_RAWKEY,
                               LAWN_Title,    "Remap",
                               WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                               LAWN_HelpHook, &RemapHelpHook,
                               TAG_END);

   if(!partwin)
   {
      LT_DeleteHandle(parthandle);
      DisplayBeep(NULL);
      FreeTempNodes(&remaplist);
      FreeTempNodes(&remapnodelist);
      return;
   }

   RefreshRemaplist();
   RefreshRemapNodelist();
   remapactivepage=0;

   if(remapnum)
   {
      LT_SetAttributes(parthandle,4711,GTLV_Selected,0,TAG_END);
      SetRemap(0);
   }

   tmpcurrentremapnode=-1;

   if(remapnodenum)
   {
      LT_SetAttributes(parthandle,14711,GTLV_Selected,0,TAG_END);
      tmpcurrentremapnode=0;
   }

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(parthandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;

         LT_ReplyIMsg(IntuiMessage);
         
         switch(class)
         {
            case IDCMP_RAWKEY:         if(code == TAB)
                                       {
                                          if(remapactivepage == 0) LT_Activate(parthandle,4712);
                                          if(remapactivepage == 1) LT_Activate(parthandle,14712);
                                       }
                                       break;
            case IDCMP_GADGETUP:       switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 98:
                                             if(remapactivepage==0)
                                             {
                                                tmpcurrentremap=currentremap;
                                                SetRemap(-1);
                                                LT_SetAttributes(parthandle,99,LAGR_ActivePage,code,TAG_END);
                                                SetRemapNode(tmpcurrentremapnode);
                                             }
                                             else
                                             {
                                                tmpcurrentremapnode=currentremapnode;
                                                SetRemapNode(-1);
                                                LT_SetAttributes(parthandle,99,LAGR_ActivePage,code,TAG_END);
                                                SetRemap(tmpcurrentremap);
                                             }
                                             remapactivepage=code;
                                             break;

                                          case 10:
                                             AddRemap();
                                             break;
                                          case 11:
                                             RemRemap();
                                             break;
                                          case 12:
                                             if(currentremap != 0)
                                             {
                                                tmp=currentremap;
                                                SetRemap(-1);
                                                MoveUp(&RemapList,tmp);
                                                RefreshRemaplist();
                                                SetRemap(tmp-1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentremap,
                                                   GTLV_MakeVisible,currentremap,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 13:
                                             if(currentremap != remapnum-1)
                                             {
                                                tmp=currentremap;
                                                SetRemap(-1);
                                                MoveDown(&RemapList,tmp);
                                                RefreshRemaplist();
                                                SetRemap(tmp+1);
                                                LT_SetAttributes(parthandle,4711,
                                                   GTLV_Selected,currentremap,
                                                   GTLV_MakeVisible,currentremap,
                                                   TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 51:
                                             if(!Parse4DDestPat(getgadgetstring((struct Gadget *)iaddress),&n4dpat))
                                             {
                                                DisplayBeep(NULL);
                                                LT_Activate(parthandle,51);
                                             }
                                             break;
                                          case 4711:
                                             SetRemap(code);
                                             break;
                                          case 4712:
                                             if(CheckDupeRemap((UBYTE *)LT_GetAttributes(parthandle,4712,TAG_END),currentremap))
                                             {
                                                remap=RemapList.First;
                                                for(c=0;c<currentremap;c++) remap=remap->Next;
                                                strcpy(remap->Pattern,(UBYTE *)LT_GetAttributes(parthandle,4712,TAG_END));

                                                tempnode=remaplist.lh_Head;
                                                for(c=0;c<currentremap;c++) tempnode=tempnode->Node.ln_Succ;

                                                LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
                                                strcpy(tempnode->buf,(UBYTE *)LT_GetAttributes(parthandle,4712,TAG_END));
                                                LT_SetAttributes(parthandle,4711,GTLV_Labels,&remaplist,TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             else
                                             {
                                                LT_Activate(parthandle,4712);
                                             }
                                             break;
                                          case 110:
                                             AddRemapNode();
                                             break;
                                          case 111:
                                             RemRemapNode();
                                             break;
                                          case 112:
                                             if(currentremapnode != 0)
                                             {
                                                tmp=currentremapnode;
                                                SetRemapNode(-1);
                                                MoveUp(&RemapNodeList,tmp);
                                                RefreshRemapNodelist();
                                                SetRemapNode(tmp-1);
                                                LT_SetAttributes(parthandle,14711,GTLV_Selected,currentremapnode,TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 113:
                                             if(currentremapnode != remapnodenum-1)
                                             {
                                                tmp=currentremapnode;
                                                SetRemapNode(-1);
                                                MoveDown(&RemapNodeList,tmp);
                                                RefreshRemapNodelist();
                                                SetRemapNode(tmp+1);
                                                LT_SetAttributes(parthandle,14711,GTLV_Selected,currentremapnode,TAG_END);
                                                saveconfig=TRUE;
                                             }
                                             break;
                                          case 151:
                                             if(!Parse4DDestPat(getgadgetstring((struct Gadget *)iaddress),&n4dpat))
                                             {
                                                DisplayBeep(NULL);
                                                LT_Activate(parthandle,151);
                                             }
                                             break;
                                          case 14711:
                                             SetRemapNode(code);
                                             break;
                                          case 14712:
                                             if(CheckDupeRemapNode((UBYTE *)LT_GetAttributes(parthandle,14712,TAG_END),currentremapnode))
                                             {
                                                remapnode=RemapNodeList.First;
                                                for(c=0;c<currentremapnode;c++) remapnode=remapnode->Next;
                                                Parse4DPat((UBYTE *)LT_GetAttributes(parthandle,14712,TAG_END),&remapnode->NodePat);

                                                tempnode=remapnodelist.lh_Head;
                                                for(c=0;c<currentremapnode;c++) tempnode=tempnode->Node.ln_Succ;

                                                LT_SetAttributes(parthandle,14711,GTLV_Labels,~0,TAG_END);
                                                Print4DPat(&remapnode->NodePat,tempnode->buf);
                                                LT_SetAttributes(parthandle,14711,GTLV_Labels,&remapnodelist,TAG_END);
                                             }
                                             else
                                             {
                                                LT_Activate(parthandle,14712);
                                             }
                                             break;
                                       }
                                       break;

            case IDCMP_CLOSEWINDOW:    if(remapactivepage == 0) SetRemap(-1);
                                       else                     SetRemapNode(-1);
                                       LT_DeleteHandle(parthandle);
                                       parthandle=NULL;
                                       FreeTempNodes(&remaplist);
                                       FreeTempNodes(&remapnodelist);
                                       return;
         }
      }
      WaitPort(partwin->UserPort);
   }
}
