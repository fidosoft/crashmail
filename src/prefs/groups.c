#include <CrashPrefs.h>

struct List grouplist;
ULONG currentgroup;

extern STRPTR *nodegroupflags[];

struct List groupnodelist;

struct Hook GroupsHelpHook = { {NULL}, HelpHookEntry,NULL,HELP_GROUPS};

void RefreshGrouplist(void)
{
   struct TempNode *tempnode;
   ULONG c;

   LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
   FreeTempNodes(&grouplist);

   for(c=0;c<26;c++)
   {
      if(!(tempnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(&grouplist);
         return;
      }

      sprintf(tempnode->buf,"%lc: %s",'A'+c,cfg_GroupNames[c]);
      tempnode->Node.ln_Name=tempnode->buf;
      AddTail(&grouplist,(struct Node *)tempnode);
   }

   LT_SetAttributes(parthandle,4711,GTLV_Labels,&grouplist,TAG_END);
}

RefreshGroupNodelist(void)
{
   struct ConfigNode *cnode;
   struct TempNode *tempnode;
   UBYTE *add;
   
   LT_SetAttributes(parthandle,5711,GTLV_Labels,~0,TAG_END);
   FreeTempNodes(&groupnodelist);

   for(cnode=CNodeList.First;cnode;cnode=cnode->Next)
   {
      if(!(tempnode=(struct TempNode *)AllocMem(sizeof(struct TempNode),MEMF_CLEAR)))
      {
         DisplayBeep(NULL);
         FreeTempNodes(&groupnodelist);
         return;
      }

      add="";
      
      if(CheckFlags('A'+currentgroup,cnode->AddGroups))
         add="@";
         
      if(CheckFlags('A'+currentgroup,cnode->Groups))
      {
         if(CheckFlags('A'+currentgroup,cnode->ReadOnlyGroups))
            sprintf(tempnode->buf,"%s!%lu:%lu/%lu.%lu",add,cnode->Node.Zone,cnode->Node.Net,cnode->Node.Node,cnode->Node.Point);

         else
            sprintf(tempnode->buf,"%s*%lu:%lu/%lu.%lu",add,cnode->Node.Zone,cnode->Node.Net,cnode->Node.Node,cnode->Node.Point);
      }
      else
      {
         sprintf(tempnode->buf,"%s%lu:%lu/%lu.%lu",add,cnode->Node.Zone,cnode->Node.Net,cnode->Node.Node,cnode->Node.Point);
      }

      tempnode->Node.ln_Name=tempnode->buf;
      tempnode->UserData=cnode;
      AddTail(&groupnodelist,(struct Node *)tempnode);
   }

   LT_SetAttributes(parthandle,5711,GTLV_Labels,&groupnodelist,TAG_END);
}

void SetGroup(ULONG num)
{
   struct ConfigNode *cnode;

   LT_SetAttributes(parthandle,4712,GTST_String,cfg_GroupNames[num],TAG_END);
   currentgroup=num;
   RefreshGroupNodelist();

   LT_SetAttributes(parthandle,5711,GTLV_Selected,0,GTLV_MakeVisible,0,TAG_DONE);

   cnode=CNodeList.First;

   if(cnode)
   {
      if(CheckFlags('A'+currentgroup,cnode->Groups))
      {
         if(CheckFlags('A'+currentgroup,cnode->ReadOnlyGroups))
         {
            LT_SetAttributes(parthandle,5713,GTCY_Active,2,TAG_DONE);
         }
         else
         {
            LT_SetAttributes(parthandle,5713,GTCY_Active,1,TAG_DONE);
         }
      }
      else
      {
         LT_SetAttributes(parthandle,5713,GTCY_Active,0,TAG_DONE);
      }

      if(CheckFlags('A'+currentgroup,cnode->AddGroups))
      {
         LT_SetAttributes(parthandle,5714,GTCB_Checked,TRUE,TAG_DONE);
      }
      else
      {
         LT_SetAttributes(parthandle,5714,GTCB_Checked,FALSE,TAG_DONE);
      }
   }
}

void PartGroups(void)
{
   ULONG class,code,num;
   struct IntuiMessage *IntuiMessage;
   APTR iaddress;
   UBYTE buf[100];
   ULONG d,e;
   struct ConfigNode *groupnode;

   NewList(&grouplist);
   NewList(&groupnodelist);

   currentgroup=-1;

   parthandle = LT_CreateHandleTags(scr,LH_EditHook,&StrEditHook,LH_RawKeyFilter,FALSE,TAG_DONE);

   if(!parthandle)
   {
      FreeTempNodes(&grouplist);
      FreeTempNodes(&groupnodelist);
      return;
   }

   LT_New(parthandle,
      LA_Type,      HORIZONTAL_KIND,
      TAG_DONE);

      LT_New(parthandle,
         LA_Type,      VERTICAL_KIND,
         LA_LabelText, "Groupnames",
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,       LISTVIEW_KIND,
            LA_Chars,      20,
            LALV_LockSize, TRUE,
            LALV_Lines,    10,
            LALV_CursorKey,TRUE,
            GTLV_Labels,   &grouplist,
            LALV_Link,     NIL_LINK,
            LA_ID,4711,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,      STRING_KIND,
            LA_LabelText, "Groupname",
            LA_LabelPlace,PLACE_ABOVE,
            LA_ExtraSpace,TRUE,
            GTST_MaxChars,79,
            LA_ID,        4712,
            TAG_DONE);

      LT_EndGroup(parthandle);

      LT_New(parthandle,
         LA_Type,      VERTICAL_KIND,
         LA_LabelText, "Nodes",
         TAG_DONE);

         LT_New(parthandle,
            LA_Type,       LISTVIEW_KIND,
            LA_Lines,      12,
            LA_Chars,      20,
            LALV_LockSize, TRUE,
            LA_ID,5711,
            LALV_Link,     NIL_LINK,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,          CYCLE_KIND,
            LA_ID,            5713,
            GTCY_Labels,      nodegroupflags,
            TAG_DONE);

         LT_New(parthandle,
            LA_Type,          CHECKBOX_KIND,
            LA_ID,            5714,
            LA_LabelText,     "Get new areas",
            LA_LabelPlace,    PLACE_RIGHT,
            TAG_DONE);

      LT_EndGroup(parthandle);

   LT_EndGroup(parthandle);

   partwin=LT_Build(parthandle,LAWN_IDCMP,    IDCMP_CLOSEWINDOW|IDCMP_RAWKEY,
                               LAWN_Title,    "Groups",
                               WA_Flags,      WFLG_ACTIVATE|WFLG_CLOSEGADGET|WFLG_DEPTHGADGET|WFLG_DRAGBAR|WFLG_NEWLOOKMENUS,
                               LAWN_HelpHook, &GroupsHelpHook,
                               TAG_END);

   if(!partwin)
   {
      LT_DeleteHandle(parthandle);
      DisplayBeep(NULL);
      FreeTempNodes(&grouplist);
      FreeTempNodes(&groupnodelist);
      return;
   }

   if(CNodeList.First == NULL)
   {
      LT_SetAttributes(parthandle,5711,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,5713,GA_Disabled,TRUE,TAG_END);
      LT_SetAttributes(parthandle,5714,GA_Disabled,TRUE,TAG_END);
   }

   RefreshGrouplist();
   LT_SetAttributes(parthandle,4711,GTLV_Selected,0,TAG_END);
   SetGroup(0);

   for(;;)
   {
      while(IntuiMessage=(struct IntuiMessage *)LT_GetIMsg(parthandle))
      {
         class=IntuiMessage->Class;
         code=IntuiMessage->Code;
         iaddress=IntuiMessage->IAddress;

         LT_ReplyIMsg(IntuiMessage);

         switch(class)
         {
            case IDCMP_RAWKEY:         if(code == TAB) LT_Activate(parthandle,4712);
                                       break;
            case IDCMP_IDCMPUPDATE:    switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 5711:
                                            groupnode=CNodeList.First;

                                            for(d=0;d<code;d++)
                                               groupnode=groupnode->Next;

                                            if(CheckFlags('A'+currentgroup,groupnode->Groups))
                                            {
                                               if(CheckFlags('A'+currentgroup,groupnode->ReadOnlyGroups))
                                               {
                                                  for(d=0,e=0;d<strlen(groupnode->Groups);d++)
                                                     if(groupnode->Groups[d]!='A'+currentgroup) buf[e++]=groupnode->Groups[d];

                                                  buf[e]=0;
                                                  strcpy(groupnode->Groups,buf);

                                                  for(d=0,e=0;d<strlen(groupnode->ReadOnlyGroups);d++)
                                                     if(groupnode->ReadOnlyGroups[d]!='A'+currentgroup) buf[e++]=groupnode->ReadOnlyGroups[d];

                                                  buf[e]=0;
                                                  strcpy(groupnode->ReadOnlyGroups,buf);
                                                  LT_SetAttributes(parthandle,5713,GTCY_Active,0,TAG_DONE);
                                               }
                                               else
                                               {
                                                  buf[0]='A'+currentgroup;
                                                  buf[1]=0;
                                                  strcat(groupnode->ReadOnlyGroups,buf);
                                                  LT_SetAttributes(parthandle,5713,GTCY_Active,2,TAG_DONE);
                                               }
                                            }
                                            else
                                            {
                                               buf[0]='A'+currentgroup;
                                               buf[1]=0;
                                               strcat(groupnode->Groups,buf);
                                               LT_SetAttributes(parthandle,5713,GTCY_Active,1,TAG_DONE);
                                            }

                                            saveconfig=TRUE;
                                            RefreshGroupNodelist();
                                            break;
                                       }
                                       break;
            case IDCMP_GADGETUP:       switch(((struct Gadget *)iaddress)->GadgetID)
                                       {
                                          case 4711:
                                             SetGroup(code);
                                             break;
                                          case 4712:
                                             strcmpcpy(cfg_GroupNames[currentgroup ],getgadgetstring((struct Gadget *)iaddress));
                                             LT_SetAttributes(parthandle,4711,GTLV_Labels,~0,TAG_END);
                                             RefreshGrouplist();
                                             LT_SetAttributes(parthandle,4711,GTLV_Labels,&grouplist,TAG_END);
                                             break;
                                          case 5711:
                                            groupnode=CNodeList.First;

                                            for(d=0;d<code;d++)
                                               groupnode=groupnode->Next;

                                            if(CheckFlags('A'+currentgroup,groupnode->Groups))
                                            {
                                               if(CheckFlags('A'+currentgroup,groupnode->ReadOnlyGroups))
                                               {
                                                  LT_SetAttributes(parthandle,5713,GTCY_Active,2,TAG_DONE);
                                               }
                                               else
                                               {
                                                  LT_SetAttributes(parthandle,5713,GTCY_Active,1,TAG_DONE);
                                               }
                                            }
                                            else
                                            {
                                               LT_SetAttributes(parthandle,5713,GTCY_Active,0,TAG_DONE);
                                            }

                                            if(CheckFlags('A'+currentgroup,groupnode->AddGroups))
                                            {
                                               LT_SetAttributes(parthandle,5714,GTCB_Checked,TRUE,TAG_DONE);
                                            }
                                            else
                                            {
                                               LT_SetAttributes(parthandle,5714,GTCB_Checked,FALSE,TAG_DONE);
                                            }

                                            break;
                                          case 5713:
                                            num=LT_GetAttributes(parthandle,5711,TAG_END);

                                            groupnode=CNodeList.First;

                                            for(d=0;d<num;d++)
                                               groupnode=groupnode->Next;

                                            for(d=0,e=0;d<strlen(groupnode->Groups);d++)
                                               if(groupnode->Groups[d]!='A'+currentgroup) buf[e++]=groupnode->Groups[d];

                                            buf[e]=0;
                                            strcpy(groupnode->Groups,buf);

                                            for(d=0,e=0;d<strlen(groupnode->ReadOnlyGroups);d++)
                                               if(groupnode->ReadOnlyGroups[d]!='A'+currentgroup) buf[e++]=groupnode->ReadOnlyGroups[d];

                                            buf[e]=0;
                                            strcpy(groupnode->ReadOnlyGroups,buf);

                                            if(code == 1)
                                            {
                                               buf[0]='A'+currentgroup;
                                               buf[1]=0;
                                               strcat(groupnode->Groups,buf);
                                            }
                                            if(code == 2)
                                            {
                                               buf[0]='A'+currentgroup;
                                               buf[1]=0;
                                               strcat(groupnode->Groups,buf);
                                               strcat(groupnode->ReadOnlyGroups,buf);
                                            }
                                            saveconfig=TRUE;
                                            RefreshGroupNodelist();
                                            break;
                                          case 5714:
                                            num=LT_GetAttributes(parthandle,5711,TAG_END);

                                            groupnode=CNodeList.First;

                                            for(d=0;d<num;d++)
                                               groupnode=groupnode->Next;

                                            if(CheckFlags('A'+currentgroup,groupnode->AddGroups))
                                            {
                                               for(d=0,e=0;d<strlen(groupnode->AddGroups);d++)
                                                  if(groupnode->AddGroups[d]!='A'+currentgroup) buf[e++]=groupnode->AddGroups[d];

                                               buf[e]=0;
                                               strcpy(groupnode->AddGroups,buf);
                                            }
                                            else
                                            {
                                               buf[0]='A'+currentgroup;
                                               buf[1]=0;
                                               strcat(groupnode->AddGroups,buf);
                                            }
                                            saveconfig=TRUE;
                                            RefreshGroupNodelist();
                                       }
                                       break;
            case IDCMP_CLOSEWINDOW:    LT_DeleteHandle(parthandle);
                                       parthandle=NULL;
                                       FreeTempNodes(&grouplist);
                                       FreeTempNodes(&groupnodelist);
                                       return;
         }
      }
      WaitPort(partwin->UserPort);
   }
}
