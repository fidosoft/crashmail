
struct jbList
{
   struct jbNode *First;
   struct jbNode *Last;
};

struct jbNode
{
   struct jbNode *Next;
};

jbFreeNum(struct jbList *list,ULONG num,ULONG size);
jbAddNode(struct jbList *list, struct jbNode *node);
jbNewList(struct jbList *list);
jbFreeNode(struct jbList *list,struct jbNode *node,ULONG size);
jbFreeList(struct jbList *list,struct jbNode *firstnode,ULONG sizenode);

