BOOL nlStart(UBYTE *errbuf);
void nlEnd(void);
BOOL nlCheckNode(struct Node4D *node);
int nlGetHub(struct Node4D *node);
int nlGetRegion(struct Node4D *node);
