
#define CFG_AUTOADD                1   /* Obsolete */
#define CFG_CHECKSEENBY            2
#define CFG_CHECKPKTDEST           4
#define CFG_UMSMAUSGATE            8
#define CFG_STRIPRE               16
#define CFG_FORCEINTL             32
#define CFG_NOROUTE               64
#define CFG_PATH3D               128
#define CFG_IMPORTSEENBY         256
#define CFG_XLATMSG              512
#define CFG_IMPORTEMPTYNETMAIL  1024
#define CFG_BOUNCEPOINTS        2048
#define CFG_UMSKEEPORIGIN       4096
#define CFG_ANSWERRECEIPT       8192
#define CFG_ANSWERAUDIT        16384
#define CFG_CHANGEUMSMSGID     32768
#define CFG_NODIRECTATTACH     65536
#define CFG_UNATTENDED        131072
#define CFG_MSGHIGHWATER      262144
#define CFG_MSGWRITEBACK      524288
#define CFG_IMPORTAREAFIX    1048576
#define CFG_AREAFIXREMOVE    2097152
#define CFG_WEEKDAYNAMING    4194304
#define CFG_ADDTID           8388608
#define CFG_CODEPAGE865     16777216
#define CFG_ALLOWRESCAN     33554432
#define CFG_FORWARDPASSTHRU 67108864
#define CFG_NOTOUCHHWTOSSING 134217728
#define CFG_UMSIGNOREORIGINDOMAIN   268435456
#define CFG_UMSEMPTYTOALL           536870912
#define CFG_BOUNCEHEADERONLY       1073741824
#define CFG_REMOVEWHENFEED       2*1073741824

#define CFG2_INCLUDEFORWARD        1

struct Node4D
{
   UWORD Zone,Net,Node,Point;
};

#define PAT_PATTERN   0
#define PAT_ZONE      1
#define PAT_REGION    2
#define PAT_NET       3
#define PAT_HUB       4
#define PAT_NODE      5

struct Node4DPat
{
   UBYTE Type;
   UBYTE Zone[10];
   UBYTE Point[10];
   UBYTE Net[10];
   UBYTE Node[10];
};

struct Node2DPat
{
   UBYTE Net[10];
   UBYTE Node[10];
};

struct Node5D
{
   UWORD Zone,Net,Node,Point;
   UBYTE Domain[20];
};

#define CHARSET_ASCII 1

extern UBYTE IbmToAmiga[256];
extern UBYTE MacToAmiga[256];
extern UBYTE SF7ToAmiga[256];
extern UBYTE DefToAmiga[256];
extern UBYTE AmigaToIbm[256];
extern UBYTE AmigaToMac[256];
extern UBYTE AmigaToSF7[256];
extern UBYTE AmigaToIbn[256];
extern UBYTE IbnToAmiga[256];
extern UBYTE DfnToAmiga[256];

#define AREA_NETMAIL        1
#define AREA_UNCONFIRMED    2
#define AREA_BAD            4
#define AREA_MANDATORY      8
#define AREA_DEFREADONLY   16
#define AREA_IGNOREDUPES   32
#define AREA_IGNORESEENBY  64
#define AREA_DEFAULT      128

#define AREATYPE_UMS        1
#define AREATYPE_MSG        2

struct Area
{
   struct Area *Next;
   struct Aka  *Aka;
   UBYTE Flags;
   UBYTE Tagname[80];
   UBYTE Description[80];
   UBYTE AreaType;
   UBYTE Path[80];
   UBYTE Group;

   UBYTE *DefaultCHRS;
   UBYTE *ExportCHRS;

   /* Maint */

   ULONG KeepNum,KeepDays;

   /* Msg */

   ULONG HighMsg;
   ULONG LowMsg;
   ULONG HighWater;
   ULONG OldHighWater;

   /* Stats */

   ULONG Texts;
   ULONG NewTexts;

   ULONG Dupes;
   ULONG NewDupes;

   UWORD Last8Days[8];

   struct DateStamp FirstTime;
   ULONG LastDay;

   struct jbList TossNodes;
   struct jbList BannedNodes;
};

struct Aka
{
   struct Aka *Next;
   struct Node4D Node;
   UBYTE Domain[20];
   UWORD FakeNet;
   struct jbList AddList;
   struct jbList RemList;
};

#define TOSSNODE_READONLY  1
#define TOSSNODE_WRITEONLY 2
#define TOSSNODE_FEED      4

struct TossNode
{
   struct TossNode *Next;
   struct ConfigNode *ConfigNode;
   UWORD Flags;
};

struct ImportNode
{
   struct ImportNode *Next;
   struct Node4D Node;
};

struct BannedNode
{
   struct BannedNode *Next;
   struct ConfigNode *ConfigNode;
};

#define NODE_STONEAGE      1
#define NODE_PASSIVE       2
#define NODE_TINYSEENBY    4
#define NODE_NOSEENBY      8
#define NODE_FORWARDREQ   16
#define NODE_NOTIFY       32
#define NODE_PACKNETMAIL  64
#define NODE_AUTOADD     128
#define NODE_SENDAREAFIX 256
#define NODE_SENDTEXT    512

struct RemoteAFCommand
{
   struct RemoteAFNode *Next;
   UBYTE Command[80];
};

struct ConfigNode
{
   struct ConfigNode *Next;
   struct Node4D Node;
   UBYTE PacketPW[9];
   struct Packer *Packer;
   BOOL IsInSeenBy;
   UBYTE AreafixPW[40];
   UBYTE Groups[30];
   UBYTE ReadOnlyGroups[30];
   UBYTE AddGroups[30];
   UBYTE DefaultGroup;
   ULONG Flags;
   UBYTE SysopName[36];

   UBYTE RemoteAFName[36];
   UBYTE RemoteAFPw[72];

   struct jbList RemoteAFList;

   /* Stats */

   ULONG GotNetmails;
   ULONG GotNetmailBytes;
   ULONG SentNetmails;
   ULONG SentNetmailBytes;

   ULONG GotEchomails;
   ULONG GotEchomailBytes;
   ULONG SentEchomails;
   ULONG SentEchomailBytes;

   ULONG Dupes;

   ULONG FirstDay;

   struct Pkt *Pkt;
};

struct Packer
{
   struct Packer *Next;
   UBYTE Name[10];
   UBYTE Packer[80];
   UBYTE Unpacker[80];
   UBYTE Recog[80];
};

struct AddNode
{
   struct AddNode *Next;
   struct Node4D Node;
};

struct RemNode
{
   struct RemNode *Next;
   struct Node2DPat NodePat;
};

struct Route
{
   struct Route      *Next;
   struct Node4DPat Pattern;
   struct Node4DPat DestPat;
   struct Aka        *Aka;
};

struct PatternNode
{
   struct FileAttach *Next;
   struct Node4DPat Pattern;
};


#define PKTS_ECHOMAIL  0
#define PKTS_HOLD      1
#define PKTS_NORMAL    2
#define PKTS_DIRECT    3
#define PKTS_CRASH     4

struct Change
{
   struct Change *Next;
   struct Node4DPat Pattern;
   BOOL ChangeNormal;
   BOOL ChangeCrash;
   BOOL ChangeDirect;
   BOOL ChangeHold;
   UBYTE DestPri;
};

struct Remap
{
   struct Remap *Next;
   UBYTE Pattern[200];
   UBYTE ParsedPattern[402];
   UBYTE NewTo[36];
   struct Node4DPat DestPat;
};

struct RemapNode
{
   struct RemapNode *Next;
   struct Node4DPat NodePat;
   struct Node4DPat DestPat;
};

struct Robot
{
   struct Robot *Next;
   UBYTE Pattern[200];
   UBYTE ParsedPattern[402];
   UBYTE Command[200];
};

struct CharsetAlias
{
   struct CharsetAlias *Next;
   UBYTE Name[30];
   UBYTE AliasName[30];
   UBYTE *CHRS;
};

struct AreaFixName
{
   struct AreaFixName *Next;
   UBYTE Name[36];
};

#define AREALIST_DESC     1
#define AREALIST_FORWARD  2

struct Arealist
{
   struct Arealist *Next;
   struct ConfigNode *Node;
   UBYTE AreaFile[80];
   UWORD Flags;
   UBYTE Group;
};

#define NODELIST_TRAPLIST  1
#define NODELIST_GOTCHA    2

extern UBYTE cfg_Sysop[40];
extern UBYTE cfg_Inbound[40];
extern UBYTE cfg_Outbound[40];
extern UBYTE cfg_TempDir[40];
extern UBYTE cfg_PacketCreate[40];
extern UBYTE cfg_PacketDir[40];
extern UBYTE cfg_LogFile[40];
extern UBYTE cfg_StatsFile[40];
extern UBYTE cfg_DupeFile[40];
extern ULONG cfg_LogLevel;
extern ULONG cfg_DupeSize;
extern ULONG cfg_MaxPktSize;
extern ULONG cfg_MaxBundleSize;
extern UBYTE cfg_DefaultOrigin[80];
extern UBYTE cfg_NodeList[40];
extern UWORD cfg_NodeListType;
extern UBYTE cfg_AreaFixHelp[40];

extern ULONG cfg_LogBufferLines;
extern ULONG cfg_LogBufferSecs;

extern ULONG cfg_AreaFixMaxLines;

extern UBYTE cfg_GatewayName[40];
extern struct Node4D cfg_GatewayNode;

extern UBYTE *cfg_LocalCharset;

extern UBYTE cfg_UMSName[80];
extern UBYTE cfg_UMSPassword[80];
extern UBYTE cfg_UMSServer[80];
extern UBYTE cfg_UMSGatewayName[36];

extern UBYTE cfg_GroupNames[30][80];

extern ULONG cfg_Flags;
extern ULONG cfg_Flags2;

#define DUPE_IGNORE 0
#define DUPE_BAD    1
#define DUPE_KILL   2

extern UWORD cfg_DupeMode;

#define LOOP_IGNORE  0
#define LOOP_LOG     1
#define LOOP_LOGBAD  2

extern UWORD cfg_LoopMode;

extern ULONG cfg_DefaultZone;

extern struct jbList AkaList;
extern struct jbList AreaList;
extern struct jbList CNodeList;
extern struct jbList PackerList;
extern struct jbList RouteList;
extern struct jbList FileAttachList;
extern struct jbList BounceList;
extern struct jbList ChangeList;
extern struct jbList RemapList;
extern struct jbList RemapNodeList;
extern struct jbList RobotList;
extern struct jbList CharsetList;
extern struct jbList AreaFixList;
extern struct jbList ArealistList;

extern BOOL saveconfig;

