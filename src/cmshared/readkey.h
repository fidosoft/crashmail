struct Key
{
   UBYTE Product[10];
   UBYTE Junk1[10];
   UBYTE Name[50];
   UBYTE Junk[182];
   ULONG Serial;
};

extern struct Key key;
extern ULONG checksums[16];

/* sources/readkey.c */

void ReadKey(void);
ULONG Checksum0(void);
ULONG Checksum1(void);
ULONG Checksum2(void);
ULONG Checksum3(void);
ULONG Checksum4(void);
ULONG Checksum5(void);
ULONG Checksum6(void);
ULONG Checksum7(void);
ULONG Checksum8(void);
ULONG Checksum9(void);
ULONG Checksum10(void);
ULONG Checksum11(void);
ULONG Checksum12(void);
ULONG Checksum13(void);
ULONG Checksum14(void);
ULONG Checksum15(void);
