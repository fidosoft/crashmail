struct jbFile
{
   BPTR fh;
   UBYTE *buf;
   ULONG bufsize;
   ULONG len;
   ULONG pos;
   UWORD mode;
   ULONG total;
};

struct jbFile *jbOpen(UBYTE *name,UWORD mode,ULONG bufsize);
jbClose(struct jbFile *jb);
jbGetChar(struct jbFile *jb);
jbRead(struct jbFile *jb,const void *buf,ULONG bytes);
jbPutChar(struct jbFile *jb, UBYTE ch);
jbWrite(struct jbFile *jb,const void *buf, ULONG bytes);
jbPuts(struct jbFile *jb, UBYTE *str);
jbFGets(struct jbFile *jb,UBYTE *str,ULONG max);
ULONG jbFTell(struct jbFile *jb);

