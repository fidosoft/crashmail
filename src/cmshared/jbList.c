#include <exec/types.h>
#include <exec/memory.h>

#include <clib/exec_protos.h>

#include <cmshared/jblist.h>

jbNewList(struct jbList *list)
{
   list->First=NULL;
   list->Last=NULL;
}

jbAddNode(struct jbList *list, struct jbNode *node)
{
   if(!list->First)
      list->First=node;

   else
      list->Last->Next=node;

   list->Last=node;
   node->Next=NULL;
}

jbFreeList(struct jbList *list,struct jbNode *firstnode,ULONG sizenode)
{
   struct jbNode *tmp,*tmp2;

   if(list->First==firstnode)
   {
      list->First=NULL;
      list->Last=NULL;
   }
   else
   {
      for(tmp=list->First;tmp->Next!=firstnode;tmp=tmp->Next);

      tmp->Next=NULL;
      list->Last=tmp;
   }

   for(tmp=firstnode;tmp;)
   {
      tmp2=tmp->Next;
      FreeMem(tmp,sizenode);
      tmp=tmp2;
   }
};

jbFreeNum(struct jbList *list,ULONG num,ULONG size)
{
   struct jbNode *old=NULL,*tmp;
   ULONG c;

   tmp=list->First;

   for(c=0;c<num && tmp;c++)
   {
      if(c==num-1)
         old=tmp;

     tmp=tmp->Next;
   }

   if(c==num)
   {
      if(old)
         old->Next=tmp->Next;

      if(num==0)
         list->First=tmp->Next;

      if(tmp->Next == NULL)
         list->Last=old;

      FreeMem(tmp,size);
   }
}

jbFreeNode(struct jbList *list,struct jbNode *node,ULONG size)
{
   struct jbNode *old=NULL,*tmp;

   for(tmp=list->First;tmp;tmp=tmp->Next)
   {
      if(tmp->Next == node)
         old=tmp;

      if(tmp == node) break;
   }

   if(tmp == node)
   {
      if(old)
         old->Next=tmp->Next;

      if(node == list->First)
         list->First=tmp->Next;

      if(tmp->Next == NULL)
         list->Last=old;

      FreeMem(tmp,size);
   }
}