#include <strings.h>

#include <dos/dos.h>
#include <clib/dos_protos.h>

#include <cmshared/readkey.h>

struct Key key;
ULONG checksums[16];

UBYTE *keybuf;
UBYTE *longbuf;

ULONG Checksum9(void)
{
   ULONG rol,c;

   rol=0;

   for(c=0;c<sizeof(struct Key);c++)
      if(c<70 || c>=252 || c%2==0)
      {
         rol|=1;
         rol<<=keybuf[c]%4;
      }

   return(rol);
}

ULONG Checksum11(void)
{
   ULONG bit3,c;

   bit3=0;

   for(c=0;c<sizeof(struct Key);c++)
      if(c<70 || c>=252 || c%2==0)
      {
         bit3|=keybuf[c]+c & 0x0f0f0f0f;
         bit3<<=2;
      }

   return(bit3);
}

ULONG Checksum13(void)
{
   ULONG zm2,c;

   zm2=0;

   for(c=0;c<sizeof(struct Key);c++)
      if(c<70 || c>=252 || c%2==0)
      {
         zm2^=c*(keybuf[c]+c);
         zm2<<=c%3;
      }

   return(zm2);
}


void ReadKey()
{
   UBYTE path[200];
   ULONG c;
   UBYTE xor,newxor;
   BPTR fh;

   strcpy(key.Name,"");

   for(c=0;c<16;c++)
      checksums[c]=-1;

   keybuf=(UBYTE *)&key;
   longbuf=(UBYTE *)checksums;

   if(!(fh=Open("PROGDIR:CrashMail.key",MODE_OLDFILE)))
   {
      if(!(fh=Open("MAIL:CrashMail.key",MODE_OLDFILE)))
      {
         if(!(fh=Open("L:CrashMail.key",MODE_OLDFILE)))
         {
            if(GetVar("KEYPATH",path,100,0)!=-1)
            {
               AddPart(path,"CrashMail.key",190);

               if(!(fh=Open(path,MODE_OLDFILE)))
                 return;
            }
         }
      }
   }

   Read(fh,&key,sizeof(struct Key));
   Close(fh);

   xor=255;

   for(c=0;c<sizeof(struct Key);c++)
   {
      newxor=keybuf[c];
      keybuf[c]^=xor;
      xor=newxor;
   }

   for(c=0;c<4*16;c++)
      longbuf[c]=keybuf[70+c*2+1];
}